-- MySQL dump 10.16  Distrib 10.1.19-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: localhost
-- ------------------------------------------------------
-- Server version	10.1.19-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `travelpie_currency`
--

DROP TABLE IF EXISTS `travelpie_currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `travelpie_currency` (
  `currency_id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_name` varchar(100) DEFAULT NULL,
  `currency_short_code` varchar(25) DEFAULT NULL,
  `satus` int(11) DEFAULT '0',
  `exchange_rate_required` int(11) DEFAULT '0',
  PRIMARY KEY (`currency_id`)
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8 COMMENT='Table To Store Curreny Info';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `travelpie_currency`
--

LOCK TABLES `travelpie_currency` WRITE;
/*!40000 ALTER TABLE `travelpie_currency` DISABLE KEYS */;
INSERT INTO `travelpie_currency` VALUES (1,'U. Arab Emirates Dir','AED',1,1),(2,'Argentina Pesos     ','ARS',0,0),(3,'Austrian Schilling  ','ATS',0,0),(4,'Belgium Franc       ','BEF',0,0),(5,'Bahraini dinar      ','BHD',1,1),(6,'Bolivia Bolivianos  ','BOB',0,0),(7,'Brazilian Real      ','BRL',0,1),(8,'Bahamas Dollar      ','BSD',0,0),(9,'Canadian Dollar     ','CAD',0,1),(10,'Swiss Franc         ','CHF',0,1),(11,'Chilean Peso        ','CLP',0,0),(12,'Yuan                ','CNY',0,1),(13,'Colombia Pesos      ','COP',0,0),(14,'Costa Rica Colon    ','CRC',0,0),(15,'Koruna              ','CZK',0,1),(16,'Deutschemark        ','DEM',0,0),(17,'Danish Krone        ','DKK',0,1),(18,'Domonican Peso      ','DOP',0,0),(19,'Algerian Dinar      ','DZD',0,1),(20,'Ecuadorian Sucre    ','ECS',0,0),(21,'Egyptian Pound      ','EGP',1,1),(22,'Spanish Peseta      ','ESP',0,0),(23,'Ethiopian birr      ','ETB',0,1),(24,'Euro                ','EUR',1,1),(25,'French Franc        ','FRF',0,0),(26,'British Pound       ','GBP',1,1),(27,'Greek Drachma       ','GRD',0,0),(28,'Guatemala Quetzal   ','GTQ',0,0),(29,'Hong Kong Dollar    ','HKD',0,1),(30,'Honduras Lempira    ','HNL',0,0),(31,'Haitian Gourde      ','HTG',0,0),(32,'Hungarian Forint    ','HUF',0,1),(33,'Israeli Shekel      ','ILS',0,0),(34,'Indian Rupee        ','INR',0,1),(35,'Italian Lira        ','ITL',0,0),(36,'Jamaican Dollar     ','JMD',0,0),(37,'Jordan Dinar        ','JOD',0,1),(38,'Japanese Yen        ','JPY',0,1),(39,'South Korean Won    ','KRW',0,1),(40,'Libyan Dinar        ','LYD',0,1),(41,'Moroccan Dirham     ','MAD',0,1),(42,'Mauritanian Ouguiya ','MRO',0,1),(43,'Mexican Peso        ','MXN',0,0),(44,'Malaysian Ringgit   ','MYR',0,1),(45,'Nicaraguan Cordoba  ','NIO',0,0),(46,'Dutch Guilder       ','NLG',0,0),(47,'Norway Kroner       ','NOK',0,1),(48,'Pananiam Balboa     ','PAB',0,0),(49,'Peru Nuevos Soles   ','PEN',0,0),(50,'Pakistani Rupee     ','PKR',0,1),(51,'Poland Zloty        ','PLN',0,1),(52,'Portuguese Escudo   ','PTE',0,0),(53,'Paraguay Guarani    ','PYG',0,0),(54,'Qatari Rial         ','QAR',1,1),(55,'Saudi Arabia Riyal  ','SAR',1,1),(56,'SEB                 ','SEB',0,0),(57,'Swedish Krona       ','SEK',0,1),(58,'Singapore Dollar    ','SGD',0,1),(59,'El Salvador Colon   ','SVC',0,0),(60,'Thailand Baht       ','THB',0,1),(61,'Tunisian Dinar      ','TND',0,1),(62,'U.S. Dollar         ','USD',1,1),(63,'Uruguay Pesos       ','UYU',0,0),(64,'Venezuelan Bolivar  ','VEB',0,0),(65,'XAF-CFA Franc       ','XAF',0,1),(66,'XOF-CFA Franc       ','XOF',0,1),(67,'Franc (XPF)         ','XPF',0,0),(68,'South African Rand  ','ZAR',0,1),(69,'HEU                 ','HEU',0,0),(70,'Netherlands Antillea','ANG',0,0),(71,'Australian Dollar   ','AUD',0,1),(72,'BGL                 ','BGL',0,0),(73,'BGN                 ','BGN',0,0),(74,'Pula                ','BWP',0,1),(75,'Cypriot pound       ','CYP',0,0),(76,'Kroon               ','EEK',0,0),(77,'AFA                 ','AFA',0,0),(78,'AFN                 ','AFN',0,0),(79,'ALL                 ','ALL',0,0),(80,'AMD                 ','AMD',0,0),(81,'AOA                 ','AOA',0,0),(82,'Aruban Guilder      ','AWG',0,0),(83,'AZM                 ','AZM',0,0),(84,'AZN                 ','AZN',0,1),(85,'Convertible Marks   ','BAM',0,0),(86,'Barbados Dollar     ','BBD',0,0),(87,'BDT                 ','BDT',0,0),(88,'BIF                 ','BIF',0,0),(89,'Bermudian dollar    ','BMD',0,0),(90,'BND                 ','BND',0,0),(91,'BTN                 ','BTN',0,0),(92,'BYR                 ','BYR',0,0),(93,'BZD                 ','BZD',0,0),(94,'CDF                 ','CDF',0,0),(95,'CSD                 ','CSD',0,0),(96,'CUP                 ','CUP',0,0),(97,'CVE                 ','CVE',0,0),(98,'DJF                 ','DJF',0,0),(99,'ERN                 ','ERN',0,0),(100,'FIM                 ','FIM',0,0),(101,'FJD                 ','FJD',0,0),(102,'FKP                 ','FKP',0,0),(103,'GEL                 ','GEL',0,0),(104,'GHC                 ','GHC',0,0),(105,'GIP                 ','GIP',0,0),(106,'GMD                 ','GMD',0,0),(107,'GNF                 ','GNF',0,0),(108,'GYD                 ','GYD',0,0),(109,'Croatian Kuna       ','HRK',0,0),(110,'Rupiah              ','IDR',0,1),(111,'IEP                 ','IEP',0,0),(112,'IQD                 ','IQD',0,0),(113,'IRR                 ','IRR',0,0),(114,'ISK                 ','ISK',0,1),(115,'Kenyan Shilling     ','KES',0,0),(116,'KGS                 ','KGS',0,0),(117,'Riel                ','KHR',0,0),(118,'KMF                 ','KMF',0,1),(119,'KPW                 ','KPW',0,0),(120,'Kuwaiti dinar       ','KWD',0,1),(121,'Cayman Islands Dolla','KYD',0,0),(122,'Tenge               ','KZT',0,1),(123,'LAK                 ','LAK',0,0),(124,'LBP                 ','LBP',0,0),(125,'LKR                 ','LKR',0,1),(126,'LRD                 ','LRD',0,0),(127,'LSL                 ','LSL',0,0),(128,'Lithuanian Litas    ','LTL',0,0),(129,'LUF                 ','LUF',0,0),(130,'Latvian Lats        ','LVL',0,0),(131,'MDL                 ','MDL',0,0),(132,'MGA                 ','MGA',0,0),(133,'MGF                 ','MGF',0,0),(134,'MKD                 ','MKD',0,0),(135,'MMK                 ','MMK',0,0),(136,'MNT                 ','MNT',0,0),(137,'Macanese pataca     ','MOP',0,0),(138,'MTL                 ','MTL',0,0),(139,'Mauritian rupee     ','MUR',0,1),(140,'MVR                 ','MVR',0,0),(141,'Malawian kwacha     ','MWK',0,0),(142,'Mozambican metical  ','MZM',0,0),(143,'Mozambican new metic','MZN',0,1),(144,'NAD                 ','NAD',0,0),(145,'Naira               ','NGN',0,1),(146,'NPR                 ','NPR',0,1),(147,'New Zealand dollar  ','NZD',0,1),(148,'Rial                ','OMR',1,1),(149,'PGK                 ','PGK',0,0),(150,'Philippine Peso     ','PHP',0,1),(151,'ROL                 ','ROL',0,0),(152,'RON                 ','RON',0,1),(153,'Russian Rouble      ','RUB',0,1),(154,'RUR                 ','RUR',0,0),(155,'RWF                 ','RWF',0,0),(156,'SBD                 ','SBD',0,0),(157,'SCR                 ','SCR',0,1),(158,'SDD                 ','SDD',0,0),(159,'SIT                 ','SIT',0,0),(160,'SKK                 ','SKK',0,0),(161,'Leone               ','SLL',0,0),(162,'SOS                 ','SOS',0,0),(163,'SRD                 ','SRD',0,0),(164,'SRG                 ','SRG',0,0),(165,'STD                 ','STD',0,0),(166,'Syrian Pound        ','SYP',0,0),(167,'SZL                 ','SZL',0,1),(168,'TJS                 ','TJS',0,0),(169,'TMM                 ','TMM',0,0),(170,'TOP                 ','TOP',0,0),(171,'TPE                 ','TPE',0,0),(172,'TRL                 ','TRL',0,0),(173,'TRY                 ','TRY',0,1),(174,'Trinidad and Tobago ','TTD',0,0),(175,'New Taiwan Dollar   ','TWD',0,1),(176,'Tanzanian shilling  ','TZS',0,0),(177,'UAH                 ','UAH',0,0),(178,'Ugandan shilling    ','UGX',0,0),(179,'UZS                 ','UZS',0,0),(180,'VND                 ','VND',0,1),(181,'VUV                 ','VUV',0,0),(182,'WST                 ','WST',0,0),(183,'East Caribbean Dolla','XCD',0,0),(184,'XDR                 ','XDR',0,0),(185,'Yemeni rial         ','YER',0,0),(186,'YUM                 ','YUM',0,0),(187,'Zambian kwacha      ','ZMW',0,0),(188,'Zimbabwean dollar   ','ZWD',0,0),(189,'ZWN                 ','ZWN',0,0),(190,'Serbian Dinar       ','RSD',0,0),(191,'SDG                 ','SDG',0,1),(192,'GHS                 ','GHS',0,0),(193,'VEF                 ','VEF',0,0),(194,'ZWR                 ','ZWR',0,0),(195,'TMT                 ','TMT',0,0),(196,'CUC                 ','CUC',0,0),(197,'ZMK                 ','ZMK',0,0),(198,'RMB                 ','RMB',0,0);
/*!40000 ALTER TABLE `travelpie_currency` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-02 17:20:25
