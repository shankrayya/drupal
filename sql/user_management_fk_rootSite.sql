ALTER TABLE pierian_region ADD FOREIGN KEY (group_id) REFERENCES pierian_group(group_id);

ALTER TABLE pierian_travel_agent ADD FOREIGN KEY (community_id) REFERENCES pierian_community(community_id);

ALTER TABLE pierian_corporate ADD FOREIGN KEY (community_id) REFERENCES pierian_community(community_id);

ALTER TABLE pierian_user_community ADD FOREIGN KEY (community_id) REFERENCES pierian_community(community_id), ADD FOREIGN KEY (user_id) REFERENCES pierian_users(uid);

ALTER TABLE pierian_user_pcc ADD FOREIGN KEY (pcc_id) REFERENCES pierian_pcc(pcc_id), ADD FOREIGN KEY (user_id) REFERENCES pierian_users(uid);

ALTER TABLE pierian_user_group ADD FOREIGN KEY (group_id) REFERENCES pierian_group(group_id), ADD FOREIGN KEY (user_id) REFERENCES pierian_users(uid);

ALTER TABLE pierian_travel_agent_corporate ADD FOREIGN KEY (travel_agent_id) 
REFERENCES pierian_travel_agent(travel_agent_id), ADD FOREIGN KEY (corporate_id) REFERENCES pierian_corporate(corporate_id);


ALTER TABLE pierian_user_threads ADD FOREIGN KEY (user_id) REFERENCES pierian_users(uid) ON UPDATE CASCADE,
 ADD FOREIGN KEY (tid) REFERENCES pierian_taxonomy_term_data(tid) ON UPDATE CASCADE,
 ADD FOREIGN KEY (last_commented_by) REFERENCES pierian_users(uid) ON UPDATE CASCADE,
 ADD FOREIGN KEY (root_tid) REFERENCES pierian_taxonomy_term_data(tid) ON UPDATE CASCADE;

ALTER TABLE pierian_comments ADD FOREIGN KEY (user_id) REFERENCES pierian_users(uid) ON UPDATE CASCADE,
 ADD FOREIGN KEY (thread_id) REFERENCES pierian_user_threads(thread_id) ON UPDATE CASCADE;

ALTER TABLE pierian_user_tags ADD FOREIGN KEY (user_id) REFERENCES pierian_users(uid) ON UPDATE CASCADE,
 ADD FOREIGN KEY (thread_id) REFERENCES pierian_user_threads(thread_id) ON UPDATE CASCADE;

ALTER TABLE pierian_user_bookmarks ADD FOREIGN KEY (user_id) REFERENCES pierian_users(uid) ON UPDATE CASCADE,
 ADD FOREIGN KEY (thread_id) REFERENCES pierian_user_threads(thread_id) ON UPDATE CASCADE;
