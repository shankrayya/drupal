ALTER TABLE pierian_mystn_region ADD FOREIGN KEY (group_id) REFERENCES pierian_mystn_group(group_id);

ALTER TABLE pierian_mystn_travel_agent ADD FOREIGN KEY (community_id) REFERENCES pierian_mystn_community(community_id);

ALTER TABLE pierian_mystn_corporate ADD FOREIGN KEY (community_id) REFERENCES pierian_mystn_community(community_id);

ALTER TABLE pierian_mystn_user_community ADD FOREIGN KEY (community_id) REFERENCES pierian_mystn_community(community_id), ADD FOREIGN KEY (user_id) REFERENCES pierian_mystn_users(uid);

ALTER TABLE pierian_mystn_user_pcc ADD FOREIGN KEY (pcc_id) REFERENCES pierian_mystn_pcc(pcc_id), ADD FOREIGN KEY (user_id) REFERENCES pierian_mystn_users(uid);

ALTER TABLE pierian_mystn_user_group ADD FOREIGN KEY (group_id) REFERENCES pierian_mystn_group(group_id), ADD FOREIGN KEY (user_id) REFERENCES pierian_mystn_users(uid);

ALTER TABLE pierian_mystn_travel_agent_corporate ADD FOREIGN KEY (travel_agent_id) REFERENCES pierian_mystn_travel_agent(travel_agent_id), ADD FOREIGN KEY (corporate_id) REFERENCES pierian_mystn_corporate(corporate_id);
