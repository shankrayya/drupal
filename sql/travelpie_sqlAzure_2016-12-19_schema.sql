USE [master]
GO
/****** Object:  Database [travelpie_mssql_2]    Script Date: 12/19/2016 10:31:17 AM ******/
CREATE DATABASE [travelpie_mssql_2]
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [travelpie_mssql_2].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [travelpie_mssql_2] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [travelpie_mssql_2] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [travelpie_mssql_2] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [travelpie_mssql_2] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [travelpie_mssql_2] SET ARITHABORT OFF 
GO
ALTER DATABASE [travelpie_mssql_2] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [travelpie_mssql_2] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [travelpie_mssql_2] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [travelpie_mssql_2] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [travelpie_mssql_2] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [travelpie_mssql_2] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [travelpie_mssql_2] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [travelpie_mssql_2] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [travelpie_mssql_2] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [travelpie_mssql_2] SET  ENABLE_BROKER 
GO
ALTER DATABASE [travelpie_mssql_2] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [travelpie_mssql_2] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [travelpie_mssql_2] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [travelpie_mssql_2] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [travelpie_mssql_2] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [travelpie_mssql_2] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [travelpie_mssql_2] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [travelpie_mssql_2] SET RECOVERY FULL 
GO
ALTER DATABASE [travelpie_mssql_2] SET  MULTI_USER 
GO
ALTER DATABASE [travelpie_mssql_2] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [travelpie_mssql_2] SET DB_CHAINING OFF 
GO
USE [travelpie_mssql_2]
GO
/****** Object:  UserDefinedFunction [dbo].[CONCAT]    Script Date: 12/19/2016 10:31:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[CONCAT](@op1 sql_variant, @op2 sql_variant) RETURNS nvarchar(4000) AS
BEGIN
  DECLARE @result nvarchar(4000)
  SET @result = CAST(@op1 AS nvarchar(4000)) + CAST(@op2 AS nvarchar(4000))
  RETURN @result
END
GO
/****** Object:  UserDefinedFunction [dbo].[CONNECTION_ID]    Script Date: 12/19/2016 10:31:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[CONNECTION_ID]() RETURNS smallint AS
BEGIN
  DECLARE @var smallint
  SELECT @var = @@SPID
  RETURN @Var
END
GO
/****** Object:  UserDefinedFunction [dbo].[GREATEST]    Script Date: 12/19/2016 10:31:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GREATEST](@op1 real, @op2 real) RETURNS real AS
BEGIN
  DECLARE @result real
  SET @result = CASE WHEN @op1 >= @op2 THEN @op1 ELSE @op2 END
  RETURN @result
END
GO
/****** Object:  UserDefinedFunction [dbo].[IF]    Script Date: 12/19/2016 10:31:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[IF](@expr1 sql_variant, @expr2 sql_variant, @expr3 sql_variant) RETURNS sql_variant AS
BEGIN
  DECLARE @result sql_variant
  SET @result = CASE WHEN CAST(@expr1 AS int) != 0 THEN @expr2 ELSE @expr3 END
  RETURN @result
END
GO
/****** Object:  UserDefinedFunction [dbo].[LPAD]    Script Date: 12/19/2016 10:31:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[LPAD](@str nvarchar(max), @len int, @padstr nvarchar(max)) RETURNS nvarchar(4000) AS
BEGIN
  RETURN left(@str + replicate(@padstr,@len),@len);
END
GO
/****** Object:  UserDefinedFunction [dbo].[MD5]    Script Date: 12/19/2016 10:31:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[MD5](@value varchar(255)) RETURNS varchar(32) AS
BEGIN
  RETURN SUBSTRING(sys.fn_sqlvarbasetostr(HASHBYTES('MD5', @value)),3,32);
END
GO
/****** Object:  UserDefinedFunction [dbo].[SUBSTRING]    Script Date: 12/19/2016 10:31:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[SUBSTRING](@op1 nvarchar(max), @op2 sql_variant, @op3 sql_variant) RETURNS nvarchar(max) AS
BEGIN
  RETURN CAST(SUBSTRING(CAST(@op1 AS nvarchar(max)), CAST(@op2 AS int), CAST(@op3 AS int)) AS nvarchar(max))
END
GO
/****** Object:  UserDefinedFunction [dbo].[SUBSTRING_INDEX]    Script Date: 12/19/2016 10:31:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[SUBSTRING_INDEX](@string varchar(8000), @delimiter char(1), @count int) RETURNS varchar(8000) AS
BEGIN
  DECLARE @result varchar(8000)
  DECLARE @end int
  DECLARE @part int
  SET @end = 0
  SET @part = 0
  IF (@count = 0)
  BEGIN
    SET @result = ''
  END
  ELSE
  BEGIN
    IF (@count < 0)
    BEGIN
      SET @string = REVERSE(@string)
    END
    WHILE (@part < ABS(@count))
    BEGIN
      SET @end = CHARINDEX(@delimiter, @string, @end + 1)
      IF (@end = 0)
      BEGIN
        SET @end = LEN(@string) + 1
        BREAK
      END
      SET @part = @part + 1
    END
    SET @result = SUBSTRING(@string, 1, @end - 1)
    IF (@count < 0)
    BEGIN
      SET @result = REVERSE(@result)
    END
  END
  RETURN @result
END
GO
/****** Object:  Table [dbo].[pierian_actions]    Script Date: 12/19/2016 10:31:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_actions](
	[aid] [nvarchar](255) NOT NULL DEFAULT (N'0'),
	[type] [nvarchar](32) NOT NULL DEFAULT (N''),
	[callback] [nvarchar](255) NOT NULL DEFAULT (N''),
	[parameters] [varbinary](max) NOT NULL,
	[label] [nvarchar](255) NOT NULL DEFAULT (N'0'),
 CONSTRAINT [PK_pierian_actions_aid] PRIMARY KEY CLUSTERED 
(
	[aid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_aggregator_category]    Script Date: 12/19/2016 10:31:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_aggregator_category](
	[cid] [int] IDENTITY(2,1) NOT NULL,
	[title] [nvarchar](255) NOT NULL DEFAULT (N''),
	[description] [nvarchar](max) NOT NULL,
	[block] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_aggregator_category_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_aggregator_category$title] UNIQUE NONCLUSTERED 
(
	[title] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_aggregator_category_feed]    Script Date: 12/19/2016 10:31:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_aggregator_category_feed](
	[fid] [int] NOT NULL DEFAULT ((0)),
	[cid] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_aggregator_category_feed_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC,
	[fid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_aggregator_category_item]    Script Date: 12/19/2016 10:31:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_aggregator_category_item](
	[iid] [int] NOT NULL DEFAULT ((0)),
	[cid] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_aggregator_category_item_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC,
	[iid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_aggregator_feed]    Script Date: 12/19/2016 10:31:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_aggregator_feed](
	[fid] [int] IDENTITY(2,1) NOT NULL,
	[title] [nvarchar](255) NOT NULL DEFAULT (N''),
	[url] [nvarchar](max) NOT NULL,
	[refresh] [int] NOT NULL DEFAULT ((0)),
	[checked] [int] NOT NULL DEFAULT ((0)),
	[queued] [int] NOT NULL DEFAULT ((0)),
	[link] [nvarchar](max) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[image] [nvarchar](max) NOT NULL,
	[hash] [nvarchar](64) NOT NULL DEFAULT (N''),
	[etag] [nvarchar](255) NOT NULL DEFAULT (N''),
	[modified] [int] NOT NULL DEFAULT ((0)),
	[block] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_aggregator_feed_fid] PRIMARY KEY CLUSTERED 
(
	[fid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_aggregator_feed$title] UNIQUE NONCLUSTERED 
(
	[title] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_aggregator_item]    Script Date: 12/19/2016 10:31:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_aggregator_item](
	[iid] [int] IDENTITY(30,1) NOT NULL,
	[fid] [int] NOT NULL DEFAULT ((0)),
	[title] [nvarchar](255) NOT NULL DEFAULT (N''),
	[link] [nvarchar](max) NOT NULL,
	[author] [nvarchar](255) NOT NULL DEFAULT (N''),
	[description] [nvarchar](max) NOT NULL,
	[timestamp] [int] NULL DEFAULT (NULL),
	[guid] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_pierian_aggregator_item_iid] PRIMARY KEY CLUSTERED 
(
	[iid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_authmap]    Script Date: 12/19/2016 10:31:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_authmap](
	[aid] [bigint] IDENTITY(1,1) NOT NULL,
	[uid] [int] NOT NULL DEFAULT ((0)),
	[authname] [nvarchar](128) NOT NULL DEFAULT (N''),
	[module] [nvarchar](128) NOT NULL DEFAULT (N''),
 CONSTRAINT [PK_pierian_authmap_aid] PRIMARY KEY CLUSTERED 
(
	[aid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_authmap$authname] UNIQUE NONCLUSTERED 
(
	[authname] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_batch]    Script Date: 12/19/2016 10:31:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_batch](
	[bid] [bigint] NOT NULL,
	[token] [nvarchar](64) NOT NULL,
	[timestamp] [int] NOT NULL,
	[batch] [varbinary](max) NULL,
 CONSTRAINT [PK_pierian_batch_bid] PRIMARY KEY CLUSTERED 
(
	[bid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_block]    Script Date: 12/19/2016 10:31:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_block](
	[bid] [int] IDENTITY(165,1) NOT NULL,
	[module] [nvarchar](64) NOT NULL DEFAULT (N''),
	[delta] [nvarchar](32) NOT NULL DEFAULT (N'0'),
	[theme] [nvarchar](64) NOT NULL DEFAULT (N''),
	[status] [smallint] NOT NULL DEFAULT ((0)),
	[weight] [int] NOT NULL DEFAULT ((0)),
	[region] [nvarchar](64) NOT NULL DEFAULT (N''),
	[custom] [smallint] NOT NULL DEFAULT ((0)),
	[visibility] [smallint] NOT NULL DEFAULT ((0)),
	[pages] [nvarchar](max) NOT NULL,
	[title] [nvarchar](255) NOT NULL DEFAULT (N''),
	[cache] [smallint] NOT NULL DEFAULT ((1)),
 CONSTRAINT [PK_pierian_block_bid] PRIMARY KEY CLUSTERED 
(
	[bid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_block$tmd] UNIQUE NONCLUSTERED 
(
	[theme] ASC,
	[module] ASC,
	[delta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_block_custom]    Script Date: 12/19/2016 10:31:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_block_custom](
	[bid] [bigint] IDENTITY(4,1) NOT NULL,
	[body] [nvarchar](max) NULL,
	[info] [nvarchar](128) NOT NULL DEFAULT (N''),
	[format] [nvarchar](255) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_block_custom_bid] PRIMARY KEY CLUSTERED 
(
	[bid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_block_custom$info] UNIQUE NONCLUSTERED 
(
	[info] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_block_node_type]    Script Date: 12/19/2016 10:31:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_block_node_type](
	[module] [nvarchar](64) NOT NULL,
	[delta] [nvarchar](32) NOT NULL,
	[type] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_pierian_block_node_type_module] PRIMARY KEY CLUSTERED 
(
	[module] ASC,
	[delta] ASC,
	[type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_block_role]    Script Date: 12/19/2016 10:31:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_block_role](
	[module] [nvarchar](64) NOT NULL,
	[delta] [nvarchar](32) NOT NULL,
	[rid] [bigint] NOT NULL,
 CONSTRAINT [PK_pierian_block_role_module] PRIMARY KEY CLUSTERED 
(
	[module] ASC,
	[delta] ASC,
	[rid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_blocked_ips]    Script Date: 12/19/2016 10:31:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_blocked_ips](
	[iid] [bigint] IDENTITY(1,1) NOT NULL,
	[ip] [nvarchar](40) NOT NULL DEFAULT (N''),
 CONSTRAINT [PK_pierian_blocked_ips_iid] PRIMARY KEY CLUSTERED 
(
	[iid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_cache]    Script Date: 12/19/2016 10:31:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_cache](
	[cid] [nvarchar](255) NOT NULL DEFAULT (N''),
	[data] [varbinary](max) NULL,
	[expire] [int] NOT NULL DEFAULT ((0)),
	[created] [int] NOT NULL DEFAULT ((0)),
	[serialized] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_cache_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_cache_block]    Script Date: 12/19/2016 10:31:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_cache_block](
	[cid] [nvarchar](255) NOT NULL DEFAULT (N''),
	[data] [varbinary](max) NULL,
	[expire] [int] NOT NULL DEFAULT ((0)),
	[created] [int] NOT NULL DEFAULT ((0)),
	[serialized] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_cache_block_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_cache_bootstrap]    Script Date: 12/19/2016 10:31:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_cache_bootstrap](
	[cid] [nvarchar](255) NOT NULL DEFAULT (N''),
	[data] [varbinary](max) NULL,
	[expire] [int] NOT NULL DEFAULT ((0)),
	[created] [int] NOT NULL DEFAULT ((0)),
	[serialized] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_cache_bootstrap_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_cache_field]    Script Date: 12/19/2016 10:31:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_cache_field](
	[cid] [nvarchar](255) NOT NULL DEFAULT (N''),
	[data] [varbinary](max) NULL,
	[expire] [int] NOT NULL DEFAULT ((0)),
	[created] [int] NOT NULL DEFAULT ((0)),
	[serialized] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_cache_field_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_cache_filter]    Script Date: 12/19/2016 10:31:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_cache_filter](
	[cid] [nvarchar](255) NOT NULL DEFAULT (N''),
	[data] [varbinary](max) NULL,
	[expire] [int] NOT NULL DEFAULT ((0)),
	[created] [int] NOT NULL DEFAULT ((0)),
	[serialized] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_cache_filter_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_cache_form]    Script Date: 12/19/2016 10:31:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_cache_form](
	[cid] [nvarchar](255) NOT NULL DEFAULT (N''),
	[data] [varbinary](max) NULL,
	[expire] [int] NOT NULL DEFAULT ((0)),
	[created] [int] NOT NULL DEFAULT ((0)),
	[serialized] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_cache_form_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_cache_image]    Script Date: 12/19/2016 10:31:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_cache_image](
	[cid] [nvarchar](255) NOT NULL DEFAULT (N''),
	[data] [varbinary](max) NULL,
	[expire] [int] NOT NULL DEFAULT ((0)),
	[created] [int] NOT NULL DEFAULT ((0)),
	[serialized] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_cache_image_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_cache_libraries]    Script Date: 12/19/2016 10:31:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_cache_libraries](
	[cid] [nvarchar](255) NOT NULL DEFAULT (N''),
	[data] [varbinary](max) NULL,
	[expire] [int] NOT NULL DEFAULT ((0)),
	[created] [int] NOT NULL DEFAULT ((0)),
	[serialized] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_cache_libraries_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_cache_menu]    Script Date: 12/19/2016 10:31:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_cache_menu](
	[cid] [nvarchar](255) NOT NULL DEFAULT (N''),
	[data] [varbinary](max) NULL,
	[expire] [int] NOT NULL DEFAULT ((0)),
	[created] [int] NOT NULL DEFAULT ((0)),
	[serialized] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_cache_menu_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_cache_page]    Script Date: 12/19/2016 10:31:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_cache_page](
	[cid] [nvarchar](255) NOT NULL DEFAULT (N''),
	[data] [varbinary](max) NULL,
	[expire] [int] NOT NULL DEFAULT ((0)),
	[created] [int] NOT NULL DEFAULT ((0)),
	[serialized] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_cache_page_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_cache_path]    Script Date: 12/19/2016 10:31:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_cache_path](
	[cid] [nvarchar](255) NOT NULL DEFAULT (N''),
	[data] [varbinary](max) NULL,
	[expire] [int] NOT NULL DEFAULT ((0)),
	[created] [int] NOT NULL DEFAULT ((0)),
	[serialized] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_cache_path_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_cache_update]    Script Date: 12/19/2016 10:31:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_cache_update](
	[cid] [nvarchar](255) NOT NULL DEFAULT (N''),
	[data] [varbinary](max) NULL,
	[expire] [int] NOT NULL DEFAULT ((0)),
	[created] [int] NOT NULL DEFAULT ((0)),
	[serialized] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_cache_update_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_cache_views]    Script Date: 12/19/2016 10:31:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_cache_views](
	[cid] [nvarchar](255) NOT NULL DEFAULT (N''),
	[data] [varbinary](max) NULL,
	[expire] [int] NOT NULL DEFAULT ((0)),
	[created] [int] NOT NULL DEFAULT ((0)),
	[serialized] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_cache_views_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_cache_views_data]    Script Date: 12/19/2016 10:31:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_cache_views_data](
	[cid] [nvarchar](255) NOT NULL DEFAULT (N''),
	[data] [varbinary](max) NULL,
	[expire] [int] NOT NULL DEFAULT ((0)),
	[created] [int] NOT NULL DEFAULT ((0)),
	[serialized] [smallint] NOT NULL DEFAULT ((1)),
 CONSTRAINT [PK_pierian_cache_views_data_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_comment]    Script Date: 12/19/2016 10:31:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_comment](
	[cid] [int] IDENTITY(2,1) NOT NULL,
	[pid] [int] NOT NULL DEFAULT ((0)),
	[nid] [int] NOT NULL DEFAULT ((0)),
	[uid] [int] NOT NULL DEFAULT ((0)),
	[subject] [nvarchar](64) NOT NULL DEFAULT (N''),
	[hostname] [nvarchar](128) NOT NULL DEFAULT (N''),
	[created] [int] NOT NULL DEFAULT ((0)),
	[changed] [int] NOT NULL DEFAULT ((0)),
	[status] [tinyint] NOT NULL DEFAULT ((1)),
	[thread] [nvarchar](255) NOT NULL,
	[name] [nvarchar](60) NULL DEFAULT (NULL),
	[mail] [nvarchar](64) NULL DEFAULT (NULL),
	[homepage] [nvarchar](255) NULL DEFAULT (NULL),
	[language] [nvarchar](12) NOT NULL DEFAULT (N''),
 CONSTRAINT [PK_pierian_comment_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_comments]    Script Date: 12/19/2016 10:31:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_comments](
	[comment_id] [bigint] IDENTITY(1,1) NOT NULL,
	[thread_id] [bigint] NOT NULL,
	[user_id] [bigint] NOT NULL DEFAULT ((0)),
	[comment_text] [nvarchar](max) NULL,
	[status] [smallint] NOT NULL DEFAULT ((0)),
	[hostname] [nvarchar](128) NULL DEFAULT (NULL),
	[created_at] [datetime2](0) NULL DEFAULT (NULL),
	[updated_at] [datetime2](0) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_comments_comment_id] PRIMARY KEY CLUSTERED 
(
	[comment_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_comments_threads]    Script Date: 12/19/2016 10:31:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_comments_threads](
	[thread_id] [bigint] IDENTITY(1,1) NOT NULL,
	[user_id] [bigint] NOT NULL DEFAULT ((0)),
	[tid] [bigint] NOT NULL,
	[block_name_1] [nvarchar](255) NULL DEFAULT (NULL),
	[block_name_2] [nvarchar](255) NULL DEFAULT (NULL),
	[block_name_3] [nvarchar](255) NULL DEFAULT (NULL),
	[last_updated] [int] NULL DEFAULT ((0)),
	[no_comments] [int] NULL DEFAULT ((0)),
	[no_tags] [int] NULL DEFAULT ((0)),
	[img_path] [nvarchar](512) NULL DEFAULT (NULL),
	[status] [smallint] NOT NULL DEFAULT ((0)),
	[created_at] [datetime2](0) NULL DEFAULT (NULL),
	[updated_at] [datetime2](0) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_comments_threads_thread_id] PRIMARY KEY CLUSTERED 
(
	[thread_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_community]    Script Date: 12/19/2016 10:31:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_community](
	[community_id] [int] IDENTITY(1,1) NOT NULL,
	[community_name] [nvarchar](255) NOT NULL,
	[community_logo] [nvarchar](max) NULL,
 CONSTRAINT [PK_pierian_community_community_id] PRIMARY KEY CLUSTERED 
(
	[community_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_contact]    Script Date: 12/19/2016 10:31:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_contact](
	[cid] [bigint] IDENTITY(2,1) NOT NULL,
	[category] [nvarchar](255) NOT NULL DEFAULT (N''),
	[recipients] [nvarchar](max) NOT NULL,
	[reply] [nvarchar](max) NOT NULL,
	[weight] [int] NOT NULL DEFAULT ((0)),
	[selected] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_contact_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_contact$category] UNIQUE NONCLUSTERED 
(
	[category] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_corporate]    Script Date: 12/19/2016 10:31:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_corporate](
	[corporate_id] [int] IDENTITY(1,1) NOT NULL,
	[corporate_name] [nvarchar](255) NOT NULL,
	[community_id] [int] NULL DEFAULT (NULL),
	[description] [nvarchar](max) NULL,
	[dk_number] [int] NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_corporate_corporate_id] PRIMARY KEY CLUSTERED 
(
	[corporate_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_corporate$corporate_name] UNIQUE NONCLUSTERED 
(
	[corporate_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_ctools_access_ruleset]    Script Date: 12/19/2016 10:31:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_ctools_access_ruleset](
	[rsid] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NULL DEFAULT (NULL),
	[admin_title] [nvarchar](255) NULL DEFAULT (NULL),
	[admin_description] [nvarchar](max) NULL,
	[requiredcontexts] [nvarchar](max) NULL,
	[contexts] [nvarchar](max) NULL,
	[relationships] [nvarchar](max) NULL,
	[access] [nvarchar](max) NULL,
 CONSTRAINT [PK_pierian_ctools_access_ruleset_rsid] PRIMARY KEY CLUSTERED 
(
	[rsid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_ctools_css_cache]    Script Date: 12/19/2016 10:31:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_ctools_css_cache](
	[cid] [nvarchar](128) NOT NULL,
	[filename] [nvarchar](255) NULL DEFAULT (NULL),
	[css] [nvarchar](max) NULL,
	[filter] [smallint] NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_ctools_css_cache_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_ctools_custom_content]    Script Date: 12/19/2016 10:31:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_ctools_custom_content](
	[cid] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NULL DEFAULT (NULL),
	[admin_title] [nvarchar](255) NULL DEFAULT (NULL),
	[admin_description] [nvarchar](max) NULL,
	[category] [nvarchar](255) NULL DEFAULT (NULL),
	[settings] [nvarchar](max) NULL,
 CONSTRAINT [PK_pierian_ctools_custom_content_cid] PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_ctools_object_cache]    Script Date: 12/19/2016 10:31:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_ctools_object_cache](
	[sid] [nvarchar](64) NOT NULL,
	[name] [nvarchar](128) NOT NULL,
	[obj] [nvarchar](128) NOT NULL,
	[updated] [bigint] NOT NULL DEFAULT ((0)),
	[data] [varbinary](max) NULL,
 CONSTRAINT [PK_pierian_ctools_object_cache_sid] PRIMARY KEY CLUSTERED 
(
	[sid] ASC,
	[obj] ASC,
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_currency]    Script Date: 12/19/2016 10:31:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_currency](
	[currency_id] [int] IDENTITY(199,1) NOT NULL,
	[currency_name] [nvarchar](100) NULL DEFAULT (NULL),
	[currency_short_code] [nvarchar](25) NULL DEFAULT (NULL),
	[satus] [int] NULL DEFAULT ((0)),
	[exchange_rate_required] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_currency_currency_id] PRIMARY KEY CLUSTERED 
(
	[currency_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_dashboard_widgets]    Script Date: 12/19/2016 10:31:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_dashboard_widgets](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[dashboard_index] [bigint] NOT NULL,
	[widget_id] [nvarchar](250) NULL DEFAULT (NULL),
	[created_at] [datetime2](0) NULL DEFAULT (NULL),
	[updated_at] [datetime2](0) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_dashboard_widgets_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_dashboards]    Script Date: 12/19/2016 10:31:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_dashboards](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[dashboard_id] [nvarchar](250) NOT NULL,
	[dashboard_created_time] [datetime2](0) NULL DEFAULT (NULL),
	[dashboard_updated_time] [datetime2](0) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_dashboards_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_dashboards$dashboard_id] UNIQUE NONCLUSTERED 
(
	[dashboard_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_date_format_locale]    Script Date: 12/19/2016 10:31:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_date_format_locale](
	[format] [nvarchar](100) NOT NULL,
	[type] [nvarchar](64) NOT NULL,
	[language] [nvarchar](12) NOT NULL,
 CONSTRAINT [PK_pierian_date_format_locale_type] PRIMARY KEY CLUSTERED 
(
	[type] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_date_format_type]    Script Date: 12/19/2016 10:31:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_date_format_type](
	[type] [nvarchar](64) NOT NULL,
	[title] [nvarchar](255) NOT NULL,
	[locked] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_date_format_type_type] PRIMARY KEY CLUSTERED 
(
	[type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_date_formats]    Script Date: 12/19/2016 10:31:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_date_formats](
	[dfid] [bigint] IDENTITY(36,1) NOT NULL,
	[format] [nvarchar](100) NOT NULL,
	[type] [nvarchar](64) NOT NULL,
	[locked] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_date_formats_dfid] PRIMARY KEY CLUSTERED 
(
	[dfid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_date_formats$formats] UNIQUE NONCLUSTERED 
(
	[format] ASC,
	[type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_drupalchat_msg]    Script Date: 12/19/2016 10:31:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_drupalchat_msg](
	[message_id] [nvarchar](50) NOT NULL,
	[uid1] [nvarchar](32) NOT NULL,
	[uid2] [nvarchar](32) NOT NULL,
	[message] [nvarchar](max) NOT NULL,
	[timestamp] [int] NOT NULL,
	[__pk] [uniqueidentifier] NOT NULL DEFAULT (newid())
)

GO
/****** Object:  Table [dbo].[pierian_drupalchat_users]    Script Date: 12/19/2016 10:31:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_drupalchat_users](
	[uid] [int] NOT NULL,
	[session] [nvarchar](60) NOT NULL,
	[name] [nvarchar](60) NOT NULL,
	[status] [smallint] NOT NULL CONSTRAINT [pierian_drupalchat_users_status_df]  DEFAULT ((0)),
	[timestamp] [int] NOT NULL,
 CONSTRAINT [pierian_drupalchat_users_pkey] PRIMARY KEY CLUSTERED 
(
	[uid] ASC,
	[session] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_config]    Script Date: 12/19/2016 10:31:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_field_config](
	[id] [int] IDENTITY(12,1) NOT NULL,
	[field_name] [nvarchar](32) NOT NULL,
	[type] [nvarchar](128) NOT NULL,
	[module] [nvarchar](128) NOT NULL DEFAULT (N''),
	[active] [smallint] NOT NULL DEFAULT ((0)),
	[storage_type] [nvarchar](128) NOT NULL,
	[storage_module] [nvarchar](128) NOT NULL DEFAULT (N''),
	[storage_active] [smallint] NOT NULL DEFAULT ((0)),
	[locked] [smallint] NOT NULL DEFAULT ((0)),
	[data] [varbinary](max) NOT NULL,
	[cardinality] [smallint] NOT NULL DEFAULT ((0)),
	[translatable] [smallint] NOT NULL DEFAULT ((0)),
	[deleted] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_field_config_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_field_config_instance]    Script Date: 12/19/2016 10:31:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_field_config_instance](
	[id] [int] IDENTITY(28,1) NOT NULL,
	[field_id] [int] NOT NULL,
	[field_name] [nvarchar](32) NOT NULL DEFAULT (N''),
	[entity_type] [nvarchar](32) NOT NULL DEFAULT (N''),
	[bundle] [nvarchar](128) NOT NULL DEFAULT (N''),
	[data] [varbinary](max) NOT NULL,
	[deleted] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_field_config_instance_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_field_data_body]    Script Date: 12/19/2016 10:31:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_data_body](
	[entity_type] [nvarchar](128) NOT NULL DEFAULT (N''),
	[bundle] [nvarchar](128) NOT NULL DEFAULT (N''),
	[deleted] [smallint] NOT NULL DEFAULT ((0)),
	[entity_id] [bigint] NOT NULL,
	[revision_id] [bigint] NULL DEFAULT (NULL),
	[language] [nvarchar](32) NOT NULL DEFAULT (N''),
	[delta] [bigint] NOT NULL,
	[body_value] [nvarchar](max) NULL,
	[body_summary] [nvarchar](max) NULL,
	[body_format] [nvarchar](255) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_field_data_body_entity_type] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_data_comment_body]    Script Date: 12/19/2016 10:31:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_data_comment_body](
	[entity_type] [nvarchar](128) NOT NULL DEFAULT (N''),
	[bundle] [nvarchar](128) NOT NULL DEFAULT (N''),
	[deleted] [smallint] NOT NULL DEFAULT ((0)),
	[entity_id] [bigint] NOT NULL,
	[revision_id] [bigint] NULL DEFAULT (NULL),
	[language] [nvarchar](32) NOT NULL DEFAULT (N''),
	[delta] [bigint] NOT NULL,
	[comment_body_value] [nvarchar](max) NULL,
	[comment_body_format] [nvarchar](255) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_field_data_comment_body_entity_type] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_data_field_dashboard_id]    Script Date: 12/19/2016 10:31:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_data_field_dashboard_id](
	[entity_type] [nvarchar](128) NOT NULL DEFAULT (N''),
	[bundle] [nvarchar](128) NOT NULL DEFAULT (N''),
	[deleted] [smallint] NOT NULL DEFAULT ((0)),
	[entity_id] [bigint] NOT NULL,
	[revision_id] [bigint] NULL DEFAULT (NULL),
	[language] [nvarchar](32) NOT NULL DEFAULT (N''),
	[delta] [bigint] NOT NULL,
	[field_dashboard_id_value] [nvarchar](255) NULL DEFAULT (NULL),
	[field_dashboard_id_format] [nvarchar](255) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_field_data_field_dashboard_id_entity_type] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_data_field_icon]    Script Date: 12/19/2016 10:31:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_data_field_icon](
	[entity_type] [nvarchar](128) NOT NULL DEFAULT (N''),
	[bundle] [nvarchar](128) NOT NULL DEFAULT (N''),
	[deleted] [smallint] NOT NULL DEFAULT ((0)),
	[entity_id] [bigint] NOT NULL,
	[revision_id] [bigint] NULL DEFAULT (NULL),
	[language] [nvarchar](32) NOT NULL DEFAULT (N''),
	[delta] [bigint] NOT NULL,
	[field_icon_fid] [bigint] NULL DEFAULT (NULL),
	[field_icon_alt] [nvarchar](512) NULL DEFAULT (NULL),
	[field_icon_title] [nvarchar](1024) NULL DEFAULT (NULL),
	[field_icon_width] [bigint] NULL DEFAULT (NULL),
	[field_icon_height] [bigint] NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_field_data_field_icon_entity_type] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_data_field_image]    Script Date: 12/19/2016 10:31:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_data_field_image](
	[entity_type] [nvarchar](128) NOT NULL DEFAULT (N''),
	[bundle] [nvarchar](128) NOT NULL DEFAULT (N''),
	[deleted] [smallint] NOT NULL DEFAULT ((0)),
	[entity_id] [bigint] NOT NULL,
	[revision_id] [bigint] NULL DEFAULT (NULL),
	[language] [nvarchar](32) NOT NULL DEFAULT (N''),
	[delta] [bigint] NOT NULL,
	[field_image_fid] [bigint] NULL DEFAULT (NULL),
	[field_image_alt] [nvarchar](512) NULL DEFAULT (NULL),
	[field_image_title] [nvarchar](1024) NULL DEFAULT (NULL),
	[field_image_width] [bigint] NULL DEFAULT (NULL),
	[field_image_height] [bigint] NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_field_data_field_image_entity_type] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_data_field_insight_link]    Script Date: 12/19/2016 10:31:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_data_field_insight_link](
	[entity_type] [nvarchar](128) NOT NULL CONSTRAINT [pierian_field_data_field_insight_link_entity_type_df]  DEFAULT (''),
	[bundle] [nvarchar](128) NOT NULL CONSTRAINT [pierian_field_data_field_insight_link_bundle_df]  DEFAULT (''),
	[deleted] [smallint] NOT NULL CONSTRAINT [pierian_field_data_field_insight_link_deleted_df]  DEFAULT ((0)),
	[entity_id] [int] NOT NULL,
	[revision_id] [int] NULL,
	[language] [nvarchar](32) NOT NULL CONSTRAINT [pierian_field_data_field_insight_link_language_df]  DEFAULT (''),
	[delta] [int] NOT NULL,
	[field_insight_link_value] [nvarchar](255) NULL,
	[field_insight_link_format] [nvarchar](255) NULL,
 CONSTRAINT [pierian_field_data_field_insight_link_pkey] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_data_field_red_zone_image]    Script Date: 12/19/2016 10:31:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_data_field_red_zone_image](
	[entity_type] [nvarchar](128) NOT NULL CONSTRAINT [pierian_field_data_field_red_zone_image_entity_type_df]  DEFAULT (''),
	[bundle] [nvarchar](128) NOT NULL CONSTRAINT [pierian_field_data_field_red_zone_image_bundle_df]  DEFAULT (''),
	[deleted] [smallint] NOT NULL CONSTRAINT [pierian_field_data_field_red_zone_image_deleted_df]  DEFAULT ((0)),
	[entity_id] [int] NOT NULL,
	[revision_id] [int] NULL,
	[language] [nvarchar](32) NOT NULL CONSTRAINT [pierian_field_data_field_red_zone_image_language_df]  DEFAULT (''),
	[delta] [int] NOT NULL,
	[field_red_zone_image_fid] [int] NULL,
	[field_red_zone_image_alt] [nvarchar](512) NULL,
	[field_red_zone_image_title] [nvarchar](1024) NULL,
	[field_red_zone_image_width] [int] NULL,
	[field_red_zone_image_height] [int] NULL,
 CONSTRAINT [pierian_field_data_field_red_zone_image_pkey] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_data_field_report_info]    Script Date: 12/19/2016 10:31:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_data_field_report_info](
	[entity_type] [nvarchar](128) NOT NULL DEFAULT (N''),
	[bundle] [nvarchar](128) NOT NULL DEFAULT (N''),
	[deleted] [smallint] NOT NULL DEFAULT ((0)),
	[entity_id] [bigint] NOT NULL,
	[revision_id] [bigint] NULL DEFAULT (NULL),
	[language] [nvarchar](32) NOT NULL DEFAULT (N''),
	[delta] [bigint] NOT NULL,
	[field_report_info_value] [nvarchar](max) NULL,
	[field_report_info_format] [nvarchar](255) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_field_data_field_report_info_entity_type] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_data_field_report_tip]    Script Date: 12/19/2016 10:31:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_data_field_report_tip](
	[entity_type] [nvarchar](128) NOT NULL DEFAULT (N''),
	[bundle] [nvarchar](128) NOT NULL DEFAULT (N''),
	[deleted] [smallint] NOT NULL DEFAULT ((0)),
	[entity_id] [bigint] NOT NULL,
	[revision_id] [bigint] NULL DEFAULT (NULL),
	[language] [nvarchar](32) NOT NULL DEFAULT (N''),
	[delta] [bigint] NOT NULL,
	[field_report_tip_value] [nvarchar](max) NULL,
	[field_report_tip_format] [nvarchar](255) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_field_data_field_report_tip_entity_type] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_data_field_slideshow_image]    Script Date: 12/19/2016 10:31:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_data_field_slideshow_image](
	[entity_type] [nvarchar](128) NOT NULL DEFAULT (N''),
	[bundle] [nvarchar](128) NOT NULL DEFAULT (N''),
	[deleted] [smallint] NOT NULL DEFAULT ((0)),
	[entity_id] [bigint] NOT NULL,
	[revision_id] [bigint] NULL DEFAULT (NULL),
	[language] [nvarchar](32) NOT NULL DEFAULT (N''),
	[delta] [bigint] NOT NULL,
	[field_slideshow_image_fid] [bigint] NULL DEFAULT (NULL),
	[field_slideshow_image_alt] [nvarchar](512) NULL DEFAULT (NULL),
	[field_slideshow_image_title] [nvarchar](1024) NULL DEFAULT (NULL),
	[field_slideshow_image_width] [bigint] NULL DEFAULT (NULL),
	[field_slideshow_image_height] [bigint] NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_field_data_field_slideshow_image_entity_type] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_data_field_visibility]    Script Date: 12/19/2016 10:31:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_data_field_visibility](
	[entity_type] [nvarchar](128) NOT NULL DEFAULT (N''),
	[bundle] [nvarchar](128) NOT NULL DEFAULT (N''),
	[deleted] [smallint] NOT NULL DEFAULT ((0)),
	[entity_id] [bigint] NOT NULL,
	[revision_id] [bigint] NULL DEFAULT (NULL),
	[language] [nvarchar](32) NOT NULL DEFAULT (N''),
	[delta] [bigint] NOT NULL,
	[field_visibility_value] [int] NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_field_data_field_visibility_entity_type] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_revision_body]    Script Date: 12/19/2016 10:31:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_revision_body](
	[entity_type] [nvarchar](128) NOT NULL DEFAULT (N''),
	[bundle] [nvarchar](128) NOT NULL DEFAULT (N''),
	[deleted] [smallint] NOT NULL DEFAULT ((0)),
	[entity_id] [bigint] NOT NULL,
	[revision_id] [bigint] NOT NULL,
	[language] [nvarchar](32) NOT NULL DEFAULT (N''),
	[delta] [bigint] NOT NULL,
	[body_value] [nvarchar](max) NULL,
	[body_summary] [nvarchar](max) NULL,
	[body_format] [nvarchar](255) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_field_revision_body_entity_type] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[revision_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_revision_comment_body]    Script Date: 12/19/2016 10:31:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_revision_comment_body](
	[entity_type] [nvarchar](128) NOT NULL DEFAULT (N''),
	[bundle] [nvarchar](128) NOT NULL DEFAULT (N''),
	[deleted] [smallint] NOT NULL DEFAULT ((0)),
	[entity_id] [bigint] NOT NULL,
	[revision_id] [bigint] NOT NULL,
	[language] [nvarchar](32) NOT NULL DEFAULT (N''),
	[delta] [bigint] NOT NULL,
	[comment_body_value] [nvarchar](max) NULL,
	[comment_body_format] [nvarchar](255) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_field_revision_comment_body_entity_type] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[revision_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_revision_field_dashboard_id]    Script Date: 12/19/2016 10:31:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_revision_field_dashboard_id](
	[entity_type] [nvarchar](128) NOT NULL DEFAULT (N''),
	[bundle] [nvarchar](128) NOT NULL DEFAULT (N''),
	[deleted] [smallint] NOT NULL DEFAULT ((0)),
	[entity_id] [bigint] NOT NULL,
	[revision_id] [bigint] NOT NULL,
	[language] [nvarchar](32) NOT NULL DEFAULT (N''),
	[delta] [bigint] NOT NULL,
	[field_dashboard_id_value] [nvarchar](255) NULL DEFAULT (NULL),
	[field_dashboard_id_format] [nvarchar](255) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_field_revision_field_dashboard_id_entity_type] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[revision_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_revision_field_icon]    Script Date: 12/19/2016 10:31:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_revision_field_icon](
	[entity_type] [nvarchar](128) NOT NULL DEFAULT (N''),
	[bundle] [nvarchar](128) NOT NULL DEFAULT (N''),
	[deleted] [smallint] NOT NULL DEFAULT ((0)),
	[entity_id] [bigint] NOT NULL,
	[revision_id] [bigint] NOT NULL,
	[language] [nvarchar](32) NOT NULL DEFAULT (N''),
	[delta] [bigint] NOT NULL,
	[field_icon_fid] [bigint] NULL DEFAULT (NULL),
	[field_icon_alt] [nvarchar](512) NULL DEFAULT (NULL),
	[field_icon_title] [nvarchar](1024) NULL DEFAULT (NULL),
	[field_icon_width] [bigint] NULL DEFAULT (NULL),
	[field_icon_height] [bigint] NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_field_revision_field_icon_entity_type] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[revision_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_revision_field_image]    Script Date: 12/19/2016 10:31:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_revision_field_image](
	[entity_type] [nvarchar](128) NOT NULL DEFAULT (N''),
	[bundle] [nvarchar](128) NOT NULL DEFAULT (N''),
	[deleted] [smallint] NOT NULL DEFAULT ((0)),
	[entity_id] [bigint] NOT NULL,
	[revision_id] [bigint] NOT NULL,
	[language] [nvarchar](32) NOT NULL DEFAULT (N''),
	[delta] [bigint] NOT NULL,
	[field_image_fid] [bigint] NULL DEFAULT (NULL),
	[field_image_alt] [nvarchar](512) NULL DEFAULT (NULL),
	[field_image_title] [nvarchar](1024) NULL DEFAULT (NULL),
	[field_image_width] [bigint] NULL DEFAULT (NULL),
	[field_image_height] [bigint] NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_field_revision_field_image_entity_type] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[revision_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_revision_field_insight_link]    Script Date: 12/19/2016 10:31:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_revision_field_insight_link](
	[entity_type] [nvarchar](128) NOT NULL CONSTRAINT [pierian_field_revision_field_insight_link_entity_type_df]  DEFAULT (''),
	[bundle] [nvarchar](128) NOT NULL CONSTRAINT [pierian_field_revision_field_insight_link_bundle_df]  DEFAULT (''),
	[deleted] [smallint] NOT NULL CONSTRAINT [pierian_field_revision_field_insight_link_deleted_df]  DEFAULT ((0)),
	[entity_id] [int] NOT NULL,
	[revision_id] [int] NOT NULL,
	[language] [nvarchar](32) NOT NULL CONSTRAINT [pierian_field_revision_field_insight_link_language_df]  DEFAULT (''),
	[delta] [int] NOT NULL,
	[field_insight_link_value] [nvarchar](255) NULL,
	[field_insight_link_format] [nvarchar](255) NULL,
 CONSTRAINT [pierian_field_revision_field_insight_link_pkey] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[revision_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_revision_field_red_zone_image]    Script Date: 12/19/2016 10:31:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_revision_field_red_zone_image](
	[entity_type] [nvarchar](128) NOT NULL CONSTRAINT [pierian_field_revision_field_red_zone_image_entity_type_df]  DEFAULT (''),
	[bundle] [nvarchar](128) NOT NULL CONSTRAINT [pierian_field_revision_field_red_zone_image_bundle_df]  DEFAULT (''),
	[deleted] [smallint] NOT NULL CONSTRAINT [pierian_field_revision_field_red_zone_image_deleted_df]  DEFAULT ((0)),
	[entity_id] [int] NOT NULL,
	[revision_id] [int] NOT NULL,
	[language] [nvarchar](32) NOT NULL CONSTRAINT [pierian_field_revision_field_red_zone_image_language_df]  DEFAULT (''),
	[delta] [int] NOT NULL,
	[field_red_zone_image_fid] [int] NULL,
	[field_red_zone_image_alt] [nvarchar](512) NULL,
	[field_red_zone_image_title] [nvarchar](1024) NULL,
	[field_red_zone_image_width] [int] NULL,
	[field_red_zone_image_height] [int] NULL,
 CONSTRAINT [pierian_field_revision_field_red_zone_image_pkey] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[revision_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_revision_field_report_info]    Script Date: 12/19/2016 10:31:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_revision_field_report_info](
	[entity_type] [nvarchar](128) NOT NULL DEFAULT (N''),
	[bundle] [nvarchar](128) NOT NULL DEFAULT (N''),
	[deleted] [smallint] NOT NULL DEFAULT ((0)),
	[entity_id] [bigint] NOT NULL,
	[revision_id] [bigint] NOT NULL,
	[language] [nvarchar](32) NOT NULL DEFAULT (N''),
	[delta] [bigint] NOT NULL,
	[field_report_info_value] [nvarchar](max) NULL,
	[field_report_info_format] [nvarchar](255) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_field_revision_field_report_info_entity_type] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[revision_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_revision_field_report_tip]    Script Date: 12/19/2016 10:31:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_revision_field_report_tip](
	[entity_type] [nvarchar](128) NOT NULL DEFAULT (N''),
	[bundle] [nvarchar](128) NOT NULL DEFAULT (N''),
	[deleted] [smallint] NOT NULL DEFAULT ((0)),
	[entity_id] [bigint] NOT NULL,
	[revision_id] [bigint] NOT NULL,
	[language] [nvarchar](32) NOT NULL DEFAULT (N''),
	[delta] [bigint] NOT NULL,
	[field_report_tip_value] [nvarchar](max) NULL,
	[field_report_tip_format] [nvarchar](255) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_field_revision_field_report_tip_entity_type] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[revision_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_revision_field_slideshow_image]    Script Date: 12/19/2016 10:31:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_revision_field_slideshow_image](
	[entity_type] [nvarchar](128) NOT NULL DEFAULT (N''),
	[bundle] [nvarchar](128) NOT NULL DEFAULT (N''),
	[deleted] [smallint] NOT NULL DEFAULT ((0)),
	[entity_id] [bigint] NOT NULL,
	[revision_id] [bigint] NOT NULL,
	[language] [nvarchar](32) NOT NULL DEFAULT (N''),
	[delta] [bigint] NOT NULL,
	[field_slideshow_image_fid] [bigint] NULL DEFAULT (NULL),
	[field_slideshow_image_alt] [nvarchar](512) NULL DEFAULT (NULL),
	[field_slideshow_image_title] [nvarchar](1024) NULL DEFAULT (NULL),
	[field_slideshow_image_width] [bigint] NULL DEFAULT (NULL),
	[field_slideshow_image_height] [bigint] NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_field_revision_field_slideshow_image_entity_type] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[revision_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_field_revision_field_visibility]    Script Date: 12/19/2016 10:31:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_field_revision_field_visibility](
	[entity_type] [nvarchar](128) NOT NULL DEFAULT (N''),
	[bundle] [nvarchar](128) NOT NULL DEFAULT (N''),
	[deleted] [smallint] NOT NULL DEFAULT ((0)),
	[entity_id] [bigint] NOT NULL,
	[revision_id] [bigint] NOT NULL,
	[language] [nvarchar](32) NOT NULL DEFAULT (N''),
	[delta] [bigint] NOT NULL,
	[field_visibility_value] [int] NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_field_revision_field_visibility_entity_type] PRIMARY KEY CLUSTERED 
(
	[entity_type] ASC,
	[entity_id] ASC,
	[revision_id] ASC,
	[deleted] ASC,
	[delta] ASC,
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_file_managed]    Script Date: 12/19/2016 10:31:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_file_managed](
	[fid] [bigint] IDENTITY(75,1) NOT NULL,
	[uid] [bigint] NOT NULL DEFAULT ((0)),
	[filename] [nvarchar](255) NOT NULL DEFAULT (N''),
	[uri] [nvarchar](255) NOT NULL DEFAULT (N''),
	[filemime] [nvarchar](255) NOT NULL DEFAULT (N''),
	[filesize] [bigint] NOT NULL DEFAULT ((0)),
	[status] [smallint] NOT NULL DEFAULT ((0)),
	[timestamp] [bigint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_file_managed_fid] PRIMARY KEY CLUSTERED 
(
	[fid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_file_managed$uri] UNIQUE NONCLUSTERED 
(
	[uri] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_file_usage]    Script Date: 12/19/2016 10:31:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_file_usage](
	[fid] [bigint] NOT NULL,
	[module] [nvarchar](255) NOT NULL DEFAULT (N''),
	[type] [nvarchar](64) NOT NULL DEFAULT (N''),
	[id] [bigint] NOT NULL DEFAULT ((0)),
	[count] [bigint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_file_usage_fid] PRIMARY KEY CLUSTERED 
(
	[fid] ASC,
	[type] ASC,
	[id] ASC,
	[module] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_filter]    Script Date: 12/19/2016 10:31:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_filter](
	[format] [nvarchar](255) NOT NULL,
	[module] [nvarchar](64) NOT NULL DEFAULT (N''),
	[name] [nvarchar](32) NOT NULL DEFAULT (N''),
	[weight] [int] NOT NULL DEFAULT ((0)),
	[status] [int] NOT NULL DEFAULT ((0)),
	[settings] [varbinary](max) NULL,
 CONSTRAINT [PK_pierian_filter_format] PRIMARY KEY CLUSTERED 
(
	[format] ASC,
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_filter_format]    Script Date: 12/19/2016 10:31:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_filter_format](
	[format] [nvarchar](255) NOT NULL,
	[name] [nvarchar](255) NOT NULL DEFAULT (N''),
	[cache] [smallint] NOT NULL DEFAULT ((0)),
	[status] [tinyint] NOT NULL DEFAULT ((1)),
	[weight] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_filter_format_format] PRIMARY KEY CLUSTERED 
(
	[format] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_filter_format$name] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_flood]    Script Date: 12/19/2016 10:31:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_flood](
	[fid] [int] IDENTITY(1,1) NOT NULL,
	[event] [nvarchar](64) NOT NULL DEFAULT (N''),
	[identifier] [nvarchar](128) NOT NULL DEFAULT (N''),
	[timestamp] [int] NOT NULL DEFAULT ((0)),
	[expiration] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_flood_fid] PRIMARY KEY CLUSTERED 
(
	[fid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_group]    Script Date: 12/19/2016 10:31:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_group](
	[group_id] [int] IDENTITY(2,1) NOT NULL,
	[group_name] [nvarchar](255) NOT NULL,
	[spark_user_id] [int] NOT NULL DEFAULT ((1)),
 CONSTRAINT [PK_pierian_group_group_id] PRIMARY KEY CLUSTERED 
(
	[group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_group$group_name] UNIQUE NONCLUSTERED 
(
	[group_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_history]    Script Date: 12/19/2016 10:31:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_history](
	[uid] [int] NOT NULL DEFAULT ((0)),
	[nid] [bigint] NOT NULL DEFAULT ((0)),
	[timestamp] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_history_uid] PRIMARY KEY CLUSTERED 
(
	[uid] ASC,
	[nid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_ife]    Script Date: 12/19/2016 10:31:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_ife](
	[form_id] [nvarchar](128) NOT NULL,
	[field_types] [nvarchar](max) NULL,
	[status] [int] NOT NULL DEFAULT ((0)),
	[display] [int] NOT NULL DEFAULT ((-1)),
 CONSTRAINT [PK_pierian_ife_form_id] PRIMARY KEY CLUSTERED 
(
	[form_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_image_effects]    Script Date: 12/19/2016 10:31:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_image_effects](
	[ieid] [bigint] IDENTITY(2,1) NOT NULL,
	[isid] [bigint] NOT NULL DEFAULT ((0)),
	[weight] [int] NOT NULL DEFAULT ((0)),
	[name] [nvarchar](255) NOT NULL,
	[data] [varbinary](max) NOT NULL,
 CONSTRAINT [PK_pierian_image_effects_ieid] PRIMARY KEY CLUSTERED 
(
	[ieid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_image_styles]    Script Date: 12/19/2016 10:31:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_image_styles](
	[isid] [bigint] IDENTITY(2,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[label] [nvarchar](255) NOT NULL DEFAULT (N''),
 CONSTRAINT [PK_pierian_image_styles_isid] PRIMARY KEY CLUSTERED 
(
	[isid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_image_styles$name] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_menu_custom]    Script Date: 12/19/2016 10:31:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_menu_custom](
	[menu_name] [nvarchar](32) NOT NULL DEFAULT (N''),
	[title] [nvarchar](255) NOT NULL DEFAULT (N''),
	[description] [nvarchar](max) NULL,
 CONSTRAINT [PK_pierian_menu_custom_menu_name] PRIMARY KEY CLUSTERED 
(
	[menu_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_menu_links]    Script Date: 12/19/2016 10:31:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_menu_links](
	[menu_name] [nvarchar](32) NOT NULL DEFAULT (N''),
	[mlid] [bigint] IDENTITY(908,1) NOT NULL,
	[plid] [bigint] NOT NULL DEFAULT ((0)),
	[link_path] [nvarchar](255) NOT NULL DEFAULT (N''),
	[router_path] [nvarchar](255) NOT NULL DEFAULT (N''),
	[link_title] [nvarchar](255) NOT NULL DEFAULT (N''),
	[options] [varbinary](max) NULL,
	[module] [nvarchar](255) NOT NULL DEFAULT (N'system'),
	[hidden] [smallint] NOT NULL DEFAULT ((0)),
	[external] [smallint] NOT NULL DEFAULT ((0)),
	[has_children] [smallint] NOT NULL DEFAULT ((0)),
	[expanded] [smallint] NOT NULL DEFAULT ((0)),
	[weight] [int] NOT NULL DEFAULT ((0)),
	[depth] [smallint] NOT NULL DEFAULT ((0)),
	[customized] [smallint] NOT NULL DEFAULT ((0)),
	[p1] [bigint] NOT NULL DEFAULT ((0)),
	[p2] [bigint] NOT NULL DEFAULT ((0)),
	[p3] [bigint] NOT NULL DEFAULT ((0)),
	[p4] [bigint] NOT NULL DEFAULT ((0)),
	[p5] [bigint] NOT NULL DEFAULT ((0)),
	[p6] [bigint] NOT NULL DEFAULT ((0)),
	[p7] [bigint] NOT NULL DEFAULT ((0)),
	[p8] [bigint] NOT NULL DEFAULT ((0)),
	[p9] [bigint] NOT NULL DEFAULT ((0)),
	[updated] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_menu_links_mlid] PRIMARY KEY CLUSTERED 
(
	[mlid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_menu_router]    Script Date: 12/19/2016 10:31:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_menu_router](
	[path] [nvarchar](255) NOT NULL DEFAULT (N''),
	[load_functions] [varbinary](max) NOT NULL,
	[to_arg_functions] [varbinary](max) NOT NULL,
	[access_callback] [nvarchar](255) NOT NULL DEFAULT (N''),
	[access_arguments] [varbinary](max) NULL,
	[page_callback] [nvarchar](255) NOT NULL DEFAULT (N''),
	[page_arguments] [varbinary](max) NULL,
	[delivery_callback] [nvarchar](255) NOT NULL DEFAULT (N''),
	[fit] [int] NOT NULL DEFAULT ((0)),
	[number_parts] [smallint] NOT NULL DEFAULT ((0)),
	[context] [int] NOT NULL DEFAULT ((0)),
	[tab_parent] [nvarchar](255) NOT NULL DEFAULT (N''),
	[tab_root] [nvarchar](255) NOT NULL DEFAULT (N''),
	[title] [nvarchar](255) NOT NULL DEFAULT (N''),
	[title_callback] [nvarchar](255) NOT NULL DEFAULT (N''),
	[title_arguments] [nvarchar](255) NOT NULL DEFAULT (N''),
	[theme_callback] [nvarchar](255) NOT NULL DEFAULT (N''),
	[theme_arguments] [nvarchar](255) NOT NULL DEFAULT (N''),
	[type] [int] NOT NULL DEFAULT ((0)),
	[description] [nvarchar](max) NOT NULL,
	[position] [nvarchar](255) NOT NULL DEFAULT (N''),
	[weight] [int] NOT NULL DEFAULT ((0)),
	[include_file] [nvarchar](max) NULL,
 CONSTRAINT [PK_pierian_menu_router_path] PRIMARY KEY CLUSTERED 
(
	[path] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_node]    Script Date: 12/19/2016 10:31:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_node](
	[nid] [bigint] IDENTITY(13,1) NOT NULL,
	[vid] [bigint] NULL DEFAULT (NULL),
	[type] [nvarchar](32) NOT NULL DEFAULT (N''),
	[language] [nvarchar](12) NOT NULL DEFAULT (N''),
	[title] [nvarchar](255) NOT NULL DEFAULT (N''),
	[uid] [int] NOT NULL DEFAULT ((0)),
	[status] [int] NOT NULL DEFAULT ((1)),
	[created] [int] NOT NULL DEFAULT ((0)),
	[changed] [int] NOT NULL DEFAULT ((0)),
	[comment] [int] NOT NULL DEFAULT ((0)),
	[promote] [int] NOT NULL DEFAULT ((0)),
	[sticky] [int] NOT NULL DEFAULT ((0)),
	[tnid] [bigint] NOT NULL DEFAULT ((0)),
	[translate] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_node_nid] PRIMARY KEY CLUSTERED 
(
	[nid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_node$vid] UNIQUE NONCLUSTERED 
(
	[vid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_node_access]    Script Date: 12/19/2016 10:31:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_node_access](
	[nid] [bigint] NOT NULL DEFAULT ((0)),
	[gid] [bigint] NOT NULL DEFAULT ((0)),
	[realm] [nvarchar](255) NOT NULL DEFAULT (N''),
	[grant_view] [tinyint] NOT NULL DEFAULT ((0)),
	[grant_update] [tinyint] NOT NULL DEFAULT ((0)),
	[grant_delete] [tinyint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_node_access_nid] PRIMARY KEY CLUSTERED 
(
	[nid] ASC,
	[gid] ASC,
	[realm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_node_comment_statistics]    Script Date: 12/19/2016 10:31:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_node_comment_statistics](
	[nid] [bigint] NOT NULL DEFAULT ((0)),
	[cid] [int] NOT NULL DEFAULT ((0)),
	[last_comment_timestamp] [int] NOT NULL DEFAULT ((0)),
	[last_comment_name] [nvarchar](60) NULL DEFAULT (NULL),
	[last_comment_uid] [int] NOT NULL DEFAULT ((0)),
	[comment_count] [bigint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_node_comment_statistics_nid] PRIMARY KEY CLUSTERED 
(
	[nid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_node_revision]    Script Date: 12/19/2016 10:31:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_node_revision](
	[nid] [bigint] NOT NULL DEFAULT ((0)),
	[vid] [bigint] IDENTITY(13,1) NOT NULL,
	[uid] [int] NOT NULL DEFAULT ((0)),
	[title] [nvarchar](255) NOT NULL DEFAULT (N''),
	[log] [nvarchar](max) NOT NULL,
	[timestamp] [int] NOT NULL DEFAULT ((0)),
	[status] [int] NOT NULL DEFAULT ((1)),
	[comment] [int] NOT NULL DEFAULT ((0)),
	[promote] [int] NOT NULL DEFAULT ((0)),
	[sticky] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_node_revision_vid] PRIMARY KEY CLUSTERED 
(
	[vid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_node_type]    Script Date: 12/19/2016 10:31:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_node_type](
	[type] [nvarchar](32) NOT NULL,
	[name] [nvarchar](255) NOT NULL DEFAULT (N''),
	[base] [nvarchar](255) NOT NULL,
	[module] [nvarchar](255) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[help] [nvarchar](max) NOT NULL,
	[has_title] [tinyint] NOT NULL,
	[title_label] [nvarchar](255) NOT NULL DEFAULT (N''),
	[custom] [smallint] NOT NULL DEFAULT ((0)),
	[modified] [smallint] NOT NULL DEFAULT ((0)),
	[locked] [smallint] NOT NULL DEFAULT ((0)),
	[disabled] [smallint] NOT NULL DEFAULT ((0)),
	[orig_type] [nvarchar](255) NOT NULL DEFAULT (N''),
 CONSTRAINT [PK_pierian_node_type_type] PRIMARY KEY CLUSTERED 
(
	[type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_nodejs_presence]    Script Date: 12/19/2016 10:31:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_nodejs_presence](
	[uid] [int] NOT NULL CONSTRAINT [pierian_nodejs_presence_uid_df]  DEFAULT ((0)),
	[login_time] [int] NOT NULL CONSTRAINT [pierian_nodejs_presence_login_time_df]  DEFAULT ((0)),
	[__pk] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[__unique_uid]  AS (CONVERT([varbinary](16),hashbytes('MD4',coalesce(CONVERT([varbinary](max),[uid]),CONVERT([varbinary](max),[__pk])))))
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_page_manager_handlers]    Script Date: 12/19/2016 10:31:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_page_manager_handlers](
	[did] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NULL DEFAULT (NULL),
	[task] [nvarchar](64) NULL DEFAULT (NULL),
	[subtask] [nvarchar](64) NOT NULL DEFAULT (N''),
	[handler] [nvarchar](64) NULL DEFAULT (NULL),
	[weight] [int] NULL DEFAULT (NULL),
	[conf] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_pierian_page_manager_handlers_did] PRIMARY KEY CLUSTERED 
(
	[did] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_page_manager_handlers$name] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_page_manager_pages]    Script Date: 12/19/2016 10:31:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_page_manager_pages](
	[pid] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NULL DEFAULT (NULL),
	[task] [nvarchar](64) NULL DEFAULT (N'page'),
	[admin_title] [nvarchar](255) NULL DEFAULT (NULL),
	[admin_description] [nvarchar](max) NULL,
	[path] [nvarchar](255) NULL DEFAULT (NULL),
	[access] [nvarchar](max) NOT NULL,
	[menu] [nvarchar](max) NOT NULL,
	[arguments] [nvarchar](max) NOT NULL,
	[conf] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_pierian_page_manager_pages_pid] PRIMARY KEY CLUSTERED 
(
	[pid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_page_manager_pages$name] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_page_manager_weights]    Script Date: 12/19/2016 10:31:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_page_manager_weights](
	[name] [nvarchar](255) NOT NULL DEFAULT (N''),
	[weight] [int] NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_page_manager_weights_name] PRIMARY KEY CLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_period]    Script Date: 12/19/2016 10:31:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_period](
	[period_id] [int] IDENTITY(6,1) NOT NULL,
	[period_name] [nvarchar](100) NULL DEFAULT (NULL),
	[period_code] [nvarchar](100) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_period_period_id] PRIMARY KEY CLUSTERED 
(
	[period_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_queue]    Script Date: 12/19/2016 10:31:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_queue](
	[item_id] [bigint] IDENTITY(40,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL DEFAULT (N''),
	[data] [varbinary](max) NULL,
	[expire] [int] NOT NULL DEFAULT ((0)),
	[created] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_queue_item_id] PRIMARY KEY CLUSTERED 
(
	[item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_rdf_mapping]    Script Date: 12/19/2016 10:31:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_rdf_mapping](
	[type] [nvarchar](128) NOT NULL,
	[bundle] [nvarchar](128) NOT NULL,
	[mapping] [varbinary](max) NULL,
 CONSTRAINT [PK_pierian_rdf_mapping_type] PRIMARY KEY CLUSTERED 
(
	[type] ASC,
	[bundle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_region]    Script Date: 12/19/2016 10:31:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_region](
	[region_id] [int] IDENTITY(1,1) NOT NULL,
	[region_name] [nvarchar](255) NOT NULL,
	[region_description] [nvarchar](max) NULL,
	[region_shortcode] [nvarchar](200) NULL DEFAULT (NULL),
	[group_id] [int] NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_region_region_id] PRIMARY KEY CLUSTERED 
(
	[region_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_registry]    Script Date: 12/19/2016 10:31:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_registry](
	[name] [nvarchar](255) NOT NULL DEFAULT (N''),
	[type] [nvarchar](9) NOT NULL DEFAULT (N''),
	[filename] [nvarchar](255) NOT NULL,
	[module] [nvarchar](255) NOT NULL DEFAULT (N''),
	[weight] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_registry_name] PRIMARY KEY CLUSTERED 
(
	[name] ASC,
	[type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_registry_file]    Script Date: 12/19/2016 10:31:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_registry_file](
	[filename] [nvarchar](255) NOT NULL,
	[hash] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_pierian_registry_file_filename] PRIMARY KEY CLUSTERED 
(
	[filename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_role]    Script Date: 12/19/2016 10:31:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_role](
	[rid] [bigint] IDENTITY(14,1) NOT NULL,
	[name] [nvarchar](64) NOT NULL DEFAULT (N''),
	[weight] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_role_rid] PRIMARY KEY CLUSTERED 
(
	[rid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_role$name] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_role_permission]    Script Date: 12/19/2016 10:31:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_role_permission](
	[rid] [bigint] NOT NULL,
	[permission] [nvarchar](128) NOT NULL DEFAULT (N''),
	[module] [nvarchar](255) NOT NULL DEFAULT (N''),
 CONSTRAINT [PK_pierian_role_permission_rid] PRIMARY KEY CLUSTERED 
(
	[rid] ASC,
	[permission] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_search_dataset]    Script Date: 12/19/2016 10:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_search_dataset](
	[sid] [bigint] NOT NULL DEFAULT ((0)),
	[type] [nvarchar](16) NOT NULL,
	[data] [nvarchar](max) NOT NULL,
	[reindex] [bigint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_search_dataset_sid] PRIMARY KEY CLUSTERED 
(
	[sid] ASC,
	[type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_search_index]    Script Date: 12/19/2016 10:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_search_index](
	[word] [nvarchar](50) NOT NULL DEFAULT (N''),
	[sid] [bigint] NOT NULL DEFAULT ((0)),
	[type] [nvarchar](16) NOT NULL,
	[score] [real] NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_search_index_word] PRIMARY KEY CLUSTERED 
(
	[word] ASC,
	[sid] ASC,
	[type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_search_node_links]    Script Date: 12/19/2016 10:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_search_node_links](
	[sid] [bigint] NOT NULL DEFAULT ((0)),
	[type] [nvarchar](16) NOT NULL DEFAULT (N''),
	[nid] [bigint] NOT NULL DEFAULT ((0)),
	[caption] [nvarchar](max) NULL,
 CONSTRAINT [PK_pierian_search_node_links_sid] PRIMARY KEY CLUSTERED 
(
	[sid] ASC,
	[type] ASC,
	[nid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_search_total]    Script Date: 12/19/2016 10:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_search_total](
	[word] [nvarchar](50) NOT NULL DEFAULT (N''),
	[count] [real] NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_search_total_word] PRIMARY KEY CLUSTERED 
(
	[word] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_semaphore]    Script Date: 12/19/2016 10:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_semaphore](
	[name] [nvarchar](255) NOT NULL DEFAULT (N''),
	[value] [nvarchar](255) NOT NULL DEFAULT (N''),
	[expire] [float] NOT NULL,
 CONSTRAINT [PK_pierian_semaphore_name] PRIMARY KEY CLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_sequences]    Script Date: 12/19/2016 10:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_sequences](
	[value] [bigint] IDENTITY(55,1) NOT NULL,
 CONSTRAINT [PK_pierian_sequences_value] PRIMARY KEY CLUSTERED 
(
	[value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_sessions]    Script Date: 12/19/2016 10:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_sessions](
	[uid] [bigint] NOT NULL,
	[sid] [nvarchar](128) NOT NULL,
	[ssid] [nvarchar](128) NOT NULL DEFAULT (N''),
	[hostname] [nvarchar](128) NOT NULL DEFAULT (N''),
	[timestamp] [int] NOT NULL DEFAULT ((0)),
	[cache] [int] NOT NULL DEFAULT ((0)),
	[session] [varbinary](max) NULL,
 CONSTRAINT [PK_pierian_sessions_sid] PRIMARY KEY CLUSTERED 
(
	[sid] ASC,
	[ssid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_shortcut_set]    Script Date: 12/19/2016 10:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_shortcut_set](
	[set_name] [nvarchar](32) NOT NULL DEFAULT (N''),
	[title] [nvarchar](255) NOT NULL DEFAULT (N''),
 CONSTRAINT [PK_pierian_shortcut_set_set_name] PRIMARY KEY CLUSTERED 
(
	[set_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_shortcut_set_users]    Script Date: 12/19/2016 10:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_shortcut_set_users](
	[uid] [bigint] NOT NULL DEFAULT ((0)),
	[set_name] [nvarchar](32) NOT NULL DEFAULT (N''),
 CONSTRAINT [PK_pierian_shortcut_set_users_uid] PRIMARY KEY CLUSTERED 
(
	[uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_spark_users]    Script Date: 12/19/2016 10:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_spark_users](
	[spark_user_id] [int] IDENTITY(2,1) NOT NULL,
	[spark_user_name] [nvarchar](255) NOT NULL,
	[spark_user_email] [nvarchar](500) NOT NULL,
	[alias] [nvarchar](200) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_spark_users_spark_user_id] PRIMARY KEY CLUSTERED 
(
	[spark_user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_spark_users$pcc_name] UNIQUE NONCLUSTERED 
(
	[spark_user_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_stylizer]    Script Date: 12/19/2016 10:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_stylizer](
	[sid] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NULL DEFAULT (NULL),
	[admin_title] [nvarchar](255) NULL DEFAULT (NULL),
	[admin_description] [nvarchar](max) NULL,
	[settings] [nvarchar](max) NULL,
 CONSTRAINT [PK_pierian_stylizer_sid] PRIMARY KEY CLUSTERED 
(
	[sid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_stylizer$name] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_system]    Script Date: 12/19/2016 10:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_system](
	[filename] [nvarchar](255) NOT NULL DEFAULT (N''),
	[name] [nvarchar](255) NOT NULL DEFAULT (N''),
	[type] [nvarchar](12) NOT NULL DEFAULT (N''),
	[owner] [nvarchar](255) NOT NULL DEFAULT (N''),
	[status] [int] NOT NULL DEFAULT ((0)),
	[bootstrap] [int] NOT NULL DEFAULT ((0)),
	[schema_version] [smallint] NOT NULL DEFAULT ((-1)),
	[weight] [int] NOT NULL DEFAULT ((0)),
	[info] [varbinary](max) NULL,
 CONSTRAINT [PK_pierian_system_filename] PRIMARY KEY CLUSTERED 
(
	[filename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_taxonomy_index]    Script Date: 12/19/2016 10:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_taxonomy_index](
	[nid] [bigint] NOT NULL DEFAULT ((0)),
	[tid] [bigint] NOT NULL DEFAULT ((0)),
	[sticky] [smallint] NULL DEFAULT ((0)),
	[created] [int] NOT NULL DEFAULT ((0))
)

GO
/****** Object:  Table [dbo].[pierian_taxonomy_menu]    Script Date: 12/19/2016 10:31:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_taxonomy_menu](
	[mlid] [bigint] NOT NULL DEFAULT ((0)),
	[tid] [bigint] NOT NULL DEFAULT ((0)),
	[vid] [bigint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_taxonomy_menu_mlid] PRIMARY KEY CLUSTERED 
(
	[mlid] ASC,
	[tid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_taxonomy_term_data]    Script Date: 12/19/2016 10:31:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_taxonomy_term_data](
	[tid] [bigint] IDENTITY(71,1) NOT NULL,
	[vid] [bigint] NOT NULL DEFAULT ((0)),
	[name] [nvarchar](255) NOT NULL DEFAULT (N''),
	[description] [nvarchar](max) NULL,
	[format] [nvarchar](255) NULL DEFAULT (NULL),
	[weight] [int] NOT NULL DEFAULT ((0)),
	[created] [int] NOT NULL DEFAULT ((0)),
	[changed] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_taxonomy_term_data_tid] PRIMARY KEY CLUSTERED 
(
	[tid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_taxonomy_term_hierarchy]    Script Date: 12/19/2016 10:31:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_taxonomy_term_hierarchy](
	[tid] [bigint] NOT NULL DEFAULT ((0)),
	[parent] [bigint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_taxonomy_term_hierarchy_tid] PRIMARY KEY CLUSTERED 
(
	[tid] ASC,
	[parent] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_taxonomy_vocabulary]    Script Date: 12/19/2016 10:31:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_taxonomy_vocabulary](
	[vid] [bigint] IDENTITY(8,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL DEFAULT (N''),
	[machine_name] [nvarchar](255) NOT NULL DEFAULT (N''),
	[description] [nvarchar](max) NULL,
	[hierarchy] [tinyint] NOT NULL DEFAULT ((0)),
	[module] [nvarchar](255) NOT NULL DEFAULT (N''),
	[weight] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_taxonomy_vocabulary_vid] PRIMARY KEY CLUSTERED 
(
	[vid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_taxonomy_vocabulary$machine_name] UNIQUE NONCLUSTERED 
(
	[machine_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_timezone]    Script Date: 12/19/2016 10:31:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_timezone](
	[timezone_id] [int] IDENTITY(3,1) NOT NULL,
	[timezone_name] [nvarchar](100) NULL DEFAULT (NULL),
	[timezone_code] [nvarchar](100) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_timezone_timezone_id] PRIMARY KEY CLUSTERED 
(
	[timezone_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_travel_agent]    Script Date: 12/19/2016 10:31:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_travel_agent](
	[travel_agent_id] [int] IDENTITY(1,1) NOT NULL,
	[travel_agent_name] [nvarchar](255) NOT NULL,
	[community_id] [int] NULL DEFAULT (NULL),
	[travel_agent_description] [nvarchar](max) NULL,
 CONSTRAINT [PK_pierian_travel_agent_travel_agent_id] PRIMARY KEY CLUSTERED 
(
	[travel_agent_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_travel_agent$travel_agent_name] UNIQUE NONCLUSTERED 
(
	[travel_agent_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_travel_agent_corporate]    Script Date: 12/19/2016 10:31:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_travel_agent_corporate](
	[travel_agent_corporate_id] [int] IDENTITY(1,1) NOT NULL,
	[travel_agent_id] [int] NULL DEFAULT (NULL),
	[corporate_id] [int] NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_travel_agent_corporate_travel_agent_corporate_id] PRIMARY KEY CLUSTERED 
(
	[travel_agent_corporate_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_url_alias]    Script Date: 12/19/2016 10:31:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_url_alias](
	[pid] [bigint] IDENTITY(4,1) NOT NULL,
	[source] [nvarchar](255) NOT NULL DEFAULT (N''),
	[alias] [nvarchar](255) NOT NULL DEFAULT (N''),
	[language] [nvarchar](12) NOT NULL DEFAULT (N''),
 CONSTRAINT [PK_pierian_url_alias_pid] PRIMARY KEY CLUSTERED 
(
	[pid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_user_bookmarks]    Script Date: 12/19/2016 10:31:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_user_bookmarks](
	[bookmark_id] [bigint] IDENTITY(1,1) NOT NULL,
	[thread_id] [bigint] NOT NULL,
	[user_id] [bigint] NOT NULL DEFAULT ((0)),
	[created_at] [datetime2](0) NULL DEFAULT (NULL),
	[updated_at] [datetime2](0) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_user_bookmarks_bookmark_id] PRIMARY KEY CLUSTERED 
(
	[bookmark_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_user_filters]    Script Date: 12/19/2016 10:31:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_user_filters](
	[user_filters_id] [bigint] IDENTITY(3,1) NOT NULL,
	[user_id] [bigint] NOT NULL,
	[currency_id] [smallint] NULL DEFAULT (NULL),
	[timezone_id] [smallint] NULL DEFAULT (NULL),
	[period] [smallint] NULL DEFAULT (NULL),
	[filters_created_time] [datetime2](0) NULL DEFAULT (NULL),
	[filters_updated_time] [datetime2](0) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_user_filters_user_filters_id] PRIMARY KEY CLUSTERED 
(
	[user_filters_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_user_filters$user_id] UNIQUE NONCLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_user_group]    Script Date: 12/19/2016 10:31:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_user_group](
	[user_group_id] [int] IDENTITY(2,1) NOT NULL,
	[user_id] [bigint] NULL DEFAULT (NULL),
	[group_id] [int] NULL DEFAULT (NULL),
	[default_group] [int] NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_user_group_user_group_id] PRIMARY KEY CLUSTERED 
(
	[user_group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_user_preferences]    Script Date: 12/19/2016 10:31:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_user_preferences](
	[uid] [int] NOT NULL DEFAULT ((0)),
	[value] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_pierian_user_preferences_uid] PRIMARY KEY CLUSTERED 
(
	[uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_user_profile]    Script Date: 12/19/2016 10:31:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_user_profile](
	[user_profile_id] [bigint] IDENTITY(1,1) NOT NULL,
	[user_id] [bigint] NOT NULL,
	[user_full_name] [nvarchar](250) NOT NULL,
	[user_gender] [nvarchar](25) NULL DEFAULT (NULL),
	[user_phone] [nvarchar](50) NULL DEFAULT (NULL),
	[user_dob] [nvarchar](50) NULL DEFAULT (NULL),
	[user_address] [nvarchar](max) NULL,
	[profile_created_time] [datetime2](0) NULL DEFAULT (NULL),
	[profile_updated_time] [datetime2](0) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_user_profile_user_profile_id] PRIMARY KEY CLUSTERED 
(
	[user_profile_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_user_profile$user_id] UNIQUE NONCLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_user_tags]    Script Date: 12/19/2016 10:31:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_user_tags](
	[tag_id] [bigint] IDENTITY(1,1) NOT NULL,
	[thread_id] [bigint] NOT NULL,
	[user_id] [bigint] NOT NULL DEFAULT ((0)),
	[tagged_by] [bigint] NOT NULL DEFAULT ((0)),
	[created_at] [datetime2](0) NULL DEFAULT (NULL),
	[updated_at] [datetime2](0) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_user_tags_tag_id] PRIMARY KEY CLUSTERED 
(
	[tag_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_user_threads]    Script Date: 12/19/2016 10:31:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_user_threads](
	[thread_id] [bigint] IDENTITY(1,1) NOT NULL,
	[user_id] [bigint] NOT NULL DEFAULT ((0)),
	[community_id] [bigint] NOT NULL,
	[tid] [bigint] NOT NULL,
	[root_tid] [bigint] NULL DEFAULT (NULL),
	[block_name_1] [nvarchar](255) NULL DEFAULT (NULL),
	[block_name_2] [nvarchar](255) NULL DEFAULT (NULL),
	[block_name_3] [nvarchar](255) NULL DEFAULT (NULL),
	[dashboard_id] [nvarchar](255) NULL DEFAULT (NULL),
	[last_updated] [int] NULL DEFAULT ((0)),
	[news_feed_id] [int] NULL DEFAULT (NULL),
	[last_commented_by] [bigint] NULL DEFAULT (NULL),
	[last_commented_time] [datetime2](0) NULL DEFAULT (NULL),
	[no_comments] [int] NULL DEFAULT ((0)),
	[no_tags] [int] NULL DEFAULT ((0)),
	[img_path] [nvarchar](512) NULL DEFAULT (NULL),
	[status] [smallint] NOT NULL DEFAULT ((0)),
	[created_at] [datetime2](0) NULL DEFAULT (NULL),
	[updated_at] [datetime2](0) NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_user_threads_thread_id] PRIMARY KEY CLUSTERED 
(
	[thread_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_users]    Script Date: 12/19/2016 10:31:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_users](
	[uid] [bigint] NOT NULL DEFAULT ((0)),
	[name] [nvarchar](60) NOT NULL DEFAULT (N''),
	[pass] [nvarchar](128) NOT NULL DEFAULT (N''),
	[mail] [nvarchar](254) NULL DEFAULT (N''),
	[theme] [nvarchar](255) NOT NULL DEFAULT (N''),
	[signature] [nvarchar](255) NOT NULL DEFAULT (N''),
	[signature_format] [nvarchar](255) NULL DEFAULT (NULL),
	[created] [int] NOT NULL DEFAULT ((0)),
	[access] [int] NOT NULL DEFAULT ((0)),
	[login] [int] NOT NULL DEFAULT ((0)),
	[status] [smallint] NOT NULL DEFAULT ((0)),
	[timezone] [nvarchar](32) NULL DEFAULT (NULL),
	[language] [nvarchar](12) NOT NULL DEFAULT (N''),
	[picture] [int] NOT NULL DEFAULT ((0)),
	[init] [nvarchar](254) NULL DEFAULT (N''),
	[data] [varbinary](max) NULL,
	[alias] [nvarchar](200) NULL DEFAULT (NULL),
	[community_id] [int] NULL DEFAULT (NULL),
	[otp] [int] NULL DEFAULT (NULL),
 CONSTRAINT [PK_pierian_users_uid] PRIMARY KEY CLUSTERED 
(
	[uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_users$name] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_users_roles]    Script Date: 12/19/2016 10:31:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_users_roles](
	[uid] [bigint] NOT NULL DEFAULT ((0)),
	[rid] [bigint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_users_roles_uid] PRIMARY KEY CLUSTERED 
(
	[uid] ASC,
	[rid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_variable]    Script Date: 12/19/2016 10:31:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_variable](
	[name] [nvarchar](128) NOT NULL DEFAULT (N''),
	[value] [varbinary](max) NOT NULL,
 CONSTRAINT [PK_pierian_variable_name] PRIMARY KEY CLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_views_display]    Script Date: 12/19/2016 10:31:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_views_display](
	[vid] [bigint] NOT NULL DEFAULT ((0)),
	[id] [nvarchar](64) NOT NULL DEFAULT (N''),
	[display_title] [nvarchar](64) NOT NULL DEFAULT (N''),
	[display_plugin] [nvarchar](64) NOT NULL DEFAULT (N''),
	[position] [int] NULL DEFAULT ((0)),
	[display_options] [nvarchar](max) NULL,
 CONSTRAINT [PK_pierian_views_display_vid] PRIMARY KEY CLUSTERED 
(
	[vid] ASC,
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_views_view]    Script Date: 12/19/2016 10:31:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_views_view](
	[vid] [bigint] IDENTITY(10,1) NOT NULL,
	[name] [nvarchar](128) NOT NULL DEFAULT (N''),
	[description] [nvarchar](255) NULL DEFAULT (N''),
	[tag] [nvarchar](255) NULL DEFAULT (N''),
	[base_table] [nvarchar](64) NOT NULL DEFAULT (N''),
	[human_name] [nvarchar](255) NULL DEFAULT (N''),
	[core] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_views_view_vid] PRIMARY KEY CLUSTERED 
(
	[vid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [pierian_views_view$name] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_watchdog]    Script Date: 12/19/2016 10:31:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pierian_watchdog](
	[wid] [int] IDENTITY(21661,1) NOT NULL,
	[uid] [int] NOT NULL DEFAULT ((0)),
	[type] [nvarchar](64) NOT NULL DEFAULT (N''),
	[message] [nvarchar](max) NOT NULL,
	[variables] [varbinary](max) NOT NULL,
	[severity] [tinyint] NOT NULL DEFAULT ((0)),
	[link] [nvarchar](255) NULL DEFAULT (N''),
	[location] [nvarchar](max) NOT NULL,
	[referer] [nvarchar](max) NULL,
	[hostname] [nvarchar](128) NOT NULL DEFAULT (N''),
	[timestamp] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_pierian_watchdog_wid] PRIMARY KEY CLUSTERED 
(
	[wid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pierian_wysiwyg]    Script Date: 12/19/2016 10:31:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_wysiwyg](
	[format] [nvarchar](255) NOT NULL,
	[editor] [nvarchar](128) NOT NULL DEFAULT (N''),
	[settings] [nvarchar](max) NULL,
 CONSTRAINT [PK_pierian_wysiwyg_format] PRIMARY KEY CLUSTERED 
(
	[format] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[pierian_wysiwyg_user]    Script Date: 12/19/2016 10:31:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pierian_wysiwyg_user](
	[uid] [bigint] NOT NULL DEFAULT ((0)),
	[format] [nvarchar](255) NULL DEFAULT (NULL),
	[status] [tinyint] NOT NULL DEFAULT ((0))
)

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [uid1_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [uid1_idx] ON [dbo].[pierian_drupalchat_msg]
(
	[uid1] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [uid2_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [uid2_idx] ON [dbo].[pierian_drupalchat_msg]
(
	[uid2] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [session_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [session_idx] ON [dbo].[pierian_drupalchat_users]
(
	[session] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
/****** Object:  Index [timestamp_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [timestamp_idx] ON [dbo].[pierian_drupalchat_users]
(
	[timestamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
/****** Object:  Index [uid_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [uid_idx] ON [dbo].[pierian_drupalchat_users]
(
	[uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [bundle_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [bundle_idx] ON [dbo].[pierian_field_data_field_insight_link]
(
	[bundle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
/****** Object:  Index [deleted_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [deleted_idx] ON [dbo].[pierian_field_data_field_insight_link]
(
	[deleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
/****** Object:  Index [entity_id_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [entity_id_idx] ON [dbo].[pierian_field_data_field_insight_link]
(
	[entity_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [entity_type_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [entity_type_idx] ON [dbo].[pierian_field_data_field_insight_link]
(
	[entity_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [field_insight_link_format_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [field_insight_link_format_idx] ON [dbo].[pierian_field_data_field_insight_link]
(
	[field_insight_link_format] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [language_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [language_idx] ON [dbo].[pierian_field_data_field_insight_link]
(
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
/****** Object:  Index [revision_id_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [revision_id_idx] ON [dbo].[pierian_field_data_field_insight_link]
(
	[revision_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [bundle_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [bundle_idx] ON [dbo].[pierian_field_data_field_red_zone_image]
(
	[bundle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
/****** Object:  Index [deleted_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [deleted_idx] ON [dbo].[pierian_field_data_field_red_zone_image]
(
	[deleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
/****** Object:  Index [entity_id_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [entity_id_idx] ON [dbo].[pierian_field_data_field_red_zone_image]
(
	[entity_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [entity_type_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [entity_type_idx] ON [dbo].[pierian_field_data_field_red_zone_image]
(
	[entity_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
/****** Object:  Index [field_red_zone_image_fid_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [field_red_zone_image_fid_idx] ON [dbo].[pierian_field_data_field_red_zone_image]
(
	[field_red_zone_image_fid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [language_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [language_idx] ON [dbo].[pierian_field_data_field_red_zone_image]
(
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
/****** Object:  Index [revision_id_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [revision_id_idx] ON [dbo].[pierian_field_data_field_red_zone_image]
(
	[revision_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [bundle_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [bundle_idx] ON [dbo].[pierian_field_revision_field_insight_link]
(
	[bundle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
/****** Object:  Index [deleted_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [deleted_idx] ON [dbo].[pierian_field_revision_field_insight_link]
(
	[deleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
/****** Object:  Index [entity_id_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [entity_id_idx] ON [dbo].[pierian_field_revision_field_insight_link]
(
	[entity_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [entity_type_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [entity_type_idx] ON [dbo].[pierian_field_revision_field_insight_link]
(
	[entity_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [field_insight_link_format_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [field_insight_link_format_idx] ON [dbo].[pierian_field_revision_field_insight_link]
(
	[field_insight_link_format] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [language_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [language_idx] ON [dbo].[pierian_field_revision_field_insight_link]
(
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
/****** Object:  Index [revision_id_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [revision_id_idx] ON [dbo].[pierian_field_revision_field_insight_link]
(
	[revision_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [bundle_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [bundle_idx] ON [dbo].[pierian_field_revision_field_red_zone_image]
(
	[bundle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
/****** Object:  Index [deleted_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [deleted_idx] ON [dbo].[pierian_field_revision_field_red_zone_image]
(
	[deleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
/****** Object:  Index [entity_id_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [entity_id_idx] ON [dbo].[pierian_field_revision_field_red_zone_image]
(
	[entity_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [entity_type_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [entity_type_idx] ON [dbo].[pierian_field_revision_field_red_zone_image]
(
	[entity_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
/****** Object:  Index [field_red_zone_image_fid_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [field_red_zone_image_fid_idx] ON [dbo].[pierian_field_revision_field_red_zone_image]
(
	[field_red_zone_image_fid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [language_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [language_idx] ON [dbo].[pierian_field_revision_field_red_zone_image]
(
	[language] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
/****** Object:  Index [revision_id_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [revision_id_idx] ON [dbo].[pierian_field_revision_field_red_zone_image]
(
	[revision_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
/****** Object:  Index [login_time_idx]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE NONCLUSTERED INDEX [login_time_idx] ON [dbo].[pierian_nodejs_presence]
(
	[login_time] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [uid_unique]    Script Date: 12/19/2016 10:31:27 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [uid_unique] ON [dbo].[pierian_nodejs_presence]
(
	[__unique_uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
ALTER TABLE [dbo].[pierian_comments]  WITH NOCHECK ADD  CONSTRAINT [pierian_comments$pierian_comments_ibfk_1] FOREIGN KEY([user_id])
REFERENCES [dbo].[pierian_users] ([uid])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[pierian_comments] CHECK CONSTRAINT [pierian_comments$pierian_comments_ibfk_1]
GO
ALTER TABLE [dbo].[pierian_comments]  WITH NOCHECK ADD  CONSTRAINT [pierian_comments$pierian_comments_ibfk_2] FOREIGN KEY([thread_id])
REFERENCES [dbo].[pierian_user_threads] ([thread_id])
GO
ALTER TABLE [dbo].[pierian_comments] CHECK CONSTRAINT [pierian_comments$pierian_comments_ibfk_2]
GO
ALTER TABLE [dbo].[pierian_corporate]  WITH NOCHECK ADD  CONSTRAINT [pierian_corporate$pierian_corporate_ibfk_1] FOREIGN KEY([community_id])
REFERENCES [dbo].[pierian_community] ([community_id])
GO
ALTER TABLE [dbo].[pierian_corporate] CHECK CONSTRAINT [pierian_corporate$pierian_corporate_ibfk_1]
GO
ALTER TABLE [dbo].[pierian_region]  WITH NOCHECK ADD  CONSTRAINT [pierian_region$pierian_region_ibfk_1] FOREIGN KEY([group_id])
REFERENCES [dbo].[pierian_group] ([group_id])
GO
ALTER TABLE [dbo].[pierian_region] CHECK CONSTRAINT [pierian_region$pierian_region_ibfk_1]
GO
ALTER TABLE [dbo].[pierian_travel_agent]  WITH NOCHECK ADD  CONSTRAINT [pierian_travel_agent$pierian_travel_agent_ibfk_1] FOREIGN KEY([community_id])
REFERENCES [dbo].[pierian_community] ([community_id])
GO
ALTER TABLE [dbo].[pierian_travel_agent] CHECK CONSTRAINT [pierian_travel_agent$pierian_travel_agent_ibfk_1]
GO
ALTER TABLE [dbo].[pierian_travel_agent_corporate]  WITH NOCHECK ADD  CONSTRAINT [pierian_travel_agent_corporate$pierian_travel_agent_corporate_ibfk_1] FOREIGN KEY([travel_agent_id])
REFERENCES [dbo].[pierian_travel_agent] ([travel_agent_id])
GO
ALTER TABLE [dbo].[pierian_travel_agent_corporate] CHECK CONSTRAINT [pierian_travel_agent_corporate$pierian_travel_agent_corporate_ibfk_1]
GO
ALTER TABLE [dbo].[pierian_travel_agent_corporate]  WITH NOCHECK ADD  CONSTRAINT [pierian_travel_agent_corporate$pierian_travel_agent_corporate_ibfk_2] FOREIGN KEY([corporate_id])
REFERENCES [dbo].[pierian_corporate] ([corporate_id])
GO
ALTER TABLE [dbo].[pierian_travel_agent_corporate] CHECK CONSTRAINT [pierian_travel_agent_corporate$pierian_travel_agent_corporate_ibfk_2]
GO
ALTER TABLE [dbo].[pierian_user_bookmarks]  WITH NOCHECK ADD  CONSTRAINT [pierian_user_bookmarks$pierian_user_bookmarks_ibfk_1] FOREIGN KEY([user_id])
REFERENCES [dbo].[pierian_users] ([uid])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[pierian_user_bookmarks] CHECK CONSTRAINT [pierian_user_bookmarks$pierian_user_bookmarks_ibfk_1]
GO
ALTER TABLE [dbo].[pierian_user_bookmarks]  WITH NOCHECK ADD  CONSTRAINT [pierian_user_bookmarks$pierian_user_bookmarks_ibfk_2] FOREIGN KEY([thread_id])
REFERENCES [dbo].[pierian_user_threads] ([thread_id])
GO
ALTER TABLE [dbo].[pierian_user_bookmarks] CHECK CONSTRAINT [pierian_user_bookmarks$pierian_user_bookmarks_ibfk_2]
GO
ALTER TABLE [dbo].[pierian_user_group]  WITH NOCHECK ADD  CONSTRAINT [pierian_user_group$pierian_user_group_ibfk_1] FOREIGN KEY([group_id])
REFERENCES [dbo].[pierian_group] ([group_id])
GO
ALTER TABLE [dbo].[pierian_user_group] CHECK CONSTRAINT [pierian_user_group$pierian_user_group_ibfk_1]
GO
ALTER TABLE [dbo].[pierian_user_group]  WITH NOCHECK ADD  CONSTRAINT [pierian_user_group$pierian_user_group_ibfk_2] FOREIGN KEY([user_id])
REFERENCES [dbo].[pierian_users] ([uid])
GO
ALTER TABLE [dbo].[pierian_user_group] CHECK CONSTRAINT [pierian_user_group$pierian_user_group_ibfk_2]
GO
ALTER TABLE [dbo].[pierian_user_tags]  WITH NOCHECK ADD  CONSTRAINT [pierian_user_tags$pierian_user_tags_ibfk_1] FOREIGN KEY([user_id])
REFERENCES [dbo].[pierian_users] ([uid])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[pierian_user_tags] CHECK CONSTRAINT [pierian_user_tags$pierian_user_tags_ibfk_1]
GO
ALTER TABLE [dbo].[pierian_user_tags]  WITH NOCHECK ADD  CONSTRAINT [pierian_user_tags$pierian_user_tags_ibfk_2] FOREIGN KEY([thread_id])
REFERENCES [dbo].[pierian_user_threads] ([thread_id])
GO
ALTER TABLE [dbo].[pierian_user_tags] CHECK CONSTRAINT [pierian_user_tags$pierian_user_tags_ibfk_2]
GO
ALTER TABLE [dbo].[pierian_user_threads]  WITH NOCHECK ADD  CONSTRAINT [pierian_user_threads$pierian_user_threads_ibfk_1] FOREIGN KEY([user_id])
REFERENCES [dbo].[pierian_users] ([uid])
GO
ALTER TABLE [dbo].[pierian_user_threads] CHECK CONSTRAINT [pierian_user_threads$pierian_user_threads_ibfk_1]
GO
ALTER TABLE [dbo].[pierian_user_threads]  WITH NOCHECK ADD  CONSTRAINT [pierian_user_threads$pierian_user_threads_ibfk_2] FOREIGN KEY([tid])
REFERENCES [dbo].[pierian_taxonomy_term_data] ([tid])
GO
ALTER TABLE [dbo].[pierian_user_threads] CHECK CONSTRAINT [pierian_user_threads$pierian_user_threads_ibfk_2]
GO
ALTER TABLE [dbo].[pierian_user_threads]  WITH NOCHECK ADD  CONSTRAINT [pierian_user_threads$pierian_user_threads_ibfk_3] FOREIGN KEY([last_commented_by])
REFERENCES [dbo].[pierian_users] ([uid])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[pierian_user_threads] CHECK CONSTRAINT [pierian_user_threads$pierian_user_threads_ibfk_3]
GO
ALTER TABLE [dbo].[pierian_user_threads]  WITH NOCHECK ADD  CONSTRAINT [pierian_user_threads$pierian_user_threads_ibfk_4] FOREIGN KEY([root_tid])
REFERENCES [dbo].[pierian_taxonomy_term_data] ([tid])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[pierian_user_threads] CHECK CONSTRAINT [pierian_user_threads$pierian_user_threads_ibfk_4]
GO
ALTER TABLE [dbo].[pierian_drupalchat_msg]  WITH CHECK ADD CHECK  (([timestamp]>=(0)))
GO
ALTER TABLE [dbo].[pierian_drupalchat_users]  WITH CHECK ADD CHECK  (([status]>=(0)))
GO
ALTER TABLE [dbo].[pierian_drupalchat_users]  WITH CHECK ADD CHECK  (([timestamp]>=(0)))
GO
ALTER TABLE [dbo].[pierian_drupalchat_users]  WITH CHECK ADD CHECK  (([uid]>=(0)))
GO
ALTER TABLE [dbo].[pierian_field_data_field_insight_link]  WITH CHECK ADD CHECK  (([delta]>=(0)))
GO
ALTER TABLE [dbo].[pierian_field_data_field_insight_link]  WITH CHECK ADD CHECK  (([entity_id]>=(0)))
GO
ALTER TABLE [dbo].[pierian_field_data_field_insight_link]  WITH CHECK ADD CHECK  (([revision_id]>=(0)))
GO
ALTER TABLE [dbo].[pierian_field_data_field_red_zone_image]  WITH CHECK ADD CHECK  (([delta]>=(0)))
GO
ALTER TABLE [dbo].[pierian_field_data_field_red_zone_image]  WITH CHECK ADD CHECK  (([entity_id]>=(0)))
GO
ALTER TABLE [dbo].[pierian_field_data_field_red_zone_image]  WITH CHECK ADD CHECK  (([field_red_zone_image_fid]>=(0)))
GO
ALTER TABLE [dbo].[pierian_field_data_field_red_zone_image]  WITH CHECK ADD CHECK  (([field_red_zone_image_width]>=(0)))
GO
ALTER TABLE [dbo].[pierian_field_data_field_red_zone_image]  WITH CHECK ADD CHECK  (([field_red_zone_image_height]>=(0)))
GO
ALTER TABLE [dbo].[pierian_field_data_field_red_zone_image]  WITH CHECK ADD CHECK  (([revision_id]>=(0)))
GO
ALTER TABLE [dbo].[pierian_field_revision_field_insight_link]  WITH CHECK ADD CHECK  (([delta]>=(0)))
GO
ALTER TABLE [dbo].[pierian_field_revision_field_insight_link]  WITH CHECK ADD CHECK  (([entity_id]>=(0)))
GO
ALTER TABLE [dbo].[pierian_field_revision_field_insight_link]  WITH CHECK ADD CHECK  (([revision_id]>=(0)))
GO
ALTER TABLE [dbo].[pierian_field_revision_field_red_zone_image]  WITH CHECK ADD CHECK  (([delta]>=(0)))
GO
ALTER TABLE [dbo].[pierian_field_revision_field_red_zone_image]  WITH CHECK ADD CHECK  (([entity_id]>=(0)))
GO
ALTER TABLE [dbo].[pierian_field_revision_field_red_zone_image]  WITH CHECK ADD CHECK  (([field_red_zone_image_fid]>=(0)))
GO
ALTER TABLE [dbo].[pierian_field_revision_field_red_zone_image]  WITH CHECK ADD CHECK  (([field_red_zone_image_width]>=(0)))
GO
ALTER TABLE [dbo].[pierian_field_revision_field_red_zone_image]  WITH CHECK ADD CHECK  (([field_red_zone_image_height]>=(0)))
GO
ALTER TABLE [dbo].[pierian_field_revision_field_red_zone_image]  WITH CHECK ADD CHECK  (([revision_id]>=(0)))
GO
ALTER TABLE [dbo].[pierian_nodejs_presence]  WITH CHECK ADD CHECK  (([login_time]>=(0)))
GO
ALTER TABLE [dbo].[pierian_nodejs_presence]  WITH CHECK ADD CHECK  (([uid]>=(0)))
GO
/****** Object:  StoredProcedure [dbo].[SearchAllTables]    Script Date: 12/19/2016 10:31:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SearchAllTables]
(
@SearchStr nvarchar(100)
)
AS
BEGIN

-- Copyright © 2002 Narayana Vyas Kondreddi. All rights reserved.
-- Purpose: To search all columns of all tables for a given search string
-- Written by: Narayana Vyas Kondreddi
-- Site: http://vyaskn.tripod.com
-- Tested on: SQL Server 7.0 and SQL Server 2000
-- Date modified: 28th July 2002 22:50 GMT


CREATE TABLE #Results (ColumnName nvarchar(370), ColumnValue nvarchar(3630))

SET NOCOUNT ON

DECLARE @TableName nvarchar(256), @ColumnName nvarchar(128), @SearchStr2 nvarchar(110)
SET  @TableName = ''
SET @SearchStr2 = QUOTENAME('%' + @SearchStr + '%','''')

WHILE @TableName IS NOT NULL
BEGIN
    SET @ColumnName = ''
    SET @TableName = 
    (
        SELECT MIN(QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME))
        FROM    INFORMATION_SCHEMA.TABLES
        WHERE       TABLE_TYPE = 'BASE TABLE'
            AND QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME) > @TableName
            AND OBJECTPROPERTY(
                    OBJECT_ID(
                        QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME)
                         ), 'IsMSShipped'
                           ) = 0
    )

    WHILE (@TableName IS NOT NULL) AND (@ColumnName IS NOT NULL)
    BEGIN
        SET @ColumnName =
        (
            SELECT MIN(QUOTENAME(COLUMN_NAME))
            FROM    INFORMATION_SCHEMA.COLUMNS
            WHERE       TABLE_SCHEMA    = PARSENAME(@TableName, 2)
                AND TABLE_NAME  = PARSENAME(@TableName, 1)
                AND DATA_TYPE IN ('char', 'varchar', 'nchar', 'nvarchar')
                AND QUOTENAME(COLUMN_NAME) > @ColumnName
        )

        IF @ColumnName IS NOT NULL
        BEGIN
            INSERT INTO #Results
            EXEC
            (
                'SELECT ''' + @TableName + '.' + @ColumnName + ''', LEFT(' + @ColumnName + ', 3630) 
                FROM ' + @TableName + ' (NOLOCK) ' +
                ' WHERE ' + @ColumnName + ' LIKE ' + @SearchStr2
            )
        END
    END 
END

SELECT ColumnName, ColumnValue FROM #Results
 END
GO
USE [master]
GO
ALTER DATABASE [travelpie_mssql_2] SET  READ_WRITE 
GO
