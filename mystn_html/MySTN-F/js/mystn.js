$(document).ready(function(){
	var customSign = 0;
	var screenHeight =  $(window).height();
	var screenWidth =  $(window).width();
	var bodyHeight = $('body').height();
	var headerHeight = $('header').height();
	var header2Height = $('.header').height();
	var subheaderHeight = $(".sub-header").height();
	var dashHeight = $('.dash-contents').height();
	var footerHeight = $('footer').height();
	var footer2Height = $('.footer > .inner-container').height();
	var sideMenuWidth = $(".navigation").width();
	var logincontHeight = $('.login-container').height();
	var space = (screenHeight - logincontHeight - headerHeight - footerHeight - 2)/2;
	var space2 = logincontHeight + headerHeight + footerHeight + 20;
	var dashContents = screenWidth - sideMenuWidth;
	var popHeight = dashHeight + header2Height + subheaderHeight + footer2Height + footer2Height;
/* Custom Sign in Drop Down*/
	$(".custom-signin").click(function(){
		if(customSign==0) {
			$(".custom-drop").slideDown("slow");
			$(".custom-arrow").addClass("arrow-down");
			customSign=1;
		} else {
			$(".custom-drop").slideUp("slow");
			$(".custom-arrow").removeClass("arrow-down");
			customSign=0;
		}
    });
/* popup container height */    
	$('.popup-container').css({
		"height": bodyHeight + 'px',
			'top': header2Height - 5 + 'px'
	});

/* Fixes Login Container margin top and bottom based on the screen size */
	if(screenWidth > 991 ) {
		if(screenHeight > space2 ) {
			$(".login-container").css({
				"marginTop": space + 'px',
				"marginBottom": space + 'px',
			});
		}
	}
	
/* width of the Dashboard contents container */
	$(".dash-contents").css({
		"width": (dashContents - 1) + 'px'
	})


	/* Browse Report */
	var slider = 0;
    $('.browse-report-button').click(function(){
		$('.popup-container2').fadeOut();
		messsage = 0;
    	if(slider == 0){
    		$('.popup-container1').fadeIn();
			$('.browse-report-tab ').show();
			
    		slider = 1;
    	} else if(slider == 1) {
    		$('.popup-container1').fadeOut();
			$('.browse-report-tab ').hide();
    		slider = 0;
    	}
        
    });
	/* Messages */
	var messsage = 0;
    $('.message-notification').click(function(){
		$('.popup-container1').fadeOut();
    	slider = 0;
    	if(messsage == 0){
    		$('.popup-container2').fadeIn();
			$('.message-tab ').show();
    		messsage = 1;
    	} else if(messsage == 1) {
    		$('.popup-container2').fadeOut();
			$('.message-tab ').hide();
    		messsage = 0;
    	}
        
    });
    
    $('.message-content').click(function () {
    	$('.message-tab ').hide();
    	$('.message-container').show()
    });
    
 
	/* More Filter */
	var filter = 0;
	$('.more-icon').click(function(){
		
    	if(filter == 0){
    		$('.more-filters ').fadeIn();
    		filter = 1;
    	} else if(filter == 1) {
    		$('.more-filters').fadeOut();
    		filter = 0;
    	}
        
    });
	/* Profile name */
	var profile = 0;
	$('.name-label').click(function(){
		
    	if(profile == 0){
    		$('.profile-tab').fadeIn();
    		profile = 1;
    	} else if(profile == 1) {
    		$('.profile-tab').fadeOut();
    		profile = 0;
    	}
        
    });
	$(window).click(function() {
		$('.profile-tab, .more-filters').fadeOut();
		profile = 0;
		filter = 0;
	});
	
	$('.popup-container1').click(function() {
		$('.popup-container1').fadeOut();
		slider = 0;
	});
	$('.popup-container2, .message-close').click(function() {
		$('.popup-container2').fadeOut();
		$('.message-container').hide();
		messsage = 0;
	});
	$('.browse-report-tab, .message-tab, .name-label, .profile-tab, .message-container, .more-filters').click(function(event){
		event.stopPropagation();
	});
	
	
	
	
	//rotation speed and timer
    var speed = 5000;
    
    var run = setInterval(rotate, speed);
    var slides = $('.slide');
    var container = $('#slides ul');
    var elm = container.find(':first-child').prop("tagName");
    var item_width = container.width();
    var previous = 'prev'; //id of previous button
    var next = 'next'; //id of next button
    slides.width(item_width); //set the slides to the correct pixel width
    container.parent().width(item_width);
    container.width(slides.length * item_width); //set the slides container to the correct total width
    container.find(elm + ':first').before(container.find(elm + ':last'));
    resetSlides();
    
    
    //if user clicked on prev button
    
    $('#buttons a').click(function (e) {
        //slide the item
        
        if (container.is(':animated')) {
            return false;
        }
        if (e.target.id == previous) {
            container.stop().animate({
                'left': 0
            }, 1500, function () {
                container.find(elm + ':first').before(container.find(elm + ':last'));
                resetSlides();
            });
        }
        
        if (e.target.id == next) {
            container.stop().animate({
                'left': item_width * -2
            }, 1500, function () {
                container.find(elm + ':last').after(container.find(elm + ':first'));
                resetSlides();
            });
        }
        
        //cancel the link behavior            
        return false;
        
    });
    
    //if mouse hover, pause the auto rotation, otherwise rotate it    
    container.parent().mouseenter(function () {
        clearInterval(run);
    }).mouseleave(function () {
        run = setInterval(rotate, speed);
    });
    
    
    function resetSlides() {
        //and adjust the container so current is in the frame
        container.css({
            'left': -1 * item_width
        });
    }

});

function rotate() {
    $('#next').click();
}

//This function will be executed when the user scrolls the page.
$(window).scroll(function(e) {
	var subheaderHeight = $(".sub-header").height();
	var dockbarHeight = $('.header').height();
    var headerWidth = $(".navigation").width();
	var footerHeight = $("footer").height();
	var navHeight = $(".navigation").height();
	//var scroller_anchor = $(".scroller_anchor").offset().top;
	var scroller_anchor = dockbarHeight;
	
	//alert(scroller_anchor);
    // Get the position of the location where the scroller starts.
	
    // Check if the user has scrolled and the current position is after the scroller's start location and if its not already fixed at the top 
    if ($(this).scrollTop() >= scroller_anchor && $('.sub-header').css('position') != 'fixed') 
    {    // Change the CSS of the scroller to hilight it and fix it at the top of the screen.
     
    	$('.sub-header').css({
            'position': 'fixed',
            'top': '0'
        });
    	$('.navigation').css({
            'position': 'fixed',
            'top': subheaderHeight + 'px',
            'width': headerWidth + 'px'
        });
		$('.dash-contents').css({
			"marginTop": 82 + 'px'
		});
    	$('.popup-container').css({
    		'top': subheaderHeight - 5 + 'px',
    	});
        // Changing the height of the scroller anchor to that of scroller so that there is no change in the overall height of the page.
       
    } 
    else if ($(this).scrollTop() < scroller_anchor && $('.sub-header').css('position') != 'relative') 
    {    // If the user has scrolled back to the location above the scroller anchor place it back into the content.
        
        // Change the height of the scroller anchor to 0 and now we will be adding the scroller back to the content.
       
        // Change the CSS and put it back to its original position.
        $('.sub-header').css({
            'position': 'relative',
            'top': 'auto'
        });
        $('.navigation').css({
        	'position': 'relative',
            'top': '0'
        });
		$('.dash-contents').css({
			"marginTop": 0 + 'px'
		});
        $('.popup-container').css({
    		'top': dockbarHeight - 5 + 'px',
    	});
        
    }
//	if($(window).scrollTop() + $(window).height() == $(document).height()) {
//	$('#navigation').css({
//        'position': 'fixed',
//        'top':'auto',
//        'bottom': 25 + footerHeight + 'px',
//        'width': headerWidth + 'px'
//    }, 1500);
//} else {
//	$('#navigation').css({
//        'position': 'relative',
//        'top': subheaderHeight + 'px',
//        'bottom':'auto',
//        'width': headerWidth + 'px'
//    }, 1500);
//}
         
});