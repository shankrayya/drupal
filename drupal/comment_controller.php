<?php
define('DRUPAL_ROOT', getcwd());

include_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
//checkAjaxCall();
if(!user_threads_checkLoggedIn()){
	return null;
}
$action ='';
foreach($_REQUEST as $key =>$val){
	${$key}=$val;
}
/*
if(isset($_REQUEST['action'])){
	$action =$_REQUEST['action'];
}
*/
//echo "ACTION $action <br/>";

switch($action){
	
	case 'filterText':
		echo json_encode($response);
		/*
		$response=_phonetic_apply_filter($text);
		echo json_encode($response);
		*/
		break;

		
	case 'user_thread_info':
		
		$thread_id = (isset($_REQUEST['thread_id']))  ? $_REQUEST['thread_id'] : '';
		$notify = (isset($_REQUEST['notify']))  ? $_REQUEST['notify'] : false;
		$response = user_thread_info($thread_id,$notify);
		echo json_encode($response);
		break;
		
	/*case 'newThread':
		list($thread_id,$img_path) = user_thread_create($content);
		echo $thread_id;
		break;
		*/
	case 'addComments':
		$response = user_comments_add($thread_id,$comment,$content);
		echo json_encode($response);
		break;
		
	case 'addBookMarks':
		$response = user_bookmarks_add($thread_id,$content);
		echo json_encode($response);
		break;
	
	case 'getMoreBlocks':
		$allFlag=0;
		if($pageName =='all')
			$allFlag=1;
		if($pageName='sabre-insight'){
			$insight=1;
		}else{
			$insight=0;
		}
		
		$response = web_container_get_block($pageName,$insight,$allFlag,$paginationObj);
		web_load_more($response);
		break;
		
	case 'getUserProfileImage':
		$response=get_user_profile_image();
		echo json_encode($response);
		break;	
	
	case 'getDashboardFilters':
			$dashboardData['dashboard_id']=$dashboard_id;
      //$dashboardData['report_id']=$report_id;
      //list($dashboardArr,$report_info)= browse_reports_get_reportInfo($report_id);
      //$dashboardData = array_merge($dashboardData, $dashboardArr);
			//$result = get_dashboard_response($dashboardData);
			try {
				 list($filtersToSend,$filtersJson,$cubeUpdated) =getDashboardDefaultFilterData($dashboardData,$filterFlagObj,$globalApplyArr,$syncFlag,$report_table);	
				$filterStr='';
				$filterJson =null;
				$response =array('errorCode'=> 0 , 'filterStr'=>$filterStr,'filterJson'=>$filterJson);
				echo json_encode($response);
			} catch(Exception $ex){
				$response = array('errorCode'=>1, 'msg'=>'Something went wrong');
				echo json_encode($response);
			}
			
			
			/*
			if($result->code == 200) {
				list($filterStr,$filterJson) = get_filter_format($result);
				$patchJsonObj = json_decode($filterJson);
				$patchObj['filter']=$patchJsonObj;
				dopatchFilterSync($patchObj,$dashboard_id);
				$filterStr='';
				$response =array('errorCode'=> 0 , 'filterStr'=>$filterStr,'filterJson'=>$filterJson);
				echo json_encode($response);
			} else {
				$response = array('errorCode'=>1, 'msg'=>'Logged out');
				echo json_encode($response);
			}
			*/

			break;
	case  'updateCustomPeriodFilter':
			$msg = updateCustomPeriodFilter($period,$from,$to);
			echo $msg;
			break;
}
exit();