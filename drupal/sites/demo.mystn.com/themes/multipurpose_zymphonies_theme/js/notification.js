
$(document).ready(function(){
	
   
    var time=1000;
       
    //make it a named function
    $(function poll(){
    	
        //this makes the setTimeout a self run function it runs the first time always
        setTimeout(function(){
        	
            $.ajax({
                url:'controller.php', // Url to which the request is send
                type: "GET",             // Type of request to be send, called as method
                data : {action:'checkNewThreads'},
                
                success: function(data)   // A function to be called if request succeeds
                {
               	 	
                	if(data >=0){
                		$('.message-notification span').text(data);
                		getNewNotifications();
                		console.log("Ajax Processed");
						//console.log("DATA "+data);
                	}
                    
                },
        //this is where you call the function again so when ajax complete it will cal itself after the time out you set.
        complete: poll
            });
    //end setTimeout and ajax 
        },time);
         time=5000;
//end poll function
});

    function GetSortOrder(prop) {  
    return function(a, b) {  
        if (a[prop] > b[prop]) {  
            return 1;  
        } else if (a[prop] < b[prop]) {  
            return -1;  
        }  
        return 0;  
    }  
}  
  
    function getNewNotifications(){
    	 $.ajax({
             url:'controller.php', // Url to which the request is send
             type: "GET",             // Type of request to be send, called as method
             data : {action:'getUserNotification'},
             dataType: 'json',
             success: function(data)   // A function to be called if request succeeds
             {
				
             	$(".notify-body " ).html('');
				var totalCnt=0;
				var counter=1;
				//data.sort(GetSortOrder("ncount"));
				var len=data.length;
             	//var popupNotificationData=data.slice(len-5,len);
             	for (i = 0; i < len; i++) { 
					var displayData='';
             		var cnt=0;
             		if(data[i].ncount>0){
             			cnt=data[i].ncount;
             		}
					console.log("data count" +data[i].ncount);
             	
					 displayData="<div class='notify-content'>"+
											"<input class='notifyThreadId' type='hidden' value='"+ data[i].threadId+"'/>"+
											"<input class='tagFlag' type='hidden' value='"+ data[i].tag+"'/>"+
											"<div class='notify-icon '>"+
											"<img src='"+data[i].filePath+" ' alt='icon' />"+
											"</div>"+
											"<div class='notify-subject'>"+
											"<h3>"+data[i].actionBy+"</h3>";
											if(cnt > 0){
							displayData+=	"<span>"+cnt+"</span>";
											}
							displayData+=	"<p>"+data[i].content+"</p>"+
											
											"<h5>"+data[i].time+"</h5>"+
											"</div>"+
											"</div>";
					
     			
					if( i >= len-5){	
						$(".notify-pop" ).prepend( displayData);
						
					}
			
					$(".notification-container").prepend( displayData );
					totalCnt=	totalCnt+cnt;
				}
			   $('.message-notification span').text(totalCnt);
			}
    	 });
    }

    
    $(".notify-body").on("click",".notify-content",function(){
        var ele=$(this).find('.notifyThreadId').val();
       
        console.log("threadid "+ ele);
    	loadPopUpNotificationContainer(ele,$(this) );
		updateSeenNotification(ele);
       
    });
	
	
		
	
    
    /* widget popup container */
    function messaging () {
        $('.widget-messaging').fadeOut("100", function() {
        $('.widget-tagging').fadeIn(100);
      })
        
        $('.widget-tab .comments').removeClass("widget-eng-selected");
        $('.widget-tab .tags').addClass("widget-eng-selected");
    }
   	
    function commenting () {
        $('.widget-tagging').fadeOut("100", function() {
            $('.widget-messaging').fadeIn(100);
        });
        $('.widget-tab .tags').removeClass("widget-eng-selected");
        $('.widget-tab .comments').addClass("widget-eng-selected");
    }
    
    function loadPopUpNotificationContainer(threadId , $this){
        var visibility =$('.widget-popup').is(":visible");
        if(!visibility){
            $("#errorTxt").text('');
            $("#errorTxt").hide();
           // noscrollBody();
            var tagFlag=$this.find('.tagFlag').val();
            console.log("tagFlag "+tagFlag);
            if(tagFlag == 1){
            	messaging();
            }else{
            	commenting();
            }
            
            getUserTags(threadId);
            loadNotificationThread(threadId);
			
			updateSeenNotification(threadId);
            $('.widget-popup').show(100);
        }
        
    }
    
    function updateSeenNotification(threadId){
		 $.ajax({
             url:'controller.php', // Url to which the request is send
             type: "GET",             // Type of request to be send, called as method
             data : {action:'updateSeenNotification', threadId:threadId},
             dataType: 'json',
             success: function(data)   // A function to be called if request succeeds
             {
             	console.log("data " +data);
				
				
				
			 }
    	 });
		
	}
    function loadNotificationThread(thread_id){
    	//console.log( ' call '+thread_id+' '+Drupal.settings.basePath);
    	$("#hdn_thread_id").val(thread_id);

    	
    		$.ajax({
    			type:'get',
    			url:'comment_controller.php',
    			dataType:'json',
    			data:{action:'user_thread_info',thread_id:thread_id, notify:true },
    			success: function(data){
    				console.log("data "+data);
    				//console.log('txt'+$('#last_updated').text()+'|');
    				formatNotificationThreadBlock(data);
    			}
    		});	
    	
    	
    }
 

    function formatNotificationThreadBlock(data){
    	$("#comment-container .widget-inner").html('');
    	$("#superParentName").text(data.superParentName);
    	$('#parent_name').text(data.parent_name);
    	$("#name").text(data.name);
    	$("#last_updated").text(data.last_updated);
    	//$("#report_icon").attr('src',data.feed_image);
    	$("#block_icon").attr('src',data.icon_src);
    	$(".commenting-field main").addClass('message-content');
    	$(".commenting-field main").addClass('widget-textfield');
    	//$("#comment-container .widget-inner").html('');

    	if(typeof(data.comments) !== 'undefined' && data.comments.length >0) {
    		commentsLoad(data.comments);
    		$("#commentsTxt").text('('+data.comments.length +' comments)');
    		$("#commentsTxt").data('comments',data.comments.length);
    	
    	} else {
    		commentsLoad([]);
    		$("#commentsTxt").text('(0 Comment)');
    	}
    	if(typeof(data.tags) !== 'undefined' && data.tags.length > 0 ){
    		$("#tagsTxt").text('('+data.tags.length+' Users Tagged)');
    		$("#tagsTxt").data('tags',data.tags.length);
    	}  else {
    		$("#tagsTxt").text('(0 User Tagged)');
    	}
    	
    	
    	if( data.news_feed_flag == 1 || data.insight_flag==1){
    		var newsfeed="<div class='widget-image pull-left'> <img src='"+data.feed_image+"' /></div><div class='widget-contents pull-left'><h2><a href='"+data.link+"' target='_blank'>"+data.title+ "</a> <br/></h2> '"+ data.content+"'</p></div>";
    		$("#comment-container .widget-inner").html(newsfeed);
    	}else{
    		var html = "<img src='"+data.feed_image+"' style='width: 100%; height: 220px'  />";
    		$("#comment-container .widget-inner").html(html);
    	}

    	
   
    	//var currentElem = localStorage.getItem('footId');
    	console.log("data "+data);
    	
    	//$("#comment-container .widget-inner").html(data.content);

    	/*
    	{"superParentName":"Leaderboard","parent_name":"Air","name":"leaderboard Air Dashboard 1",
    	"last_updated":"0","icon_src":"\/sites\/all\/themes\/multipurpose_zymphonies_theme\/images\/banner_1.png",
    	"feed_image":"","content":"","thread_id":"1",
    	"comments":[{"name":"naveen","comment_text":"ollo first comment","created_at":"2016-11-28 09:43:12"},{"name":"naveen","comment_text":"lets discuss at 12 today",
    	"created_at":"2016-11-28 09:43:28"},{"name":"naveen","comment_text":"how did the discussion go today",
    	"created_at":"2016-11-28 09:43:44"}
    	*/
    }


});





