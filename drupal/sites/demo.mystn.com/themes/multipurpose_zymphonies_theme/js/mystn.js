
$(document).ready(function() {

	var customSign = 0;
	var screenHeight =  $(window).height();
	var screenWidth =  $(window).width();
	var bodyHeight = $('body').height();
	var headerHeight = $('header').height();
	var header2Height = $('.header').height();
	var subheader = $('.sub-header').height();
	var footerHeight = $('footer').height();
	var footer2Height = $('#footer').height();
	var sideMenuWidth = $("aside").width();
	var bodyHeight = $('body').height();
	var logincontHeight = $('.region-content').height();
	var space = (screenHeight - logincontHeight - headerHeight - footer2Height - 4)/2;
	var space2 = logincontHeight + headerHeight + footer2Height + 20;
	var pageWrap = screenHeight - header2Height - footer2Height - subheader - 1;
	var dashContents = screenWidth - sideMenuWidth;
	$("#edit-pass, #edit-name").keypress(function(event) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == 13) {
	        $("#edit-submit").click();
		event.preventDefault();
	    }
	});
    $("#toolbar-home a").attr('href','/all');
	// li search 
	 $('#searchReports').keyup(function(){
        
        var searchText = $(this).val().toLowerCase();
        
        $('.report_menu ul > li').each(function(){
            
            var currentLiText = $(this).text().toLowerCase(),
                showCurrentLi = currentLiText.indexOf(searchText) !== -1;
            
            $(this).toggle(showCurrentLi);
            
        });     
    });
	// li search ends 
	
	/* Swipe Functionality for mobile with screen width less than 768 */
	if (screenWidth < 768){
		$(".header, .sub-header, #content, #footer").on("swiperight",function(){
			var prevPage = $('.nav-menu.selected').prev().find("a").attr("href")
			if (prevPage != undefined ){
				window.location.href = prevPage;
			} //else {
				//alert('No Page Available before this');
			//}
		 });
		 $("body").on("swipeleft",function(){
			var nextPage = $('.nav-menu.selected').next().find("a").attr("href")
			if (nextPage != undefined ){
				window.location.href = nextPage;
			} //else {
				//alert('No Page Available after this');
			//}
		 });
	}
	
	/* Mobile navigations(Left menu) scroll positions*/
	/* Temporary fix */
	$(".page-leaderboard #sidebar-first, .page-settings #sidebar-first").scrollLeft(50);
	$(".page-newsfeed #sidebar-first").scrollLeft(190);
	$(".page-trends #sidebar-first").scrollLeft(330);
	$(".page-sabreinsight #sidebar-first").scrollLeft(470);
	//alert($(".nav-menu.selected").scrollLeft() + 'px');
	
		
	//var offset;
	//offset = $(".nav-menu.selected").offset();
	//alert(offset.left);
	//$("#sidebar-first").scrollLeft(offset.left);
    

	/* Fixes Login Container margin top and bottom based on the screen size */
	if(screenWidth > 991 ) {
		if(screenHeight > space2 ) {
			$(".not-logged-in .region-content").css({
				"marginTop": space + 'px',
				"marginBottom": space + 'px',
			});
		//$(".region-content:has(div.block-user-management)").css({'margin-top': '0px','margin-bottom': '0px'});
		}
	}
	
	/* Report Menu Accordian for mobile devices */
	if(screenWidth < 792 ) {
		var reportAccord = 0;
		$('.report-accord').click(function(){
			$(this).siblings('.report-accord-drop').slideToggle(300);
			$(this).parent('.taxonomy-term').children('p.report-accord').toggleClass('browse-report-up');
		});
	}   
    
	/* Adjusting the margin top of report info to '0' */
	//$(".report_block").parent().parent().parent().css({"marginTop":"0","marginBottom":"0"});

	var toolbarHeight = $("#toolbar").height();
	
	/* height of report info */
	/*var iframeHeight = screenHeight - (header2Height + subheader + footer2Height + 1);
	$(".report_block").parent().parent().parent().parent().css("height", iframeHeight + "px");
	$(".report_block").parent().parent().parent().parent().parent().css("height", iframeHeight + 30 + "px");
	$(".report_block iframe").css("height", iframeHeight + "px");
	*/

	/* width of the Dashboard contents container */
	if(screenWidth > 985 ) {
		//$("#content").css({
		//	"width": (dashContents - 1) + 'px'
		//})
	}
	
	$('#page-wrap').css({
			'top': 0 + 'px',
			'min-height': pageWrap + 'px'
	});
	$('.not-logged-in #page-wrap').css({
			'top': 0 + 'px',
			'min-height': 300 + 'px'
	});
	
	/* popup container height */    
    $('.popup-container').css({
        'height': bodyHeight - header2Height + 5 + 'px',
    });

	/* Disables/Enables scroll bar on adding this function */
    function noscrollBody () {
        $("body").css({
            "overflow": "hidden"
        });
    }
    function scrollBody () {
        $("body").css({
            "overflow": "auto"
        });
    }
	/* To reduce the z-index of header inorder to make the pop show above the shadow of header */
	function headerDown () {
		$(".header").css({
            "z-index": "80"
        });
	}
	function headerUp (){
		$(".header").css({
            "z-index": "99"
        });
	}
	/* Browse Report */
	var slider = 0;
    $('.browse-report-button').click(function(){
		$('.message-tab ').hide();
		$('.popup-container2').hide();
		$('.profile-tab').hide();
		headerUp ();
		messsage = 0;
		profile = 0;
		$('.reports-menu').animate({scrollTop: $('.reports-menu').offset().top},'slow');
    	if(slider == 0){
			//$(".reports-menu").scrollTop(0);
    		 //noscrollBody ();
    		$('.popup-container1 ').show();
			$('.browse-report-tab ').show();
			headerDown ();
    		slider = 1;
    	} else if(slider == 1) {
    		//scrollBody ();
    		$('.popup-container1').hide();
			$('.browse-report-tab ').hide();
			headerUp ();
    		slider = 0;
    	}
        
    });
		
	/* Messages */
	var messsage = 0;
    $('.msg-noti-click').click(function(){ /* ADD 'msg-noti-click' class in the 'message-notification' div in the page.tpl.php for click to work */
		$('.browse-report-tab ').hide();
		$('.popup-container1').hide();
		$('.profile-tab').hide();
		headerUp ();
		slider = 0;
		profile = 0;
    	if(messsage == 0){
    		 //noscrollBody ();
    		$('.popup-container2 ').show();
			$('.message-tab ').show();
			headerDown ();
    		messsage = 1;
    	} else if(messsage == 1) {
    		// scrollBody ();
    		$('.popup-container2').hide();
			$('.message-tab ').hide();
    		messsage = 0;
			headerUp ();
    	}
    });
	
	/* Profile name */
	var profile = 0;
	$('.name-label, .mobile-content.more-icon').click(function(){
		$('.popup-container1, .popup-container2').hide();
		slider = 0;
		messsage = 0;
		headerUp ();
    	if(profile == 0){
    		$('.profile-tab').show();
    		profile = 1;
    	} else if(profile == 1) {
    		$('.profile-tab').hide();
    		profile = 0;
    	}
    });
		
	// /* More Filter */
	// var filter = 0;
	// $('.more-icon').click(function(){
    	// if(filter == 0){
    		// $('.period-filter-desk ').show();
    		// filter = 1;
    	// } else if(filter == 1) {
    		// $('.period-filter-desk').hide();
    		// filter = 0;
    	// } 
    // });
    /* More Filter */
	var filter = 0;
	$('.filter-more-icon').click(function(){
		if(filter == 0){
			$('.period-filter-desk ').show();
			filter = 1;
		} else if(filter == 1) {
			$('.period-filter-desk').hide();
			filter = 0;
		} 
	});
	/* notifications -toggle-notify-btn */
    var showNotify = 0;
    $('.toggle-notify-btn').click(function(){
        if(showNotify == 0){
            $('.toggle-notify-btn').addClass('toggle-notify-btn-select');
            showNotify = 1;
        } else if(showNotify == 1) {
            $('.toggle-notify-btn').removeClass('toggle-notify-btn-select');
            showNotify = 0;
        }
    });
	
	/* Show filter in mobile */
	var mobileFilter = 0;
	$('.mobile-filter-form').css("top", header2Height -2 + "px");
	$('.mobile-filter-menu, .close-filter').click(function(){
		if(mobileFilter == 0){
    		$('.mobile-filter-form').fadeIn();
			noscrollBody();
    		mobileFilter = 1;
    	} else if(mobileFilter == 1) {
    		$('.mobile-filter-form').fadeOut();
			scrollBody();
    		mobileFilter = 0;
    	}
	});
	
	/* Changing the checkbox color */
	var checkColor = 0;
	$('.form-item-remember-me .option').click(function(){
		if(checkColor == 0){
    		$('.form-item-remember-me').addClass('checkbox-select');
    		checkColor = 1;
    	} else if(checkColor == 1) {
    		$('.form-item-remember-me').removeClass('checkbox-select');
    		checkColor = 0;
    	}
	});
	
	/* Report scheduler */
	$('.report-timer').click(function(){
		$('.event-scheduler-box').show();
	});
	$('.add-report, .edit-button').click(function(){
		$('.add-edit-report').slideToggle();
	});
	$('.close-scheduler, .event-scheduler-box').click(function(){
		$('.event-scheduler-box').hide();
	});
	$( "#datepicker, #datepicker2" ).datepicker();
    $('#schedule-selector').on('change', function() {
        $('.not-daily').hide();
        $('#' + $(this).val()).show();
    });
	/* Anchor link animations */

	//$(".browse-report-tab a").on("click",function(){
		//event.preventDefault();
		//$("<div class='loading-overlay' id='#pre-loader'></div>").prependTo(document.body); 
		//event.preventDefault();
	 //});	
	 //$('#pre-loader').css("display","none");
	
	/* Toolbar animations */
	$("body:not(.page-manage)").unbind().on( 'DOMMouseScroll mousewheel', function ( event ) {
	  if( event.originalEvent.detail > 0 || event.originalEvent.wheelDelta < 0 ) { //alternative options for wheelData: wheelDeltaX & wheelDeltaY
		$('#toolbar').slideUp();
	  } else {
		$('#toolbar').slideDown();
	  }
	});

	/* breadcrumb text margin top adjustment */
	var nameLenght = $('#reportbreadname').text().length
	if (nameLenght >= 26 ) {
		$('#reportbreadname').css("marginTop","0");
	}
    /* widget popup container */
    function messaging () {
        $('.widget-messaging').fadeOut("100", function() {
			$('.widget-tagging').fadeIn(100);
		})
        $('.widget-tab .comments').removeClass("widget-eng-selected");
        $('.widget-tab .tags').addClass("widget-eng-selected");
    }
    function commenting () {
        $('.widget-tagging').fadeOut("100", function() {
            $('.widget-messaging').fadeIn(100);
        });
        $('.widget-tab .tags').removeClass("widget-eng-selected");
        $('.widget-tab .comments').addClass("widget-eng-selected");
    }
	
    /*function to load Pop up comment container  */
    function widgetpopTop() {
        var screenTop = $(document).scrollTop();
        $('.widget-tab').css({'marginTop': + (screenTop + 60) + 'px'})
    }
    
    function loadPopUpContainer(thisElem){
        var visibility =$('.widget-popup').is(":visible");
        if(!visibility){
            $("#errorTxt").text('');
            $("#errorTxt").hide();
            //noscrollBody();
            commenting();
            var thread_id =$(thisElem).parent().parent().find(".hdnthr_id").val();
           
            var content ='';
            if(thread_id == '') {
                var content = $(thisElem).parent().parent().find(".block_data").data('json');
            }
            /*
            console.log('while open ' + thread_id);
            console.log((content));
            console.log('while open ends');
           */
            
            if(localStorage){
                var indices = $(thisElem).parent().parent().find(".hdn_ind").data('indices');
                //console.log(indices);
                //console.log($(thisElem).parent().parent());
                var foot_str = '#foot_'+indices;
                //console.log('id ind ');console.log(foot_str);
                localStorage.setItem('footId',foot_str);
                //console.log('id val ');console.log(localStorage.getItem('footId'));
            }
            
            getUserTags(thread_id);
            loadThread(thread_id,content);
            $('.widget-popup').show(100);
        }
        
    }

    /*
    $(".widget-footer > ul > .tags").click(function() {
        //noscrollBody ();
        messaging ();
        var thread_id =$(this).parent().parent().find(".hdnthr_id").val();
        var content ='';
        if(thread_id == '') {
            var elem = $(this).parent().parent().find(".block_data");
            var content =  elem.data('json');
        }
        console.log((content));
        loadThread(thread_id,content);
        $('.widget-popup').show(100);
    });    
    $(".widget-footer > ul > .comments").click(function() {
       // noscrollBody ();
        commenting ();
        var thread_id =$(this).parent().parent().find(".hdnthr_id").val();
        var content ='';
        console.log('thread '+thread_id);
        if(thread_id == '') {
            var elem = $(this).parent().parent().find(".block_data");
            var content =  elem.data('json');
        }
        console.log('content');
        console.log((content));
        loadThread(thread_id,content);
        $('.widget-popup').show(100);
    });
    */
    /*
    $(".widget-footer > ul ").click(function(event) {
        console.log(event);
        return;
    });
    */
    /*$(".widget-footer").on('click',".tags",function(){
        loadPopUpContainer(this);
        messaging();
    });
    $(".widget-footer").on("click",".comments",function(){
        loadPopUpContainer(this);
        commenting();
    });
    $(".widget-footer").on("click", "li.tags", function(){
        alert($(this).text());
    });
    */

    $(".show-widget-pop").on('click',".tags",function(){
        loadPopUpContainer(this);
        messaging();
        widgetpopTop();
    });
    $(".show-widget-pop").on("click",".comments",function(){
        loadPopUpContainer(this);
        commenting();
        widgetpopTop();
    });
    $(document).on("click", ".show-widget-pop > ul > li.comments", function(){
        loadPopUpContainer(this);
        commenting();
        //noscrollBody ();
        widgetpopTop();
        
    });
    $(document).on("click", ".show-widget-pop > ul > li.tags", function(){
        loadPopUpContainer(this);
        messaging();
        //noscrollBody ();
        widgetpopTop();
    });
    
    $(".widget-footer-reports").on("click",".comments",function(){
        commenting();
    });
    $(".widget-footer-reports").on("click",".tags",function(){
        messaging();
    });
    /* comming soon */
    /*
    $(".widget-footer > ul > .bookmark").click(function() {
        bookmarking(this);
    });
    */
    
    /*
    $('.widget-popup, .message-close').click(function() {
        $('.widget-popup').hide(100);
        $('.widget-messaging').hide(100);
        $('.widget-tagging').hide(100);
        //scrollBody ();
        $("#commentsTxt").data('comments',0);
    });
    */
    $('.widget-popup, .message-close, .popup-container_browse_report').click(function() {
        $('.widget-popup').hide(100);
        $('.widget-messaging').hide(100);
        $('.widget-tagging').hide(100);
        $('.popup-container_browse_report').hide(100);
        //scrollBody ();
        $("#commentsTxt_popup").data('comments',0);
        $("#tagsTxt_popup").data('tags',0);
        $("#commentsTxt").data('comments',0);
        $("#tagsTxt").data('tags',0);

        $("#commentsTxt_popup").text('(0 Comment)');
        $("#tagsTxt_popup").text('(0 User Tagged)');

        $("#commentsTxt").text('(0 Comment)');
        $("#tagsTxt").text('(0 User Tagged)');
        var setFields = 0;
        if($("#reportDashPopup").length){
            // reset hdn_thread_id
            $("#hdn_thread_id").val('');
            $(".tagging-body").html('');
            commentsLoad_browseReports([]);
        }

    });
	
	/* Close on clicking anywhere in the window */
	$(window).click(function() {
		$('.profile-tab').hide();
		profile = 0;
	});
	
	// if (screenWidth < 1200 ) {
		// $(window).click(function() {
			// $('.period-filter-desk').hide();
			// filter = 0;
		// });
	// }
	
	$('.popup-container1, .header').click(function() {
		$('.popup-container1').hide();
		headerUp ();
		slider = 0;
	});
	$('.popup-container2, .header').click(function() {
		$('.popup-container2').hide();
		messsage = 0;
	});

	$('.browse-report-tab, .message-tab, .message-container, .name-label, .profile-tab, .more-filters, .more-icon, .widget-container, .browse-report-button, .message-notification, .filter-more-icon, .filter-form, .event-scheduler').click(function(event){
		event.stopPropagation();
	});
	
	
	$('.carousel').carousel({
		  interval: 2000
		}) 
		$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
	
	/* Limiting the texts */
	var reportName = $('#reportbreadname').text();
	if(reportName.length > 10) {
		reportName = reportName.substring(0,10) + '...';
	}
	$('#reportbreadname').text(reportName);
	
	/* Report name in the report dashboard popup */
	var reportpopName = $('.report-name').text();
	if(reportpopName.length > 28) {
		reportpopName = reportpopName.substring(0,28) + '...';
	}
	$('.report-name').text(reportpopName);
	
	/* News Feed widget heading and para */
	if ( screenWidth < 768 ) {
		var widgetHead = $('.widget-contents h2 a').text();
		var widgetPara = $('.widget-contents p').text();
		
		if(widgetHead.length > 75) {
			widgetHead = widgetHead.substring(0,75) + '...';
		}
		$('.widget-contents h2 a').text(widgetHead);
		if(widgetPara.length > 170) {
			widgetPara = widgetPara.substring(0,170) + '...';
		}
		$('.widget-contents p').text(widgetPara);
	} 
	if ( screenWidth < 400 ) {
		var widgetHead = $('.widget-contents h2 a').text();
		var widgetPara = $('.widget-contents p').text();
		
		if(widgetHead.length > 70) {
			widgetHead = widgetHead.substring(0,70) + '...';
		}
		$('.widget-contents h2 a').text(widgetHead);
		if(widgetPara.length > 130) {
			widgetPara = widgetPara.substring(0,130) + '...';
		}
		$('.widget-contents p').text(widgetPara);
	}
	/* report tips starts */
	var speed = 5000;
    
    var run = setInterval(rotate, speed);
    var slides = $('.slide');
    var container = $('#slides ul');
    var elm = container.find(':first-child').prop("tagName");
    var item_width = container.width() < 800 ? 800 : container.width() ;
    var previous = 'prev'; //id of previous button
    var next = 'next'; //id of next button
    slides.width(item_width); //set the slides to the correct pixel width
    container.parent().width(item_width);
    container.width(slides.length * item_width); //set the slides container to the correct total width
    container.find(elm + ':first').before(container.find(elm + ':last'));
    resetSlides();
    
    
    //if user clicked on prev button
    
    $('#buttons a').click(function (e) {
        //slide the item
        
        if (container.is(':animated')) {
            return false;
        }
        if (e.target.id == previous) {
            container.stop().animate({
                'left': 0
            }, 1500, function () {
                container.find(elm + ':first').before(container.find(elm + ':last'));
                resetSlides();
            });
        }
        
        if (e.target.id == next) {
            container.stop().animate({
                'left': item_width * -2
            }, 1500, function () {
                container.find(elm + ':last').after(container.find(elm + ':first'));
                resetSlides();
            });
        }
        
        //cancel the link behavior            
        return false;
        
    });
    
    //if mouse hover, pause the auto rotation, otherwise rotate it    
    container.parent().mouseenter(function () {
        clearInterval(run);
    }).mouseleave(function () {
        run = setInterval(rotate, speed);
    });
    
    
    function resetSlides() {
        //and adjust the container so current is in the frame
        container.css({
            'left': -1 * item_width
        });
    }

  
		/* report tip ends */


     /* Load More process starts */
     $("#loadMore").click(function(e){
            var newsFeedId= '';
            var dashboardId='';
            var thread_id='';
            e.preventDefault();
            $(".no_posts").remove();
            LoadMore();
			$(".loading-widget").show();

     });  
     /* Load More Process Ends */   
     
     /*Load user community logo in user login page*/
	var username='';
	username = $(".sign-in #edit-name").val();
	if(username){
		username=username.trim();
	
	if(username.length !=0){
		$.ajax({
		 url:"controller.php",
		 type: 'GET',
		 data: {action:'getUserCommunityLogo',username: username},

		 success: function(data){
		   
		  if(data.trim()){
		 
			  $("#user-community-logo").attr("src", data);
			  $("#user-community-logo").css({'display':'block'});
			  }else{
			 
			  $("#user-community-logo").css({'display':'none'});
			  }
		 
			}
		});
	}
	}
  	$(".sign-in #edit-name").on('change', function () {
		var username = $(".sign-in #edit-name").val();

		username=username.trim();
  	  if(username.length !=0){
  	     
  	    $.ajax({
  	      url:"controller.php",
  	      type: 'GET',
  	      data: {action:'getUserCommunityLogo',username: username},
  	
  	      success: function(data){
  	    	
  			  	if(data.trim()){
  			  
		  			$("#user-community-logo").attr("src", data);
		  			$("#user-community-logo").css({'display':'block'});
		  		}else{
		  			$("#user-community-logo").css({'display':'none'});
		  		}
  			  
  			}
  	    });
	  }else{
		  $("#user-community-logo").css({'display':'none'});
	  }
  	});
  	
	/* Escape key to close all the pop ups in the website */
	$(document).keyup(function(e) {
		 if (e.keyCode == 27) { // escape key maps to keycode `27`
			$('.popup-container1, .popup-container2, .message-tab, .browse-report-tab, .profile-tab, .more-filters, .widget-popup, .widget-messaging, .widget-tagging, .popup-container_browse_report').hide();
			slider == 0;
			messsage = 0;
			profile = 0;
			filter = 0;
			mobileFilter = 0;
		}
	});
	if (screenWidth <= 1200) {
		$(document).keyup(function(e) {
			 if (e.keyCode == 27) { // escape key maps to keycode `27`
				$('.period-filter-desk').hide();
				filter = 0;
			}
		});
	}

	/* Drag and drop */
	$( "#draggable" ).draggable({ axis: "x", containment: ".dash-contents", scroll: false });
	
	/* --Show Password-- */
	var showPassword = 0;
	//$("#new-password").blur(function(){
	$("#new-password").live('keyup', function () {
		$(".show-password").show();
	});
	function hidePasswordeye () {
		$("#new-password").attr('type','password');
		$(".show-password").css('opacity','0.8');
		showPassword = 0;
	}
	$(".password-setting").click(function(){
		$(".show-password").hide();
		hidePasswordeye ();
	});
	$(".show-password").click(function(){
		if (showPassword == 0){
			$("#new-password").attr('type','text');
			$(".show-password").css('opacity','0.2');
			showPassword = 1;
		} else if (showPassword == 1) {
			hidePasswordeye ();
		}
	});
	
});
//a simple function to click next link
//a timer will call this function, and the rotation will begin

function rotate() {
    $('#next').click();
}
/*var initialScreen = $(window).width();
$(window).on('resize', function() {
	var screenWidth =  $(window).width();
	if(screenWidth < 989 && initialScreen > 989) {
		this.location.href = this.location.href;
	}   
});*/
//This function will be executed when the user scrolls the page.
$(window).scroll(function(e) {
	var subheaderHeight = $(".sub-header").height();
	var dockbarHeight = $('.header').height();
    var headerWidth = $("aside").width();
	var footerHeight = $("footer").height();
	var navHeight = $("aside").height();
	//var scroller_anchor = $(".scroller_anchor").offset().top;
	var scroller_anchor = dockbarHeight;
	var screenWidth =  $(window).width();
	
	//alert(dockbarHeight);
    // Get the position of the location where the scroller starts.
	
    // Check if the user has scrolled and the current position is after the scroller's start location and if its not already fixed at the top 
    if ($(this).scrollTop() >= scroller_anchor && $('.sub-header').css('position') != 'fixed') 
    {    // Change the CSS of the scroller to hilight it and fix it at the top of the screen.
     
    	$('.sub-header').css({
            'position': 'fixed',
            'top': '0',
			'max-width': '1400px'
        });
		$('aside').css({
			'position': 'fixed',
			'top': subheaderHeight + 'px'
		});
		if (screenWidth < 1399){
			$('aside').css({
				'position': 'fixed',
				'top': subheaderHeight + 'px'
			});
		} else if (screenWidth > 1399){
			$('aside').css({
				'position': 'fixed',
				'top': subheaderHeight + 'px',
				'width': headerWidth + 'px'
			});
		}
		$('#page-wrap').css({
			"marginTop": subheaderHeight - 10 + 'px'
		});
		$('.mobile-filter-form').css("top", subheaderHeight + "px");
        // Changing the height of the scroller anchor to that of scroller so that there is no change in the overall height of the page.
       
    } 
    else if ($(this).scrollTop() < scroller_anchor && $('.sub-header').css('position') != 'relative') 
    {    // If the user has scrolled back to the location above the scroller anchor place it back into the content.
        
        // Change the height of the scroller anchor to 0 and now we will be adding the scroller back to the content.
       
        // Change the CSS and put it back to its original position.
        $('.sub-header').css({
            'position': 'relative',
            'top': '0'
        });
        $('aside').css({
        	'position': 'relative',
            'top': '0'
        });
		$('#page-wrap').css({
			"marginTop": 0 + 'px'
		});
		$('.mobile-filter-form').css("top", dockbarHeight - 2 + "px");
        
    } 

	
    // var newsImg = $(".widget-image > img").attr("src");
    // var widgetImageheight = $(".widget-image").height();
   
    // $(".widget-contents").css({
        // "height": "widgetImageheight" + 'px'
    // });

    // if (newsImg == 0) {
        // $(".widget-image").css({ "display": "none"});
        // $(".widget-contents").css({
            // "width": "100%",
        // });
    // }
//	if($(window).scrollTop() + $(window).height() == $(document).height()) {
//	$('#navigation').css({
//        'position': 'fixed',
//        'top':'auto',
//        'bottom': 25 + footerHeight + 'px',
//        'width': headerWidth + 'px'
//    }, 1500);
//} else {
//	$('#navigation').css({
//        'position': 'relative',
//        'top': subheaderHeight + 'px',
//        'bottom':'auto',
//        'width': headerWidth + 'px'
//    }, 1500);
//}
   

    
});

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function isValidPhoneNumber(number) {
  var regex = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
  return regex.test(number);
}

$(document).ready(function() {
  $(".block-user-management td a[href*='/delete/']").on('click', function() {
    return confirm("Confirm to Delete Record !");
  });
  var userGenderValues = [
    {value: 'm', text: 'Male'},
    {value: 'f', text: 'Female'},
    {value: 'o', text: 'Other'}
  ];
  $(".icon-calendar").daterangepicker({
      maxDate: moment(),
      locale: {
          format: 'DD-MM-YYYY'
      },
      opens: "left",
      /*ranges: {
         'Today': [moment(), moment()],
         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
         'This Month': [moment().startOf('month'), moment().endOf('month')],
         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      */
  });
    if($("#cus_from_date").length){
        startDate = $("#cus_from_date").val();
        startDate =convertDateIST(startDate);
    }
    if($("#cus_to_date").length){
        endDate = $("#cus_to_date").val();
        endDate =convertDateIST(endDate);
    }  
  if(startDate !== '') {
    $(".icon-calendar").data('daterangepicker').setStartDate(startDate); 
  } 
  if(endDate !== '') {
    $(".icon-calendar").data('daterangepicker').setEndDate(endDate);
  }
  
  $('.icon-calendar').on('click',function(){
  	if(window.location.href.search('settings') != -1){
  		if($("#cus_from_date").length){
	        startDate = $("#cus_from_date").val();
	        startDate =convertDateIST(startDate);
	    }
	    if($("#cus_to_date").length){
	        endDate = $("#cus_to_date").val();
	        endDate =convertDateIST(endDate);
	    } 
	    if(startDate !='') {
	    	$("input[name='daterangepicker_start']").val(startDate);
			$("input[name='daterangepicker_end']").val(endDate);
	    }
  	}
  		
  });
    
    
    
  if($("#user_dob_datepicker").length !== 0) {
    $("#user_dob_datepicker").datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "-100:-18",
      dateFormat: 'dd-mm-yy',
      defaultDate: '-18y',
      showButtonPanel: true,
      autoClose: false,
      onSelect: function(selectedDate){
        $.post("/update_profile_info.php", {name: 'user_dob', value: selectedDate}, function(result){
          console.log(result);
        });
        toggleMessageStatusElem();
      }
    });
  }
  
  $(".user-dob-edit").on('click', function(){
    $("#user_dob_datepicker").datepicker('show');
  });
  
  if($(".text-editable").length !== 0 ) {
    $(".text-editable").editable({
      url: 'update_profile_info.php',
      send: 'always',
      validate: function (value) {
        if (value === null || value === '') {
          return 'Empty values not allowed';
        }
        var validationType = $(this).attr('data-validate-type');
        if(validationType == 'email') {
          if(!isEmail(value))
            return "Invalid Email Address";
        } else if (validationType == 'phone') {
          /*	
          if(value.length > 10 || value.length < 10)
            return "Phone Number should be of 10 Digits.";
          if(!isValidPhoneNumber(value))
            return "Invalid Phone Number";
          */
          var phone_regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,10}$/im;
          if(!phone_regex.test(value)){
          	return "Invalid Phone Number";
          }  
        } else if(validationType == 'name') {
          if(value.length > 30)
            return "Max 30 characters allowed for Full Name";
        }
      },
      success: function(data) {
        console.log(data);
        toggleMessageStatusElem();
      }
    });
  }
  $(".profile-name > .profile-edit, .profile-number > .profile-edit, .profile-adress > .profile-edit").on('click', function(e) {
    e.stopPropagation();
    $(this).parent().find(".text-editable").editable('toggle');
  });
  
  if($(".select-editable").length !== 0 ) {
    $(".select-editable").editable({
      source: userGenderValues,
      value: $(this).attr('default-selected-value'),
      emptytext: 'Select Gender',
      url: 'update_profile_info.php', send: 'always',
      success: function() {
        toggleMessageStatusElem();
      }
    });
  }
  
  $(".upload-pic").on('click', function() {
    $("#upload_profile_pic_file").click();
  });
  
  $("#upload_profile_pic_file").on('change', function() {
    $(".upload_image_form_submit_btn").click();
  });

  $(".report-dashboard .report-chat").on('click',function(){
    //console.log('click of the comment button');
    // the interaction module in dashboard view pages 
        loadBrowseReportPopup();
  });
});

function toggleMessageStatusElem() {
  var messageElem = $("div.messages.status");
  messageElem.show();
  messageElem.html("Details Updated.");
  setTimeout(function(){
    messageElem.fadeOut("slow", function () {
      messageElem.hide();
    });
  }, 1500);
}

jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();

function make_menu(){
	return;
	var browse_menu = $("#browse_menu");
	var dashboard_base_url = $("#dashboard_url").val();
	var str ='No Dashboards';
	if(typeof browse_menu !== 'undefined'){
		str ="<div><ul>";
		data = $.parseJSON($("#browse_menu").val());
		$.each(data, function(key,valArr) {
		    if(key%7 == 0 && key != 0 )
		    	str +="</ul></div><div><ul>";
		    str +="<li><a href='"+dashboard_base_url+"/"+valArr['oid']+"' >"+valArr['title']+" </a></li>";

		});
		str +="<ul></div>"; 

		$(".reports-menu").html(str);
	}
}

function deleteAllCookies() {
     console.log(document.cookie);
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
         console.log(cookies[i]);
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
   localStorage.clear();
   sessionStorage.clear();
}
$(document).ready(function() {
   var passetHeight = $('.password-setting').height();
	$('.password-setting').click(function(){
		$('.change-password').css({
			"marginTop": - passetHeight + 'px'
		});
		$('.change-password').slideDown(100);
		$(".passwordmessage").html("");
		
	});
	$('input[type="reset"]').click(function(){
    var currentPasswordObj = $("input[name*='current_password']");
    var newPasswordObj = $("input[name*='new_password']");
    var retypNewPasswordObj = $("input[name*='retype_new_password']");
    clearAndHidePasswordContainer(currentPasswordObj, newPasswordObj, retypNewPasswordObj);
		$('.change-password').slideUp(100);
	});
  $("#currency_filter, #timezone_filter, #group_filter").on('change', function() {
    var currentFilterObj = $(this);
    var filterName = currentFilterObj.attr('name');
    var filterid = currentFilterObj.val();
    if(filterid == 0)
      return;
    
    $.ajax({
      url:"/update_profile_settings.php",
      type: 'POST',
      data: {name: filterName, value: filterid},
      beforeSend : function() {
          $.blockUI({
            message: '<h3> Updating Filter.. </h3>',
            css: { border: 'none', 'border-radius' : '5px'}
          });
       }, 
      complete: function () {
        $.unblockUI();
      }
    });
    return false;
  });
  $("#group_filter").on('change', function() {
    var currentFilterObj = $(this);
    var filterName = currentFilterObj.attr('name');
    var filterid = currentFilterObj.val();
    if(filterid == 0)
      return;
    
    $.ajax({
      url:"/update_profile_settings.php",
      type: 'POST',
      data: {name: filterName, value: filterid},
      beforeSend : function() {
          $.blockUI({
            message: '<h3> Updating Filter.. </h3>',
            css: { border: 'none', 'border-radius' : '5px'}
          });
       }, 
      complete: function () {
        $.unblockUI();
        groupSwitchTo = filterid;
        redirectUrl =  Drupal.settings.basePath+'settings.php'
        doJwtCall(groupSwitchTo,redirectUrl);
      }
    });
    return false;
  });

  $(".period-filter").on('click', function () {
    var currentFilterObj = $(this);
    var selectedValue = currentFilterObj.attr('data-id');
    console.log(currentFilterObj.attr('class'));
    var classHaving = currentFilterObj.attr('class');
    var splFlag =0;
    if(classHaving.search('icon-calendar') != -1){
    	splFlag =1;
    }
    if(splFlag == 0){
    	$.ajax({
	      url:"/update_profile_settings.php",
	      type: 'POST',
	      data: {name: 'period', value: selectedValue},
	      beforeSend : function() {
	      		if(splFlag != 1)
	          		$.blockUI({ message: '<h2> Updating Filter.. </h2>' });
	       },
	      success: function (data) {
	        $(".setting-container .period-filter").removeClass('selected-period-filter');
	        currentFilterObj.addClass('selected-period-filter');
	      }, 
	      complete: function () {
	        $.unblockUI();
	      }
	    });	
    }
    
  });
  /*$(".ranges > .applyBtn").on('click',function(){
        var currentFilterObj = $(this);
        var selectedValue = currentFilterObj.attr('data-id');
    
        $.ajax({
          url:"/update_profile_settings.php",
          type: 'POST',
          data: {name: 'period', value: selectedValue},
          beforeSend : function() {
              $.blockUI({ message: '<h2> Updating Filter.. </h2>' });
           },
          success: function (data) {
            console.log(data);
            $(".setting-container .period-filter").removeClass('selected-period-filter');
            currentFilterObj.addClass('selected-period-filter');
          }, 
          complete: function () {
            $.unblockUI();
          }
        });
  });
  */
  $(".nofilter-period-filter").on('click', function () {
   /* var currentFilterObj = $(this);
    $(".nofilter-period-filter").removeClass('selected-period-filter');
    currentFilterObj.addClass('selected-period-filter');*/
  });
  $("#change_password_action").on('click', function() {
    $(this).attr('disabled', 'disabled');
    var passwordErrorElem = $(".passwordmessage");
    var currentPasswordObj = $("input[name*='current_password']");
    var newPasswordObj = $("input[name*='new_password']");
    var retypNewPasswordObj = $("input[name*='retype_new_password']");
    
    if(currentPasswordObj.val().trim() === "" ) {
      passwordErrorElem.html("Current Password Cannot be Empty");
      $(this).removeAttr('disabled');
      return false;
    }
    if(newPasswordObj.val().trim() == "") {
      passwordErrorElem.html('Empty / Invalid Password');
      $(this).removeAttr('disabled');
      return false;
    }
    if(newPasswordObj.val() !== retypNewPasswordObj.val()) {
      passwordErrorElem.html('New Password doesnt match');
      $(this).removeAttr('disabled');
      return false;
    } else {
      
      $.ajax({
        url:"/update_profile_settings.php",
        type: 'POST',
        data: {name: 'user_pass', currentPassword: currentPasswordObj.val(), value: newPasswordObj.val()},
        beforeSend : function() {
            $.blockUI({ message: '<h2> Updating Password.. </h2>' });
        },
        success: function (data) {
          respObj = jQuery.parseJSON(data);
          if(respObj.hasOwnProperty('err')){
            passwordErrorElem.html("");
            $.each(respObj.err, function (i, item) {
              passwordErrorElem.append("* "+ item +"<br>");
            });
            passwordErrorElem.removeClass('success');
            passwordErrorElem.addClass('error');
          } else {
            passwordErrorElem.removeClass('error');
            passwordErrorElem.addClass('success');
            passwordErrorElem.html(respObj.success);
            passwordErrorElem.append("<br>Redirecting to Login Page in 5 seconds!.");
            clearAndHidePasswordContainer(currentPasswordObj, newPasswordObj, retypNewPasswordObj);
            window.location.href = "/user/logout";
          }
        }, 
        complete: function () {
          $.unblockUI();
        }
      });
    }
    $(this).removeAttr('disabled');
  });
});

function clearAndHidePasswordContainer(currentPasswordObj, newPasswordObj, retypNewPasswordObj) {
  currentPasswordObj.val("");
  newPasswordObj.val("");
  retypNewPasswordObj.val("");
  $('.change-password').slideUp(100);
}
function removeParameter(url, parameter)
{
  var urlparts= url.split('?');

  if (urlparts.length>=2)
  {
      var urlBase=urlparts.shift(); //get first part, and remove from array
      var queryString=urlparts.join("?"); //join it back up

      var prefix = encodeURIComponent(parameter)+'=';
      var pars = queryString.split(/[&;]/g);
      for (var i= pars.length; i-->0;)               //reverse iteration as may be destructive
          if (pars[i].lastIndexOf(prefix, 0)!==-1)   //idiom for string.startsWith
              pars.splice(i, 1);
      url = urlBase+'?'+pars.join('&');
  }
  return url;
}


function getCommunityChatEnabled(){
	
	$.ajax({
		
		url:'controller.php',
		dataType:"json",
		
		data:{action:'getCommunityChatEnabled'},
		type:'get',
		success: function(data){
			
		  if(!data){
				jQuery('.chatboxtextarea').attr("disabled", true);
				$(".errorMsg").text("Chat has been disabled by the admin");
		  			
			}
			
		}
		
	});
	
}

function updateQueryStringParameter(uri, key, value) {
  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  }
  else {
    return uri + separator + key + "=" + value;
  }
}

$(document).on('change','#nofilter_currency_filter',function(){
  if(window.location.href.search('dashboard') != -1) {
        var globalFilterFlag =1;
        var filterFlagObj = {'currencyFlag':1,'dateFlag':0};
        showSection($("#cur_sectionName").val(), true,globalFilterFlag,filterFlagObj);
      /*  
      $.ajax({
          url:'/setDashboardFilterData.php',
          data:{filter_data: {currency: $(this).val()}},
          type:'POST',
          success: function(data){
              //console.log(data);
              console.log('is this happening iframe 2');
              showSection($("#cur_sectionName").val(), true,globalFilterFlag);
          }
      });
      */
  }
});

$(".nofilter-period-filter").on('click',function() {
	 var currentFilterObj = $(this);
    $(".nofilter-period-filter").removeClass('selected-period-filter');
    currentFilterObj.addClass('selected-period-filter');
     var classHaving = currentFilterObj.attr('class');
    var splFlag =0;
    if(classHaving.search('icon-calendar') != -1){
    	splFlag =1;
    }
  if(window.location.href.search('dashboard') != -1 && splFlag == 0) {
        var globalFilterFlag =1;
        var filterFlagObj = {'currencyFlag':0,'dateFlag':1};
        showSection($("#cur_sectionName").val(), true,globalFilterFlag,filterFlagObj);
      /*  
      $.ajax({
          url:'/setDashboardFilterData.php',
          data:{filter_data: {timerange: $(this).attr('data-id')}},
          type:'POST',
          success: function(data){
              //showSection($("#cur_sectionName").val(), true,globalFilterFlag);
          }
      });
      */
  }
});


$('.icon-calendar').on('apply.daterangepicker', function(ev, picker) {
	
    console.log('trigger saving dashboard filters');
    if(window.location.href.search('dashboard') != -1) {
      var globalFilterFlag =1;
      if($("#cur_sectionName").length)
      	var filterFlagObj = {'currencyFlag':0,'dateFlag':1};
	    $("#picker_from_date").val(picker.startDate.format('YYYY-MM-DD'));
        $("#picker_to_date").val(picker.endDate.format('YYYY-MM-DD'))
        showSection($("#cur_sectionName").val(), true,globalFilterFlag,filterFlagObj);
    }  else { 
	    if(window.location.href.search('settings') != -1) {
	      var currentFilterObj = $(this);
	      //$(".full_loader").show(10);
	      $.blockUI({ message: '<h2> Updating Filter.. </h2>' });
	      $(".setting-container .period-filter").removeClass('selected-period-filter');
	      currentFilterObj.addClass('selected-period-filter');
 		  var selectedValue = currentFilterObj.attr('data-id');
	      $.ajax({
	          url:"/comment_controller.php",
	          type:'POST',
	          data:{'action': 'updateCustomPeriodFilter', period:selectedValue, from: picker.startDate.format('YYYY-MM-DD'), to: picker.endDate.format('YYYY-MM-DD')},
	          success: function(data){
	          		console.log('ajax call happening');
	          		console.log(data);
	              //$(".full_loader").hide();
	               $.unblockUI();
	          }
	      });	
	    }
    }  
    //}
  });

$(document).on('change','#nofilter_group_filter',function() {
  if($(this).val() == 0)
    return;

  var sisense_url = $("#sisense_url").val();  
  $("#group_switch_logout_content").attr("src", sisense_url+"/api/auth/logout");
  $.ajax({
    url:"/switch_portal_group.php",
    type: 'POST',
    data: {group_id: $(this).val()},
    beforeSend : function() {
        $.blockUI({ message: '<h2> Switching Group.. </h2>',
          css: {border: 'none', padding: '5px', 'background-color': '#000', 'border-radius': '5px', opacity: '.5', color: '#fff'}
        });
    },
    success: function (data) {
      $("#group_switch_logout_content").attr("src", "");
      resultObj = $.parseJSON(data);
      if(!resultObj.hasOwnProperty('err')) {
        redirectUrl = resultObj.success.url;
        window.location.href = redirectUrl;
      }
    }, 
    complete: function () {
      $.unblockUI();
    }
  });
});
function toggleFilter(){
    console.log('toggle logg');
    $(".trillapser-container").trigger('click');
}

function loadBrowseReportPopup(){
    var visibility =$('.popup-container_browse_report').is(":visible");
  
    if(!visibility){
        $("#errorTxt").text('');
        $("#errorTxt").hide();
        $('.popup-container_browse_report').show(100);
        $(".widget-footer-reports .comments").click();
        var parent_name = $("#reportbreadname").html();
        var child_name = $("#child_name").html();
        $("#name_1").html(parent_name);
        $("#name_2").html(child_name);
    }
}
$( window ).resize(function() {
	 function scrollBody () {
        $("body").css({
            "overflow": "auto"
        });
    }
	var screenWidth =  $(window).width();
	if (screenWidth > 1200 && $('.filter-res').css('display') == 'none' ) {
		$('.filter-res').show();
	}
	if (screenWidth <= 1200 && $('.filter-res').css('display') == 'block' ) {
		$('.filter-res').hide();
	}
	if (screenWidth > 860 && $('.mobile-filter-form').css('display') == 'block') {
		$('.mobile-filter-form').hide();
		mobileFilter = 0;
		scrollBody ();
	}
	
	
});
