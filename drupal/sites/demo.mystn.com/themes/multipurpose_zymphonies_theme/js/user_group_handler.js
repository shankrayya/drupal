jQuery(document).ready(function($) {
    $("input[name='roles']").on('click', function() {
        var selectedRoleText = $(this).parent().find('label').html();
        $("input[name='selected_role_text']").val(selectedRoleText);
    });
    
    $("input[name='enabled_group[]']").each(function(i, element) {
      var defaultGroupSelection = $(element).parent().parent().find("input[name='default_group']");
      if(!$(element).prop('checked'))
        defaultGroupSelection.attr('disabled', 'disabled');
    });
    
    $("input[name='enabled_group[]']").click(function() {
        var defaultGroupSelection = $(this).parent().parent().find("input[name='default_group']");
        var isChecked = $(this).prop('checked');
        if(isChecked) {
          defaultGroupSelection.removeAttr('disabled');
        } else {
          defaultGroupSelection.attr('disabled', 'disabled');
          defaultGroupSelection.prop('checked', false);
        }
    });
    
    $("input[name*='report_ids']").on('click', function() {
        var currentElement = $(this);
        var reportId = currentElement.val();
        var parentId = currentElement.closest('tr').attr('data-parent-id');
        var allParentTableRows = $("input[name*='report_ids']");
        if(currentElement.prop('checked')) {
          allParentTableRows.closest("tr[data-parent-id='"+ reportId +"'], tr[data-id='"+ parentId +"']")
          .addClass('selected').find("input[name*='report_ids']").prop('checked', true);
        } else {
          if($("tr[data-parent-id='"+ parentId +"']").find("input[name*='report_ids']:checked").length < 1) {
            $("tr[data-id='"+ parentId +"']").find("input[name*='report_ids']:checked").prop('checked', false);
          }
          allParentTableRows.closest("tr[data-parent-id='"+ reportId +"']").removeClass('selected').find("input[name*='report_ids']").prop('checked', false);
        }
    });
    
    $('tr.report_row').each(function(index, elem) {
        var currentLevel = $(this).attr('data-level');
        $(elem).find('td:nth-child(even)').css('padding-left', (currentLevel*20) +"px");
    });
    
    $("input[name='user_full_name']").on('focusout', function() {
        var userAliasName = $(this).val().replace(/\s/g, '');
        $("input[name='alias_name']").val(userAliasName.toLowerCase() +"@mystn.com");
    });

    // new handler to filter the groups based on the group id selected 
    $("#edit-community-id").on('change',function(){
       var comm_id = $("#edit-community-id").val();
       var show_row = '.row_'+comm_id;
       $(".sticky-enabled tbody tr").hide();
       $(".report_access_table tbody tr").show();
       $(".sticky-enabled "+show_row+"").parent().parent().show();
    });
    $('#edit-community-id').trigger('change');
    // end 
});