
// add user tags from Jquery Comments Plugin
function addUserTag(user_id){
	
	var thread_id = $("#hdn_thread_id").val();
	var dataObj={action:'addUserTag',thread_id:thread_id,'user_id':user_id};
	if(thread_id == '') {
		var content = $("#popup_data").data('json');
		dataObj['content']= content;
	}
	
	var setFields = 0;
	if($("#reportDashPopup").length){
		setFields =1;
	}
	$.ajax({
		type:'post',
		url: Drupal.settings.basePath+'user_tagging_controller.php',
		data: dataObj,
		success: function(data){
			try {
                var response = $.parseJSON(data);
            } catch(ex){
                   var response ={'error':1};
            }

			if(typeof response.error != 'undefined' && response.error == 1){
				$("#errorTxt").text(response.errorMsg);
				$("#errorTxt").show();

			}else{
				
				var tagStr = '';
				var tagField=1;

				if($("#reportDashPopup").length){
					$tagVal= $("#tagsTxt_popup").data('tags');
					$tagVal=$tagVal+1;
					$("#tagsTxt_popup").data('tags',$tagVal);
					tagStr =  getFormatedString($tagVal,tagField);
					$("#tagsTxt_popup").text(tagStr);

				} else {
					$tagVal=$("#tagsTxt").data('tags');
					$tagVal=$tagVal+1;
					$("#tagsTxt").data('tags',$tagVal);
					var tagField =1;
					var tagStr =  getFormatedString($tagVal,tagField);
					$("#tagsTxt").text(tagStr);
				}

				var currentElem = localStorage.getItem('footId');
			if(thread_id =='' && typeof response.thread_id !=='undefined'){
				//$(thisElem).parent().parent().find(".hdnthr_id").val(response.thread_id);
				//$(thisElem).parent().parent().find(".block_data").data('json').thread_id=thread_id;
				
				if(localStorage) {
					var currentElem = localStorage.getItem('footId');
					//console.log($(currentElem));
					//$(currentElem).find(".hdnthr_id").val(response.thread_id);
					//$(currentElem).find(".block_data").data('json').thread_id=thread_id;
				}
					$("#hdn_thread_id").val(response.thread_id);
					//$("#popup_data").data('json').thread_id=response.thread_id;
					
					if(setFields == 0) {

						var block_data = $(currentElem).find('.block_data').data('json');
						// if its a insight or news feed remove current element portal box
						if( (typeof block_data.insight_flag != undefined && block_data.insight_flag ==1) || (typeof block_data.news_feed_flag != undefined && block_data.news_feed_flag ==1 )) {
							$(currentElem).find(".block_data").data('json').thread_id=response.thread_id;
							$(currentElem).find(".hdnthr_id").val(response.thread_id);							
							$(currentElem).find(".tags").text(tagStr);
						} else {
							var html = $(currentElem).closest('.portalBox').html();
							var topElem =$(".contents-dash .content");
							topElem.prepend("<div class='widget-container portalBox'>"+html+'</div>');
							/*console.log(html);
							console.log(html);
							// copy this div and prepend to top
							console.log('current');console.log($(currentElem));
							console.log('top');console.log($(topElem).attr('class'));
							*/
							var newElem = $('.portalBox:first');
							//console.log('new');console.log($(newElem).attr('class'));
							//console.log('response');console.log(response);
							var totalInd = $(".portalBox").length;
							$(newElem).find(".hdn_ind").data('indices',totalInd);
							$(newElem).find(".block_data").data('json').thread_id=response.thread_id;
							$(newElem).find(".hdnthr_id").val(response.thread_id);
							$(newElem).find(".tags").text(tagStr);
							$(newElem).find(".widget-footer").attr('id','foot_'+totalInd);
							if(response.img_path !='') {
								$(newElem).find(".threadBlock").html("<div class='widget-inner'><img src='"+response.img_path+"' alt='' /> </div>");
							}
							var foot_str = '#foot_'+totalInd;
			                localStorage.setItem('footId',foot_str);
						}		
					}
					 
        		
				//$("#hdn_thread_id").val(response.thread_id);
				//$("#popup_data").data('json').thread_id=response.thread_id;
			} else {
				$("#tagsTxt").text(tagStr);
				$(currentElem).find(".tags").text(tagStr);
			}
			//$(currentElem).find(".tags").text(tagStr); 
			//console.log('txt'+$('#last_updated').text()+'|');
			//formatThreadBlock(data);
			}
			
//			scroll(0,0);
//			location.reload();
		}
	});

}

//remove user tag
function removeUserTag(user_id){
	console.log(user_id);
	var thread_id = $("#hdn_thread_id").val();
	$.ajax({
		type:'post',
		url: Drupal.settings.basePath+'user_tagging_controller.php',
		data:{action:'removeUserTag',thread_id:thread_id,'user_id':user_id },
		success: function(data){
			console.log("User removed");
			
		}
	});
}

//get allm user tags
function getUserTags(thread_id){
	
	
	$.ajax({
		type:'post',
		url: Drupal.settings.basePath+'user_tagging_controller.php',
		dataType:"json",
		data:{action:'getUsertags',thread_id:thread_id},
		success: function(data){
			
			$( ".tagging-body" ).html('');
			for (i = 0; i < data.length; i++) { 
				$( ".tagging-body" ).append( "<li><img src="+data[i]['file']+" alt='icon' /><h3 title='"+data[i]['user_full_name']+"'>"+data[i]['display_name']+"</h3></li>" );
				
			}
		
		}
	});
	
};

//get allm user tags
function getAllTaggedUsers(user_id){
	
	console.log('hi tag called');
	var thread_id = $("#hdn_thread_id").val();
	$(".full_loader").show();
	var flag=1;
	$.ajax({
		type:'post',
		url: Drupal.settings.basePath+'user_tagging_controller.php',
		dataType:"json",
		data:{action:'getUsertags',thread_id:thread_id},
		success: function(data){
		
			for (i = 0; i < data.length; i++) { 
				
				if(data[i]['user_id']==user_id){
					
					$(".tag-error-msg").text("User has been already tagged");
					flag=0;
					$(".full_loader").hide();
					break;
				}
			}
			console.log("flag "+flag);
			if(flag!=0){
				
				$(".tag-error-msg").text("");
				  getUserImage(user_id);
				
			}
		}
	});
	
	
};
function getUserImage(user_id){
	//var thread_id = $("#hdn_thread_id").val();
	
	$.ajax({
		type:'post',
		url: Drupal.settings.basePath+'user_tagging_controller.php',
		dataType:"json",
		data:{action:'getUserImage',user_id:user_id},
		success: function(data){
		
			if(data==null){
				console.log('Invalid User');
			}else{
			
				$( ".tagging-body" ).append( '<li><img src='+data['file']+' alt="icon" /><h3 title="'+data['user_full_name']+'">'+data['display_name']+'</h3><input type="hidden"  value="'+data['user_id']+'" /></li>');				
				//<span class="removeUserTag" data-role="remove"></span>
				addUserTag(user_id);
				$(".full_loader").hide();
			}
			
		}
	});
	
};
function getFormatedString(val,tagField){

	tagField = tagField || 0;
	var temp ='';
	if(tagField) {
		temp = (val <= 1) ? 'User Tagged' : 'Users Tagged';	
	} else {
		temp = (val <= 1) ? 'Comment' : 'Comments';	
	}
	
	var str = '(' + val + ' '+ temp+')';

	return str;
}	
	
$("div").on('click', ".removeUserTag",function(){
	  var item=$(this).prev('input').val();
	  var thread_id = $("#hdn_thread_id").val();
	  var currentElem = localStorage.getItem('footId');
	  if(localStorage) {
			var currentElem = localStorage.getItem('footId');
	  }
	   removeUserTag(item);
	   var tagField =1;
	   // reportDashPopup means its in browse report pages
	   if($("#reportDashPopup").length){
			$tagsVal= $("#tagsTxt_popup").data('tags');
			$tagsVal= ($tagsVal-1 == 0) ? 0 : $tagsVal -1;
			$("#tagsTxt_popup").data('tags',$tagsVal);
			tagStr =  getFormatedString($tagsVal,tagField);
			$("#tagsTxt_popup").text(tagStr);

		} else {
			$tagsVal=$("#tagsTxt").data('tags');
			$tagsVal= ($tagsVal-1 == 0) ? 0 : $tagsVal -1;
			$("#tagsTxt").data('tags',$tagsVal);
			var tagField =1;
			var tagStr =  getFormatedString($tagsVal,tagField);
			$("#tagsTxt").text(tagStr);
		}
		 $(currentElem).find(".tags").text(tagStr);
	  $(this).closest('li').remove();
   
	 
	});
