<?php


/**
 * Implements hook_html_head_alter().
 * This will overwrite the default meta character type tag with HTML5 version.
 */

function multipurpose_zymphonies_theme_html_head_alter(&$head_elements) {
  $head_elements['system_meta_content_type']['#attributes'] = array(
    'charset' => 'utf-8'
  );
}

/**
 * Insert themed breadcrumb page navigation at top of the node content.
 */
function multipurpose_zymphonies_theme_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  if (!empty($breadcrumb)) {
    // Use CSS to hide titile .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
    // comment below line to hide current page to breadcrumb
	$breadcrumb[] = drupal_get_title();
    $output .= '<nav class="breadcrumb">' . implode(' » ', $breadcrumb) . '</nav>';
    return $output;
  }
}

/**
 * Override or insert variables into the page template.
 */
function multipurpose_zymphonies_theme_preprocess_page(&$vars) {
  if (isset($vars['main_menu'])) {
    $vars['main_menu'] = theme('links__system_main_menu', array(
      'links' => $vars['main_menu'],
      'attributes' => array(
        'class' => array('links', 'main-menu', 'clearfix'),
      ),
      'heading' => array(
        'text' => t('Main menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      )
    ));
  }
  else {
    $vars['main_menu'] = FALSE;
  }
  if (isset($vars['secondary_menu'])) {
    $vars['secondary_menu'] = theme('links__system_secondary_menu', array(
      'links' => $vars['secondary_menu'],
      'attributes' => array(
        'class' => array('links', 'secondary-menu', 'clearfix'),
      ),
      'heading' => array(
        'text' => t('Secondary menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      )
    ));
  }
  else {
    $vars['secondary_menu'] = FALSE;
  }

  $alias_parts = explode('/', drupal_get_path_alias());
  if (count($alias_parts) && $alias_parts[0] == 'dashboard') {
    $vars['dashboardid']=$alias_parts[1];
    // this is also working to get the hook
    //$vars['theme_hook_suggestions'][] = 'page__mycustomtemplate';
  } 

}

/**
 * Duplicate of theme_menu_local_tasks() but adds clearfix to tabs.
 */
function multipurpose_zymphonies_theme_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="tabs primary clearfix">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs secondary clearfix">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }
  return $output;
}

/**
 * Override or insert variables into the node template.
 */
function multipurpose_zymphonies_theme_preprocess_node(&$variables) {
  $node = $variables['node'];
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }
}

function multipurpose_zymphonies_theme_page_alter($page) {
  // <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
  $viewport = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
    'name' =>  'viewport',
    'content' =>  'width=device-width'
    )
  );
  drupal_add_html_head($viewport, 'viewport');
}
function mytheme_theme($existing, $type, $theme, $path) {
    // Ex 1: the "story" node edit form.
    $items['story_node_form'] = array(
        'render element' => 'form',
        'template' => 'node-edit--story',
        'path' => drupal_get_path('theme', 'mytheme') . '/template/form',
    );

    // Ex 2: a custom form that comes from a custom module's "custom_donate_form()" function.
    $items['custom_donate_form'] = array(
        'render element' => 'form',
        'template' => 'donate',
        'path' => drupal_get_path('theme', 'mytheme') . '/template/form',
    );

    return $items;
}
//drupal_add_css(drupal_get_path('theme', 'multipurpose_zymphonies_theme') . "/css/mystn.css");
//multipurpose_zymphonies_theme_theme();
function multipurpose_zymphonies_theme_theme(){
 
  $items = array();
  $themeName = 'multipurpose_zymphonies_theme';
  
//     $items['newsfeed'] = array(
//         'render element' => 'view',
//         'path' => drupal_get_path('theme', $themeName) . '/templates',
//         'template' => '',
//         'preprocess functions' => array(
//             $themeName.'_preprocess_news_feed'
//         ),
//     );
	 $catArr = array('Air'=>array('Air report'),'Hotel'=>array('Hotel Report','Ground report'));
  $items['links__system_main_menu'] = array( //Override the main menu theme
      'variables' => $catArr,
      'path' => drupal_get_path('theme', $themeName) . '/templates',
      'template' => 'browse_menu', //Note that the template name defined here has no .tpl.php extension. 
        'preprocess functions' => array(
            $themeName.'_preprocess_report_links'
        ),
  );

  
    return $items;
}
function multipurpose_zymphonies_theme_preprocess_user_login(&$variables) {
  
  $variables['tr'] =' looking for you';
  //$variables['form'] = drupal_build_form('user_login', user_login(array(),$form_state)); ## I have to build the user login myself.
}
 
function multipurpose_zymphonies_theme_preprocess_user_register_form(&$variables) {
  $variables['intro_text'] = t('This is my super awesome reg form');
}
function multipurpose_zymphonies_theme_preprocess_user_pass(&$variables) {
  $variables['intro_text'] = t('This is my super awesome request new password form');
}



/**
 * Implements hook_form_alter().
 */
function multipurpose_zymphonies_theme_form_alter(array &$form, array &$form_state = array(), $form_id = NULL) {

	if ($form_id) {
		switch ($form_id) {
			
			case 'user_login_block':
				
        if(!empty($_COOKIE['login_error'])) {
            drupal_set_message($_COOKIE['login_error']);
            setcookie('login_error', '', time() - 3600);
        }
				
				$form['#attributes']['class'][] = 'sign-in';
				$form['#attributes']['class'][] = 'row';
				$form['name']['#attributes']['placeholder'][] = t('Your Email ID');
				$form['name']['#title'] = 'Email ID';
				$form['name']['#prefix']  = "<img src='' id='user-community-logo'>";
				
				$form['name']['#title_display'] = 'invisible';
				$form['name']['#description'] = '';
				$form['pass']['#title_display'] = 'invisible';
				$form['pass']['#description'] = '';
				$form['pass']['#attributes']['placeholder'][] = t('Enter your Password');
				
				$form['customize'] = array(
						'#type' => 'button',
						'#value' => t('Customize'),
						'#button' => array('multipurpose_zymphonies_theme_user_login_customize_submit'),
						'#limit_validation_errors' => array(),
				);
        //array_unshift($form['#validate'], 'create_jwt_token');
				$form['actions']['submit']['#attributes']['class'][] = 'login-button';
				$form['actions']['submit']['#value']="LOGIN";
				

				break;
				
				 
			case 'user_pass':
			
				$form['#attributes']['class'][] = 'sign-in';
				$form['#attributes']['class'][] = 'row';
				$form['name']['#title'] = 'RESET YOUR PASSWORD';
				$form['name']['#attributes']['placeholder'][] = t("Enter your Account's Email ID");
				$form['actions']['submit']['#attributes']['class'][] = 'login-button';
				$form['actions']['submit']['#attributes']['class'][] = 'next-button';
				$form['actions']['submit']['#value'] = 'NEXT';
				$form['#submit'][] = 'custom_user_pass_submit';
				break;
				
			
		}

	}
}

function custom_user_pass_submit($form, &$form_state) {

	$form_state['redirect'] = 'otp/authentication';
	
	
}

function multipurpose_zymphonies_theme_preprocess_dashboard(&$variables){
 $variables['intro_text'] = t('verify and tell'); 
}

function multipurpose_zymphonies_theme_preprocess_report_links(&$variables){

$tree= taxonomy_get_nested_tree(2,10);
$output=output_taxonomy_nested_tree($tree);
$tipArr =array();
$node_type= 'report_tips_carousal';
 $nids = db_query("SELECT nid,title FROM {node} WHERE type = :type", array(':type' => $node_type))
   ->fetchCol();
   foreach($nids as $ky =>$nid){
      $node = node_load($nid);
      $tips = field_get_items('node', $node, 'field_report_tip');
      $tipOutput = field_view_value('node', $node, 'field_report_tip', $tips[0]); 
      if($tipOutput['#access'] == 1)
        $tipArr[]=$tipOutput['#markup'];
     
   }
  $variables['report_links']=$output;
  $variables['report_tips']=$tipArr;
}




function multipurpose_zymphonies_theme__system_main_menu(array $variables) {
  echo "Is this theme func called ";
       $html = "  Naveen \n"; 
  
  foreach ($variables['links'] as $link) {
    $html .= "".l($link['title'], $link['path'], $link)."\n";
  }

  $html .= "  \n";

  
  return $html;
}


