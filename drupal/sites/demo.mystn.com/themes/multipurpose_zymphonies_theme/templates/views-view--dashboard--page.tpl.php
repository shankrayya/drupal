<?php
global $user;

$reportAccessDetails = getAllUserReportAccessDetails($user->uid);
$link = $_SERVER['REQUEST_URI'];
$link_array = explode('/',$link);
//$dashboard_taxonomy_id = end($link_array);
$dashboard_term = str_replace('/','',variable_get('drupal_dashboard_base_url'));
$dIndex = array_search($dashboard_term,$link_array);
if(isset($link_array[$dIndex+1])){
	$dashboard_taxonomy_id =$link_array[$dIndex+1];
	$dashArr = strtok($dashboard_taxonomy_id, '?');
	$dashboard_taxonomy_id =$dashArr;
}
if(!user_access('custom_admin_role') && (!isset($reportAccessDetails[$dashboard_taxonomy_id]) || !$reportAccessDetails[$dashboard_taxonomy_id])) {
  //exit('Access Denied');
	drupal_goto('/access-denied');
}
  
if($dashboard_taxonomy_id !='') {
	$allData =1; // get all content content
	list($dashboardArr,$report_info)= browse_reports_get_reportInfo($dashboard_taxonomy_id,$allData);
	$dashboard_id= isset($dashboardArr['dashboard_id']) ? $dashboardArr['dashboard_id'] : '';
	$name = isset($dashboardArr['name']) ? $dashboardArr['name'] : '';
	
}
$superName = isset($dashboardArr['superName']) ? $dashboardArr['superName'] : '';
$dashboardArr['report_id'] = $dashboard_taxonomy_id;
module_load_include('inc', 'custom_login');
list($filtersToSend,$filtersJson,$cubeUpdated) = @getDashboardDefaultFilterData($dashboardArr);
if(isset($dashboard_taxonomy_id) && $dashboard_taxonomy_id !== NULL && ctype_digit($dashboard_taxonomy_id)) {
  insertOrUpdateReportViewTime($dashboard_taxonomy_id);
  $_SESSION['last_viewed_report_id'] = $dashboard_taxonomy_id;
}
//$filtersToSend ='';

//echo "|$filtersToSend| <br/>"; exit();
drupal_add_js("/misc/ui/jquery.ui.core.min.js?v=1.8.7", array('scope' => 'footer'));
drupal_add_js("/misc/ui/jquery.ui.datepicker.min.js?v=1.8.7", array('scope' => 'footer'));
?>
<?php 
	$dashboard_id = ($dashboardArr['dashboard_id'] != '') ?  $dashboardArr['dashboard_id'] : '';
	$report_table_dashboard_id = ($dashboardArr['report_table_dashboard_id'] != '') ? $dashboardArr['report_table_dashboard_id'] : '';

	$dashInfo = array('dashboard_id'=>$dashboard_id,'report_table_dashboard_id'=>$report_table_dashboard_id, 'report_id' => $dashboard_taxonomy_id);
	if($dashboard_id != '') { 
		$png_url = variable_get('api_base_url')."dashboards/".$dashboard_id."/export/png"; 
		$embed_base_url = variable_get('embed_base_url');
?>
<div class="full-widh report_block">
    <div class="report-header">
        <div class="inner-container">
		<input id="reportname" type=hidden value=<?php echo $superName?> />
            <h2 id='child_name'><?php echo $name; ?></h2>
			<h6>|</h6> 
            <h4 id='buildtime'></h4>
              <!--<a href='#' class='report_scheduler_trigger'>
                <img src="/sites/all/themes/multipurpose_zymphonies_theme/images/icons/ic_clock_gray.png">
              </a>
              <h3 class="bookmark">(Bookmark Report)</h3> -->
			<h3 class="download">
				<a href='<?php echo $png_url ?>' class="report-dashboard-link" target="_blank"><span>(Download)</span></a>
			</h3>
			<!--  // scheduler commented for now
			<h3 class='report-timer'>
				<a class="report-dashboard-link"><span>(Schedule Report)</span></a>
			</h3>
			-->
            <!--
            <h3 class='retry'>
				<a href='<?php $base_url?>/controller.php' class="report-dashboard-link"><span>(Retry)</span></a>
			</h3>
			-->
        </div>
    </div>
</div>
<?php 
	$dashboard_url = $embed_base_url.$dashboard_id."?embed=true";
	if($filtersToSend !=''){
		$dashboard_url .="&filter=".$filtersToSend;
	}
?>
<div class='report_container'>
	<div class='report_trends' style='min-height:410px; width:100%;'>
		<div id='reportTrend_div'>
			<!-- <iframe src="http://google.com" style="width: 100%; height: 600px"
			scrolling="no" marginwidth="0" marginheight="0" frameborder="0" vspace="0" hspace="0">
			</iframe> -->
			 <iframe src="<?php echo $dashboard_url;?>" style="width: 100%; height: 600px"
			scrolling="no" marginwidth="0" marginheight="0" frameborder="0" vspace="0" hspace="0">
			</iframe>
		</div>
	</div>
	<div class='report_info' style='display:none'>
		<?php echo $report_info; ?>
	</div>
	<div class='report_table' style='min-height:410px; width:100%;display:none'>
		<div id='reportTbl_div'>
			Report Table Loading...
		</div>	
	</div>
</div>
<div class="report-dashboard">
	<div id="draggable" class="ui-widget-content report-chat" title="Drag or Click">
	</div>
</div>
<?php 
} // end dashboard Embed 
else {
	echo " <h3> &emsp; No Dashboard Found. </h3>";
}
$jsonDashInfo = json_encode($dashInfo);
echo "<input type='hidden' id='hdn_dashboard_data' value='".$jsonDashInfo."' />";
// by default trends
echo "<input type='hidden' id='cur_sectionName' value='report_trends' />";
echo "<input type='hidden' id='reportDashPopup' value='1' />";
echo "<input type='hidden' id='fSend' value='".$filtersJson."' />";
echo "<input type='hidden' id='lastbuildtime' value='".$cubeUpdated."' />";

?>
<div style='display:none;' class='report_data' data-json="<?php echo htmlentities(json_encode($dashboardArr)); ?>" /></div>

<script>
// Remove URL Tag Parameter from Address Bar
if (window.parent.location.href.match(/jwt=/)){
    if (typeof (history.pushState) != "undefined") {
        var obj = { Title: document.title, Url: window.parent.location.pathname };
        history.pushState(obj, obj.Title, obj.Url);
    } else {
        window.parent.location = window.parent.location.pathname;
    }
}

jQuery(document).ready(function(){
	var content = $(".report_data").data('json')
	$("#popup_data").data('json',content);
	
	//document.title =document.getElementById("reporttitle").innerHTML;
	//document.getElementById("reportbreadname").innerHTML =document.getElementById("reportname").value;

	// if there is no Report Related table disable View Report and add tooltip
	var dashData = $("#hdn_dashboard_data").val();
	var dashJson = $.parseJSON(dashData);
	if(dashJson.report_table_dashboard_id == ''){
		$(".report_table_lnk a").attr('onclick','return false');
		$(".report_table_lnk").addClass('sub_wrapper');
		$(".report_table_lnk").append('<div class="tooltip_spl">View Report Unavailable for this Report</div>');
	}
	// tool tip adding ends 
	var lastbuildtime = $("#lastbuildtime").val();
	// change the build time value
	if(lastbuildtime !='') {
		var buildDate = new Date(lastbuildtime * 1);
		var dateStr = 'Data as of '+ moment(buildDate).format('h:mm A, Do MMMM, YYYY ');
		$("#buildtime").html(dateStr);	
	}
	
});
document.getElementsByClassName('contents-dash')[0].style.width ='100%';


</script>
 