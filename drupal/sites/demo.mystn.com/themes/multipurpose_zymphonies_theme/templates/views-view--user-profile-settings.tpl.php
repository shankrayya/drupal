<?php
error_reporting(0);
module_load_include('inc', 'user_profile');
global $user;

$allGroupDetailsHtml = getAllUserGroupFiltersForUser();
list($allReportPeriodDetailsHtml,$custDateArr) = getAllReportTimePeriodFiltersForUser();
$allCurrencyFitersHtml = getAllCurrencyFiltersForUser();
$allTimeZoneFiltersHtml = getAllReportTimezoneFiltersForUser();
$profileDetails = getAllUserProfileInfoForUser($user->uid);

$pwdChange = getPwdChange($profileDetails);
if($pwdChange !='') {
   $pwdChange = 'Last changed '.$pwdChange;
}

unset($_SESSION['messages']['error']);
?>
<style type="text/css">
.region-content{
  margin-top: 0 !important;
}
.transparent-bg {
  background: transparent !important;
  border: 0 !important;
  color: #337ab7;
}
.profile-details h3 form {
  text-indent: 0 !important;
}
</style>
<?php
  drupal_add_js("$base_url/misc/ui/jquery.ui.core.min.js?v=1.8.7", array('scope' => 'footer'));
  drupal_add_js("$base_url/misc/ui/jquery.ui.datepicker.min.js?v=1.8.7", array('scope' => 'footer'));
  //drupal_add_css("$base_url/sites/all/themes/multipurpose_zymphonies_theme/css/bootstrap-editable.css");
  drupal_add_js("$base_url/sites/all/themes/multipurpose_zymphonies_theme/js/bootstrap-editable.js", array('scope' => 'footer'));
?>
<div class="full-widh">
    <div class="settings-content">
      <div class="inner-container">
        <h1>Default Global Filter Values</h1>
        <div class="setting-container">
          <div class="filter-form">
              <ul>
                <li>
                  <label for="group">GROUP</label>
                  <div class="select-container group-filter">
                      <?php echo $allGroupDetailsHtml; ?>
                  </div>
                </li>
                <li>
                  <label for="currency">CURRENCY</label>
                  <div class="select-container">
                      <?php echo $allCurrencyFitersHtml; ?>
                  </div>
                </li>
                <li>
                  <label for="currency">PERIOD</label>
                    <?php echo $allReportPeriodDetailsHtml; ?>
                </li>
              </ul> 
          </div>
      </div>
    </div>
    <div class="inner-container">
      <h1>Default Time Zone</h1>
      <div class="setting-container time-zone filter-form">
        <div class="timezone_filter_content">
          <label for="group">TIMEZONE</label>
          <ul><li>
            <div class="select-container time-zone-filter">
                <?php echo $allTimeZoneFiltersHtml; ?>
            </div>
          </li></ul>
        </div>
      </div>
    </div>
    <?php if(user_access('reset_own_password')) { ?>
    <div class="inner-container">
       <h1>Password</h1>
      <div class="setting-container password-setting">
          <h2>&#9899;&nbsp;&#9899;&nbsp;&#9899;&nbsp;&#9899;&nbsp;&#9899;&nbsp;&#9899;&nbsp;&#9899;</h2>
		  <h3><?php echo $pwdChange; ?></h3>
          <span class="profile-edit"></span>
      </div>
      <div class="change-password">
        <input type="password" autocomplete="off" name="current_password" placeholder="Current Password" />
        <input type="password" autocomplete="off" name="new_password" placeholder="New Password" id="new-password"/>
		<div class="show-password"></div>
        <input type="password" autocomplete="off" name="retype_new_password" placeholder="Re-Type New Password" />
        <input type="submit" id="change_password_action" value="SAVE CHANGES">
        <input type="reset" value="CANCEL">
        <span class="passwordmessage error"></span> <br>
        <span id="password_rules_hint">(1.) 6 - 20 Characters.
        <br>(2.) Must Contain 1 Uppercase Character.
        <br>(3.) Must Contain 1 Lower Case Character.
        <br>(4.) Must Contain 1 Special Character.</span>
        
      </div>
    </div>
    <?php } ?>
  </div>
</div>