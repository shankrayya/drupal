<!DOCTYPE html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php print $head; ?>
	<title><?php print $head_title; ?></title>
	<?php print $styles; ?>
	<link rel="stylesheet" href="/sites/demo.mystn.com/themes/multipurpose_zymphonies_theme/css/mystn.css">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
	<?php print $scripts; ?>
	<!--[if IE 8 ]><html class="ie8 ielt9"> <![endif]-->
	<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<link rel="stylesheet" href="/sites/all/themes/multipurpose_zymphonies_theme/css/bootstrap.min.css">
	<link rel="stylesheet"	href="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/css/mystn-responsive.css">
	<link rel="stylesheet"	href="/sites/all/themes/multipurpose_zymphonies_theme/css/jquery-comments.css">
	<link rel="stylesheet"	href="/sites/all/themes/multipurpose_zymphonies_theme/css/daterangepicker.css">
	<link rel="stylesheet"	href="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/css/bootstrap-tagsinput.css">
	<link rel="stylesheet" href="<?php echo $base_url; ?>/sites/all/themes/multipurpose_zymphonies_theme/css/bootstrap-editable.css">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/jquery-1.12.4.min.js"></script>
	<?php

	if (! ($user->uid)) {
		$logout_url = variable_get('logout_api');
		?>
		<link rel="stylesheet" href="/sites/all/themes/multipurpose_zymphonies_theme/css/login.css">
		<link rel="stylesheet"	href="/sites/all/themes/multipurpose_zymphonies_theme/css/login-responsive.css">
	<?php
	}
	?>

</head>
<body class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>

  <!-- Create a hdn variable to store the browse reports stuff -->
  <?php 
  	$reports = get_user_preferences('browse_reports');
  	echo "<input type='hidden' id='browse_menu' value='".json_encode($reports)."' />";
  	echo "<input type='hidden' id='dashboard_url' value='".variable_get('drupal_dashboard_base_url')."' />";
  ?>
  <script type='text/javascript'>
  	make_menu();
  </script>
</body>
</html>