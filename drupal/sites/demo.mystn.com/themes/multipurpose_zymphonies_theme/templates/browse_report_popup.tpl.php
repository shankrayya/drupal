<!-- popup container for widget messaging and tagging -->
	<?php global $base_url;?>
	<div class="popup-container_browse_report">
		<div class="widget-tab popup-tab" >
			<div class="widget-container widget-report">
				<div class="inner-container">
					<div class="message-close">+</div>
					<div class="widget-head">
					    <div class="widget-heading">
					    <?php
					    $trend_img = $base_url.'/sites/all/themes/multipurpose_zymphonies_theme/images/icons/trends.png';
					    echo "<span style='background-image: url(\"".$trend_img."\");' ></span>";
					    ?>
					    <span class='widget-name'>TRENDS</span>
					    </div>
					    <div class="widget-breadcrumb">
					        <p><span id='name_1'></span></p><div class="bread-icon"></div> <h3 class="report-name" id='name_2'></h3>
					    </div>
					    <div class="widget-time">
					        <!-- <p>Last Updated : <b  ></b></p> -->
					    </div>
					</div>
					<div class="widget-footer widget-footer-reports">
					     <ul>
					    	<li class="comments" id='commentsTxt_popup' data-comments='0'>(0 Comment)</li>
				        	<li class="tags" id='tagsTxt_popup' data-tags='0'>(0 User Tagged)</li>
				        	<li id='errorTxt_popup' style='display:none;'></li>
					    </ul>
					</div>
					<div class="widget-engage">
						<div class="widget-messaging">
							<div class='message-body' id='thread_comment_container' >

							</div>
						</div>
						<div class="widget-tagging">
							<div class="tagging-body">
							</div>
							<div class="message-content widget-textfield">
								<div class="message-icon">
									<img src='/sites/all/themes/multipurpose_zymphonies_theme/images/icons/ic_tag.png' alt="icon" />
								</div>
								<div class="message-type">
									<div class="message-form">
										<input type="text"  data-role="tagsinput"  placeholder="Type User To Tag" id="thread_tag" />  
										<input type="hidden" />  
									</div>
								</div>

							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
  	
<script >

$(document).ready(function(){
	getUserProfileImage();
	commentsLoad_browseReports([]);
//To fetch users from DB
var browse_usernames = new Bloodhound({
	  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('user_full_name'),
	  queryTokenizer: Bloodhound.tokenizers.whitespace,
	  prefetch:  Drupal.settings.basePath+'user_tagging_controller.php?action=get_users'
	});
browse_usernames.initialize();

	var elt = $('#thread_tag');
	elt.tagsinput({
	  itemValue: 'user_id',
	  itemText: 'user_full_name',
	  typeaheadjs: {
	    name: 'usernames',
	    displayKey: 'user_full_name',
	    source: browse_usernames.ttAdapter()
	  }
	});
});
function commentsLoad_browseReports(){
	 $('#thread_comment_container').comments({
        getComments: function(success, error) {
             var commentsArray = []; 
            success(commentsArray);
    	}
	});
}
/*
function toggleEmoji(){
	console.log($('.textarea'));
	$('.textarea').emojiPicker('toggle');
}
*/
</script>	