<?php 
	$dashFlag=0;
?>
<div class="widget-container portalBox">
	<div class="inner-container">
		<div class="widget-head">
		    <div class="widget-heading">
				<span style="background-image: url('<?php echo $contentBlock['icon_src']; ?>');" ></span>
		    	<!-- <img src='<?php //echo $contentBlock['icon_src']; ?>' />-->
		    	
		        <p> <?php 
		        echo $contentBlock['superParentName']; ?></p>
			</div>
		    <div class="widget-breadcrumb">
		        <p><span><?php echo $contentBlock['parent_name']; ?></span> </p>
				<?php if(!isset($contentBlock['news_feed_flag']) && !isset($contentBlock['insight_flag'])){?>
		        	 <?php echo " <div class='bread-icon'></div><h3>".$contentBlock['name']."</h3>" ?>
		        <?php }?>
		        
		    </div>
		    <div class="widget-time">
		        <p>Last Updated : <b><?php echo $contentBlock['last_updated'];?></b></p>
		    </div>
		</div>
		<div class="widget-body threadBlock" >
			<div class="widget-inner">
				<?php 
				if((isset($contentBlock['news_feed_flag']) && $contentBlock['news_feed_flag'] == 1) || (isset($contentBlock['insight_flag']) && $contentBlock['insight_flag'] == 1)){
					
					?>
				
				<div class="widget-image <?php if ( $contentBlock['feed_image'] == NULL ){ ?>dummy-image<?php }?> white-bg pull-left">
					<?php if ( $contentBlock['feed_image'] != NULL ){ ?>
						<img src="<?php echo $contentBlock['feed_image'] ?>" />
					<?php }?>
                </div>
				<div class="widget-contents white-bg pull-left">
					<h2><?php  echo "<a href='".$contentBlock['link']."' target='_blank'>".$contentBlock['title']. "</a> <br/>";?></h2>
                    <p><?php echo $contentBlock['content']; echo "&nbsp;<a href='".$contentBlock['link']."' target='_blank'>Read more...</a>";?></p>
				</div>
				<?php 
				} 
				else if(isset($contentBlock['thread_id']) && $contentBlock['thread_id'] !=''){
				?> 
					<img src='<?php echo $contentBlock['feed_image']?>' />
				<?php
				} else if(isset($contentBlock['dashboard_id']) && $contentBlock['dashboard_id'] !='' ){
					$dashboard_id = $contentBlock['dashboard_id'];
					$dashFlag=1;
				?>	
				<div class='dashboard_box'>
				
				</div>
				<?php 
			    /*
			    <iframe src="<?php echo $embed_base_url.$dashboard_id;?>?embed=true?r=false" style="width: 100%; height: 220px"
					scrolling="no" marginwidth="0" marginheight="0" frameborder="0" vspace="0" hspace="0">
				</iframe>
				*/
				?>
				<?php 
				} // end of else if
        $contentBlock['no_comments'] = isset($contentBlock['no_comments']) ? $contentBlock['no_comments'] : 0;
        $contentBlock['no_tags'] = isset($contentBlock['no_tags']) ? $contentBlock['no_tags'] : 0;
				?>
			</div>
		</div>
		<div class="widget-footer show-widget-pop" id='foot_<?php echo $contentBlock['ind']; ?>' >
		    <ul>
		        
		         <li class="comments">
		         <a>(<?php echo $contentBlock['no_comments'] . "<span> comment". ($contentBlock['no_comments'] > 1 ? "s" : ""); ?></span>)</a></li>
		         <li class="tags">
		         <a> (<?php echo $contentBlock['no_tags'] . "<span> User". ($contentBlock['no_tags'] > 1 ? "s Tagged" : " Tagged"); ?></span>)</a></li>
				<!-- <li class="toggle-notify-btn"><a> <span></span></a></li> -->
		         
		         <!--<li class="bookmark"><a data-toggle="tooltip" data-placement="bottom" title=""
						data-original-title="Coming Soon">(Add to Bookmark)</a></li> -->
				<?php 
					if($dashFlag ==1){
						echo "<li><a onclick='refreshLeaderboard();'><span class='glyphicon glyphicon-repeat'></span></a>";
					}
				?>
		     </ul>
		     <?php
						
		     	$commented_by = (isset($contentBlock['last_commented_by'])) ? 'Commented by '.$contentBlock['last_commented_by'] : 'No Comments';
		     	$commented_ago =(isset($contentBlock['last_commented_ago']) && isset($contentBlock['last_commented_by'])) ? $contentBlock['last_commented_ago'] : '';
				
		     ?>
			
		     <p <?php if($commented_ago=='') echo "class='no-comments'";?> ><a ><?php echo $commented_by ?> <span> <?php if($commented_ago != '') {echo "&nbsp; | &nbsp;";} ?><?php echo $commented_ago; ?></span></a></p>
		     <input type='hidden' class='hdnthr_id' value='<?php echo $contentBlock['thread_id'];?>' />
		     <input type='hidden' class='hdn_ind' value='' data-indices='<?php echo $contentBlock['ind']; ?>' />
		     <?php $content = json_encode($contentBlock); ?>
		     <div style='display:none;' class='block_data' data-json="<?php echo htmlentities($content); ?>" /></div>
		</div>
		

	</div>
</div>
<script>
function refreshLeaderboard(){
	$(".p-table #pivot_").removeAttr('style');
	$(".p-table #pivot_").css('width','100%');
}
</script>
