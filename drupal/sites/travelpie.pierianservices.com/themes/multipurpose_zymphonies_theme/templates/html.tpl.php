<!DOCTYPE html>
<head>
<?php print $head; ?>
<title><?php print $head_title; ?></title>
<?php print $styles; ?>
<link rel="stylesheet" href="/sites/all/themes/multipurpose_zymphonies_theme/css/mystn.css">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
<?php print $scripts; ?>
<!--[if IE 8 ]>    <html class="ie8 ielt9"> <![endif]-->
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>

  <!-- Create a hdn variable to store the browse reports stuff -->
  <?php 
  	$reports = get_user_preferences('browse_reports');
  	echo "<input type='hidden' id='browse_menu' value='".json_encode($reports)."' />";
  	echo "<input type='hidden' id='dashboard_url' value='".variable_get('drupal_dashboard_base_url')."' />";
  ?>
  <script type='text/javascript'>
  	make_menu();
  </script>
</body>
</html>