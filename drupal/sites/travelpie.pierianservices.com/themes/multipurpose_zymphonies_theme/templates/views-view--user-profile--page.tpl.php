<?php
error_reporting(0);
module_load_include('inc', 'user_profile');
global $user;

$userProfileDataSet = db_query('select * from {users} u where uid = :uid', 
	array(
		'uid' => $user->uid
	)
);
$isUserFound = $userProfileDataSet->rowCount() > 0 ? TRUE : FALSE;
$userData = $userProfileDataSet->fetchAssoc();
$userProfileInfo = getAllUserProfileInfoForUser($user->uid);
$userGenderDetails = array(
  'm' => 'Male',
  'f' => 'Female',
  'o' => 'Other'
);
$userEmail = $userData['mail'];
if($user->picture == 0) {
  $path = "/sites/all/themes/multipurpose_zymphonies_theme/images/no_image.png";
} else {
  $userProfilePicFileObj = file_load($user->picture);
  $url = file_create_url($userProfilePicFileObj->uri);
  $url = parse_url($url);
  $path = $url['path'];
}
unset($_SESSION['messages']['error']);
?>
<style type="text/css">
.contents-dash {
	width: 100% !important;
}
.region-content{
  margin-top: 0 !important;
}
.transparent-bg {
  background: transparent !important;
  border: 0 !important;
  color: #555555;
}
.profile-details h3 form {
  text-indent: 0 !important;
}
</style>
<?php
  drupal_add_js("$base_url/misc/ui/jquery.ui.core.min.js?v=1.8.7", array('scope' => 'footer'));
  drupal_add_js("$base_url/misc/ui/jquery.ui.datepicker.min.js?v=1.8.7", array('scope' => 'footer'));
  //drupal_add_css("$base_url/sites/all/themes/multipurpose_zymphonies_theme/css/bootstrap-editable.css");
  drupal_add_js("$base_url/sites/all/themes/multipurpose_zymphonies_theme/js/bootstrap-editable.js", array('scope' => 'footer'));
?>
<div class="messages status" style="display: none;"></div>
<div class="messages error" style="display: none;"></div>
<div class="full-width">
  <div class="profile-content">
      <div class="inner-container">
          <div class="col-md-3">
              <div class="profile-pic">
                  <?php print drupal_render(drupal_get_form('user_profile_upload_profile_pic_form')); ?>
                  <img src="<?php echo $path; ?>" alt="Profile Pic" />
                  <span class="upload-pic"><a href="#">Upload Picture</a></span>
              </div>
          </div>
          <div class="col-md-9 profile-details">
              <h3 class="profile-name">
                <a href="#" data-type="text" data-validate-type="name" data-emptytext="Enter Full Name" data-validate-type="text" id="user_full_name" data-container="body" class="text-editable"><?php echo $userProfileInfo['user_full_name'];?></a>
                <span class="profile-edit"></span>
              </h3>
              <h3 class="profile-email">
                <a  data-type="text" data-validate-type="email" id="user_email" data-container="body" class=""><?php echo $userEmail;?></a>
              </h3>
              <h3 class="profile-sex">
                <a href="#" id="user_gender" data-selected-value = "<?php echo $userProfileInfo['user_gender']; ?>" data-validate-type="gender" data-type="select"data-container="body" class="select-editable"><?php echo $userGenderDetails[$userProfileInfo['user_gender']];?></a>
                <span class="profile-edit"></span>
              </h3>
              <h3 class="profile-number">
                <a href="#" id="user_phone" data-emptytext="Enter Phone Number" data-validate-type="phone" data-type="text" data-container="body" class="text-editable"><?php echo $userProfileInfo['user_phone'];?></a>
                <span class="profile-edit"></span>
              </h3>
              <h3 class="profile-date">
                  <input type="text" placeholder="DOB (DD-MM-YYYY)" class="transparent-bg" id="user_dob_datepicker" value="<?php echo $userProfileInfo['user_dob'];?>" />
                  <span class="profile-edit user-dob-edit"></span>
              </h3>
              <h3 class="profile-adress">
                <p id="user_address" data-emptytext="Enter Address" data-validate-type="text" data-type="textarea" data-container="body" class="text-editable"><?php echo $userProfileInfo['user_address'];?></p>
                <span class="profile-edit" id="addressedit"></span>
              </h3>
          </div>
      </div>
  </div>
</div>
