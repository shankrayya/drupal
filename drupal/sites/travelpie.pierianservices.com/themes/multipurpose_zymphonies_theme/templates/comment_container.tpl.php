
<!-- popup container for widget messaging and tagging -->
	
	<?php global $base_url;?>
	<div class="popup-container widget-popup" >
		<div class="widget-tab popup-tab" >
			<div class="widget-container widget-report">
				<div class="inner-container">
					<div class="message-close">X</div>
					<div class="widget-head">
					    <div class="widget-heading">

					        <img id='block_icon' src='' /><p id='superParentName'></p>
					    </div>
					    <div class="widget-breadcrumb">
					        <p><span id='parent_name'></span> > <span id='name'></span></p>
					    </div>
					    <div class="widget-time">
					        <p>Last Updated : <b id='last_updated'></b></p>
					    </div>
					</div>
					
					<div class="widget-body threadBlock" id="comment-container" >
						<div class="widget-inner">
						</div>
					</div>
					<div class="widget-footer">
					    <ul>
					    	<li class="comments" id='commentsTxt' data-comments='0'></li>
				        	<li class="tags" id='tagsTxt' data-tags='0'></li>
				        	<li id='errorTxt' style='display:none;'></li>

					    </ul>
					</div>
					<div class="widget-engage">
						<div class="widget-messaging">
							<div class='message-body' id='comments-container'>

							</div>
						</div>
						<div class="widget-tagging">
							<div class="tagging-body">
							</div>
							<div class="message-content widget-textfield">
								<div class="message-icon">
									<img src='/sites/all/themes/multipurpose_zymphonies_theme/images/icons/ic_tag.png' alt="icon" />
								</div>
								<div class="message-type">
									<div class="message-form">

										<input type="text"  data-role="tagsinput"  placeholder="Type User To Tag" id="tag" />  
										<input type="hidden"  id="tag-hidden" />  
									</div>
								</div>

							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<input type="hidden"  id="userProfile" value=''/>  

<input type='hidden' id='hdn_thread_id' value='' />
	<div style='display:none;' id='popup_data' /></div>
<div style='display: none' id='sisenseApp'></div>
	<script src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/jquery-1.12.4.min.js"></script>

		
	<script src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/comments.js"></script>
<script src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/jquery-comments.js"></script>
	<link rel="stylesheet"	href="/sites/all/themes/multipurpose_zymphonies_theme/css/jquery-comments.css">
	
	<?php 
		global $user;

		$sisense_url = variable_get('sisense_url');
		if($user->uid){
	?>	
	<script  src="<?php echo $sisense_url; ?>/js/sisense.js" ></script>
	<script type='text/javascript' src='<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/dashboard_func.js'></script>	
	<!-- <script type='text/javascript' src='<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/sisenseFunctions.js'></script> -->
	<?php 
	} // load if user logged in 
	?>
<script >


function commentsLoad(commentsJson){
	$('#comments-container').comments({

		
		profilePictureURL: $('#userProfile').val(),
	    getComments: function(success, error) {
	         /*var commentsArray = [{
	            id: 1,
	            created: '2015-10-01',
	            content: 'Lorem ipsum dolort sit amet',
	            fullname: 'Simon Powell',
	            upvote_count: 2,
	            user_has_upvoted: false
	        }];
			*/
			var commentsArray = commentsJson;
	        success(commentsArray);
	      
    }
});
}
$(document).ready(function(){
	getDashboards();
	getUserProfileImage();

//To fetch users from DB
var usernames = new Bloodhound({
	  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('user_full_name'),
	  queryTokenizer: Bloodhound.tokenizers.whitespace,
	  prefetch: 'user_tagging_controller.php?action=get_users'
	});
usernames.initialize();

	var elt = $('#tag');
	elt.tagsinput({
	  itemValue: 'user_id',
	  itemText: 'user_full_name',
	  typeaheadjs: {
	    name: 'usernames',
	    displayKey: 'user_full_name',
	    source: usernames.ttAdapter()
	  }
	});

	
});
/*
function toggleEmoji(){
	console.log($('.textarea'));
	$('.textarea').emojiPicker('toggle');
}
*/
</script>	