<?php
drupal_add_http_header('Cache-Control', 'no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0');
drupal_add_http_header('Pragma', 'no-cache');
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to main-menu administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */
?>
<?php
global $user;
global $base_url;
module_load_include('inc', 'user_profile');
$allGroupDetailsHtml = getAllUserGroupFiltersForUser('html', false);
$allReportPeriodDetailsHtml = getAllReportTimePeriodFiltersForUser('html', false);
$allCurrencyFitersHtml = getAllCurrencyFiltersForUser('html', false);
$allTimeZoneFiltersHtml = getAllReportTimezoneFiltersForUser('html', false);
$userProfile=getUserProfileInfo();
$userProfile['user_full_name'] = isset($userProfile['user_full_name']) ? $userProfile['user_full_name'] : '';
$currentUrlPath = current_path ();
$isHeaderCanBeDisplayed = TRUE;
$isFilterCanBeDisplayed = TRUE;
if (stripos ( $currentUrlPath, '/community' ) || stripos ( $currentUrlPath, '/spark_user' ) || stripos ( $currentUrlPath, '/group' ) || stripos ( $currentUrlPath, '/user_group' ) || stripos ( $currentUrlPath, '/region' ) || stripos ( $currentUrlPath, '/travel_agent' ) || stripos ( $currentUrlPath, '/corporate' ) || stripos ( $currentUrlPath, '/user_import' ))
	$isHeaderCanBeDisplayed = FALSE;
if(stripos($currentUrlPath, 'settings') !== FALSE || stripos($currentUrlPath, 'profile') !== FALSE)
   $isFilterCanBeDisplayed = FALSE;
   
$filterFormDisplay = '';
if(!$isFilterCanBeDisplayed)
  $filterFormDisplay = 'display:none;';

$theme_path = path_to_theme();
$logo_url= $theme_path."/images/pierian.png";
?>
<link rel="stylesheet"
	href="/sites/all/themes/multipurpose_zymphonies_theme/css/bootstrap.min.css">
	<link rel="stylesheet"	href="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/css/mystn-responsive.css">
	
<?php

if (! ($user->uid)) {
	$logout_url = variable_get('logout_api');
	?>
	<link rel="stylesheet" href="/sites/all/themes/multipurpose_zymphonies_theme/css/login.css">
	<link rel="stylesheet"	href="/sites/all/themes/multipurpose_zymphonies_theme/css/login-responsive.css">
	<link rel="stylesheet" href="/sites/all/themes/multipurpose_zymphonies_theme/css/swiper.min.css">
<iframe src="<?php echo $logout_url; ?>" style='display:none;'></iframe>
<?php
}
?>



<!-- header -->

<!-- old layout 

Old layout -->
<?php

if ($user->uid && $isHeaderCanBeDisplayed) {
	?>
<div class="header">
	<div class="section-container">
		<div class="inner-container">
			<div class="browse-report-button" data-name="browse-report-tab">
				BROWSE REPORTS</div>
			<div class="header-right">
				<ul>
				
					<li class='logoLi'><img
						src="<?php $logo_url?>"
						alt="Brand Logo" /></li>
					<li class="message-notification" data-name="message-tab"><a
						data-toggle="tooltip" data-placement="bottom" title=""
						data-original-title="Coming Soon"><img
							src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/images/icons/ic_mag_gray.png"
							alt="Brand Logo" /><span>0</span> </a></li>

					<!-- 
					<li class="bookmark" data-name="bookmark-tab" data-toggle="tooltip"
						data-placement="bottom" title="" data-original-title="Coming Soon"><img
						src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/images/icons/ic_bookmark.png"
						alt="Bookmark Logo" /></li>
					-->	

					<li class="name-label" data-name="profile-tab">
						<div class="profile-tab">
						<?php  $len=strlen($userProfile['user_full_name']);if($len>=9){ $name= substr($userProfile['user_full_name'],0,9).'...';}else{$name= $userProfile['user_full_name'];} ?>
							<h3 class="desktop-content name-label"><?php echo $name; ?><span></span>
							
							</h3>
							
							<h3>
								<a href="/profile">Profile</a>
							</h3><?php /*$base_url */?>
							<!-- /user -->
							<h3 class="mobile-content">
								<a href="#">Recently Viewed Reports</a>
							</h3>
							<h3>
								<a href="/settings">Settings</a>
							</h3>
							<h3>
								<a href="<?php $base_url?>/user/logout">Log Out</a>
							</h3>
						</div>
						<h3 class="mobile-content"><span class="more-icon"></span></h3>
						<p class="desktop-content"><?php echo $name;?></p>
						<span class="desktop-content"></span>
					</li>

				</ul>
			</div>
		</div>
	</div>
</div>
<div class="section">
	<div class="sub-header">
		<div class="section-container">
		
			<div class="sub-brand-logo">
				<div class="page-icon">
					<a href='/all'><img
						src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/images/icons/ic_home.png"
						alt="Home Icon"></a>
				</div>
				<div class="less-than">
					<img
						src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/images/icons/ic_arrow.png"
						alt="&lt;">
				</div>
				<div class="brand-icon">
				<?php $community_logo = (isset( $userProfile['community_logo']))  ?  $userProfile['community_logo']:'';
				if($community_logo){
				?>
					<img
						src="<?php echo $community_logo;?>"
						alt="Home Icon">
				<?php }?>	
				</div>
			</div>
			<div class="mobile-filter-menu"></div>
			<div class="mobile-filter-form" style="<?php echo $filterFormDisplay; ?>" >
				<div class="filter-form-heading">
					<div class="mobile-filter-section">
							<h3>FILTER</h3><h4>Reset Filter</h4>
					</div>
				</div>
				<div class="mobile-filter-section-filter">
					<div class="mobile-filter-section">
						<ul>
							<li><label for="currency">Currency</label>
								<div class="select-container">
									<div class="select-inner-container">
										<?php echo $allCurrencyFitersHtml ;?>
									</div>
								</div>
							</li>
							<li><label for="group">Group</label>
								<div class="select-container">
									<div class="select-inner-container">
										<?php echo $allGroupDetailsHtml; ?>
									</div>
								</div>
							</li>
							<li><label for="group">Period</label>
								<div class="select-container">
									<div class="select-inner-container">
										<select>
											<option>Today</option>
											<option>Last 7 days</option>
											<option>MTD</option>
											<option>YTD</option>
											<option>All Time</option>
										</select>
									</div>
								</div>
							</li>
						</ul>
						<button>APPLY FILTER</button>
					</div>
				</div>
			</div>
			<div class="filter-form" style="<?php echo $filterFormDisplay; ?>" >
					<ul><li><button style='background-color:#EA3836; color:#fff;' onclick="setFilterAllDashboards()">Go</button></li></ul>
					<ul class="more-filters">
						
						<li><label for="currency">CURRRENCY</label>
							<div class="select-container">
								<div class="select-inner-container">
									<?php echo $allCurrencyFitersHtml ;?>
								</div>
							</div></li>
						<li><label for="group">GROUP</label>
							<div class="select-container">
								<div class="select-inner-container">
									<?php echo $allGroupDetailsHtml; ?>
								</div>
							</div></li>
					</ul>
					<ul>
						<li><span class="more-icon"></span></li>
						<li><label for="currency">PERIOD</label>
							<?php echo $allReportPeriodDetailsHtml; ?>
              <div id = "period_range_filter"></div>
            </li>
						<li><label for="currency">CURRRENCY</label>
							<div class="select-container">
								<div class="select-inner-container">
									<?php echo $allCurrencyFitersHtml ;?>
								</div>
							</div></li>
						<li><label for="group">GROUP</label>
							<div class="select-container">
								<div class="select-inner-container">
									<?php echo $allGroupDetailsHtml; ?>
								</div>
							</div></li>
			
					</ul>

			</div>
		</div>
		<div class="popup-container popup-container1">
			<div class="section-container">
				<div class="browse-report-tab popup-tab">
					<span class="speach-arrow"></span>
					<div class="search-bar">
						<input name="search" id='searchReports'
							placeholder="Enter Report Name to Search" type="text">
					</div>

					<div class="row-fluid reports-menu report_menu">
                        <?php
	print theme ( 'links__system_main_menu', array (
			'links' => $main_menu,
			'attributes' => array (
					'id' => 'main-menu',
					'class' => array (
							'links',
							'inline',
							'clearfix' 
					) 
			),
			'heading' => t ( 'Main menu' ) 
	) );
	?>
                    </div>
					<!-- This close is for report carousel -->
				</div>
			</div>
		</div>
            <?php
	/*
	 * ?>
	 * <div class="popup-container popup-container2">
	 * <div class="message-tab popup-tab" >
	 * <div class="message-header">
	 * <p>COLLABORATE INSIGHTS</p>
	 * <span></span>
	 * </div>
	 * <div class="message-body">
	 * <div class="message-content">
	 * <div class="message-icon">
	 * <img src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/images/icons/ic_fb.png" alt="icon" />
	 * </div>
	 * <div class="message-subject">
	 * <h3>MICHAEL BEVAN</h3><span>DATA SCIENTIST</span>
	 * <p>Insight #1 - There is no guarantee of the data forecast we are planning based on the trend</p>
	 * <h5>33 mins ago</h5>
	 * </div>
	 * </div>
	 * <div class="message-content">
	 * <div class="message-icon">
	 * <img src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/images/icons/ic_fb.png" alt="icon" />
	 * </div>
	 * <div class="message-subject">
	 * <h3>Yuvraj Singh</h3><span>DATA SCIENTIST</span>
	 * <p>Insight #1 - There is no guarantee of the data forecast we are planning based on the trend</p>
	 * <h5>33 mins ago</h5>
	 * </div>
	 * </div>
	 * <div class="message-content">
	 * <div class="message-icon">
	 * <img src="images/icons/ic_fb.png" alt="icon" />
	 * </div>
	 * <div class="message-subject">
	 * <h3>Shane Bond</h3><span>DATA SCIENTIST</span>
	 * <p>Insight #1 - There is no guarantee of the data forecast we are planning based on the trend</p>
	 * <h5>33 mins ago</h5>
	 * </div>
	 * </div>
	 * <div class="message-content">
	 * <div class="message-icon">
	 * <img src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/images/icons/ic_fb.png" alt="icon" />
	 * </div>
	 * <div class="message-subject">
	 * <h3>graeme smith</h3><span>DATA SCIENTIST</span>
	 * <p>Insight #1 - There is no guarantee of the data forecast we are planning based on the trend</p>
	 * <h5>33 mins ago</h5>
	 * </div>
	 * </div>
	 * </div>
	 * </div>
	 * <div class="profile-menu-tab popup-tab" >
	 *
	 * </div>
	 * </div>
	 * <?php
	 */
	?>
<?php } elseif($isHeaderCanBeDisplayed) { ?>
    <!-- browse report content ends -->
    <header>
			<div class="section-container">
				<div class="brand-logo">
				

					<img
						src="<?php echo $logo_url;?>"
						alt="Brand Logo" />
				</div>
			</div>
		</header>
    
    
<?php
}

?>
<!-- End Header -->
		<div class="section-container">
			<div class="inner-container">

				<div id="page-wrap">

  <?php if ($is_front): ?>           
    <?php if ($page['top_first'] || $page['top_second'] || $page['top_third']): ?> 
      <div id="top-area" class="page-wrap clearfix">
        <?php if ($page['top_first']): ?>
        <div class="column one"><?php print render($page['top_first']); ?></div>
        <?php endif; ?>
        <?php if ($page['top_second']): ?>
        <div class="column two"><?php print render($page['top_second']); ?></div>
        <?php endif; ?>
        <?php if ($page['top_third']): ?>
        <div class="column three"><?php print render($page['top_third']); ?></div>
        <?php endif; ?>
      </div>
    <?php endif; ?>
  <?php endif; ?>
  <div id="container">
						<div class="container-wrap">
							<div class="content-sidebar-wrap">
								<div id="content">

          <?php if (theme_get_setting('breadcrumbs')): ?>
            <div id="breadcrumbs">
              <?php if ($breadcrumb): print $breadcrumb; endif;?>
            </div>
          <?php endif; ?>

          <section id="post-content" role="main">
            <?php print $messages; ?>
            <?php print render($title_prefix); ?>
            <?php if ($title): ?>
              <h1 class="page-title"><?php print $title; ?></h1>
            <?php endif; ?>
            <?php print render($title_suffix); ?>
            <?php if (!empty($tabs['#primary'])): ?>
              <div class="tabs-wrapper"><?php print render($tabs); ?></div>
            <?php endif; ?>
            <?php print render($page['help']); ?>
            <?php if ($action_links): ?>
              <ul class="action-links"><?php print render($action_links); ?></ul>
            <?php endif; ?>
            
             <?php
													if ($user->uid) {
														?>
            <div class="dash-contents pull-right">
											<div class="contents-dash pull-left">
                    <?php print render($page['content']); ?>
                </div>
											<div class="aside-dash pull-left">
                        <?php if($currentUrlPath == "all") { ?>
												<div class="recent-reports">
                        <div class="recent-reports-head">
                            <span></span>
                            <h2>Recently Viewed Reports</h2>
                        </div>
                        <div class="recent-reports-body">
                            <?php echo getRecentlyViewedReportListHTMLByUser();?>
                            
                        </div>
                    </div>
                        <?php } ?>
											</div>
										</div>
            <?php
													} else {
														?>
            
            <div>
            <?php print render($page['content']); ?>
            </div>    
            <?php
													}
													?>
          </section>
								</div>
      
        <?php if ($page['sidebar_first']): ?>
          <aside id="sidebar-first" role="complementary"><?php print render($page['sidebar_first']); ?>
        <?php
									/*
									 * print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('links', 'inline', 'clearfix')), 'heading' => t('Main menu')));
									 */
									?>
    </aside>
        <?php endif; ?>
      
        </div>

        <?php if ($page['sidebar_second']): ?>
          <aside id="sidebar-second" role="complementary"><?php print render($page['sidebar_second']); ?></aside> 
        <?php endif; ?>

    </div>
					</div>

  <?php if ($is_front): ?>

    <div id="footer_wrapper" class="footer_block bottom_widget">
      <?php if ($page['bottom_widget_1'] || $page['bottom_widget_2'] || $page['bottom_widget_3']): ?> 
        <div id="footer-area" class="full-wrap clearfix">
          <?php if ($page['bottom_widget_1']): ?>
          <div class="column">
								<div class="footerNavigation">
          <?php print render($page['bottom_widget_1']); ?>
          </div>
							</div>
          <?php endif; ?>
          <?php if ($page['bottom_widget_2']): ?>
          <div class="column two"><?php print render($page['bottom_widget_2']); ?></div>
          <?php endif; ?>
          <?php if ($page['bottom_widget_3']): ?>
          <div class="column"><?php print render($page['bottom_widget_3']); ?></div>
          <?php endif; ?>
        </div>
      <?php endif; ?>
    </div>

  <?php endif; ?>

</div>
			</div>
		</div>
	</div>
	<div class='full_loader'></div>
	<input type='hidden' id='embed_base_url' value="<?php echo variable_get('embed_base_url'); ?>"/>
	<input type='hidden' id='sisense_url' value="<?php echo variable_get('sisense_url'); ?>" />
	<input type='hidden' id='hdn_dashboard_url' value='<?php echo $base_url ."".variable_get('drupal_dashboard_base_url') ?>' />
	<!-- Footer -->
<div id="footer">
  <div class="footer_credit">

			 <?php if ($page['footer_first']): ?> 
    
      <?php if ($page['footer_first']): ?>
      <div class="column"><?php print render($page['footer_first']); ?></div>
      <?php endif; ?>

    
  <?php endif; ?>

		</div>
		<!-- <div class="credits">
    <?php print t('Design by'); ?><a href="http://www.zymphonies.com"> Zymphonies</a>
  </div>
-->
	</div>

	<div class="loading-overlay">
    </div>
	<?php include("comment_container.tpl.php");         ?>
  <script>
    var startDate = '<?php echo isset($_GET['fromDate']) && (bool)strtotime($_GET['fromDate']) ? $_GET['fromDate'] : NULL;?>';
    var endDate = '<?php echo isset($_GET['endDate']) && (bool)strtotime($_GET['endDate']) ? $_GET['endDate'] : NULL;?>';
  </script>
  
	<script
		src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/jquery-1.12.4.min.js"></script>
    <script src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/jquery.blockUI.js">
    </script>
	<script
		src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/bootstrap.min.js"></script>
    <script src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/moment.min.js"></script>
    <script src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script
		src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/daterangepicker.js"></script>
  <script
		src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/mystn.js"></script>
		<script
		src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/jquery-comments.js"></script>
	
		<script
		src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/comments.js"></script>
		<link rel="stylesheet"	href="/sites/all/themes/multipurpose_zymphonies_theme/css/jquery-comments.css">
    <link rel="stylesheet"	href="/sites/all/themes/multipurpose_zymphonies_theme/css/daterangepicker.css">
		<!-- 
		<script type="text/javascript" src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/jquery.emojipicker.js"></script>
		-->
    <link rel="stylesheet" href="<?php echo $base_url; ?>/sites/all/themes/multipurpose_zymphonies_theme/css/bootstrap-editable.css">
		<!-- Emoji Data -->
		
		<!--<link rel="stylesheet" type="text/css" href="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/css/jquery.emojipicker.a.css">
		
		<script type="text/javascript" src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/jquery.emojis.js"></script>-->
	<link rel="stylesheet"	href="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/css/bootstrap-tagsinput.css">
		
				<script src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/user-tag.js"></script>
	   <script src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/typeahead.js"></script>
		<script src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/bootstrap-tagsinput.js"></script>
		<script src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/js/swiper.min.js"></script>
		<iframe id="group_switch_logout_content" style="display: none;" src=""></iframe>
