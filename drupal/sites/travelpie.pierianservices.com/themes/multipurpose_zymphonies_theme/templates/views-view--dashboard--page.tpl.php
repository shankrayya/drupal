<?php
global $user;
$reportAccessDetails = getAllUserReportAccessDetails($user->uid);
$link = $_SERVER['REQUEST_URI'];
$link_array = explode('/',$link);
//$dashboard_taxonomy_id = end($link_array);
$dashboard_term = str_replace('/','',variable_get('drupal_dashboard_base_url'));
$dIndex = array_search($dashboard_term,$link_array);
if(isset($link_array[$dIndex+1])){
	$dashboard_taxonomy_id =$link_array[$dIndex+1];
	$dashArr = strtok($dashboard_taxonomy_id, '?');
	$dashboard_taxonomy_id =$dashArr;
}
if(!user_access('custom_admin_role') && (!isset($reportAccessDetails[$dashboard_taxonomy_id]) || !$reportAccessDetails[$dashboard_taxonomy_id])) {
  exit('Access Denied');
}
  
if($dashboard_taxonomy_id !='') {
	list($dashboardArr,$report_info)= browse_reports_get_reportInfo($dashboard_taxonomy_id);
	$dashboard_id= isset($dashboardArr['dashboard_id']) ? $dashboardArr['dashboard_id'] : '';
	$name = isset($dashboardArr['name']) ? $dashboardArr['name'] : '';
}
if(isset($dashboard_taxonomy_id) && $dashboard_taxonomy_id !== NULL && ctype_digit($dashboard_taxonomy_id)) {
  insertOrUpdateReportViewTime($dashboard_taxonomy_id);
}
module_load_include('inc', 'custom_login');
//$filtersToSend = getDashboardDefaultFilterData($dashboardArr);
$filtersToSend ='';

//echo "|$filtersToSend| <br/>"; exit();
drupal_add_js("/misc/ui/jquery.ui.core.min.js?v=1.8.7", array('scope' => 'footer'));
drupal_add_js("/misc/ui/jquery.ui.datepicker.min.js?v=1.8.7", array('scope' => 'footer'));
?>
<?php 
	$dashboard_id = ($dashboardArr['dashboard_id'] != '') ?  $dashboardArr['dashboard_id'] : '';
	$report_table_dashboard_id = ($dashboardArr['report_table_dashboard_id'] != '') ? $dashboardArr['report_table_dashboard_id'] : '';

	$dashInfo = array('dashboard_id'=>$dashboard_id,'report_table_dashboard_id'=>$report_table_dashboard_id);
	if($dashboard_id != '') { 
		$png_url = variable_get('api_base_url')."dashboards/".$dashboard_id."/export/png"; 
		$embed_base_url = variable_get('embed_base_url');
?>
<div class="full-widh report_block">
    <div class="report-header">
        <div class="inner-container">
            <h2><?php echo $name; ?></h2>
            <!--<h3 class="bookmark">(Bookmark Report)</h3> -->
            <h3 class="download"><a href='<?php echo $png_url ?>'>(Download)</h3>
            <a href='<?php $base_url?>/controller.php'><h3 class='retry'>Retry</h3></a> 
        </div>
    </div>
</div>
<div class='report_container'>
	<div class='report_trends' style='min-height:410px; width:100%;'>
		<div id='reportTrend_div'>
			<iframe src="<?php echo $embed_base_url.$dashboard_id;?>?embed=true&filter=<?php echo $filtersToSend; ?>" style="width: 100%; height: 410px"
			scrolling="no" marginwidth="0" marginheight="0" frameborder="0" vspace="0" hspace="0">
			</iframe>
		</div>
	</div>
	<div class='report_info' style='display:none'>
		<?php echo $report_info; ?>
	</div>
	<div class='report_table' style='min-height:410px; width:100%;display:none'>
		<div id='reportTbl_div'>
			Report Table Loading...
		</div>	
	</div>
</div>

<?php 
} // end dashboard Embed 
else {
	echo " <h3> &emsp; No Dashboard Found. </h3>";
}
$jsonDashInfo = json_encode($dashInfo);
echo "<input type='hidden' id='hdn_dashboard_data' value='".$jsonDashInfo."' />";
// by default trends
echo "<input type='hidden' id='cur_sectionName' value='report_trends' />";
?>
<script>
// Remove URL Tag Parameter from Address Bar
if (window.parent.location.href.match(/jwt=/)){
    if (typeof (history.pushState) != "undefined") {
        var obj = { Title: document.title, Url: window.parent.location.pathname };
        history.pushState(obj, obj.Title, obj.Url);
    } else {
        window.parent.location = window.parent.location.pathname;
    }
}

document.getElementsByClassName('contents-dash')[0].style.width ='100%';
</script>
 