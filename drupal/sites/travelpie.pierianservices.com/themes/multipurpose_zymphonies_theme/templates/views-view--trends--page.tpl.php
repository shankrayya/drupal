<!-- Leader board widgets come below , see as requried. -->
<!-- Get all Dashboards of this type and display . store updated date somewhere and us  -->
<?php 
	$pageName = browse_reports_get_page_path();
	
	$insight=false;
	$containers = web_container_get_block($pageName,$insight);
	$embed_base_url = variable_get('embed_base_url');

	foreach($containers as $ky =>$contentBlock){
		if(dispCond($contentBlock)){
 			include("web_container.tpl.php");
		} // end if 
	} // end foreach
	if(count($containers) > 0) {
		include('load_more.tpl.php');
	}
?>