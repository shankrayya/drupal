<!-- popup container for event scheduler -->
<div class="event-scheduler-box">
	<div class="event-scheduler">
		<div class="scheduler-inner">
			<div class="event-scheduler-head">
					<p>SCHEDULE REPORTS</p><span class="close-scheduler">+</span>
			</div>
			<div class="event-scheduler-body">
				<button class="add-report">ADD</button>
				<div class="add-edit-report">
					<p>Send me this report</p>
					<div class="select-cont">
						<select id="schedule-selector">
							<option value="weekly">Weekly</option>
							<option value="daily">Daily</option>
							<option value="monthly">Monthly</option>
						</select>
					</div>
					<span class="daily not-daily" id="daily">
					</span>
					<span class="weekly not-daily" id="weekly">
					<p>on every</p>
						<span class="sch-weekly">
							<div class="select-cont">                                    
								<select>
									<option>Monday</option>
									<option>Tuesday</option>
									<option>Wednesday</option>
									<option>Thursday</option>
									<option>Friday</option>
									<option>Saturday</option>
									<option>Sunday</option>

								</select>
							</div>
						</span>
						
					</span>
					<span class="monthly not-daily" id="monthly">
					<p>on every</p>
						<span class="sch-weekly">
							<div class="select-cont">                                    
								<select>
									<option>1st</option>
									<option>2nd</option>
									<option>3rd</option>
									<option>4th</option>
									<option>5th</option>
									<option>6th</option>
									<option>7th</option>

								</select>
							</div>
						</span>
						
					</span>
					<p>at</p> 
						<div class="select-cont">
							<select>
									<option>00:30 pm</option>
									<option>01:00 am</option>
									<option>01:30 am</option>
									<option>02:00 am</option>
									<option>02:30 am</option>
									<option>03:00 am</option>
									<option>03:30 am</option>
									<option>04:00 am</option>
									<option>04:30 am</option>
									<option>05:00 am</option>
									<option>05:30 am</option>
							</select>
						</div>
					<p>
					Starting from</p> <span class="datepicker"><input type="text" id="datepicker" placeholder="Start Date"></span><p>till</p> <span class="datepicker"><input type="text" id="datepicker2" placeholder="End Date"></span>                            
					<br />
					<button class="brand-color">Save</button>
					<button class="brand-color">Change Filter</button>
				</div>
				<div class="scheduled-lists">
					<table>
						<thead>
							<tr>
								<td>Sch No.</td>
								<td>Frequency</td>
								<td>Start Time</td>
								<td>End Time</td>
								<td>Update</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Daily</td>
								<td>02/07/2017</td>
								<td>02/23/2017</td>
								<td><button class="edit-button">Edit</button></td>
							</tr>
							<tr>
								<td>1</td>
								<td>Daily</td>
								<td>02/07/2017</td>
								<td>02/23/2017</td>
								<td><button class="edit-button">Edit</button></td>
							</tr>
							<tr>
								<td>1</td>
								<td>Daily</td>
								<td>02/07/2017</td>
								<td>02/23/2017</td>
								<td><button class="edit-button">Edit</button></td>
							</tr>

						</tbody>
					</table>
				</div>
			</div>
		</div> 
	</div>	
</div>
<script>
    $(document).ready(function(){
        $(".event-scheduler-box").hide();
        $( "#datepicker, #datepicker2" ).datepicker();
        $('#schedule-selector').on('change',function() {
              $('.not-daily').hide();
            $('#' + $(this).val()).show();
         });
    });
    

</script>