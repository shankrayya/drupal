<!-- Leader board widgets come below , see as requried. -->
<!-- Get all Dashboards of this type and display . store updated date somewhere and us  -->
<?php 
	$pageName = browse_reports_get_page_path();
	$pageName ='all';
	$allFlag=1;
	$insight=true;

	
	$containers = web_container_get_block($pageName,$insight,$allFlag);
	
	
	$embed_base_url = variable_get('embed_base_url');
	//if(isset($contentBlock['news_feed_flag']) || $contentBlock['dashboard_id'] !='' ){
	foreach($containers as $ky =>$contentBlock){
		if(dispCond($contentBlock) || isset($contentBlock['news_feed_flag']) || isset($contentBlock['insight_flag'])){
			$dashboard_id = isset($contentBlock['dashboard_id']) ? $contentBlock['dashboard_id'] : '';
			$thread_id = isset($contentBlock['thread_id']) ? $contentBlock['thread_id'] : '';
            if($dashboard_id !='' && $thread_id =='') {
                continue;
			}

			include("web_container.tpl.php");
		} // end if 
	} // end foreach

	if(count($containers) > 0) {
		include('load_more.tpl.php');
	}
?>
