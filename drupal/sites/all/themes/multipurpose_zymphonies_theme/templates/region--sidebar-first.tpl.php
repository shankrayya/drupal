<!-- This is a new first side add content here from the taxanomy. -->
<?php 
 
$link = $_SERVER['REQUEST_URI'];
$link_array = explode('/',$link);
//echo $page = end($link_array);
$reportFlag =0;
$totalInd = count($link_array);
$browse_report_page = str_replace("/",'',variable_get("drupal_dashboard_base_url"));
if($link_array[$totalInd-1] == $browse_report_page || $link_array[$totalInd-2] == $browse_report_page){
	$reportFlag =1;
}

$folders = get_folders();
global $base_url;
$pagepath = browse_reports_get_page_path();
$currentPath = $_SERVER['HTTP_HOST'] . '/' . request_uri();
?>
<div class="navigation">
<?php 
$menuArr = array('Report Trends'=>array('img'=>'trends.png','link'=>'#','section'=>'report_trends'),'View Report'=>array('img'=>'report.png','link'=>'#','section'=>'report_table'),'Report Info'=>array('img'=>'report-info.png','link'=>'#','section'=>'report_info'));
$userProfileMenuDetails = array('Profile'=>array('img'=>'profile.png','link'=>'#','url'=>'/profile'),'Settings'=>array('img'=>'settings_ic.png','link'=>'#','url'=>'/settings'));
//,'Notifications'=>array('img'=>'ic_notifications.png','link'=>'#','url'=>'/notifications)'

if($reportFlag == 1){
	$ind=0;
	echo "<ul>";
	foreach($menuArr as $menuName => $infoArr){
		$ind++;
		$class='';
		if($ind ==1){
			$class='selected';
		}

?>
	<li class='nav-menu nav-all <?php echo $infoArr['section']."_lnk ". $class; ?> '>
		<a href='#'  onclick='showSection("<?php echo $infoArr['section']; ?>")'>
			<span style="background-image: url('<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/images/icons/<?php echo $infoArr['img'] ?>');" ></span>
			<!-- <img src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/images/icons/<?php echo $infoArr['img'] ?>" alt='Icons'>  -->
			&nbsp; <?php echo $menuName; ?>
		</a>

	</li>

<?php
	}
	echo "</ul>";
} else if(stripos($currentPath, 'profile') || stripos($currentPath, 'settings')) {
	// || stripos($currentPath, 'notifications')

  echo "<ul>";

  foreach($userProfileMenuDetails as $menuName => $menuDetails) {
    $selected = stripos($currentPath, $menuDetails['url']) ? 'selected' : '';
    echo "<li class='nav-menu nav-all ". $selected ."'>";
    echo "<a href='". $menuDetails['url'] ."'>";
	?>
	<span style="background-image: url('<?php echo $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/images/icons/<?php echo $menuDetails['img']?>');" ></span>
	<?php
//	echo "<span style='background-image: url(='". $base_url ."/sites/all/themes/multipurpose_zymphonies_theme/images/icons/". $menuDetails['img'] ."');' ></span>";
   // echo "<img src='". $base_url ."/sites/all/themes/multipurpose_zymphonies_theme/images/icons/". $menuDetails['img'] ."' alt='Icons'> ";
    echo $menuName;
    echo "</a></li>";
  }
  echo "</ul>";
} else {
	$all = '';
	if($pagepath == 'all'){
		$all = 'selected';
	}

?> 
    <ul>
    	<li class="nav-menu nav-all <?php echo $all; ?>">
        <a href="<?php echo $base_url.'/all'; ?>">
		<span style="background-image: url('<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/images/icons/all.png');" ></span>
        	<!-- <img src="<?php //$base_url?>/sites/all/themes/multipurpose_zymphonies_theme/images/icons/all.png" alt='Icons'> -->
        	&nbsp; All
        </a>
        </li>        	
<?php 
	$ind =0;

	foreach($folders as $ky => $valArr){

		$active ='';
		$ind++;
		$active ='';
		if ($pagepath == $valArr['link']) {
			$active='selected';
		}
?>
	<li class="nav-menu nav-all <?php echo $active; ?>">
        <a href="<?php echo $base_url.'/'.$valArr['link']?>" >
			<span style="background-image: url('<?php echo $valArr['icon_img']; ?>');" ></span>
        	<!-- <img src="<?php //echo $valArr['icon_img']; ?>" alt='Icons'> -->
        	&nbsp; <?php echo $valArr['name']; ?>         
        </a>
        <!-- <span class="msg-notify">2</span> -->
    </li>
<?php 		
	}
	echo "</ul>";

} // end else Folders display		
?> 
 </div>	                   
<?php
		

?>

 <div class="navigation nav-mobile">
 
 <?php
 if($reportFlag == 1){
 $ind =0;
 foreach($menuArr as $menuName => $infoArr){
		$ind++;
		$class='';
		if($ind ==1){
			$class='selected';
		}

?>
	<div class='nav-menu <?php echo $infoArr['section']."_lnk ". $class; ?> '>
		<a href='#'  onclick='showSection("<?php echo $infoArr['section']; ?>")'>

			<span style="background-image: url('<?php echo  $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/images/icons/<?php echo $infoArr['img'] ?>');" ></span>
			<!-- <img src="<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/images/icons/<?php echo $infoArr['img'] ?>" alt='Icons'>  -->
			&nbsp; <?php echo $menuName; ?>
		</a>

	</div>

<?php
	}
 }else if(stripos($currentPath, 'profile') || stripos($currentPath, 'settings')) {
	 // || stripos($currentPath, 'notifications')

 
  foreach($userProfileMenuDetails as $menuName => $menuDetails) {
    $selected = stripos($currentPath, $menuDetails['url']) ? 'selected' : '';
    echo "<div class='nav-menu ". $selected ."'>";
    echo "<a href='". $menuDetails['url'] ."'>";
	
	?>
	<span style="background-image: url('<?php echo $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/images/icons/<?php echo $menuDetails['img']?>');" ></span>
	<?php
	//echo "<span style='background-image: url(='". $base_url ."/sites/all/themes/multipurpose_zymphonies_theme/images/icons/". $menuDetails['img'] ."');' ></span>";
   // echo "<img src='". $base_url ."/sites/all/themes/multipurpose_zymphonies_theme/images/icons/". $menuDetails['img'] ."' alt='Icons'> ";
    echo $menuName;
    echo "</a></div>";
  }
 
} else {
	$all = '';
	if($pagepath == 'all'){
		$all = 'selected';
	}

?> 
    <ul>
    	<div class="nav-menu  <?php echo $all; ?>">
        <a href="<?php echo $base_url.'/all'; ?>">
		<span style="background-image: url('<?php $base_url?>/sites/all/themes/multipurpose_zymphonies_theme/images/icons/all.png');" ></span>
        	<!-- <img src="<?php //$base_url?>/sites/all/themes/multipurpose_zymphonies_theme/images/icons/all.png" alt='Icons'> -->
        	&nbsp; All
        </a>
        </div>        	
<?php 
	$ind =0;

	foreach($folders as $ky => $valArr){

		$active ='';
		$ind++;
		$active ='';
		if ($pagepath == $valArr['link']) {
			$active='selected';
		}
?>
	<div class="nav-menu <?php echo $active; ?>">
        <a href="<?php echo $base_url.'/'.$valArr['link']?>" >
			<span style="background-image: url('<?php echo $valArr['icon_img']; ?>');" ></span>
        	<!-- <img src="<?php //echo $valArr['icon_img']; ?>" alt='Icons'> -->
        	&nbsp; <?php echo $valArr['name']; ?>         
        </a>
        <!-- <span class="msg-notify">2</span> -->
    </div>
<?php 		
	}


} // end else Folders display		
?> 
	       

</div>
	<?php
	/*
 	<div class="nav-menu selected"><a href="#"><img alt="Icons" src="/sites/all/themes/multipurpose_zymphonies_theme/images/icons/all.png">All</a></div>
 	<div class="nav-menu"><a href="#"><img alt="Icons" src="/sites/all/themes/multipurpose_zymphonies_theme/images/icons/all.png">Trends</a></div>
 	<div class="nav-menu"><a href="#"><img alt="Icons" src="/sites/all/themes/multipurpose_zymphonies_theme/images/icons/all.png">Leaderboard</a></div>
 	<div class="nav-menu"><a href="#"><img alt="Icons" src="/sites/all/themes/multipurpose_zymphonies_theme/images/icons/all.png">Report</a></div>
 	<div class="nav-menu"><a href="#"><img alt="Icons" src="/sites/all/themes/multipurpose_zymphonies_theme/images/icons/all.png">News Feeds</a></div>
 	<div class="nav-menu"><a href="#"><img alt="Icons" src="/sites/all/themes/multipurpose_zymphonies_theme/images/icons/all.png">Sabre Insight</a></div>
 */
