 
window.dashArr =[];
window.currencyStr ='CURRENCY_CODE';
window.dateFieldArr = ['TRANSACTIONAL_DATE','CREATEDATETIME','ISSUED_DATE'];
window.dashFlags=[];
window.callbackFlag=0;
window.myapp =null;
function loadDashboards(){
	var dashboards = getDashAdded();
	var dashboardArr = $.map(dashboards, function(el) { return el });
	loadSisenseDash(dashboardArr);
}

function getDashboards(){
	var dashObj={};
	var data='';
	var str ='';
	var dashboardElem ='';
	var dashboard_id='';
	//var dir_path = 'files/dash_json/';
	var dir_path = Drupal.settings.basePath+$("#dash_path").val();
	var cache_path = Drupal.settings.basePath+$("#dash_cached").val();
	var file_path ='';
	var reportBlk_flag=0;
	// get sisense app
	var app = sisenseApp.getApp(getDashboards,[]);
	if(app){
		$(".block_data").each(function(i,obj){
		data=$(obj).data('json');
			if(!data.hasOwnProperty('dash_loaded') && data.dash_loaded != 1){
				if(!isInt(data.thread_id) && typeof data.dashboard_id !== 'undefined' && data.dashboard_id !=''){
					dashObj[i]=data.dashboard_id;
					str ='#foot_'+data.ind;
					dashboardElem = $(str).parent().find('.dashboard_box');
					dashboard_id=data.dashboard_id;
					// get json and return widget id and title 
					file_path = dir_path+dashboard_id+'.json';
					cache_file_path = cache_path+dashboard_id+'.json';
					if(data.report_blk == 1){
						reportBlk_flag =1;
					}
					tid=data.tid;
					loadSisense(app,file_path,data.ind,dashboardElem,dashboard_id,obj,tid,reportBlk_flag,cache_file_path);
				}
			}
		});
	} else {
		//window.location = '/user/logout';
	}
	
}
function loadSisense(app,file_path,dashboard_ind,dashboardElem,dashboard_id,obj,tid,reportBlk_flag,cache_file_path,retry){
	//$(dashboardElem).html("<div class='loading-overlay'></div>");
	$(dashboardElem).html("<i class='fa fa-spinner fa-spin fa-3x fa-fw bigS'></i>");
	var retry = retry || '';
	$.ajax({
	    cache: false,
	    url: file_path,
	    dataType: "json",
	    success: function(data) {
	        filterLoadData(app,data,dashboard_ind,dashboardElem,dashboard_id,tid,reportBlk_flag);
	        $(obj).data('json').dash_loaded=1;
	    }, error: function(){
	    	if(retry == '') {
	    		file_path = cache_file_path;
	    		var retry =1;
	    		loadSisense(app,file_path,dashboard_ind,dashboardElem,dashboard_id,obj,tid,reportBlk_flag,cache_file_path,retry);	
	    	} else {
	    		$(dashboardElem).html("&emsp; Error loading dashboard");
	    	}
	    }
	});

}
function filterLoadData(app,data,dashboard_ind,dashboardElem,dashboard_id,tid,reportBlk_flag){
	if(!data.hasOwnProperty('layout')){
		$(dashboardElem).html('&emsp; Cannot show dashboard');
		return ;
	}
	var dashObj =data.layout.columns[0].cells;
	var currencyFlag =0;
	var dateFlag=0;
	var dateField='';
	window.dashFlags[dashboard_id]= {currencyFlag:0,dateFlag:0,filterJaql:null};
	$.each(data.filters,function(i,filters){
		if(filters.jaql){
			if(filters.jaql.column == window.currencyStr){
				currencyFlag =1;
				window.dashFlags[dashboard_id].currencyFlag=1;
			}
			if(jQuery.inArray(filters.jaql.column, window.dateFieldArr) !== -1) {
				dateFlag=1;
				dateField= filters.jaql.column;
				window.dashFlags[dashboard_id].dateFlag=1;
				window.dashFlags[dashboard_id].filterJaql= filters.jaql;
			}	
		}
		
	});
	
	var widgetArr=[];
	var report_blk_cnt=2;
	var width='';
	$.each(dashObj,function(i,cellObj){
		$.each(cellObj,function(j,subcellObj){
			$.each(subcellObj,function(k,elemObj){
				width = elemObj.width; // + (elemObj.width * 0.6);
				widgetArr.push([elemObj.elements[0].widgetid,width]);
			});
		});

		if(reportBlk_flag ==1 && widgetArr.length > report_blk_cnt){
			// for reports block show only 3 widgets
			return false;
		}	
	});

	// si sense connect and load dashboard
	
	try {
	var serverUrl = $("#sisense_url").val();
//	Sisense.connect(serverUrl+'/').then(function(app) {
//	console.log("Sisense load.......")

	var widget_id_str ='';
	var obj ='';

	var dateToApplyFilter = getDateJaqls();
	var currencyFlag =0;
	var dateFlag =0;
	var date_filterJaql ='';
	var selectedCurrencyCode = $("select[name='currency_id'] option:selected")[1].text;
	// check for date and cur fields
	var cur_code = (selectedCurrencyCode == 'Select') ? '' : selectedCurrencyCode;
	app.dashboards.load(dashboard_id).then(function
		(dash) { 
		var index=0;
		$(dashboardElem).html(''); // clear
		$.each(widgetArr,function(i,widget){
			widget_id_str = 'd_'+dashboard_ind+'_w'+index;
			jQuery('<div></div>',{
				id:widget_id_str
			}).appendTo(dashboardElem);
			wid =widget[0];
			if(dash.widgets.get(wid)) {
				dash.widgets.get(wid).container =
				document.getElementById(widget_id_str);
			}	
			index++;
		});
		/*
		dash.on('refreshend', function(sender, ev){
			setTimeout(function(){
				$(".number_span").attr('style','font-size:42px');
				$(".title_span").attr('style','font-size:18px');
			},4000);
		});
		*/

		dash.on('refreshend', function(sender, ev){
			setTimeout(function(){
				//$(".number_span").attr('style','font-size:42px');
				//$(".title_span").attr('style','font-size:18px');
				$(".p-table #pivot_").removeAttr('style');
				$(".p-table #pivot_").css('width','100%');
			},5000);
		});
		dash.on('refreshend', function(sender, ev){
			setTimeout(function(){
				$(".p-table #pivot_").removeAttr('style');
				$(".p-table #pivot_").css('width','100%');
			},11000);
		});

		// apply global settings filters starts 
		currencyFlag= window.dashFlags[dash.$$id].currencyFlag;
		if(cur_code !='' && currencyFlag ==1 )  {
			$.each(dash.$$filters.$$filters,function(i,filters){

				if(typeof filters.jaql != 'undefined'){
					if(filters.jaql.column == window.currencyStr) {
						dash.$$filters.$$filters[i].jaql.filter.members = [cur_code];
					}	
				}
			}); 
		}
		dateFlag = window.dashFlags[dash.$$id].dateFlag;
		if(dateFlag ==1 && dateToApplyFilter !=''){
			$.each(dash.$$filters.$$filters,function(i,filters){
				if(typeof filters.jaql != 'undefined'){
					if(jQuery.inArray(filters.jaql.column, window.dateFieldArr) !== -1) {
						dash.$$filters.$$filters[i].jaql.filter = dateToApplyFilter;
					}
				}	
			}); 
		}

		// apply global settings filters ends 
		dash.refresh();
		window.dashArr.push(dash);

		if(reportBlk_flag == 1) {
			var dash_url = $("#hdn_dashboard_url").val()+'/'+tid;
			// add a report overlay to it 
			$(dashboardElem).parent().parent().parent().append("<div class='widget-overlay'><a href='"+dash_url+"' target='_blank'>Click on chart for full view</a></div>");
		}
		
		
		},function(){
			$(dashboardElem).html('Cannot access Dashboard');
		});
	

	//});

	}catch(e){
		var exmsg = "";
		if (e.message) {
		    exmsg += e.message;
		}
		if (e.stack) {
		    exmsg += ' | stack: ' + e.stack;
		}
		//console.log(exmsg);
		// load sisense using ajax call
		if(typeof Sisense == 'undefined'){
			//doJwtCall();	
		}
	} 
	
	// si sense connect ends 
}

function doJwtCall(groupSwitchTo,redirectUrl){
	var groupSwitchTo = groupSwitchTo || '';
	var redirectUrl = redirectUrl || '';
	var groupId ='';
	var redirectUrl ='';
	if(groupSwitchTo =='') {
		group_id = $("#nofilter_group_filter").val();
	} else {
		group_id = groupSwitchTo;
		redirectUrl = redirectUrl;
	}

	$.ajax({
    url:"/switch_portal_group.php",
    type: 'POST',
    data: {group_id: group_id},
    beforeSend : function() {
        $.blockUI({ message: '<h2> Connecting... </h2>',
          css: {border: 'none', padding: '5px', 'background-color': '#000', 'border-radius': '5px', opacity: '.5', color: '#fff'}
        });
    },
    success: function (data) {
      $("#group_switch_logout_content").attr("src", "");
      resultObj = $.parseJSON(data);
      if(!resultObj.hasOwnProperty('err')) {
        if(redirectUrl == '') {
        	redirectUrl = resultObj.success.url;
        }
        window.location.href = redirectUrl;
      }
    }, 
    complete: function () {
      $.unblockUI();
    }
  });
}
// function to show and hide report and report info
function showSection(sectionName, override,globalFilterFlag,filterFlagObj){
	var cur_sectionName = $("#cur_sectionName").val();
	if(cur_sectionName == sectionName && (typeof override === undefined || override === false)) {
		return false; // already on same page why you do this
	}
	var globalFilterFlag = globalFilterFlag || '';
	var filterFlagObj = filterFlagObj || '';

    $(".nav-all").removeClass('selected');
    var actClass = '.'+sectionName+'_lnk';
    $(actClass).addClass('selected');
    $(".report_trends, .report_info, .report_table").hide();
    $("."+sectionName).show();
    // iframe sections , get insert from ajax
    if(sectionName !='report_trends'){
    	$(".report-chat").hide();
    } else {
    	$(".report-chat").show();
    }
    if(sectionName != 'report_info'){
    	// get iframe filter from switching to inverse and pass to iframe url 
    	getFiltersInsertIframe(sectionName,globalFilterFlag,filterFlagObj);
    }
    $("#cur_sectionName").val(sectionName);
}
// function to get filters data and insert new iframe in div with the filter data changed in switching from 
// iframe 
function getFiltersInsertIframe(sectionName,globalFilterFlag,filterFlagObj){
	var dashboardData= $.parseJSON($("#hdn_dashboard_data").val());
	var iframe_dashboardId='';
	var divElem ='';
	
 	var reportId = dashboardData.report_id;
 	var sameSectionFlag = 0;
 	var cur_sectionName = $("#cur_sectionName").val();
 	if(cur_sectionName == sectionName){
 		sameSectionFlag = 1;
 	}
 	var dashboard_id ='';
 	var report_table ='';
 	var syncFlag=0;
	if(sectionName =='report_trends'){
		// means if switched from table dashboard view so get filters of table and use in other dashboard
		// get filter of report_trends ie table dashboard id 

		if(globalFilterFlag ==1 || sameSectionFlag ==1 ){
			dashboard_id = dashboardData.dashboard_id;
		} else {
			dashboard_id = dashboardData.report_table_dashboard_id;
			report_table = dashboardData.dashboard_id;
			syncFlag=1;
		}
		iframe_dashboardId=dashboardData.dashboard_id;
		divElem="#reportTrend_div";	
		
	} else {
		if(globalFilterFlag ==1 || sameSectionFlag ==1 ){
			dashboard_id = dashboardData.report_table_dashboard_id;
		} else {
			dashboard_id = dashboardData.dashboard_id;
			report_table = dashboardData.report_table_dashboard_id;
			syncFlag=1;
		}
		
		iframe_dashboardId=dashboardData.report_table_dashboard_id;
		divElem="#reportTbl_div";
	}
	$(divElem).html('Loading ...');

	var filtersToSend= '';
	var iframe_path = $("#embed_base_url").val()+iframe_dashboardId+"?embed=true";
	//var iframe_str="<iframe src='"+iframe_path+"' style='width: 100%; height: 410px' scrolling='no' marginwidth='0' marginheight='0' frameborder='0' vspace='0' hspace='0'>";
	
	var iframe = document.createElement('iframe');
	var screenHeight =  $(window).height();
	var header2Height = $('.header').height();
	var subheader = $('.sub-header').height();
	var footer2Height = $('#footer').height();
	var iframeHeight = screenHeight - (header2Height + subheader + footer2Height) + 30;
	iframe.frameBorder=0;
	iframe.width="100%";
	iframe.height= "600px"; iframeHeight + "px"; //
	iframe.id="random";
	iframe.onload=function(){
		/*
		try {
			console.log(this.contentDocument.title);
		} catch(ex){
			$(divElem).html("<h5> &emsp; Something went wrong/You dont have access to this dashboard</h5>");
		}
		*/ 
	}
	iframe.onerror= function(){
		console.log('Failed to load Iframe.');
	}
	/*
	$.ajax({
		type: 'get',
		url: '/comment_controller.php',
		data: {'action':'getDashboardFilters','dashboard_id': dashboard_id, 'report_id': reportId},
		success: function(data){
			var response = $.parseJSON(data); 
			if(response.errorCode != 1){
				// success 
				filtersToSend = response.filterStr;
				if(filtersToSend !='')
					iframe_path +='&filter='+filtersToSend;
				iframe.setAttribute("src", iframe_path);
				$(divElem).html(iframe);
       			$(".download a").attr('href', $("#sisense_url").val() +"/api/v1/dashboards/"+ dashboard_id +"/export/png");
			} else {
				$(divElem).html(response.msg);
			}
		}
	});
	*/
	/*
	if(globalFilterFlag ==1 ){
		 // it is only yo change global filter, just change the filters and append to iframe url 
		 var jaq = $("#fSend").val();
		 var jaqlJson = $.parseJSON(jaq);
		 var dashFlags = checkFilterFlag(jaqlJson);
		 if(dashFlags.currencyFlag == 1 || dashFlags.dateFlag ==1){
		 	// change the filter url 
		 	if(dashFlags.dateFlag ==1 ){
		 		var dateToApplyFilter = getDateJaqls();
		 		var dateInd = dashFlags.dateFieldInd;
		 		jaqlJson[dateInd].jaql.filter = dateToApplyFilter;
		 	}
		 	if(dashFlags.currencyFlag ==1 ){
		 		var selectedCurrencyCode = $("select[name='currency_id'] option:selected")[1].text;
		 		var cur_code = (selectedCurrencyCode == 'Select') ? '' : selectedCurrencyCode;
		 		var curInd = dashFlags.curFieldInd;
		 		if(cur_code !=''){
		 			jaqlJson[curInd].jaql.filter.members = [cur_code];
		 		}
		 	}
		 }
		var uri_encode = encodeURIComponent(JSON.stringify(jaqlJson));
		 if(uri_encode !='')
			iframe_path +='&filter='+uri_encode;
		iframe.setAttribute("src", iframe_path);
		$(divElem).html(iframe);
		$(".download a").attr('href', $("#sisense_url").val() +"/api/v1/dashboards/"+ dashboard_id +"/export/png");

	} else {
 	*/	
 	var globalApplyArr ={};
 	if(globalFilterFlag ==1){
 		var jaq = $("#fSend").val();
		var jaqlJson = $.parseJSON(jaq);
 		var dashFlags = checkFilterFlag(jaqlJson);
		 if(dashFlags.currencyFlag == 1 || dashFlags.dateFlag ==1){
		 	// change the filter url 
		 	if(dashFlags.dateFlag ==1 ){
		 		var globalFlag =1;
		 		var dateToApplyFilter = getDateJaqls(globalFlag);
		 		globalApplyArr.dateToApplyFilter=dateToApplyFilter;
		 	}
		 	if(dashFlags.currencyFlag ==1 ){
		 		var selectedCurrencyCode = $("select[name='currency_id'] option:selected")[1].text;
		 		var cur_code = (selectedCurrencyCode == 'Select') ? '' : selectedCurrencyCode;
		 		globalApplyArr.cur_code=cur_code;
		 	}
		 }
 	}
	$.ajax({
		type: 'get',
		url: '/comment_controller.php',
		data: {'action':'getDashboardFilters','dashboard_id': dashboard_id, 'report_table':report_table,'syncFlag':syncFlag,'report_id': reportId,'globalApplyArr':globalApplyArr,'filterFlagObj':filterFlagObj},
		success: function(data){
			var response = $.parseJSON(data); 
			if(response.errorCode != 1){
				// success 
				filtersToSend = response.filterStr;
				/*if(globalFilterFlag	 ==1 ){
					filterJson = response.filterJson;
					filtersToSend= getFilterStrFromJson(filterJson,filterFlagObj);
				}
				*/
				if(filtersToSend !='')
					iframe_path +='&filter='+filtersToSend;

				iframe.setAttribute("src", iframe_path);
				$(divElem).html(iframe);
       			$(".download a").attr('href', $("#sisense_url").val() +"/api/v1/dashboards/"+ iframe_dashboardId +"/export/png");
			} else {
				$(divElem).html(response.msg);
			}
		}
	});
	/*
	}
	*/
}
/*
window.myapp.setFilter = function(jaql) {

  //  Get the active dashboard, only run if sisense is loaded
  var dashboard = window.myapp.activeDashboard;
  if (dashboard) {
    if (jaql) {     
      //  Create the filter jaql
      var filterJaql = {
        jaql: jaql
      };

      //  Create the filter options
      var filterOptions = {
        save:true, 
        refresh:true, 
        unionIfSameDimensionAndSameType:false
      };

      //  Set the new filter
      window.myapp.activeDashboard.$$model.filters.update(filterJaql,filterOptions);
    }
  }
}
*/

function setFilterAllDashboards(){
	var customStr = "from";
	/*var selectedCurrencyCode = $("select[name='currency_id'] option:selected")[1].text;
	var filterObj = $(".selected-period-filter").attr('data-filter');
	var dateFilter = JSON.parse(filterObj);
	filterStr = JSON.stringify(filterObj);
	if(filterStr.indexOf(customStr) !== -1){
		dateFilter.from ='';
		dateFilter.to ='';
		var from = $("input[name='daterangepicker_start']").val();
		var to =$("input[name='daterangepicker_end']").val();
		dateFilter.from = convertDate(from);
		dateFilter.to = convertDate(to);
	}
	*/
	var dateFilter = getDateJaqls();
	// get the filter values a
	for(var i=0; i<window.dashArr.length;i++){
		sisenseApp.activeDashboard=window.dashArr[i];
		changeFilters(dateFilter);
	}
}

function changeFilters(dateFilter){
	//var selectedCurrencyCode = $("select[name='currency_id'] option:selected").text();
	var selectedCurrencyCode = $("select[name='currency_id'] option:selected")[1].text;
	// check for date and cur fields
	var cur_code = (selectedCurrencyCode == 'Select') ? '' : selectedCurrencyCode;
	var currencyFlag= window.dashFlags[sisenseApp.activeDashboard.$$id].currencyFlag;
	var dateFlag = window.dashFlags[sisenseApp.activeDashboard.$$id].dateFlag;

	// meta data // change metadata of that object for currency and date filters

	var filterJaql = [];
	var currency_filterJaql = null;
	var date_filterJaql = null;
	if(cur_code !='' && currencyFlag ==1 )  {
		var currency_filterJaql = {
         	'collapsed':true,
		 	'column':"CURRENCY_CODE",
			 'datatype':"text",
		 	'dim':"dbo.CURRENCY.CURRENCY_CODE",
			filter:{explicit: true, multiSelection: false, members: [cur_code]}
	    };
	}
	if(dateFlag == 1 && dateFilter !=''){
		var date_filterJaql = window.dashFlags[sisenseApp.activeDashboard.$$id].filterJaql;
		date_filterJaql.filter = dateFilter;
	}
  	sisenseApp.setFilter(currency_filterJaql,date_filterJaql)
}
/* sisense functions starts */
//	Function to set a filter
sisenseApp.setFilter = function(jaql1,jaql2) {

	//	Get the active dashboard, only run if sisense is loaded
	var dashboard = sisenseApp.activeDashboard;
	if (dashboard) {
		if (jaql1 || jaql2) {			
			//	Create the filter jaql
			/*var filterJaql = {
				jaql: jaql
			};
			*/
			var filterJaql=[];
			if(jaql1) {
				var obj1 = {jaql:jaql1};
				filterJaql.push({jaql:jaql1});
			}

			if(jaql2){
				filterJaql.push({jaql:jaql2});
			}
			//	Create the filter options
			var filterOptions = {
				save:true, 
				refresh:true, 
				unionIfSameDimensionAndSameType:false
			};

			//	Set the new filter
			sisenseApp.activeDashboard.$$model.filters.update(filterJaql,filterOptions);
		}
	}
}
//	Function to Instantiate the Sisense web app	
sisenseApp.getApp = function(callback,param) {
	var serverUrl = $("#sisense_url").val();
	if(window.callbackFlag > 5) {
		// after 5 callbacks logout
		window.location = '/user/logout';
	}
	window.callbackFlag++;
	if (typeof Sisense == "function") {
		//	Has it been instantiated?
		if (!sisenseApp.savedInstance) {
			Sisense.connect(serverUrl).then(function(app){		
				console.log('sisense loaded');
				//	Save the instance
				sisenseApp.savedInstance = app;

				//	Check for window resizes
				var resizeTimer;
				/*
				$(window).on('resize', function(e) {
				  clearTimeout(resizeTimer);
				  resizeTimer = setTimeout(function() {
					// Resizing has "stopped"
					sisenseApp.loadDashboard();
				  }, 250);
				});
				*/
				//	Run any callback functions
				if (callback) {
					setTimeout(function(){
						callback(param);
					},500);
				}
			});	
		} else {
			//	Return the instance
			return sisenseApp.savedInstance;	
		}	
	} else {
		//window.location.reload();
	}
	
};
/* sisense functions ends */
function convertDate(indDate) {
  var dateParts = indDate.split(/(\d{1,2})-(\d{1,2})-(\d{4})/);

  if(dateParts[3] != undefined) {
  	return dateParts[3] + "-" + dateParts[2] + "-" + dateParts[1];	
  } else {
  	var date = new Date();
	var formattedDate = moment(date).format('YYYY-MM-DD');
  	return formattedDate;
  }
} 
  function convertDateIST(dateVal){
  	var dateParts = dateVal.split(/(\d{4})-(\d{1,2})-(\d{1,2})/);

  	return dateParts[3] + "-" + dateParts[2] + "-" + dateParts[1];
  }

function getDateJaqls(globalFlag){
	var customStr = "from";
	var filterObj = $(".selected-period-filter").attr('data-filter');
	var dateFilter ='';
	var globalFlag = globalFlag || '';
	if(filterObj){
		dateFilter = JSON.parse(filterObj);
		var filterStr = JSON.stringify(filterObj);
		if(filterStr.indexOf(customStr) !== -1){
			dateFilter.from ='';
			dateFilter.to ='';

			var from = $("input[name='daterangepicker_start']").val();
			var to =$("input[name='daterangepicker_end']").val();
			if(from == ''){
			   if($("#picker_from_date").length){
				   from = $("#picker_from_date").val();
				   to = $("#picker_to_date").val();
				   dateFilter.from = from;
				   dateFilter.to = to;
			   }
			} else {
			   dateFilter.from = convertDate(from);
			   dateFilter.to = convertDate(to);
			}		
			if(globalFlag =='') {
				if($("#cus_from_date").length){
		        	startDate = $("#cus_from_date").val();
		        	dateFilter.from= startDate;
		        	startDate =convertDateIST(startDate);
		    	}
		    	if($("#cus_to_date").length){
		        	endDate = $("#cus_to_date").val();
		        	dateFilter.to =endDate;
		        	endDate =convertDateIST(endDate);
		    	}	
			}
			
		}	
	}
	return dateFilter;
}

// function to return currency and date filter flag 
function checkFilterFlag(jaqlJson){
	var currencyFlag =0;
	var dateFlag=0;
	var dateField='';
	var dateFieldInd =null;
	var dashFlags= {currencyFlag:0,dateFlag:0,filterJaql:'',curFieldInd:0,dateFieldInd:0};
	
	$.each(jaqlJson,function(i,filters){
		if(filters.jaql){
			if(filters.jaql.column == window.currencyStr){
				dashFlags.currencyFlag=1;
				dashFlags.curFieldInd = i;
			}
			if(jQuery.inArray(filters.jaql.column, window.dateFieldArr) !== -1) {
				dateField= filters.jaql.column;
				dashFlags.dateFlag=1;
				dashFlags.filterJaql= filters.jaql;
				dashFlags.dateFieldInd = i;
			}
		}
	});
	return dashFlags;
}
function getFilterStrFromJson(filterJson,filterFlagObj){
	//  var filterFlagArr = array('currencyFlag'=>1,'dateFlag'=>0);
	var jaqlJson = $.parseJSON(filterJson);
	var dashFlags = checkFilterFlag(jaqlJson);
	var uri_encode =''
	 if(dashFlags.currencyFlag == 1 || dashFlags.dateFlag ==1){
	 	// change the filter url 
	 	if(dashFlags.dateFlag ==1 && filterFlagObj.dateFlag ==1 ){
	 		var dateToApplyFilter = getDateJaqls();
	 		var dateInd = dashFlags.dateFieldInd;
	 		if(dateToApplyFilter ==''){
	 			dateFilterObj = $(".nofilter-period-filter:nth-child(3)").attr('data-filter'); //apply some default to prevent query browser error	
	 			$(".nofilter-period-filter:nth-child(3)").addClass('selected-period-filter');// default last 7 days
	 			dateToApplyFilter  = JSON.parse(dateFilterObj);
	 		}
	 		jaqlJson[dateInd].jaql.filter = dateToApplyFilter;
	 	}
	 	if(dashFlags.currencyFlag ==1 && filterFlagObj.currencyFlag == 1 ){
	 		var selectedCurrencyCode = $("select[name='currency_id'] option:selected")[1].text;
	 		var cur_code = (selectedCurrencyCode == 'Select') ? '' : selectedCurrencyCode;
	 		var curInd = dashFlags.curFieldInd;
	 		if(cur_code !=''){
	 			jaqlJson[curInd].jaql.filter.members = [cur_code];
	 		}
	 	}
	 }
	if(jaqlJson)
		uri_encode = encodeURIComponent(JSON.stringify(jaqlJson))

	 return uri_encode;
}