
/* common file for handling threads of user interactions */

function showThreadContainer(){
	
}

function commentsLoad(commentsJson){
    $('#comments-container').comments({
        getComments: function(success, error) {
             /*var commentsArray = [{
                id: 1,
                created: '2015-10-01',
                content: 'Lorem ipsum dolort sit amet',
                fullname: 'Simon Powell',
                upvote_count: 2,
                user_has_upvoted: false
            }];
            */
            var commentsArray = commentsJson;
            success(commentsArray);
    	}
	});
}
function loadThread(thread_id,content){
	//console.log( ' call '+thread_id+' '+Drupal.settings.basePath);
	$("#hdn_thread_id").val(thread_id);
	console.log(thread_id);
	if(thread_id == ''){
		$("#popup_data").data('json',content);
		formatThreadBlock(content);
	} else {
		$.ajax({
			type:'get',
			url:Drupal.settings.basePath+'comment_controller.php',
			dataType:'json',
			data:{action:'user_thread_info',thread_id:thread_id },
			success: function(data){
				//console.log(data);
				//console.log('txt'+$('#last_updated').text()+'|');
				
				formatThreadBlock(data);
			}
		});	
	}
	
}

function formatThreadBlock(data) {
	$(".tag-error-msg").text("");
	$("#superParentName").text(data.superParentName);
	$('#parent_name').text(data.parent_name);
	$("#name").text(data.name);
	$("#last_updated").text(data.last_updated);
	//$("#report_icon").attr('src',data.feed_image);
	$("#block_icon").attr('src',data.icon_src);
	$(".commenting-field main").addClass('message-content');
	$(".commenting-field main").addClass('widget-textfield');
	//$("#comment-container .widget-inner").html('');
  var totalComments = data.hasOwnProperty('comments') ? data.comments.length : 0;
  var totalTags = data.hasOwnProperty('tags') ? data.tags.length : 0;
  var commentsText = (totalComments > 1) ? (totalComments + " Comments") : (totalComments +" Comment");
  var userTagsText = (totalTags > 1) ? (totalTags + " Users") : (totalTags +" User");
	if(typeof(data.comments) !== 'undefined' && data.comments.length >0) {
		commentsLoad(data.comments);
		$("#commentsTxt").data('comments',data.comments.length);
	} else {
		commentsLoad([]);
	}
  $("#commentsTxt").text('('+ commentsText +')');
	if(typeof(data.tags) !== 'undefined' && data.tags.length > 0 ){
		$("#tagsTxt").data('tags',data.tags.length);
	}
   $("#tagsTxt").text('('+ userTagsText +' Tagged)');
	/*
	if(data.thread_id == '' && data.news_feed_flag != 1){
		// load an iframe and embed the dashboard 
		var dashboard_url= $("#embed_base_url").val()+data.dashboard_id+"?embed=true";
		//var html = "<iframe src='"+dashboard_url+"' style='width: 100%; height: 220px' scrolling='no' marginwidth='0' marginheight='0' frameborder='0' vspace='0' hspace='0'></iframe>";
		var currentElem = localStorage.getItem('footId');
		//var html = $(currentElem).parent().find('.threadBlock').html();
		//console.log(currentElem);
		$("#comment-container .widget-inner").html(html);
	}
	
	if( data.news_feed_flag == 1){
		var newsfeed="<div class='widget-image pull-left'> <img src='"+data.feed_image+"' /></div><div class='widget-contents pull-left'><h2><a href='"+data.link+"' target='_blank'>"+data.title+ "</a> <br/></h2> '"+ data.content+"'</p></div>";
		$("#comment-container .widget-inner").html(newsfeed);
	}

	*/
	$("#comment-container .widget-inner").html('');
	var currentElem = localStorage.getItem('footId');
	console.log("Foot id "+currentElem);
	var html = '';
	html = $(currentElem).parent().find('.threadBlock .widget-inner').html();
	$("#comment-container .widget-inner").html(html);

	/*
	{"superParentName":"Leaderboard","parent_name":"Air","name":"leaderboard Air Dashboard 1",
	"last_updated":"0","icon_src":"\/sites\/all\/themes\/multipurpose_zymphonies_theme\/images\/banner_1.png",
	"feed_image":"","content":"","thread_id":"1",
	"comments":[{"name":"naveen","comment_text":"ollo first comment","created_at":"2016-11-28 09:43:12"},{"name":"naveen","comment_text":"lets discuss at 12 today",
	"created_at":"2016-11-28 09:43:28"},{"name":"naveen","comment_text":"how did the discussion go today",
	"created_at":"2016-11-28 09:43:44"}
	*/
}

// add user comments from Jquery Comments Plugin
function addUserComments(comment){
	comment = $.trim(comment);
	var thread_id = $("#hdn_thread_id").val();
	var dataObj = {action:'addComments',thread_id:thread_id,'comment':comment };
	if(thread_id == '') {
		var content = $("#popup_data").data('json');
		dataObj['content']= content;
	}
	var setFields = 0;
	if($("#reportDashPopup").length){
		setFields =1;
	}
	$(".full_loader").show();
	$.ajax({
		type:'post',
		url: Drupal.settings.basePath+'comment_controller.php',
		data: dataObj,
		success: function(data){
			
			try {
				var response = $.parseJSON(data);
			} catch(ex){
				var response ={'error':1};
			}
			if(typeof response.error != 'undefined' && response.error == 1){
				$("#errorTxt").text(response.errorMsg);
				$("#errorTxt").show();
				$("#comment-list li").first().remove();
				$(".message-body").html(' <h4 >'+response.errorMsg+'</h4>');
			}else {
				var commentStr = '';
				var tagField=0;
				// reportDashPopup means its in browse Reports
				if($("#reportDashPopup").length){
					$commentsVal= $("#commentsTxt_popup").data('comments');
					$commentsVal=$commentsVal+1;
					$("#commentsTxt_popup").data('comments',$commentsVal);
					commentStr = getFormatedString($commentsVal,tagField);
					$("#commentsTxt_popup").text(commentStr);

				} else {
					$commentsVal= $("#commentsTxt").data('comments');
					$commentsVal=$commentsVal+1;
					$("#commentsTxt").data('comments',$commentsVal);
					commentStr = getFormatedString($commentsVal,tagField);
					$("#commentsTxt").text(commentStr);
				}

				var currentElem = localStorage.getItem('footId');
				if(thread_id =='' && typeof response.thread_id !=='undefined'){
					if(localStorage) {
						//$(currentElem).find(".hdnthr_id").val(response.thread_id);
						//$(currentElem).find(".block_data").data('json').thread_id=thread_id;
					}
					$("#hdn_thread_id").val(response.thread_id);
					//$("#commentsTxt").text(commentStr);
					//$("#hdn_thread_id").val(response.thread_id);
					//$("#popup_data").data('json').thread_id=response.thread_id;
					if(setFields == 0) {
						
						var block_data = $(currentElem).find('.block_data').data('json');
						// if its a insight or news feed remove current element portal box
						if( (typeof block_data.insight_flag != undefined && block_data.insight_flag ==1) || (typeof block_data.news_feed_flag != undefined && block_data.news_feed_flag ==1 )) {
							$(currentElem).find(".block_data").data('json').thread_id=response.thread_id;
							$(currentElem).find(".hdnthr_id").val(response.thread_id);
							
							$(currentElem).find(".comments").text(commentStr);
							console.log('thread +'+response.thread_id);
							
						} else {
							var html = $(currentElem).closest('.portalBox').html();
							var topElem =$(".contents-dash .content");
							topElem.prepend("<div class='widget-container portalBox'>"+html+'</div>');
							var newElem = $('.portalBox:first');
							//console.log('new');console.log($(newElem).attr('class'));
							//console.log('response');console.log(response);
							//console.log('thread id new one');console.log(response.thread_id);
							var totalInd = $(".portalBox").length;
							$(newElem).find(".hdn_ind").data('indices',totalInd);
							$(newElem).find(".block_data").data('json').thread_id=response.thread_id;
							$(newElem).find(".hdnthr_id").val(response.thread_id);
							$(newElem).find(".comments").text(commentStr);
							$(newElem).find(".widget-footer").attr('id','foot_'+totalInd);
							if(response.img_path !='') {
								$(newElem).find(".threadBlock").html("<div class='widget-inner'><img src='"+response.img_path+"' alt='' /> </div>");
							}
							var foot_str = '#foot_'+totalInd;
			                localStorage.setItem('footId',foot_str);
						}
					}
					
				}  else {
					//if(setFields == 1) {
						$("#commentsTxt").text(commentStr);
						$(currentElem).find(".comments").text(commentStr); 
					//}	
				}
				//$(currentElem).find(".comments").text(commentStr); 
			}
			$(".full_loader").hide();
//			scroll(0,0);
//			location.reload();
		}
	});

}

function bookmarking(thisElem){
	var thread_id =$(thisElem).parent().parent().find(".hdnthr_id").val();
    var content ='';
    var dataObj = {action:'addBookMarks',thread_id:thread_id};
	if(thread_id == '') {
		var content = $(thisElem).parent().parent().find(".block_data").data('json');
		dataObj['content']= content;
	}
	//console.log('while open ' + thread_id);
    //console.log((content)); 
	$.ajax({
		type:'post',
		url:'comment_controller.php',
		data: dataObj,
		success: function(data){
			var response = $.parseJSON(data);
			if(typeof response.error != 'undefined' && response.error == 1){
				$("#errorTxt").text(response.errorMsg);
				$("#errorTxt").show();
			}

			if(thread_id =='' && typeof response.thread_id !=='undefined'){
				$(thisElem).parent().parent().find(".hdnthr_id").val(response.thread_id);
				$(thisElem).parent().parent().find(".block_data").data('json').thread_id=thread_id;
			}
		}
	});   
}

function LoadMore(){
	var paginationObj = {};
	var pageName = $("#pgName").val();
	var lastInd =0;
	var totalInd = $(".portalBox").length;
	var boxObj = getTotalBoxes();
	var dashObj = getDashAdded();
	paginationObj.currentInd=totalInd;
	paginationObj.thread_starts=boxObj.threads;
	paginationObj.newsFeed_starts=boxObj.feeds;
	paginationObj.dashAdd= JSON.stringify(dashObj); 
	
	// here get thread start and news feed start vals 
	// get more items and append to Content 
	$.ajax({
		type:'get',
		url:'comment_controller.php',
		data:{'action':'getMoreBlocks','paginationObj':paginationObj,'pageName':pageName},
		success:function(data){
			$(".loadMoreBlk").append(data);
			$(".loading-widget").hide();
			getDashboards();
		}
	})
}
function getTotalBoxes(){
	var noThreads=noDash=noFeed=0;
	var data='';
	$(".block_data").each(function(i,obj){
		data=$(obj).data('json');
		if(isInt(data.thread_id)){
			noThreads++;
		} else {
			if(typeof data.news_feed_flag !=='undefined' && data.news_feed_flag ==1){
				noFeed++;
			} else {
				noDash++;
			}
		}
	});

	return {'threads':noThreads,'dashs':noDash,'feeds':noFeed};
}
function getDashAdded(){
	var dashObj={};
	var data='';
	$(".block_data").each(function(i,obj){
		data=$(obj).data('json');
		if(!isInt(data.thread_id) && typeof data.dashboard_id !== 'undefined'){
			//if(typeof data.dashboard_id !== 'undefined'){
				dashObj[i]=data.dashboard_id;
			//}
		}
	});
	return dashObj;
}
function isInt(value) {
  return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
}
function timeSince(date) {

    var seconds = Math.floor((new Date() - date) / 1000);

    var interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
        return interval + " years";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
        return interval + " months";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
        return interval + " days";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
        return interval + " hours";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
        return interval + " minutes";
    }
    return Math.floor(seconds) + " seconds";
}

function getUserProfileImage(){
	//var thread_id = $("#hdn_thread_id").val();
	
	$.ajax({
		type:'post',
		url: Drupal.settings.basePath+'comment_controller.php',
		dataType:"json",
		data:{action:'getUserProfileImage'},
		success: function(data){
		
			if(data==null){
				console.log('Invalid User');
			}else{
			
				$('#userProfile').val(data['file']);
			}
			
		}
	});
}


function filterText(text){
	var filter='';
	$.ajax({
		type:'post',
		url:Drupal.settings.basePath+'comment_controller.php',
		dataType:"json",
		async: false,
		data:{action:'filterText',text:text},
		success: function(data){
			
			filter=data;
			
		}
		
	});
	return filter;
}



