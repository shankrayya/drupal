<?php



function sortByOrder($a, $b) {
	return (strtotime($a['created_at']) - strtotime($b['created_at']));
}


function get_new_comment_id(){

	global $user;
	$subcommentResult =db_select('comments', 'c');
	$subcommentResult->join('custom_notification', 'n', 'n.thread_id = c.thread_id');
	$subcommentResult->join('user_threads', 'ut', 'n.thread_id = ut.thread_id');
	
	$subcommentResult->condition('c.user_id', array($user->uid),'NOT IN');
	
	$db_or = db_or();
	$db_or->condition('ut.user_id',$user->uid,'=');
	$db_or->condition('n.user_id', $user->uid,'=');
	$subcommentResult->condition($db_or);
	$subcommentResult->addExpression('MAX(c.comment_id)', 'comment_id');
	$subcommentResult->groupBy('c.thread_id');
	$subcommentResult=$subcommentResult->execute();
	$subCommentId=array();
	$i=0;
	foreach ($subcommentResult as $c){
		$subCommentId[$i++]=$c->comment_id;
	}
	return  $subCommentId;
	
}

function get_user_notification( $notificationPage=0){
	
	global $user;
	$start=0;
	$end=5;
	$resultArray=array();
/*
	$tagResult = db_select('custom_notification','n');
	$tagResult->join('user_tags', 't', 'n.thread_id = t.thread_id');
	$tagResult->join('user_threads', 'ut', 't.thread_id = ut.thread_id');
	$tagResult->join('user_profile', 'p', 'p.user_id = t.tagged_by');
	
	$tagResult->fields('n', array('user_id','thread_id', 'tag_seen'));
	$tagResult->fields('t', array('tag_id', 'tagged_by','created_at'));
	$tagResult->fields('ut', array('block_name_1','block_name_2', 'block_name_3'));
	$tagResult->fields('p', array('user_full_name'));
	$tagResult->condition('n.tag_seen','0','=');
	$tagResult->condition('n.user_id', $user->uid,'=');
	$tagResult->condition('t.tagged_by', array($user->uid),'NOT IN');
	$tagResult->condition('t.user_id',$user->uid,'=');
	$tagResult	->range($start,$end);
	$tagResult=$tagResult->execute();

	
	$ind=0;

	$userTag=1;
	$tagArr=display_notification($tagResult,$ind, $userTag);
	$resultArray=array_merge($resultArray,$tagArr);
*/
	
	$subCommentId=get_new_comment_id();
	
	$commentResult =db_select ('custom_notification', 'n');
	$commentResult->join('user_threads', 'ut', 'n.thread_id = ut.thread_id');
	$commentResult->leftjoin('user_tags', 't', 'n.thread_id = t.thread_id ');
	$commentResult->leftjoin('comments', 'c', 'c.thread_id = n.thread_id AND c.user_id !='.$user->uid);
	
	$commentResult->leftjoin('user_profile', 'p', ' p.user_id = t.tagged_by ');
	$commentResult->leftjoin('user_profile', 'pc', ' pc.user_id = c.user_id ');
	
	$commentResult->distinct('n.notification_id');
	$commentResult->fields('ut', array('thread_id','block_name_1','block_name_2', 'block_name_3','last_commented_time'));
	$commentResult->fields('c', array('user_id','comment_id'));
	$commentResult->fields('p', array('user_full_name'));
	$commentResult->fields('pc', array('user_full_name'));
	$commentResult->fields('n', array('user_id','thread_id', 'tag_seen','comment_id'));
	$commentResult->fields('t', array('tag_id', 'tagged_by','created_at'));
//  $commentResult->condition('t.tagged_by', array($user->uid),'NOT IN');
	$commentResult->condition('n.user_id',$user->uid,'=');
	
//	$commentResult->condition('t.user_id',$user->uid,'=');
	//$commentResult->condition('c.user_id', array($user->uid),'NOT IN');
	//$commentResult->condition('t.tagged_by', array($user->uid),'NOT IN');
	
//	
	$db_or = db_or();
	if($subCommentId){
		$db_or->condition('c.comment_id', $subCommentId,'IN');
	}
	//$commentResult->condition('t.tagged_by', array($user->uid),'NOT IN');
	$db_or->condition('t.user_id',$user->uid,'=');
	$db_or->condition('ut.user_id',$user->uid,'=');
	//$db_or->condition('t.user_id',$user->uid,'=');
//	$db_or->condition('ut.user_id',$user->uid,'=');
	$commentResult->condition($db_or);
	$commentResult->addExpression('COUNT(c.comment_id)', 'ncount');
	$commentResult->groupBy('thread_id');
	if( $notificationPage==1){
		$commentResult	->range($start,$end);
	}

	$commentResult=$commentResult->execute();
	$userTag=0;
	//print_r($commentResult);
	$commentArr=display_notification($commentResult,$ind, $userTag);
	$resultArray=array_merge($resultArray,$commentArr);

	$field='created_at';
//	$resultArray=sort_by_time($resultArray,$field);
	usort($resultArray, 'sortByOrder');
	if( $notificationPage==1){
		$resultArray=array_slice($resultArray, $start, $end);
	
	}
	//print_r($resultArray);
	
	return $resultArray;

}

function check_new_threads(){
	
	global $user;
	
	$subCommentId=get_new_comment_id();
	
	$res = db_select('custom_notification','n');
	$res->leftjoin('comments', 'c', 'n.thread_id = c.thread_id');
	$res->fields('n', array('notification_id','user_id','thread_id', 'comment_id', 'tag_seen'));
	$res->fields('c', array('comment_text','comment_id'));
	
	$res->condition('n.user_id',$user->uid,'=');
	//$db_or = db_or();
	
	if($subCommentId){
		
		$res->condition('c.comment_id', $subCommentId,'IN');
	}
	
	//$res->condition($db_or);
	$res=$res->execute();

	$newCommentsCount = 0;
	foreach($res as $r){
	
		if($r->c_comment_id > $r->comment_id  ){
			$newCommentsCount++;
			
		}
		if($r->tag_seen == 0 ){
			$newCommentsCount++;
		}
		
	}
	//print_r("comments id ".  $newCommentsCount);
	return $newCommentsCount;

}
function update_seen_notification($threadId){
	global $user;
	$res = db_select('comments','c');
	$res->addExpression('MAX(c.comment_id)', 'comment_id');
	//$res->condition('c.user_id',$user->uid,'=');
	$res->condition('c.thread_id',$threadId,'=');
	$res=$res->execute();
	foreach($res as $r){
	//	print_r($r);
		$update=db_update('custom_notification');
		if($r->comment_id){
			//print_r( "commemt ".$threadId);
			$commentId=$r->comment_id;
			$update->fields(array('comment_id'=>$commentId,'tag_seen'=>'1'));
		
		}else{
			$update->fields(array('tag_seen'=>'1'));
		}
		$update->condition('thread_id', $threadId)
				->condition('user_id', $user->uid)
				->execute(); 
	}
	return true;
	
}