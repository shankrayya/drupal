<?php 
function user_bookmarks_add($thread_id,$content){
	global $user;
	if($thread_id == ''){
		list($thread_id,$img_path) = user_thread_create($content);
	}
	if($thread_id !=''){
		$comment = db_insert('user_bookmarks')
		->fields(array(
		    'thread_id' => $thread_id,
		    'user_id' => $user->uid,
		    'created_at'=> date("Y-m-d H:i:s", time()) 
		  ))
		->execute();
		return array('thread_id'=>$thread_id,'img_path'=>$img_path);
	} else {
	return array('error'=>1,'errorMsg'=>'Comment saving failed');
	}
}
?>