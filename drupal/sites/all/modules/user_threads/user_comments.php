<?php

/* function to return the comments of a thread */
function user_comments_get($thread_id){
	global $user;
	global $base_url;
	$commentsArr =array();
	$ind=0;
	
	if($thread_id == '' )
	return $commentsArr; 

  	$results = db_query('
      SELECT a.thread_id,b.uid, a.comment_id,a.user_id, a.comment_text, a.created_at, b.name FROM {comments} a 
  		INNER JOIN {users} b on a.user_id =b.uid 
      WHERE a.thread_id = :thread_id AND b.community_id = :community_id  ORDER BY created_at ',array(':thread_id'=>$thread_id,  ':community_id'=>$user->community_id));
  	foreach ( $results as $comments ) {
  		
  		$commentsArr[$ind]['id']=$comments->comment_id;
  		$commentsArr[$ind]['user_id']=$comments->uid;
  		
  		$commentsArr[$ind]['fullname']=$comments->name;
  		$commentsArr[$ind]['content']=$comments->comment_text;
		$created_at = convertTimeZone($comments->created_at);
        $commentsArr[$ind]['created']= convertDispTimeElapsed($created_at);

  		$account = user_load($comments->uid);
  		if($account){
  			$filepath='';
  		
  			if (is_numeric($account->picture)) {
  				$account->picture = file_load($account->picture);
  			}
  				
  			if (!empty($account->picture->uri)) {
  				$filepath = $account->picture->uri;
  				$filepath=file_create_url( $filepath);
  					
  			}else{
  				$filepath=$base_url.'/sites/all/themes/multipurpose_zymphonies_theme/images/user-profile.png';
  					
  			}
  		}else{
  			$filepath=null;
  		}
  		$commentsArr[$ind]['profile_picture_url']=$filepath;
  		$ind++;
  	}	
	return $commentsArr;
}
/* function to add comments to thread  */
function user_comments_add($thread_id,$comment,$content=array()){
	
	global $user;

  if($thread_id == ''){
	$p_tag=0;
    list($thread_id,$img_path) = user_thread_create($content,$p_tag);
		$tag_seen=1;

		//$notification=create_user_notification($thread_id,$user->uid,$tag_seen=1);
	}
	/*
	else if($thread_id !=''){
		$tag_seen=0;
		$data=get_user_notification_data($thread_id,$user->uid);
		foreach($data as $d){
			if($d){
				db_update('custom_notification')
				->fields(array('tag_seen'=>'1'))
				->condition('thread_id', $thread_id,'=')
				->condition('user_id', $user->uid,'=')
				->execute(); 
			
			}
		}
		 
	}
	*/

  if($thread_id !=''){
    $lastUpdated = @gmdate('Y-m-d H:i:s');
   //$comments= _phonetic_apply_filter($comment);
   
    $comment = db_insert('comments')
    ->fields(array(
        'thread_id' => $thread_id,
        'user_id' => $user->uid,
        'comment_text' => $comment,
        'created_at'=> $lastUpdated 
      ))
    ->execute();
  db_update('user_threads')
  ->fields(array('last_commented_by'=>$user->uid,'last_commented_time'=>$lastUpdated,'updated_at'=>$lastUpdated))
  ->expression('no_comments', 'no_comments + 1')
  ->condition('thread_id', $thread_id)
  ->execute();
  
	//update_seen_notification($thread_id);
	
    return array('thread_id'=>$thread_id,'img_path'=>$img_path);  
  } else {
    return array('error'=>1,'errorMsg'=>'Comment saving failed');
  }
	
}




function get_user_profile_image(){

	global $base_url;
	global $user;
	$data=array();
	$account = $user;
	if($account){
		$filepath='';

		if (is_numeric($account->picture)) {
			$account->picture = file_load($account->picture);
		}
			
		if (!empty($account->picture->uri)) {
			$filepath = $account->picture->uri;
			$filepath=file_create_url( $filepath);
				
		}else{
			$filepath=$base_url.'/sites/all/themes/multipurpose_zymphonies_theme/images/user-profile.png';
				
		}
	}else{
		$filepath=null;
	}
	$data['file']=$filepath;
	$data['user_id']=$user->uid;

	return $data;
}



function getCommunityChatEnabled(){
	
	global $user;
	
	$results = db_query('
      SELECT u.is_chat_enabled FROM {community} u
      WHERE  u.community_id = :community_id ',array( ':community_id'=>$user->community_id));
	
	foreach($results as $res){
		return $res->is_chat_enabled;
	}
	
}

?>