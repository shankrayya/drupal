<?php 

function get_user_tags($thread_id){
	global $user;
	global $base_url;
	$tagsArr =array();
	$ind=0;
	if($thread_id == '' )
	return $tagsArr; 
	

  	$results = db_query('
      SELECT a.thread_id, a.user_id, a.created_at, b.name , b.picture , c.user_full_name FROM {user_tags} a 
  	  LEFT JOIN  {users} b on a.user_id =b.uid 
  	  LEFT JOIN {user_profile} c on c.user_id =b.uid 
      WHERE a.thread_id = :thread_id AND b.community_id = :community_id ORDER BY created_at ',array(':thread_id'=>$thread_id , ':community_id'=>$user->community_id));
	  	foreach ( $results as $tags ) {
	  		$filepath="";
	  		if (is_numeric($tags->picture)) {
	  			$tags->picture = file_load($tags->picture);
	  		}
	  		 
	  		if (!empty($tags->picture->uri)) {
	  			$filepath = $tags->picture->uri;
	  			$filepath=file_create_url( $filepath);
	  		}else{
	  			$filepath=$base_url.'/sites/all/themes/multipurpose_zymphonies_theme/images/user-profile.png';
	  		}
			$name=$name=web_reduce_name($tags->user_full_name,$maxlength);
			
			$tagsArr[$ind]['display_name']=$name;
	  		$tagsArr[$ind]['user_full_name']=$tags->user_full_name;
	  		$tagsArr[$ind]['user_id']=$tags->user_id;
	  		$tagsArr[$ind]['file']= $filepath ;
	  		$tagsArr[$ind]['created_at']= convertDispTimeElapsed($tags->created_at);
	  	
	  		$ind++;
	  	}
  
	return $tagsArr;
	
}

function get_users(){
	global $user;
	$tagsArr =array();
	$tagsFinalArr =array();
	$ind=0;
	
		
		$results = db_query('
      	SELECT  distinct u.uid, u.name , up.user_full_name FROM  {users} u
			LEFT JOIN  {user_profile} up on up.user_id =u.uid
		
			WHERE  u.uid !=:uid AND u.community_id = :community_id',array(':uid'=>$user->uid, ':community_id'=>$user->community_id));
				foreach($results as $res){
					$tagsArr[$ind]['user_id']=$res->uid;
		
					$tagsArr[$ind]['user_full_name']= ($res->user_full_name == '') ? $res->name : $res->user_full_name;
					$ind++;
				}

		
	
	$tagsFinalArr=$tagsArr;

	
	return $tagsFinalArr;
}

function get_user_image($user_id){

	global $base_url;
	$data=array();
	$account = user_load($user_id);
	$userCommunity = db_query('SELECT up.user_full_name FROM {user_profile} up WHERE  up.user_id = :uid',array(':uid'=>$account->uid));
	foreach($userCommunity as $userC){
		if($userC->user_full_name){
			$len=strlen($userC->user_full_name);
			if($len>=15){ 
				$name= substr($userC->user_full_name,0,15).'...';
			}else{
				$name= $userC->user_full_name;
			} 

			$data['user_full_name']=$userC->user_full_name;
			$data['display_name']=$name;
		}
		
	}
	 
	if($account){
		$filepath='';
		
		if (is_numeric($account->picture)) {
			$account->picture = file_load($account->picture);
		}
		 
		if (!empty($account->picture->uri)) {
			$filepath = $account->picture->uri;
			$filepath=file_create_url( $filepath);
			
		}else{
			$filepath=$base_url.'/sites/all/themes/multipurpose_zymphonies_theme/images/user-profile.png';
			
		}
	}else{
		$filepath=null;
	}
	$data['file']=$filepath;
	$data['user_id']=$user_id;

	 
						
	return $data;
}

/* function to add comments to thread  */
function add_user_tag($thread_id,$user_id,$content=array()){
	global $user;
//print_r("user id".$user_id);
	if($thread_id == ''){
		$p_tag=1;
		list($thread_id,$img_path)= user_thread_create($content,$p_tag);
	
		$tag_seen=1;
		/*
		$notification=create_user_notification($thread_id,$user_id,$tag_seen=0);
		$notification=create_user_notification($thread_id,$user->uid,$tag_seen=1);
		*/
	}
	/*
	else if($thread_id !=''){
		$tag_seen=0;
		
		//$data=get_user_notification_data($thread_id,$user_id);
		foreach($data as $d){
			if($d ){
				if( $d->ut_user_id == $user_id){
					$tagSeen=0;
				}else{
					$tagSeen=1;
				}
				db_update('custom_notification')
				->fields(array('tag_seen'=>$tagSeen,'updated_time'=>date("Y-m-d H:i:s", time())))
				->condition('thread_id', $thread_id)
				->condition('user_id', $user_id)
				->execute(); 
			
			}else{
				$notification=create_user_notification($thread_id,$user_id,$tag_seen=0);
			}
		}
		
	}
	*/
	$account=null;
	
	$account = user_load($user_id);

	if($thread_id !=''){
		$created_at = @gmdate('Y-m-d H:i:s');
		$tags = db_insert('user_tags')
		->fields(array(
				'thread_id' => $thread_id,
				'user_id' => $account->uid,
				'tagged_by' => $user->uid,
				
				'created_at'=> $created_at
		))
		->execute();
		
	db_update('user_threads')
	->fields(array('updated_at'=>$created_at))
	->expression('no_tags', 'no_tags + 1')
	->condition('thread_id', $thread_id)
	->execute();
	
	return array('thread_id'=>$thread_id,'img_path'=>$img_path);
	/*
	if($notification){
		
	}else{
		
		return array('error'=>1,'errorMsg'=>'tag saving failed 2');
	}
	*/
  } else {
	  
    return array('error'=>1,'errorMsg'=>'tag saving failed');
  }
	
}


function remove_user_tag($thread_id,$user_id){
	global $user;
	$account =user_load($user_id);
	$tag = db_delete('user_tags')
	->condition('user_id', $account->uid)
	->condition('tagged_by', $user->uid)
	->condition('thread_id', $thread_id)
	
	->execute();
	
	
	db_update('user_threads')
	->fields(array('updated_at'=>date("Y-m-d H:i:s", time())))
	->expression('no_tags', 'no_tags - 1')
	->condition('thread_id', $thread_id)
	->execute();
}


