<?php 
error_reporting(E_ERROR);
/**
* Implements hook_block_view().
*/
function user_management_manage_community_block_view($delta = '') {
    global $user;
    $userProfileInfo = getAllGroupPccCommunityForUser($user->uid);
    $defaultCommunity = NULL;
    $isCommunityAdmin = in_array('Community Admin', $user->roles);
    if($isCommunityAdmin)
      $defaultCommunity = $userProfileInfo['community_id'];
    $tableHeader = array(t('Commmunity Id'), t('Commmunity Name'), t('Community Logo'), t('Community Type'),
                         t('Travel Agency'), t('Edit'));
    $addCommunityContent = l('Add New Commmunity Details', '/add/community') . "<br><br>";
    if(!$isCommunityAdmin) {
      $tableHeader[] = t('Delete');
    }
    
    $allCommunityData = db_select('community', 'c');
    $allCommunityData->fields('c', array('community_id', 'community_name', 'community_logo', 'travel_agency_id'));
    $allCommunityData->fields('ct', array('community_type_display_name'));
    $allCommunityData->fields('tr', array('community_name'));
    $allCommunityData->leftJoin('community_types', 'ct', 'c.community_type = ct.community_type_id');
    $allCommunityData->leftJoin('community', 'tr', 'c.travel_agency_id = tr.community_id');
    $allCommunityData = $allCommunityData->execute();
    $allResults = array();
    while($data = $allCommunityData->fetchAssoc()) {
        $url = file_create_url($data['community_logo']);
        $fileUrl = t("Not Available");
        $data['tr_community_name'] = $data['tr_community_name'] == '' ||
          $data['tr_community_name'] === NULL ? t('Not Applicable') : $data['tr_community_name'];
        if($data['community_logo'] !== NULL
           && file_exists(drupal_realpath($data['community_logo'])))
          $fileUrl = l('View Logo', file_create_url($data['community_logo']), array('attributes' => array('target' => '_blank')));
          
        $data['community_logo'] = $fileUrl;
        $data['edit_link'] = l('Edit Commmunity', '/edit/community', array('query' => array('id' => $data['community_id'])));
        if(!$isCommunityAdmin) {
          $data['delete_link'] = l('Delete Commmunity', '/delete/community', array('query' => array('id' => $data['community_id'])));
        }
        if($isCommunityAdmin && $defaultCommunity != $data['community_id'] && $defaultCommunity != $data['travel_agency_id'])
          continue;
        unset($data['travel_agency_id']);
        $allResults[] = $data;
    }
    $tableContentRows['header'] = $tableHeader; 
    count($allResults) > 0 ? ($tableContentRows['rows'] = $allResults) : '';
    
    $tableContents = theme('table', $tableContentRows);
    $tableContents = $addCommunityContent . $tableContents;
    $blocks['content'] = $tableContents;
    return $blocks;   
}

/**
* Implements hook_block_view().
*/
function user_management_add_community_block_view() {
    $form = drupal_get_form('user_management_add_community_form');
    $block['content'] = $form;
    return $block;
}

function user_management_add_community_form($form, &$form_state) {
    global $user;
    $isCommunityAdmin = in_array('Community Admin', $user->roles);
    $userProfileInfo = getAllGroupPccCommunityForUser($user->uid);
    $allTravelAgentDetails = array();
    $travelAgentDetails = db_select('community', 'c')
    ->fields('c', array('community_id', 'community_name'));
    $travelAgentDetails->join('community_types', 'ct', "ct.community_type_id = c.community_type and ct.community_type_code = 'travel_agency'");
    $travelAgentDetails = $travelAgentDetails->execute();
    while($row = $travelAgentDetails->fetchAssoc()) {
        $allTravelAgentDetails[$row['community_id']] = $row['community_name'];
    }
    
    $allCommunityTypeDetails = array();
    $communityTypeDetails = db_select('community_types', 'ct')
        ->fields('ct', array('community_type_id', 'community_type_display_name', 'community_type_code'))
        ->execute();
    while($row = $communityTypeDetails->fetchAssoc()) {
        $allCommunityTypeDetails[$row['community_type_code']] = $row['community_type_display_name'];
    }
    $defaultCommunityType = '';
    $isCommunityTypeDisabled = FALSE;
    if($isCommunityAdmin) {
      $defaultCommunityType = 'corporate';
    }
    $disabledAttr = $isCommunityAdmin ? array('disabled' => TRUE) : array();
    $form['community_name'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#title' => t("Community Name"),
        '#attributes' => array(
            'placeholder' => t('Enter Community Name')
        )
    );
    $form['community_address'] = array(
        '#type' => 'textarea',
        '#title' => 'Address',
        '#attributes' => array(
            '#placeholder' => t('Enter Address')
        )
    );
    $form['community_phone_number'] = array(
        '#type' => 'textfield',
        '#title' => 'Phone Number',
        '#attributes' => array(
            '#placeholder' => t('Enter Phone Number')
        )
    );
    $form['community_website'] = array(
        '#type' => 'textfield',
        '#title' => 'Website',
        '#attributes' => array(
            '#placeholder' => t('Enter Website')
        )
    );
    $form['community_logo_file'] = array(
        '#type' => 'file',
        '#title' => t('Select Community Logo')
    );
    $form['community_type'] = array(
        '#type' => 'select',
        '#options' => $allCommunityTypeDetails,
        '#required' => 'required',
        '#default_value' => $defaultCommunityType,
        '#attributes' => $disabledAttr,
        '#title' => 'Select Community Type',
    );
    
    $form['travel_agency_id'] = array(
        '#type' => 'select',
        '#options' => $allTravelAgentDetails,
        "#empty_option"=>t('- Select -'),
        '#title' => 'Select Travel Agency',
        '#default_value' => $userProfileInfo['community_id'],
        '#attributes' => $disabledAttr,
        '#states' => array(
            'visible' => array(
              "select[name*='community_type']" => array('value' => 'corporate'),
            ),
            'required' => array(
              'select[name*="community_type"]' => array('value' => 'corporate'),
            )
        )
    );
    
    $form['community_email_domains'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#title' => 'Email Domains',
        '#attributes' => array(
            '#placeholder' => t('Enter Email Domains')
        )
    );
    $form['community_dk_number'] = array(
        '#type' => 'textfield',
        '#title' => 'Spark Reference (DK Number)',
        '#attributes' => array(
            '#placeholder' => t('Enter DK Number')
        ),
        '#states' => array(
            'visible' => array(
              "select[name*='community_type']" => array('value' => 'corporate'),
            ),
            'required' => array(
              'select[name*="community_type"]' => array('value' => 'corporate'),
            )
        )
    );
    $form['is_chat_enabled'] = array(
            '#type' => 'checkbox',
            '#default_value' => 1,
            '#title' => t('Enable Community Chat'),
    );


    if(isSuperAdmin()) {
        $form['change_password'] = array(
            '#type' => 'checkbox',
            '#default_value' => 0,
            '#title' => t('Add Spark Password'),
        );
         $form['sisense_community_pwd'] = array(
            '#type' => 'textfield',
            '#title' => 'Add Spark Password',
            '#attributes' => array(
                '#placeholder' => t('Enter password')
            ),
            '#description' => 'Please be carefull when changing spark password for community and make the changes in spark before its done here',
            '#states' => array(
              'visible' => array(   // action to take.
                ':input[name="change_password"]' => array('checked' => TRUE),
                )
            )
        );

    }

    $form['community_form_submit'] = array(
        '#type' => 'submit',
        '#value' => 'Add Community Details',
    );
    return $form;
}

function user_management_add_community_form_validate($form, &$form_state) {
    $communityName = $form_state['values']['community_name'];
    $result = db_select('community', 'c')
    ->fields('c', array('community_name'))
    ->condition('community_name', $communityName, '=')
    ->execute();
    $validators = array(
      'file_validate_size' => array(1024 * 1024),
      'file_validate_extensions' => array('png jpg jpeg gif'), // Validate extensions.
    );
    $destinationUrl = file_build_uri('community_logo');
    if(!file_exists(drupal_realpath($destinationUrl)))
      file_prepare_directory(drupal_realpath($destinationUrl), FILE_CREATE_DIRECTORY);
    $logoFile = file_save_upload('community_logo_file', $validators, $destinationUrl, FILE_EXISTS_RENAME);
    $phoneNumberMatch = preg_match('/^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/', $form_state['values']['community_phone_number']);
    $emailDomainsList = explode(",", $form_state['values']['community_email_domains']);
    $isEmailDomainsValid = TRUE;
    foreach($emailDomainsList as $domain) {
      if(!preg_match('/([a-zA-Z\d]+:\/\/)?(\w+:\w+@)?([a-zA-Z\d.-]+\.[A-Za-z]{2,4})(:\d+)?(\/.*)?/', $domain)) {
        $isEmailDomainsValid = FALSE;
        break;
      }
    }
    if(trim($communityName) == '') 
        form_set_error('community_name', 'Community Name Cannot be Empty');
    if(!$logoFile)
        form_set_error('community_logo_file', 'Invalid or No File Selected');
    if($result->rowCount() > 0)
        form_set_error('community_name', $communityName .' already Exists');
    if(!empty($form_state['values']['community_phone_number']) && !$phoneNumberMatch)
        form_set_error('community_phone_number', 'Invalid Phone Number');
    if(!empty($form_state['values']['community_website']) &&
       !preg_match('/([a-zA-Z\d]+:\/\/)?(\w+:\w+@)?([a-zA-Z\d.-]+\.[A-Za-z]{2,4})(:\d+)?(\/.*)?/', $form_state['values']['community_website']))
        form_set_error('community_website', 'Invalid Website Entered');
    if($form_state['input']['community_type'] == 'corporate' && (!isset($form_state['input']['travel_agency_id'])
      || $form_state['input']['travel_agency_id'] === NULL || !ctype_digit($form_state['input']['travel_agency_id'])))
       form_set_error('travel_agency_id', 'Please Select Travel Agency');
    if(!$isEmailDomainsValid)
        form_set_error('community_email_domains', 'Text Contains one or more invalid email Domains');
    if(count($emailDomainsList) > 10)
        form_set_error('community_email_domains', 'Maximum of 10 email domains are allowed.');
    if($form_state['input']['community_type'] == 'corporate' && preg_match('/[^0-9A-Za-z-]/', $form_state['values']['community_dk_number']))
        form_set_error('community_dk_number', 'Only Alphanumeric characters and "-" allowed in Community DK Number');
}

function user_management_add_community_form_submit($form, &$form_state) {
    $validators = array(
      'file_validate_size' => array(1024 * 1024),
      'file_validate_extensions' => array('png jpg jpeg gif'), // Validate extensions.
    );
    $destinationUrl = file_build_uri('community_logo');
    $logoFile = file_save_upload('community_logo_file', $validators, $destinationUrl, FILE_EXISTS_RENAME);
    if($logoFile) {
      $logoFile->status = FILE_STATUS_PERMANENT;
      file_save($logoFile);
    }
    $result = db_select('community_types', 'ct')
    ->fields('ct', array('community_type_id'))
    ->condition('community_type_code', $form_state['values']['community_type'], '=')
    ->execute()
    ->fetchAssoc();
    $communityType = count($result) > 0 ? $result['community_type_id'] : NULL;
    $travelAgencyId = isset($form_state['values']['travel_agency_id']) ? $form_state['values']['travel_agency_id'] : NULL;
    $result = db_insert('community')
    ->fields(
        array(
            'community_name' => $form_state['values']['community_name'],
            'community_logo' => $logoFile->uri,
            'is_chat_enabled'=> $form_state['values']['is_chat_enabled'],
            'community_address'=> $form_state['values']['community_address'],
            'community_phone_number'=> $form_state['values']['community_phone_number'],
            'community_website'=> $form_state['values']['community_website'],
            'community_type'=> $communityType,
            'community_email_domains'=> $form_state['values']['community_email_domains'],
            'dk_number'=> $form_state['values']['community_dk_number'],
            'travel_agency_id' => $travelAgencyId
        )
    )
    ->execute();
    if($result) {
        $communityId = $result;
        // update sisense password for community starts 
        if(isset($form_state['input']['change_password']) && $form_state['input']['change_password'] == 1){
            if(trim($form_state['input']['sisense_community_pwd']) !=''){
                $pwd = trim($form_state['input']['sisense_community_pwd']);
                change_community_spark_pwd($communityId,$pwd);
            }
        }

        // change ends
        drupal_set_message(t("New Community Added."));
        drupal_goto('manage/community');
    }

    
}


/**
* Implements hook_block_view().
*/
function user_management_edit_community_block_view() {
    $form = drupal_get_form('user_management_edit_community_form');
    $block['content'] = $form;
    return $block;
}

function user_management_edit_community_form($form, &$form_state) {
    global $user;
    $isCommunityAdmin = in_array('Community Admin', $user->roles);
    $userProfileInfo = getAllGroupPccCommunityForUser($user->uid);
    $communityId = $_GET['id'];
    
    $result = array();
    $allTravelAgentDetails = array();
    $travelAgentDetails = db_select('community', 'c')
    ->fields('c', array('community_id', 'community_logo', 'community_name', 'is_chat_enabled', 'community_address',
                        'community_phone_number', 'community_website', 'community_email_domains', 'dk_number',
                        'travel_agency_id', 'community_type'));
    $travelAgentDetails->fields('ct', array('community_type_code'));
    $travelAgentDetails->leftJoin('community_types', 'ct', "ct.community_type_id = c.community_type");
    $travelAgentDetails = $travelAgentDetails->execute();
    while($row = $travelAgentDetails->fetchAssoc()) {
        if($row['community_type_code'] == 'travel_agency')
            $allTravelAgentDetails[$row['community_id']] = $row['community_name'];
        $result = ($row['community_id'] == $communityId) ? $row : $result;
    }
    if($isCommunityAdmin && $communityId != $userProfileInfo['community_id'] && $userProfileInfo['community_id'] != $result['travel_agency_id'])
        exit('Access denied to edit Community Details !!');
    
    $allCommunityTypeDetails = array();
    $communityTypeDetails = db_select('community_types', 'ct')
    ->fields('ct', array('community_type_id', 'community_type_display_name', 'community_type_code'))
    ->execute();
    $selectedCommunityType = NULL;
    while($row = $communityTypeDetails->fetchAssoc()) {
        $allCommunityTypeDetails[$row['community_type_code']] = $row['community_type_display_name'];
        $selectedCommunityType = ($row['community_type_id'] == $result['community_type'])
        ? $row['community_type_code'] : $selectedCommunityType;
    }
    $fileUrl = NULL;
    if($result['community_logo'] !== NULL
           && file_exists(drupal_realpath($result['community_logo'])))
          $fileUrl = file_create_url($result['community_logo']);
    $imageLogoText = $fileUrl !== NULL ?
       '<img src ="'. file_create_url($fileUrl) .'" width="100">'
      : '<div class ="description">Logo not available !</div>';
    
    $defaultCommunityType = '';
    $isCommunityTypeDisabled = FALSE;
    if(!$isCommunityAdmin) {
      $defaultCommunityType = 'corporate';
    }
    $disabledAttr = $isCommunityAdmin ? array('disabled' => TRUE) : array();  
      
    
    $form['edit_community_name'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#value' => $result['community_name'],
        '#title' => t('Enter Commmunity Name'),
        '#attributes' => array(
            'placeholder' => t('Enter Commmunity Name')
        )
    );
    $form['edit_community_address'] = array(
        '#type' => 'textarea',
        '#title' => 'Address',
        '#value' => $result['community_address'],
        '#attributes' => array(
            '#placeholder' => t('Enter Address')
        )
    );
    $form['edit_community_phone_number'] = array(
        '#type' => 'textfield',
        '#value' => $result['community_phone_number'],
        '#title' => 'Phone Number',
        '#attributes' => array(
            '#placeholder' => t('Enter Phone Number')
        )
    );
    $form['edit_community_website'] = array(
        '#type' => 'textfield',
        '#value' => $result['community_website'],
        '#title' => 'Website',
        '#attributes' => array(
            '#placeholder' => t('Enter Website')
        )
    );
    $form['edit_community_logo_file'] = array(
        '#prefix' => $imageLogoText,
        '#type' => 'file',
        '#title' => t('Select Community Logo'),
        '#description' => 'Upload new image to change logo'
    );
    $form['edit_community_type'] = array(
        '#type' => 'select',
        '#options' => $allCommunityTypeDetails,
        '#default_value' => $selectedCommunityType,
        '#required' => 'required',
        '#attributes' => $disabledAttr,
        '#title' => 'Select Community Type',
    );
    
    $form['edit_travel_agency_id'] = array(
        '#type' => 'select',
        '#options' => $allTravelAgentDetails,
        '#default_value' => $result['travel_agency_id'],
        "#empty_option"=>t('- Select -'),
        '#title' => 'Select Travel Agency',
        '#attributes' => $disabledAttr,
        '#states' => array(
            'visible' => array(
              "select[name*='community_type']" => array('value' => 'corporate'),
            ),
            'required' => array(
              'select[name*="community_type"]' => array('value' => 'corporate'),
            )
        )
    );
    
    $form['edit_community_email_domains'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#title' => 'Email Domains',
        '#value' => $result['community_email_domains'],
        '#attributes' => array(
            '#placeholder' => t('Enter Email Domains')
        )
    );
    $form['edit_community_dk_number'] = array(
        '#type' => 'textfield',
        '#value' => $result['dk_number'],
        '#title' => 'Spark Reference (DK Number)',
        '#attributes' => array(
            '#placeholder' => t('Enter DK Number')
        ),
        '#states' => array(
            'visible' => array(
              "select[name*='community_type']" => array('value' => 'corporate'),
            ),
            'required' => array(
              'select[name*="community_type"]' => array('value' => 'corporate'),
            )
        )
    );
    $form['is_chat_enabled'] = array(
            '#type' => 'checkbox',
            '#default_value' => $result['is_chat_enabled'],
            '#title' => t('Enable Community Chat'),
    );
    
    if(isSuperAdmin()) {
        $form['change_password'] = array(
            '#type' => 'checkbox',
            '#default_value' => 0,
            '#title' => t('Change Spark Password'),
        );
         $form['sisense_community_pwd'] = array(
            '#type' => 'textfield',
            '#title' => 'Change Spark Password',
            '#attributes' => array(
                '#placeholder' => t('Enter password')
            ),
            '#description' => 'Please be carefull when changing spark password for community and make the changes in spark before its done here',
            '#states' => array(
              'visible' => array(   // action to take.
                ':input[name="change_password"]' => array('checked' => TRUE),
                )
            )
        );
    }
    $form['edit_community_form_submit'] = array(
        '#type' => 'submit',
        '#value' => 'Update Community Details',
    );
    $form_state['rebuild'] = TRUE;
    return $form;
}

function user_management_edit_community_form_validate($form, &$form_state) {
    $communityName = $form_state['input']['edit_community_name'];
    $communityId = $_GET['id'];
    $existingCommDetails = db_select('community', 'c')
    ->fields('c', array('community_name', 'community_id', 'is_chat_enabled', 'community_logo'))
    ->condition('community_id', $communityId, '=')
    ->execute()
    ->fetchAssoc();
    $result = db_select('community', 'c')
    ->fields('c', array('community_name', 'community_id', 'is_chat_enabled', 'community_logo'))
    ->condition('community_name', $communityName, '=')
    ->execute()
    ->fetchAssoc();
    $validators = array(
      'file_validate_size' => array(1024 * 1024),
      'file_validate_extensions' => array('png jpg jpeg gif'), // Validate extensions.
    );
    $destinationUrl = file_build_uri('community_logo');
    if(!file_exists(drupal_realpath($destinationUrl)))
      file_prepare_directory(drupal_realpath($destinationUrl), FILE_CREATE_DIRECTORY);
    $logoFile = file_save_upload('edit_community_logo_file', $validators, $destinationUrl, FILE_EXISTS_RENAME);
    $form_state['community_logo_file_id'] = $logoFile && $logoFile !== FALSE ? $logoFile->fid : NULL;
    $phoneNumberMatch = preg_match('/^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/', $form_state['input']['edit_community_phone_number']);
    $emailDomainsList = explode(",", $form_state['input']['edit_community_email_domains']);
    $isEmailDomainsValid = TRUE;
    foreach($emailDomainsList as $domain) {
      if(!preg_match('/([a-zA-Z\d]+:\/\/)?(\w+:\w+@)?([a-zA-Z\d.-]+\.[A-Za-z]{2,4})(:\d+)?(\/.*)?/', $domain)) {
        $isEmailDomainsValid = FALSE;
        break;
      }
    }
    
    if(trim($communityName) == '') 
        form_set_error('edit_community_name', 'Community Name Cannot be Empty');
    if(!$logoFile && ((isset($_SESSION['messages']) && count($_SESSION['messages']['error'] > 0))
                      || trim($existingCommDetails['community_logo']) == ''))
        form_set_error('edit_community_logo', 'Invalid or No File Selected');
    if($result !== FALSE && count($result) > 0 && $result['community_id'] != $_GET['id'])
        form_set_error('edit_community_name', $communityName .' already Exists');
    if(!empty($form_state['input']['edit_community_phone_number']) && !$phoneNumberMatch)
        form_set_error('edit_community_phone_number', 'Invalid Phone Number');
    if(!empty($form_state['input']['edit_community_website']) &&
       !preg_match('/([a-zA-Z\d]+:\/\/)?(\w+:\w+@)?([a-zA-Z\d.-]+\.[A-Za-z]{2,4})(:\d+)?(\/.*)?/', $form_state['input']['edit_community_website']))
        form_set_error('edit_community_website', 'Invalid Website Entered');
    if(!$isEmailDomainsValid)
        form_set_error('edit_community_email_domains', 'Text Contains one or more invalid email Domains');
    if(count($emailDomainsList) > 10)
        form_set_error('edit_community_email_domains', 'Maximum of 10 email domains are allowed.');
    if($form_state['input']['edit_community_type'] == 'corporate' && (!isset($form_state['input']['edit_travel_agency_id'])
      || $form_state['input']['edit_travel_agency_id'] === NULL || !ctype_digit($form_state['input']['edit_travel_agency_id'])))
       form_set_error('edit_travel_agency_id', 'Please Select Travel Agency');
    if($form_state['input']['edit_community_type'] == 'corporate'
       && (trim($form_state['input']['edit_community_dk_number']) == ''
           || preg_match('/[^0-9A-Za-z-]/', $form_state['input']['edit_community_dk_number'])))
        form_set_error('edit_community_dk_number', 'Only Alphanumeric characters and "-" allowed in Community DK Number');

    if(isset($form_state['input']['change_password']) && $form_state['input']['change_password'] == 1){
        if(trim($form_state['input']['sisense_community_pwd']) ==''){
            form_set_error('sisense_community_pwd','Please enter a spark password for the community '); 
        }
    }
}

function user_management_edit_community_form_submit($form, &$form_state) {
    $communityId = $_GET['id'];
    $result = db_select('community_types', 'ct')
    ->fields('ct', array('community_type_id'))
    ->condition('community_type_code', $form_state['input']['edit_community_type'], '=')
    ->execute()
    ->fetchAssoc();
    $communityType = count($result) > 0 ? $result['community_type_id'] : NULL;
    $travelAgencyId = isset($form_state['input']['edit_travel_agency_id']) ? $form_state['input']['edit_travel_agency_id'] : NULL;
    $filedsToUpdate = array(
        'community_name' => $form_state['input']['edit_community_name'],
        'is_chat_enabled'=> $form_state['input']['is_chat_enabled'],
        'community_address'=> $form_state['input']['edit_community_address'],
        'community_phone_number'=> $form_state['input']['edit_community_phone_number'],
        'community_website'=> $form_state['input']['edit_community_website'],
        'community_type'=> $communityType,
        'community_email_domains'=> $form_state['input']['edit_community_email_domains'],
        'dk_number'=> $form_state['input']['edit_community_dk_number'],
        'travel_agency_id' => $travelAgencyId
    );
    $logoFile = isset($form_state['community_logo_file_id']) && $form_state['community_logo_file_id'] !== NULL ?
      file_load($form_state['community_logo_file_id']) : FALSE;
    if($logoFile) {
      $logoFile->status = FILE_STATUS_PERMANENT;
      file_save($logoFile);
      $filedsToUpdate['community_logo'] = $logoFile->uri;
    }
    $result = db_update('community')
    ->fields($filedsToUpdate)
    ->condition('community_id', $communityId, '=')
    ->execute();

    // update sisense password for community starts 
    if(isset($form_state['input']['change_password']) && $form_state['input']['change_password'] == 1){
        if(trim($form_state['input']['sisense_community_pwd']) !=''){
            $pwd = trim($form_state['input']['sisense_community_pwd']);
            change_community_spark_pwd($communityId,$pwd);
            //drupal_set_message(t("Spark password Changed."));
        }
    }
    // update sisense password for community ends 
    drupal_set_message(t("Community Details Updated."));
    drupal_goto('manage/community');
}

function user_management_delete_community_block_view() {
    global $user;
    $isCommunityAdmin = in_array('Community Admin', $user->roles);
    $userProfileInfo = getAllGroupPccCommunityForUser($user->uid);
    $communityId = $_GET['id'];
    if($isCommunityAdmin && $communityId != $userProfileInfo['community_id'])
        exit('Access denied to edit Community Details !!');
    $num_updated = db_update('users')
    ->fields(
        array(
            'community_id' => NULL
        )
    )
    ->condition('community_id', $communityId, '=')
    ->execute();
    $num_updated = db_delete('community')
    ->condition('community_id', $communityId, '=')
    ->execute();
    drupal_set_message(t('Record has been deleted!'));
    drupal_goto('manage/community');
}

function getCommunityDetails($communityId) {
    $communityDetails = array();
    $communityDetailsResultSet = db_select('community', 'c')
    ->fields('c', array('community_name', 'community_email_domains'))
    ->condition('community_id', $communityId, '=')
    ->execute();
    if($communityDetailsResultSet->rowCount() > 0)
        $communityDetails = $communityDetailsResultSet->fetchAssoc();
    return $communityDetails;
}

/* insert and update status of old Community sisense password based on community id */
function change_community_spark_pwd($community_id,$pwd){
    global $user;
    //echo " COM |$community_id| PWD |$pwd| $user->name<br/>";
    list($encryptedPwd,$slt) = encrypt_password($pwd);
    //echo "PWD |$encryptedPwd| SLT|$slt| <br/>";
    $curDt = date("Y-m-d H:i:s", time());

    $filedsToUpdate = array('active'=>0);
    $res = db_update('sisense_community_pwd')
    ->fields($filedsToUpdate)
    ->condition('community_id', $community_id, '=')
    ->execute();

    $result = db_insert('sisense_community_pwd')
    ->fields(
        array(
            'community_id' => $community_id,
            'pwd'=> $encryptedPwd,
            'slt'=> $slt,
            'active'=> 1,
            'created_by'=>$user->name,
            'created_time'=> $curDt,
        )
    )->execute();
}
