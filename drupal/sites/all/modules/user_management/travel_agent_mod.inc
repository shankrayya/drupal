<?php 
/**
* Implements hook_block_view().
*/
function user_management_manage_travel_agent_block_view($delta = '') {
    $tableHeader = array(t('Travel Agency Id'), t('Travel Agency Name'), t('Travel Agency Description'), t('Edit'), t('Delete'));
    
    $allPccData = db_select('travel_agent', 'c')
    ->fields('c', array('travel_agent_id', 'travel_agent_name', 'travel_agent_description'))
    ->execute();
    $allResults = array();
    while($data = $allPccData->fetchAssoc()) {
        $data['edit_link'] = l('Edit Travel Agency', '/edit/travel_agent', array('query' => array('id' => $data['travel_agent_id'])));
        $data['delete_link'] = l('Delete Travel Agency', '/delete/travel_agent', array('query' => array('id' => $data['travel_agent_id'])));
        $allResults[] = $data;
    }
    $tableContentRows = $allResults;
    
    $tableContents = theme('table', array('header' => $tableHeader, 'rows' => $tableContentRows));
    $tableContents = l('Add New Travel Agecy Details', '/add/travel_agent') . "<br><br>".$tableContents;
    $blocks['content'] = $tableContents;
    return $blocks;   
}


/**
* Implements hook_block_view().
*/
function user_management_add_travel_agent_block_view() {
    $form = drupal_get_form('user_management_add_travel_agent_form');
    $block['content'] = $form;
    return $block;
}

function user_management_add_travel_agent_form($form, &$form_state) {
    $allCorporateDetails = array();
    $corporateDetails = db_select('corporate', 'g')
        ->fields('g', array('corporate_id', 'corporate_name'))
        ->execute();
    while($row = $corporateDetails->fetchAssoc()) {
        $allCorporateDetails[$row['corporate_id']] = $row['corporate_name'];
    }
    
    $form['travel_agent_name'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#attributes' => array(
            'placeholder' => t('Enter travel Agency Name')
        )
    );
    $form['travel_agent_desc'] = array(
        '#type' => 'textarea',
        '#required' => 'required',
        '#attributes' => array(
            'placeholder' => t('Enter Travel Agency Description')
        )
    );
    $form['add_travel_agent_submit'] = array(
        '#type' => 'submit',
        '#value' => 'Add Details',
    );
    return $form;
}

function user_management_add_travel_agent_form_validate($form, &$form_state) {
    $travelAgentName = $form_state['values']['travel_agent_name'];
    $description = $form_state['values']['travel_agent_desc'];
    if(trim($travelAgentName) == '') 
        form_set_error('travel_agent_name', 'Travel Agency Name Cannot be Empty');
    if(trim($description) == '') 
        form_set_error('travel_agent_desc', 'Travel Agency Cannot be Empty');
}

function user_management_add_travel_agent_form_submit($form, &$form_state) {
  try {
    $result = db_insert('travel_agent')
    ->fields(
        array(
            'travel_agent_name' => $form_state['values']['travel_agent_name'],
            'travel_agent_description' => $form_state['values']['travel_agent_desc'],
        )
    )
    ->execute();
    if($result) {
        drupal_set_message(t("New Travel Agency Added."));
        drupal_goto('manage/travel_agent');
    }
  } catch(PDOException $ex) {
    $message = $ex->getMessage();
    $message = $ex->getCode() == "23000" ? 'Travel Agency Name already exists' : $message;
    drupal_set_message(t($message), 'error');
    $form_state['rebuild'] = true;
  }
}

/**
* Implements hook_block_view().
*/
function user_management_edit_travel_agent_block_view() {
    $form = drupal_get_form('user_management_edit_travel_agent_form');
    $block['content'] = $form;
    return $block;
}

function user_management_edit_travel_agent_form($form, &$form_state) {
    $allCorporateDetails = array();
    $travel_agent_id = $_GET['id'];
    
    /*$result = db_query("select travel_agent_name, travel_agent_description, tc.travel_agent_id, c.corporate_name, c.corporate_id 
            from pierian_mystn_travel_agent as t LEFT JOIN  pierian_mystn_travel_agent_corporate as tc 
            ON t.travel_agent_id = tc.travel_agent_id
            LEFT JOIN 
            pierian_mystn_corporate as c ON
            tc.corporate_id = c.corporate_id
            where t.travel_agent_id = :tid", array('tid' => $travel_agent_id));
    $result = $result->fetchAssoc();*/
    
    $result = db_select('travel_agent', 'c')
    ->fields('c', array('travel_agent_name', 'travel_agent_description'))
    ->condition('travel_agent_id', $travel_agent_id, '=')
    ->execute()
    ->fetchAssoc();
    $form['edit_travel_agent_name'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#value' => $result['travel_agent_name'],
        '#title' => t('Enter Travel Agency Name'),
        '#attributes' => array(
            'placeholder' => t('Enter Travel Agency Name')
        )
    );
    $form['edit_travel_agent_desc'] = array(
        '#type' => 'textarea',
        '#required' => 'required',
        '#value' => $result['travel_agent_description'],
        '#title' => t('Enter Travel Agency Description'),
        '#attributes' => array(
            'placeholder' => t('Enter Travel Agency Description')
        )
    );
    $form['edit_travel_agent_submit'] = array(
        '#type' => 'submit',
        '#value' => 'Update Travel Agency Info',
    );
    return $form;
}

function user_management_edit_travel_agent_form_validate($form, &$form_state) {
    $travelAgentName = $form_state['input']['edit_travel_agent_name'];
    $description = $form_state['input']['edit_travel_agent_desc'];
    if(trim($travelAgentName) == '') 
        form_set_error('edit_travel_agent_name', 'Travel Agency Name Cannot be Empty');
    if(trim($description) == '') 
        form_set_error('edit_travel_agent_desc', 'Travel Agency Cannot be Empty');
}

function user_management_edit_travel_agent_form_submit($form, &$form_state) {
    $travel_agent_id = $_GET['id'];
    try {
      $result = db_update('travel_agent')
      ->fields(
          array(
              'travel_agent_name' => $form_state['input']['edit_travel_agent_name'],
              'travel_agent_description' => $form_state['input']['edit_travel_agent_desc'],
          )
      )
      ->condition('travel_agent_id', $travel_agent_id, '=')
      ->execute();
      drupal_set_message(t("Travel Agency Details Updated."));
      drupal_goto('manage/travel_agent');
    } catch(PDOException $ex) {
      $message = $ex->getMessage();
      $message = $ex->getCode() == "23000" ? 'Travel Agency Name already exists' : $message;
      drupal_set_message(t($message), 'error');
      $form_state['rebuild'] = true;
  }
}

function user_management_delete_travel_agent_block_view() {
    $travel_agent_id = $_GET['id'];
    
    $num_updated = db_delete('travel_agent_corporate')
    ->condition('travel_agent_id', $travel_agent_id, '=')
    ->execute();
    
    $num_updated = db_delete('travel_agent')
    ->condition('travel_agent_id', $travel_agent_id, '=')
    ->execute();
    drupal_set_message(t('Record has been deleted!'));
    drupal_goto('manage/travel_agent');
}

