<?php 
/**
* Implements hook_block_view().
*/
function user_management_manage_group_block_view($delta = '') {
    global $user;
    $isCommunityAdmin = in_array('Community Admin', $user->roles);
    $userProfileInfo = getAllGroupPccCommunityForUser($user->uid);
    
    $tableHeader = array(t('Group Id'), t('Group Name'), t('Spark User Name'), t('Status'), t('Edit'), t('Delete'));
    $status = array(0 => 'Inactive', 1 => 'Active');
    $allGroupData = db_select("group", 'g')
    ->fields('g', array('group_id', 'group_name', 'spark_user_email', 'status'))
    ->orderBy('status', 'desc')
    ->execute();
    $allResults = array();
    while($data = $allGroupData->fetchAssoc()) {
        $selectedCommunityIds = getSelectedCommunityIdsForGroup($data['group_id']);
        if($isCommunityAdmin && !array_key_exists($userProfileInfo['community_id'], $selectedCommunityIds))
          continue;
        $data['status'] = $status[$data['status']];
        $data['edit_link'] = l('Edit Group', '/edit/group', array('query' => array('id' => $data['group_id'])));
        $data['delete_link'] = l('Delete Group', '/delete/group', array('query' => array('id' => $data['group_id'])));
        $allResults[] = $data;
    }
    $tableContentRows = $allResults;
    
    $tableContents = theme('table', array('header' => $tableHeader, 'rows' => $tableContentRows));
    $tableContents = l('Add New Group', '/add/group') . "<br><br>".$tableContents;
    $blocks['content'] = $tableContents;
    return $blocks; 
}


/**
* Implements hook_block_view().
*/
function user_management_add_group_block_view() {
    $form = drupal_get_form('user_management_add_group_form');
    $block['content'] = $form;
    return $block;
}

function getCommunityTableFieldData(&$selectedCommunityIds = NULL, $communityIdToFilter = NULL) {
    $allCommunityDetails = array();
    $communityDetails = db_select('community', 'c')
        ->fields('c', array('community_id', 'community_name'))
        ->execute();
    while($row = $communityDetails->fetchAssoc()) {
        if($communityIdToFilter !== NULL &&
           $communityIdToFilter != $row['community_id'])
            continue;
        $allCommunityDetails[$row['community_id']] = array(
            'id' => $row['community_id'],
            'community_name' => $row['community_name'],
        );
        if($selectedCommunityIds !== NULL && !array_key_exists($row['community_id'], $selectedCommunityIds))
          $selectedCommunityIds[$row['community_id']] = FALSE;
    }
    $header = array(
      'community_name' => t('Community Name'),
    );
    return array('header' => $header, 'rows' => $allCommunityDetails);
}
function getCommunities($communityIdToFilter = NULL){
  $adminCommName = variable_get('sisense_admin_community');
  $isSuperAdmin = isSuperAdmin();
  $communityDetails = db_select('community', 'c')
        ->fields('c', array('community_id', 'community_name','is_hidden'))
        ->execute();
  while($row = $communityDetails->fetchAssoc()) {
      if($communityIdToFilter == NULL || intval($communityIdToFilter) == $row['community_id']) {

      if($row['community_name'] == $adminCommName && !$isSuperAdmin)
        continue; // dont show admin community for non admins

      $allCommunityDetails[$row['community_id']] = array(
          'id' => $row['community_id'],
          'community_name' => $row['community_name'],
      );
      /*
      if($selectedCommunityIds !== NULL && !array_key_exists($row['community_id'], $selectedCommunityIds))
        $selectedCommunityIds[$row['community_id']] = FALSE;
      */ 
    }  
  }
  return $allCommunityDetails;
}

function getSelectedCommunityIdsForGroup($groupId) {
    $selectedCommunityIds = array();
    $result = db_select('group_community', 'gc')
    ->fields('gc', array('group_id', 'community_id'))
    ->condition('group_id', $groupId, '=')
    ->execute();
    if($result && $result->rowCount() > 0) {
        while($row = $result->fetchAssoc()) {
            $selectedCommunityIds[$row['community_id']] = $row['community_id'];
        }
    }
    return $selectedCommunityIds;
}

function user_management_add_group_form($form, &$form_state) {
    global $user;
    $isCommunityAdmin = in_array('Community Admin', $user->roles);
    $userProfileInfo = getAllGroupPccCommunityForUser($user->uid);
    $communityIdToFilter = $isCommunityAdmin ? $userProfileInfo['community_id'] : NULL; 
    $selectedCommunity = NULL;
    //$communityFilterData = getCommunityTableFieldData($selectedCommunity, $communityIdToFilter);
    $communityFilterData = getCommunities($communityIdToFilter);
    $sparkUsersFromSisense = getSparkUsersInfoFromSisense();
    $allExistingSparkUser = getExistingSparkUserFromGroup();
    $allAvailableSparkUserToCreate = array_unique(array_diff($sparkUsersFromSisense, $allExistingSparkUser));
    $form['group_name'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#title' => t('Group Name'),
        '#attributes' => array(
            'placeholder' => t('Enter Group Name')
        )
    );
    $form['spark_user_email'] = array(
      '#type' => 'select',
      '#required' => 'required',
      '#options' => $allAvailableSparkUserToCreate,
      '#title' => t('Spark User Email'),
      '#attributes' => array(
            'placeholder' => t('Enter Spark User Email')
        )
    );
    /*
    $form['community_ids'] = array(
      '#type' => 'tableselect',
      '#required' => 'required',
      '#title' => t('Community'),
      '#header' => $communityFilterData['header'],
      '#options' => $communityFilterData['rows'],
      '#multiple' => TRUE,
      '#attributes' => array(
        'style' => 'max-width: 250px;'
      ),
      '#empty' => t('No Community found'),
    );
    */
    //print_r($communityFilterData);
    // start 
    /*
    foreach ($communityFilterData as $communityId => $valArr) {
      $form['community_ids'][$valArr['community_name']] = array(
        '#type' => 'radio',
        '#value' => 'default',
        '#return_value' => $communityId,
        '#title' => $valArr['community_name'],
        '#parents' => array('community_ids'),
      );
    }
    */
    $form['item']['table_start'] = array(
      '#markup' => '<table class="smallTbl"><thead><tr><th>Community</th></tr></thead><tbody>'
    );
    foreach ($communityFilterData as $communityId => $valArr) {
      $form['item']['tr_start_'.$communityId] = array('#markup' => '<tr>');
      $form['item'][$communityId]['community_ids'] = array(
        '#type' => 'radio',
        '#title' => t($valArr['community_name']),
        '#return_value' => $communityId,
        '#prefix' => '<td>',
        '#suffix' => '</td>',
      );
      //$form['item']['item_'.$communityId] = array('#markup' => '<td>'. $communityId .'</td>');
      $form['item']['tr_end_'.$communityId] = array('#markup' => '</tr>');
    }
    $form['item']['table_end'] = array('#markup' => '</tbody></table>');
    // end 
    $form['add_group_submit'] = array(
        '#type' => 'submit',
        '#value' => 'Add Group',
    );
    return $form;
}

function validSelectedCommunity($value) {
  return (ctype_digit($value) && $value !== 0);
}

function user_management_add_group_form_validate($form, &$form_state) {
    $groupName = $form_state['values']['group_name'];
    $sparkUserEmail = $form_state['values']['spark_user_email'];
    $allCommunityIds = $form_state['values']['community_ids'];
    //print_r($allCommunityIds);
    $selectedCommunityIds = array_filter($allCommunityIds,"validSelectedCommunity");
    $form_state['rebuild'] = false;
    $form_state['no_cache'] = TRUE;
    if(trim($groupName) == '') 
        form_set_error('group_name', 'Group Cannot be Empty');
    if(preg_match('/[^0-9A-Za-z-]/', $groupName))
        form_set_error('group_name', 'Only Alphanumeric characters and "-" allowed in group Name');
    if(!valid_email_address($sparkUserEmail))
        form_set_error('spark_user_email', 'Invalid Spark User Email');
    /*if(count($selectedCommunityIds) < 1)
        form_set_error('community_ids', 'No Community / Invalid community values passed.');
    */
    if($allCommunityIds == ''){
       form_set_error('community_ids', 'No Community / Invalid community values passed.');
    }    
}

function user_management_add_group_form_submit($form, &$form_state) {
    $allCommunityIds = $form_state['values']['community_ids'];
    //$selectedCommunityIds = array_filter($allCommunityIds,"validSelectedCommunity");
    try{
      $group_id = db_insert('group')
      ->fields(
          array(
              'group_name' => $form_state['values']['group_name'],
              'spark_user_email' => $form_state['values']['spark_user_email'],
              'status' => 1
          )
      )
      ->execute();
      if($group_id) {
          insertOrUpdateGroupCommunityDetails($group_id, $allCommunityIds);
          drupal_set_message(t("New Group Added."));
          drupal_goto('manage/group');
      }
    } catch(PDOException $ex) {
      $message = $ex->getMessage();
      $message = $ex->getCode() == "23000" ? 'Group / Spark User already exists' : $message;
      drupal_set_message(t($message), 'error');
      $form_state['rebuild'] = true;
  }
}

function insertOrUpdateGroupCommunityDetails($group_id, $communityIds) {
    /*
    foreach($communityIds as $community_id) {
        db_merge('group_community')
        ->key(
            array(
                'group_id' => $group_id,
                'community_id' => $community_id
            )
        )
        ->fields(
            array(
                'group_id' => $group_id,
                'community_id' => $community_id
            )
        )
        ->execute();
    }
    db_delete('group_community')
    ->condition('community_id', $communityIds, 'not in')
    ->condition('group_id', $group_id, '=')
    ->execute();
    */
    db_insert('group_community')
      ->fields(
          array(
             'group_id' => $group_id,
              'community_id' => $communityIds
          )
      )
      ->execute();
}

/**
* Implements hook_block_view().
*/
function user_management_edit_group_block_view() {
    $form = drupal_get_form('user_management_edit_group_form');
    $block['content'] = $form;
    return $block;
}

/* change history , 1 group belongs to 1 community */
function user_management_edit_group_form($form, &$form_state) {
    global $user;
	$isCommunityAdmin=0;
	if(in_array('Community Admin',$user->roles)){
		$isCommunityAdmin=1;
	}
    //$isCommunityAdmin = in_array('Community Admin', $user->roles);
    $userProfileInfo = getAllGroupPccCommunityForUser($user->uid);
    $communityIdToFilter = ($isCommunityAdmin ==1 ) ? $userProfileInfo['community_id'] : NULL;
    $sparkUsersFromSisense = getSparkUsersInfoFromSisense();
    $allExistingSparkUser = getExistingSparkUserFromGroup();
    $allAvailableSparkUserToCreate = array_diff($sparkUsersFromSisense, $allExistingSparkUser);
    $groupId = $_GET['id'];
    $selectedCommunityIds = getSelectedCommunityIdsForGroup($groupId);
    $selectedCommunityId = reset($selectedCommunityIds);
    //print_r($selectedCommunityId);exit();
    //$communityFilterData = getCommunityTableFieldData($selectedCommunityIds, $communityIdToFilter);    
    $communityDetails = getCommunities($selectedCommunityId);
    $communityDetails = reset($communityDetails);
    $communityName  = isset($communityDetails['community_name']) ? $communityDetails['community_name'] : '';
    if($isCommunityAdmin && (!array_key_exists($communityIdToFilter, $selectedCommunityIds) || !$selectedCommunityIds[$communityIdToFilter]))
        exit('Access denied to edit Group !!');
    $result = db_select('group', 'g')
    ->fields('g', array('group_name', 'spark_user_email', 'status'))
    ->condition('group_id', $groupId, '=')
    ->execute()
    ->fetchAssoc();
    $allAvailableSparkUserToCreate[$result['spark_user_email']] = $result['spark_user_email'];
    $groupName = isset($form_state['input']['edit_group_name']) ? $form_state['input']['edit_group_name'] : $result['group_name'];
    $form['edit_group_name'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#value' => $groupName,
        '#title' => t('Group Name'),
        '#attributes' => array(
            'placeholder' => t('Enter Group Name')
        ),
        '#description' => t($communityName)
    );
    $form['edit_spark_user_email'] = array(
      '#type' => 'select',
      '#required' => 'required',
      '#options' => $allAvailableSparkUserToCreate,
      '#default_value' => $result['spark_user_email'],
      '#title' => t('Spark User Email'),
      '#attributes' => array(
          'placeholder' => t('Enter Spark User Email')
      )
    );
    /* For edit prevent updating the community id , security restriction
    $form['edit_community_ids'] = array(
      '#type' => 'tableselect',
      '#required' => 'required',
      '#title' => t('Community'),
      '#header' => $communityFilterData['header'],
      '#options' => $communityFilterData['rows'],
      '#default_value' => $selectedCommunityIds,
      '#multiple' => TRUE,
      '#attributes' => array(
        'style' => 'max-width: 250px;'
      ),
      '#empty' => t('No Community found'),
    );
    */
    $form['edit_group_status'] = array(
      '#type' => 'radios',
      '#required' => 'required',
      '#title' => t('Group Status'),
      '#options' => array(1 => 'Active', 0 => 'Inactive'),
      '#default_value' => $result['status'],
    );
    $form['edit_group_submit'] = array(
        '#type' => 'submit',
        '#value' => 'Update Group Info',
    );
    return $form;
}

function user_management_edit_group_form_validate($form, &$form_state) {
    $groupName = $form_state['input']['edit_group_name'];
    $sparkUserEmail = $form_state['input']['edit_spark_user_email'];
    //$allCommunityIds = $form_state['input']['edit_community_ids'];
    //$selectedCommunityIds = array_filter($allCommunityIds,"validSelectedCommunity");
    if(trim($groupName) == '') 
        form_set_error('edit_group_name', 'Group Cannot be Empty');
    if(preg_match('/[^0-9A-Za-z-]/', $groupName))
        form_set_error('edit_group_name', 'Only Alphanumeric characters and "-" allowed in group Name');
    if(!valid_email_address($sparkUserEmail))
        form_set_error('edit_spark_user_email', 'Invalid Spark User Email');
    /*
    if(count($selectedCommunityIds) < 1)
        form_set_error('edit_community_ids', 'No Community / Invalid community values passed.');
    */   
}

function user_management_edit_group_form_submit($form, &$form_state) {
    try{
      //$allCommunityIds = $form_state['input']['edit_community_ids'];
      //$selectedCommunityIds = array_filter($allCommunityIds,"validSelectedCommunity");
      $groupId = $_GET['id'];
      $result = db_update('group')
      ->fields(
          array(
              'group_name' => $form_state['input']['edit_group_name'],
              'spark_user_email' => $form_state['input']['edit_spark_user_email'],
              'status' => $form_state['input']['edit_group_status']
          )
      )
      ->condition('group_id', $groupId, '=')
      ->execute();
      // security restriction dont allow updating community for a group
      //insertOrUpdateGroupCommunityDetails($groupId, $allCommunityIds);
      drupal_set_message(t("Group Details Updated."));
      drupal_goto('manage/group');
    } catch(PDOException $ex) {
      $message = $ex->getMessage();
      $message = $ex->getCode() == "23000" ? 'Group / Spark User already exists' : $message;
      drupal_set_message(t($message), 'error');
      $form_state['rebuild'] = true;
  }
}

function user_management_delete_group_block_view() {
    $groupId = $_GET['id'];
    $num_updated = db_update('region')
    ->fields(
        array(
            'group_id' => NULL
        )
    )
    ->condition('group_id', $groupId, '=')
    ->execute();
    
    $num_updated = db_delete('user_group')
    ->condition('group_id', $groupId, '=')
    ->execute();
    
    $num_updated = db_delete('group')
    ->condition('group_id', $groupId, '=')
    ->execute();
    drupal_set_message(t('Record has been deleted!'));
    drupal_goto('manage/group');
}

function getSparkUsersInfoFromSisense() {
    $allUserInfo = array();
    $url = variable_get('api_base_url')."users";
    $token = custom_login_getToken();
    $data = array();
    $options = array(
      'method' => 'GET',
      'data' => drupal_http_build_query($data),
      'timeout' => 15,
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded','authorization' => $token)
    );
    
    $result = drupal_http_request($url, $options);
    $resultData = json_decode($result->data, true);
    foreach($resultData as $userInfo) {
        $allUserInfo[$userInfo['email']] = $userInfo['email'];
    }
    return $allUserInfo;
}

function getExistingSparkUserFromGroup() {
    $existingSparkUser = array();
    $result = db_select('group', 'g')
    ->fields('g', array('spark_user_email'))
    ->execute();
    while($row = $result->fetchAssoc()) {
      $existingSparkUser[$row['spark_user_email']] = $row['spark_user_email'];
    }
    return $existingSparkUser;
}