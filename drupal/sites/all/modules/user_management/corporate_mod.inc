<?php 
/**
* Implements hook_block_view().
*/
function user_management_manage_corporate_block_view($delta = '') {
    $tableHeader = array(t('Corporate Id'), t('Corporate Name'), t('DK Number'), t('Corporate Description'), t('Edit'), t('Delete'));
    
    $allCorporateData = db_select('corporate', 'c')
    ->fields('c', array('corporate_id', 'corporate_name', 'dk_number', 'description'))
    ->execute();
    $allResults = array();
    while($data = $allCorporateData->fetchAssoc()) {
        $data['edit_link'] = l('Edit Corporate', '/edit/corporate', array('query' => array('id' => $data['corporate_id'])));
        $data['delete_link'] = l('Delete Corporate', '/delete/corporate', array('query' => array('id' => $data['corporate_id'])));
        $allResults[] = $data;
    }
    $tableContentRows = $allResults;
    
    $tableContents = theme('table', array('header' => $tableHeader, 'rows' => $tableContentRows));
    $tableContents = l('Add New Corporate Details', '/add/corporate') . "<br><br>".$tableContents;
    $blocks['content'] = $tableContents;
    return $blocks;   
}

/**
* Implements hook_block_view().
*/
function user_management_add_corporate_block_view() {
    $form = drupal_get_form('user_management_add_corporate_form');
    $block['content'] = $form;
    return $block;
}

function user_management_add_corporate_form($form, &$form_state) {
    $allCommunityDetails = array();
    $communityDetails = db_select('community', 'g')
        ->fields('g', array('community_id', 'community_name'))
        ->execute();
    while($row = $communityDetails->fetchAssoc()) {
        $allCommunityDetails[$row['community_id']] = $row['community_name'];
    }
    
    $allTravelAgentDetails = array();
    $travelAgentDetails = db_select('travel_agent', 'g')
        ->fields('g', array('travel_agent_id', 'travel_agent_name'))
        ->execute();
    while($row = $travelAgentDetails->fetchAssoc()) {
        $allTravelAgentDetails[$row['travel_agent_id']] = $row['travel_agent_name'];
    }
    
    $form['corporate_name'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#attributes' => array(
            'placeholder' => t('Enter Corporate Name')
        )
    );
    $form['corporate_desc'] = array(
        '#type' => 'textarea',
        '#required' => 'required',
        '#attributes' => array(
            'placeholder' => t('Enter Description')
        )
    );
    $form['corporate_dk_number'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#attributes' => array(
            'placeholder' => t('Enter DK Number')
        )
    );
    $form['travel_agent_corporate'] = array(
        '#type' => 'select',
        '#required' => 'required',
        '#multiple' => TRUE,
        '#title' => t('Select Travel Agency'),
        '#options' => $allTravelAgentDetails
    );
    $form['community_id'] = array(
        '#type' => 'select',
        "#empty_option"=>t('- Select -'),
        '#options' => $allCommunityDetails,
        '#title' => t('Select Community')
    );
    $form['corporate_form_submit'] = array(
        '#type' => 'submit',
        '#value' => 'Add Corporate Details',
    );
    return $form;
}

function user_management_add_corporate_form_validate($form, &$form_state) {
    $corporateName = $form_state['values']['corporate_name'];
    $corporateDKNumber = $form_state['values']['corporate_dk_number'];
    if(trim($corporateName) == '') 
        form_set_error('corporate_name', 'Corporate Name Cannot be Empty');
    if(!ctype_digit(trim($corporateDKNumber)) || trim($corporateDKNumber) == '')
        form_set_error('corporate_dk_number', 'Invalid Or Empty Group DK Number');
}

function user_management_add_corporate_form_submit($form, &$form_state) {
    $fieldsToUpdate = array(
        'corporate_name' => $form_state['values']['corporate_name'],
        'description' => $form_state['values']['corporate_desc'],
        'dk_number' => $form_state['values']['corporate_dk_number'] 
    );
    
    if(isset($form_state['values']['community_id']) && trim($form_state['values']['community_id'] != ''))
       $fieldsToUpdate['community_id'] = $form_state['values']['community_id'];
    try {
      $result = db_insert('corporate')
      ->fields($fieldsToUpdate)
      ->execute();
      if($result) {
          $corporateId = $result;
          foreach($form_state['values']['travel_agent_corporate'] as $travelAgentId) {
              db_insert('travel_agent_corporate')
              ->fields(
                  array(
                      'travel_agent_id' => $travelAgentId,
                      'corporate_id' => $corporateId,
                  )
              )->execute();
          }
      
          drupal_set_message(t("New Corporate Added."));
          drupal_goto('manage/corporate');
      }
    } catch(PDOException $ex) {
      $message = $ex->getMessage();
      $message = $ex->getCode() == "23000" ? $form_state['values']['corporate_name'] .' already exists' : $message;
      drupal_set_message(t($message), 'error');
      $form_state['rebuild'] = true;
    }
}

/**
* Implements hook_block_view().
*/
function user_management_edit_corporate_block_view() {
    $form = drupal_get_form('user_management_edit_corporate_form');
    $block['content'] = $form;
    return $block;
}

function user_management_edit_corporate_form($form, &$form_state) {
    $corporateId = $_GET['id'];
    $allCommunityDetails = array();
    $communityDetails = db_select('community', 'g')
        ->fields('g', array('community_id', 'community_name'))
        ->execute();
    while($row = $communityDetails->fetchAssoc()) {
        $allCommunityDetails[$row['community_id']] = $row['community_name'];
    }
    
    $selectedTravelAgents = getAllTravelAgentForCorporate($corporateId);
    $allTravelAgentDetails = array();
    $travelAgentDetails = db_select('travel_agent', 'g')
        ->fields('g', array('travel_agent_id', 'travel_agent_name'))
        ->execute();
    while($row = $travelAgentDetails->fetchAssoc()) {
        $allTravelAgentDetails[$row['travel_agent_id']] = $row['travel_agent_name'];
    }
    
    $result = db_select('corporate', 'c')
    ->fields('c', array('corporate_name', 'description', 'community_id', 'dk_number'))
    ->condition('corporate_id', $corporateId, '=')
    ->execute()
    ->fetchAssoc();
    $form['edit_corporate_name'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#value' => $result['corporate_name'],
        '#title' => t('Enter Corporate Name'),
        '#attributes' => array(
            'placeholder' => t('Enter Corporate Name')
        )
    );
    $form['edit_corporate_desc'] = array(
        '#type' => 'textarea',
        '#required' => 'required',
        '#value' => $result['description'],
        '#title' => t('Enter Description'),
        '#attributes' => array(
            'placeholder' => t('Enter Description')
        )
    );
    $form['edit_corporate_dk_number'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#value' => $result['dk_number'],
        '#title' => t('Enter DK Number'),
        '#attributes' => array(
            'placeholder' => t('Enter DK Number')
        )
    );
    $form['edit_travel_agent_corporate'] = array(
        '#type' => 'select',
        '#required' => 'required',
        '#multiple' => TRUE,
        '#default_value' => $selectedTravelAgents,
        '#title' => t('Select Travel Agent'),
        '#options' => $allTravelAgentDetails
    );
    $form['edit_community_id'] = array(
        '#type' => 'select',
        "#empty_option"=>t('- Select -'),
        '#default_value' => $result['community_id'],
        '#options' => $allCommunityDetails,
        '#title' => t('Select Community')
    );
    $form['edit_corporate_submit'] = array(
        '#type' => 'submit',
        '#value' => 'Update Corporate Info',
    );
    return $form;
}

function user_management_edit_corporate_form_validate($form, &$form_state) {
    $corporateName = $form_state['input']['edit_corporate_name'];
    $corporateDKNumber = $form_state['input']['edit_corporate_dk_number'];
    if(trim($corporateName) == '') 
        form_set_error('edit_corporate_name', 'Corporate Name Cannot be Empty');
    if(!ctype_digit(trim($corporateDKNumber)) || trim($corporateDKNumber) == '')
        form_set_error('edit_corporate_dk_number', 'Invalid Or Empty Group DK Number');
}

function user_management_edit_corporate_form_submit($form, &$form_state) {
    $fieldsToUpdate = array(
        'corporate_name' => $form_state['input']['edit_corporate_name'],
        'description' => $form_state['input']['edit_corporate_desc'],
        'dk_number' => $form_state['input']['edit_corporate_dk_number'] 
    );
    
    if(!isset($form_state['input']['edit_community_id']) || trim($form_state['input']['edit_community_id'] == ''))
       $fieldsToUpdate['community_id'] = NULL;

    $corporateId = $_GET['id'];
    try{
      $result = db_update('corporate')
      ->fields($fieldsToUpdate)
      ->condition('corporate_id', $corporateId, '=')
      ->execute();
      foreach($form_state['input']['edit_travel_agent_corporate'] as $travel_agent_id) {
          db_merge('travel_agent_corporate')
          ->key(array(
              'travel_agent_id' => $travel_agent_id,
              'corporate_id' => $corporateId
          ))
          ->fields(
              array(
                  'travel_agent_id' => $travel_agent_id,
                  'corporate_id' => $corporateId
              )
          )
          ->condition('corporate_id', $corporateId, '=')
          ->execute();
      }
      drupal_set_message(t("Corporate Details Updated."));
      drupal_goto('manage/corporate');
    } catch(PDOException $ex) {
      $message = $ex->getMessage();
      $message = $ex->getCode() == "23000" ? $form_state['input']['edit_corporate_name'] .' already exists' : $message;
      drupal_set_message(t($message), 'error');
      $form_state['rebuild'] = true;
    }
}

function user_management_delete_corporate_block_view() {
    $corporateId = $_GET['id'];
    
    $num_updated = db_delete('travel_agent_corporate')
    ->condition('corporate_id', $corporateId, '=')
    ->execute();
    
    $num_updated = db_delete('corporate')
    ->condition('corporate_id', $corporateId, '=')
    ->execute();
    drupal_set_message(t('Record has been deleted!'));
    drupal_goto('manage/corporate');
}

function getAllTravelAgentForCorporate($corporateId) {
    $allTravelAgentCorporateData = db_select('travel_agent_corporate', 'tc')
    ->fields('tc', array('travel_agent_id'))
    ->condition('corporate_id', $corporateId, '=')
    ->execute();
    $allTravelAgentIds = array();
    while($data = $allTravelAgentCorporateData->fetchAssoc()) {
        $allTravelAgentIds[] = $data['travel_agent_id'];
    }
    return $allTravelAgentIds;
}
