<?php 
/**
* Implements hook_block_view().
*/
function user_management_manage_user_group_block_view($delta = '') {
    $tableHeader = array(t('User Group Id'), t('User Name'), t('Group Name'), t('Default Group'), t('Edit'), t('Delete'));
    
    $allGroupData = db_query("select distinct ug.user_group_id, u.name, g.group_name, if(ug.default_group = 1, 'Yes', 'No') as default_group
                             from {users} u, {group} g, {user_group} ug
                             where ug.group_id = g.group_id and ug.user_id = u.uid");
    $allResults = array();
    while($data = $allGroupData->fetchAssoc()) {
        $data['edit_link'] = l('Edit User Group', '/edit/user_group', array('query' => array('id' => $data['user_group_id'])));
        $data['delete_link'] = l('Delete User Group', '/delete/user_group', array('query' => array('id' => $data['user_group_id'])));
        $allResults[] = $data;
    }
    $tableContentRows = $allResults;
    
    $tableContents = theme('table', array('header' => $tableHeader, 'rows' => $tableContentRows));
    $tableContents = l('Add New Group', '/add/user_group') . "<br><br>".$tableContents;
    $blocks['content'] = $tableContents;
    return $blocks; 
}


/**
* Implements hook_block_view().
*/
function user_management_add_user_group_block_view() {
    $form = drupal_get_form('user_management_add_user_group_form');
    $block['content'] = $form;
    return $block;
}

function user_management_add_user_group_form($form, &$form_state) {
    $allGroupDetails = array();
    $groupDetails = db_select('group', 'g')
    ->fields('g', array('group_id', 'group_name'))
    ->execute();
    while($row = $groupDetails->fetchAssoc()) {
      $allGroupDetails[$row['group_id']] = $row['group_name'];
    }
     
    $allUserDetails = array();
    $userDetails = db_select('users', 'u')
    ->fields('u', array('uid', 'name'))
    ->condition('status', 1, '=')
    ->execute();
    while($row = $userDetails->fetchAssoc()) {
      $allUserDetails[$row['uid']] = $row['name'];
    }
    
    $form['user_id'] = array(
        '#type' => 'select',
        '#required' => 'required',
        '#options' => $allUserDetails,
        '#title' => t('Select User')
    );
    $form['group_id'] = array(
      '#type' => 'select',
      '#required' => 'required',
      '#options' => $allGroupDetails,
      '#title' => t('Select Group')
    );
    $form['is_default_group'] = array(
      '#type' => 'radios',
      '#required' => 'required',
      '#options' => array(
        1 => 'yes',
        0 => 'no'
      ),
      '#default_value' => 0,
      '#title' => t('Default Group')
    );
    $form['add_group_submit'] = array(
        '#type' => 'submit',
        '#value' => 'Add User Group',
    );
    return $form;
}

function user_management_add_user_group_form_validate($form, &$form_state) {
    $userId = $form_state['values']['user_id'];
    $groupId = $form_state['values']['group_id'];
    
    if(!ctype_digit(trim($userId)) || trim($userId) == '')
        form_set_error('user_id', 'Invalid Or Empty user Id Passed');
    if(!ctype_digit(trim($groupId)) || trim($groupId) == '')
        form_set_error('group_id', 'Invalid Spark Group Id Passed');
}

function user_management_add_user_group_form_submit($form, &$form_state) {
    $result = db_select('user_group', 'ug')
    ->fields('ug', array(
        'group_id' ,'user_id '
      )
    )
    ->condition('group_id', $form_state['values']['group_id'], '=')
    ->condition('user_id', $form_state['values']['user_id'], '=')
    ->execute();
    if($result->rowCount() > 0) {
      drupal_set_message(t("User Group Already Added."));
      drupal_goto('manage/user_group');  
    }

    $result = db_insert('user_group')
    ->fields(
        array(
            'group_id' => $form_state['values']['group_id'],
            'user_id' => $form_state['values']['user_id'],
            'default_group' => $form_state['values']['is_default_group'],
        )
    )
    ->execute();
    if($result) {
        drupal_set_message(t("New User Group Added."));
        drupal_goto('manage/user_group');
    }
}

/**
* Implements hook_block_view().
*/
function user_management_edit_user_group_block_view() {
    $form = drupal_get_form('user_management_edit_user_group_form');
    $block['content'] = $form;
    return $block;
}

function user_management_edit_user_group_form($form, &$form_state) {
    $userGroupId = $_GET['id'];
    
    $allGroupDetails = array();
    $groupDetails = db_select('group', 'g')
    ->fields('g', array('group_id', 'group_name'))
    ->execute();
    while($row = $groupDetails->fetchAssoc()) {
      $allGroupDetails[$row['group_id']] = $row['group_name'];
    }
    
    $allUserDetails = array();
    $userDetails = db_select('users', 'u')
    ->fields('u', array('uid', 'name'))
    ->condition('status', 1, '=')
    ->execute();
    while($row = $userDetails->fetchAssoc()) {
      $allUserDetails[$row['uid']] = $row['name'];
    }
    
    $result = db_select('user_group', 'ug')
    ->fields('ug', array('group_id', 'user_id', 'default_group'))
    ->condition('user_group_id', $userGroupId, '=')
    ->execute()
    ->fetchAssoc();
    $form['edit_user_id'] = array(
      '#type' => 'select',
      '#options' => $allUserDetails,
      '#disabled' => TRUE,
      '#default_value' => $result['user_id'],
      '#title' => t('User')
    );
    $form['edit_group_id'] = array(
        '#type' => 'select',
        '#required' => 'required',
        '#options' => $allGroupDetails,
        '#default_value' => $result['group_id'],
        '#title' => t('Select Group')
    );
    $form['edit_default_group'] = array(
      '#type' => 'radios',
      '#required' => 'required',
      '#options' => array(
        1 => 'yes',
        0 => 'no'
      ),
      '#default_value' => $result['default_group'],
      '#title' => t('Default Group')
    );
    $form['edit_group_submit'] = array(
        '#type' => 'submit',
        '#value' => 'Update Group Info',
    );
    return $form;
}

function user_management_edit_user_group_form_validate($form, &$form_state) {
    $userId = $form_state['values']['edit_user_id'];
    $groupId = $form_state['input']['edit_group_id'];
    
    if(!ctype_digit(trim($userId)) || trim($userId) == '')
        form_set_error('edit_user_id', 'Invalid Or Empty user Id Passed');
    if(!ctype_digit(trim($groupId)) || trim($groupId) == '')
        form_set_error('edit_group_id', 'Invalid Spark Group Id Passed');
}

function user_management_edit_user_group_form_submit($form, &$form_state) {
    $userGroupId = $_GET['id'];
    if($form_state['input']['edit_default_group'] == 1) {
      db_update('user_group')
      ->fields(
        array('default_group' => 0)
      )
      ->condition('user_id', $form_state['values']['edit_user_id'], '=')
      ->execute();
    }
    $result = db_update('user_group')
    ->fields(
        array(
            'group_id' => $form_state['input']['edit_group_id'],
            'default_group' => $form_state['input']['edit_default_group'],
        )
    )
    ->condition('user_group_id', $userGroupId, '=')
    ->execute();
    if($result)
        drupal_set_message(t("User Group Details Updated."));
}

function user_management_delete_user_group_block_view() {
    $userGroupId = $_GET['id'];
    $num_updated = db_delete('user_group')
    ->condition('user_group_id', $userGroupId, '=')
    ->execute();
   
    drupal_set_message(t('Record has been deleted!'));
    drupal_goto('manage/user_group');
}
