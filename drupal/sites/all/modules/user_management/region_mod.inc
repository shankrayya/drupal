<?php 
/**
* Implements hook_block_view().
*/
function user_management_manage_region_block_view($delta = '') {
    $tableHeader = array(t('Region Id'), t('Region Name'), t('Region Description'), t('Region Short Code'), t('Group Name'), t('Edit'), t('Delete'));
    
    $allRegionData = db_select('region', 'r')
    ->fields('r', array('region_id', 'region_name', 'region_description', 'region_shortcode', 'group_id'))
    ->execute();
    $allResults = array();
    while($data = $allRegionData->fetchAssoc()) {
        $groupDetails = db_select('group', 'g')
        ->fields('g', array('group_name'))
        ->condition('group_id', $data['group_id'], '=')
        ->execute()
        ->fetchAssoc(); 
        unset($data['group_id']);
        $data['group_name'] = $groupDetails['group_name'];
        $data['edit_link'] = l('Edit Region', '/edit/region', array('query' => array('id' => $data['region_id'])));
        $data['delete_link'] = l('Delete Region', '/delete/region', array('query' => array('id' => $data['region_id'])));
        $allResults[] = $data;
    }
    $tableContentRows = $allResults;
    
    $tableContents = theme('table', array('header' => $tableHeader, 'rows' => $tableContentRows));
    $tableContents = l('Add New Region Details', '/add/region') . "<br><br>".$tableContents;
    $blocks['content'] = $tableContents;
    return $blocks;   
}

/**
* Implements hook_block_view().
*/
function user_management_add_region_block_view() {
    $form = drupal_get_form('user_management_add_region_form');
    $block['content'] = $form;
    return $block;
}

function user_management_add_region_form($form, &$form_state) {
    $allGroupDetails = array();
    $groupDetails = db_select('group', 'g')
        ->fields('g', array('group_id', 'group_name'))
        ->execute();
    while($row = $groupDetails->fetchAssoc()) {
        $allGroupDetails[$row['group_id']] = $row['group_name'];
    }
    
    $form['region_name'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#attributes' => array(
            'placeholder' => t('Enter Region Name')
        )
    );
    $form['region_shortcode'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#attributes' => array(
            'placeholder' => t('Enter Region Shortcode')
        )
    );
    $form['region_desc'] = array(
        '#type' => 'textarea',
        '#required' => 'required',
        '#attributes' => array(
            'placeholder' => t('Enter Region Description')
        )
    );
    $form['region_group'] = array(
        '#type' => 'select',
        '#required' => 'required',
        '#options' => $allGroupDetails,
        '#title' => t('Select Group')
    );
    $form['region_form_submit'] = array(
        '#type' => 'submit',
        '#value' => 'Add Region',
    );
    return $form;
}

function user_management_add_region_form_validate($form, &$form_state) {
    $regionName = $form_state['values']['region_name'];
    $regionShortCode = $form_state['values']['region_shortcode'];
    $regionDescription = $form_state['values']['region_desc'];
    $regionGroup = $form_state['values']['region_group'];
    $result = db_select('region', 'r')
    ->fields('r', array('region_name'))
    ->condition('region_name', $regionName, '=')
    ->execute();
    if(trim($regionName) == '') 
        form_set_error('region_name', 'Region Name Cannot be Empty');
    if(!ctype_digit(trim($regionGroup)) || trim($regionGroup) == '')
        form_set_error('region_group', 'Invalid Or Empty Group');
    if(trim($regionShortCode) == '') 
        form_set_error('region_shortcode', 'Region Short Code Cannot be Empty');
    if(trim($regionDescription) == '') 
        form_set_error('region_desc', 'Region Description Cannot be Empty');
    if($result->rowCount() > 0)
      form_set_error('region_name', $regionName .' already Exists');
}

function user_management_add_region_form_submit($form, &$form_state) {
    $result = db_insert('region')
    ->fields(
        array(
            'region_name' => $form_state['values']['region_name'],
            'region_shortcode' => $form_state['values']['region_shortcode'],
            'region_description' => $form_state['values']['region_desc'],
            'group_id' => $form_state['values']['region_group'],
        )
    )
    ->execute();
    if($result) {
        drupal_set_message(t("New Region Added."));
        drupal_goto('manage/region');
    }
}

/**
* Implements hook_block_view().
*/
function user_management_edit_region_block_view() {
    $form = drupal_get_form('user_management_edit_region_form');
    $block['content'] = $form;
    return $block;
}

function user_management_edit_region_form($form, &$form_state) {
    $region_id = $_GET['id'];
    $result = db_select('region', 'c')
    ->fields('c', array('region_name', 'region_description', 'region_shortcode', 'group_id'))
    ->condition('region_id', $region_id, '=')
    ->execute()
    ->fetchAssoc();
    $allGroupDetails = array();
    $groupDetails = db_select('group', 'g')
        ->fields('g', array('group_id', 'group_name'))
        ->execute();
    while($row = $groupDetails->fetchAssoc()) {
        $allGroupDetails[$row['group_id']] = $row['group_name'];
    }
    
    $form['edit_region_name'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#value' => $result['region_name'],
        '#title' => t('Enter Region Name'),
        '#attributes' => array(
            'placeholder' => t('Enter Region Name')
        )
    );
    $form['edit_region_shortcode'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#value' => $result['region_shortcode'],
        '#title' => t('Enter Region Shortcode'),
        '#attributes' => array(
            'placeholder' => t('Enter Region Shortcode')
        )
    );
    $form['edit_region_desc'] = array(
        '#type' => 'textarea',
        '#required' => 'required',
        '#value' => $result['region_description'],
        '#title' => t('Enter Region Description'),
        '#attributes' => array(
            'placeholder' => t('Enter Region Description')
        )
    );
    $form['edit_region_group'] = array(
        '#type' => 'select',
        '#required' => 'required',
        '#options' => $allGroupDetails,
        '#default_value' => $result['group_id'],
        '#title' => t('Select Group')
    );
    $form['region_edit_form_submit'] = array(
        '#type' => 'submit',
        '#value' => 'Update Region',
    );
    return $form;
}

function user_management_edit_region_form_validate($form, &$form_state) {
    $regionName = $form_state['input']['edit_region_name'];
    $regionShortCode = $form_state['input']['edit_region_shortcode'];
    $regionDescription = $form_state['input']['edit_region_desc'];
    $regionGroup = $form_state['input']['edit_region_group'];
    $result = db_select('region', 'r')
    ->fields('r', array('region_id'))
    ->condition('region_name', $regionName, '=')
    ->execute()
    ->fetchAssoc();
    if(trim($regionName) == '') 
        form_set_error('edit_region_name', 'Region Name Cannot be Empty');
    if(!ctype_digit(trim($regionGroup)) || trim($regionGroup) == '')
        form_set_error('edit_region_group', 'Invalid Or Empty Group');
    if(trim($regionShortCode) == '') 
        form_set_error('edit_region_shortcode', 'Region Short Code Cannot be Empty');
    if(trim($regionDescription) == '') 
        form_set_error('edit_region_desc', 'Region Description Cannot be Empty');
    if($result !== FALSE && count($result) > 0 && $result['region_id'] != $_GET['id'])
        form_set_error('edit_region_name', $regionName .' already Exists');
}

function user_management_edit_region_form_submit($form, &$form_state) {
    $region_id = $_GET['id'];
    $result = db_update('region')
    ->fields(
        array(
            'region_name' => $form_state['input']['edit_region_name'],
            'region_shortcode' => $form_state['input']['edit_region_shortcode'],
            'region_description' => $form_state['input']['edit_region_desc'],
            'group_id' => $form_state['input']['edit_region_group'],
        )
    )
    ->condition('region_id', $region_id, '=')
    ->execute();
    if($result) {
        drupal_set_message(t("Region Details updated."));
        drupal_goto('manage/region');
    }
}

function user_management_delete_region_block_view() {
    $region_id = $_GET['id'];
    $num_updated = db_delete('region')
    ->condition('region_id', $region_id, '=')
    ->execute();
    drupal_set_message(t('Record has been deleted!'));
    drupal_goto('manage/region');
}
