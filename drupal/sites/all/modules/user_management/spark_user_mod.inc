<?php 
/**
* Implements hook_block_view().
*/
function user_management_spark_user_block_view($delta = '') {
    $tableHeader = array(t('Id'), t('Spark User Name'), t('Spark User Email'), t('Spark User Alias'), t('Edit'), t('Delete'));
    
    $allPccData = db_select('spark_users', 'c')
    ->fields('c', array('spark_user_id', 'spark_user_name', 'spark_user_email', 'alias'))
    ->execute();
    $allResults = array();
    while($data = $allPccData->fetchAssoc()) {
        $data['edit_link'] = l('Edit Spark User', '/edit/spark_user', array('query' => array('id' => $data['spark_user_id'])));
        $data['delete_link'] = l('delete Spark User', '/delete/spark_user', array('query' => array('id' => $data['spark_user_id'])));
        $allResults[] = $data;
    }
    $tableContentRows = $allResults;
    
    $tableContents = theme('table', array('header' => $tableHeader, 'rows' => $tableContentRows));
    $tableContents = l('Add New Spark User Details', '/add/spark_user') . "<br><br>".$tableContents;
    $blocks['content'] = $tableContents;
    return $blocks;   
}

/**
* Implements hook_block_view().
*/
function user_management_manage_block_view() {
    $form = drupal_get_form('user_management_manage_spark_user_form');
    $block['content'] = $form;
    return $block;
}

function user_management_manage_spark_user_form($form, &$form_state) {
    $form['spark_user_name'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#attributes' => array(
            'placeholder' => t('Enter Spark User Name')
        )
    );
    $form['spark_user_email'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#attributes' => array(
            'placeholder' => t('Enter Spark User Email')
        )
    );
    $form['spark_user_alias'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#attributes' => array(
            'placeholder' => t('Enter Spark User Alias Name')
        )
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Add Spark User',
    );
    return $form;
}

function user_management_manage_spark_user_form_submit($form, &$form_state) {
    try{
      $result = db_insert('spark_users')
      ->fields(
          array(
              'spark_user_name' => $form_state['values']['spark_user_name'],
              'spark_user_email' => $form_state['values']['spark_user_email'],
              'alias' => $form_state['values']['spark_user_alias']
          )
      )
      ->execute();
      if($result) {
          drupal_set_message(t("New Spark User Added."));
          drupal_goto('manage/spark_user');
      }
    } catch(PDOException $ex) {
      $message = $ex->getMessage();
      $message = $ex->getCode() == "23000" ? $form_state['values']['spark_user_name'] .' already exists' : $message;
      drupal_set_message(t($message), 'error');
      $form_state['rebuild'] = true;
    } 
}

// function to return a random salt
function make_random_salt(){
    $random_salt = mcrypt_create_iv(12, MCRYPT_DEV_URANDOM);
    return $random_salt;
}
function get_random_salt($random_salt){
    $global_salt = variable_get('password_salt');
    $random_salt = base64_decode($random_salt);
    $random_salt = $random_salt.$global_salt;
    return $random_salt;   
}
function encrypt_password($pass){
    $global_salt = variable_get('password_salt');
    $random_salt = make_random_salt();
    $random_salt_b64 = base64_encode($random_salt);
    $full_salt = $random_salt.$global_salt;

    $iv = mcrypt_create_iv(
        mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC),
        MCRYPT_DEV_URANDOM
    );

    $encrypted = base64_encode(
        $iv .
        mcrypt_encrypt(
            MCRYPT_RIJNDAEL_128,
            hash('sha256', $full_salt, true),
            $pass,
            MCRYPT_MODE_CBC,
            $iv
        )
    );

    return array($encrypted,$random_salt_b64);
}

function decrypt_password($pass,$salt=''){
    $random_salt = get_random_salt($salt);// this appends global passwd
    $data = base64_decode($pass);
    $iv = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));

    $decrypted = rtrim(
        mcrypt_decrypt(
            MCRYPT_RIJNDAEL_128,
            hash('sha256', $random_salt, true),
            substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
            MCRYPT_MODE_CBC,
            $iv
        ),
        "\0"
    );

    return $decrypted;
}

function user_management_manage_spark_user_form_validate($form, &$form_state) {
    $sparkUserName = $form_state['values']['spark_user_name'];
    $sparkUserEmail = $form_state['values']['spark_user_email'];
    $alias = $form_state['values']['spark_user_alias'];
    if(trim($sparkUserName) == '') 
        form_set_error('spark_user_name', 'Spark User Name Cannot be Empty');
    if(preg_match('/[^0-9A-Za-z-]/', $sparkUserName))
        form_set_error('spark_user_name', 'Only Alphanumeric characters and "-" allowed in Spark User Name');
    if(!valid_email_address(trim($sparkUserEmail)))
        form_set_error('spark_user_email', 'Invalid Or Empty Spark User Email');
    if(trim($alias) == '') 
        form_set_error('spark_user_alias', 'Spark User Alias Cannot be Empty');
}

function user_management_edit_block_view() {
    $form = drupal_get_form('user_management_edit_spark_user_form');
    $block['content'] = $form;
    return $block;
}

function user_management_edit_spark_user_form($form) {
    $sparkUserId = $_GET['id'];
    $result = db_select('spark_users', 'c')
    ->fields('c', array('spark_user_id', 'spark_user_name', 'spark_user_email', 'alias'))
    ->condition('spark_user_id', $sparkUserId, '=')
    ->execute()
    ->fetchAssoc();
    
    $form['edit_spark_user_name'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#value' => $result['spark_user_name'],
        '#title' => t('Enter Spark User Name'),
        '#attributes' => array(
            'placeholder' => t('Enter Spark User Name')
        )
    );
    $form['edit_spark_user_email'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#value' => $result['spark_user_email'],
        '#title' => t('Enter Spark User Email'),
        '#attributes' => array(
            'placeholder' => t('Enter Spark User Email')
        )
    );
    $form['edit_spark_user_alias'] = array(
        '#type' => 'textfield',
        '#required' => 'required',
        '#value' => $result['alias'],
        '#title' => t('Enter Spark User Alias Name'),
        '#attributes' => array(
            'placeholder' => t('Enter Spark User Alias Name')
        )
    );
    $form['edit_form_submit'] = array(
        '#type' => 'submit',
        '#value' => 'Update Spark User Details',
    );
    return $form;
}

function user_management_edit_spark_user_form_validate($form, &$form_state) {
    $sparkUserName = $form_state['input']['edit_spark_user_name'];
    $sparkUserEmail = $form_state['input']['edit_spark_user_email'];
    $alias = $form_state['input']['edit_spark_user_alias'];
    if(trim($sparkUserName) == '') 
        form_set_error('edit_spark_user_name', 'Spark User Name Cannot be Empty');
    if(preg_match('/[^0-9A-Za-z-]/', $sparkUserName))
        form_set_error('edit_spark_user_name', 'Only Alphanumeric characters and "-" allowed in Spark User Name');
    if(!valid_email_address(trim($sparkUserEmail)))
        form_set_error('edit_spark_user_email', 'Invalid Or Empty Spark User Email');
    if(trim($alias) == '') 
        form_set_error('edit_spark_user_alias', 'Spark User Alias Cannot be Empty');
}

function user_management_edit_spark_user_form_submit($form, &$form_state) {
    try{
      $sparkUserId = $_GET['id'];
      $fieldsToUpdate = array(
        'spark_user_name' => $form_state['input']['edit_spark_user_name'],
        'spark_user_email' => $form_state['input']['edit_spark_user_email'],
        'alias' => $form_state['input']['edit_spark_user_alias']
      );
      $result = db_update('spark_users')
      ->fields($fieldsToUpdate)
      ->condition('spark_user_id', $sparkUserId, '=')
      ->execute();
      if($result)
          drupal_set_message(t("Spark User Details Updated."));
    } catch(PDOException $ex) {
      $message = $ex->getMessage();
      $message = $ex->getCode() == "23000" ? $form_state['input']['edit_spark_user_name'] .' already exists' : $message;
      drupal_set_message(t($message), 'error');
      $form_state['rebuild'] = true;
    }
}

function user_management_delete_spark_user_block_view() {
    $sparkUserId = $_GET['id'];
    $num_updated = db_delete('spark_users')
    ->condition('spark_user_id', $sparkUserId, '=')
    ->execute();
    drupal_set_message(t('Record has been deleted!'));
    drupal_goto('manage/spark_user');
}
// get password of a community id 
function decryptCommunityPwd($community_id){
    $decryptPass  ='';
    if($community_id !='' ){
        $res = db_query("SELECT community_id, pwd,slt FROM {sisense_community_pwd} WHERE community_id = $community_id and active= 1" );
        while($row = $res->fetchAssoc()){
            $community_id = $row['community_id'];
            $pwd = $row['pwd'];
            $slt = $row['slt'];
        }

        if($pwd !='')
            $decryptPass  = decrypt_password($pwd,$slt);
    }
    

    return $decryptPass;
}