<?php
// $Id: user.prefs.inc $

/**
 * @file
 * Saves and loads user preferences.
 * 
 * Registered users preferences are stored in a database, anonymous user data in session.
 *
 */
 
function get_user_preferences($input) { 
  $prefs = _get_user_preferences(); 
  //var_dump($prefs);
  if (is_array($input)) {    // Input is an array of names
    $output = array();
    foreach ($input as $name) {
      if (isset($prefs[$name])) {    
        $output[$name] = $prefs[$name];
      }
      else {
        $output[$name] = '';
      }
    }
  }
  else {                    // Input is a single name
    if (isset($prefs[$input])) {    
      $output = $prefs[$input];
    }
    else {
      $output = '';
    }
  }
  return $output;
}

function set_user_preferences($in_prefs) {
  $prefs = _get_user_preferences();
  foreach ($in_prefs as $name => $value) {
    $prefs[$name] = $value;
  } 
  _set_user_preferences($prefs);
}

function _get_user_preferences() {
  global $user;
  /*
  if ($user->uid) { // Registered user, preferences in database
    $result = db_query("SELECT value FROM {user_preferences} WHERE uid = '%d'", $user->uid);
    if ($result) {
      return unserialize(db_result($result));
    }   
  }
  else {            // Anonymous user, preferences in cookies
    if (isset($_SESSION['custom_login'])) {
      return $_SESSION['custom_login'];      
    } 
  }
  */
  if (isset($_SESSION['custom_login'])) {
      return $_SESSION['custom_login'];      
    }
  return array();  
}

function _set_user_preferences($prefs) {
  global $user;
  /*if ($user->uid) { // Registered user, preferences in database  
    $serialized_prefs = serialize($prefs); 
    db_query("UPDATE {user_preferences} SET value = '%s' WHERE uid = '%d'", $serialized_prefs, $user->uid);
    if (!db_affected_rows()) {
      @db_query("INSERT INTO {user_preferences} (uid, value) VALUES ('%d', '%s')", $user->uid, $serialized_prefs);
    }
  }
  else {            // Anonymous user, preferences in session
    if (!isset($_SESSION['custom_login'])) {
      $_SESSION['custom_login'] = array();
    }
    foreach ($prefs as $name => $value) {
      $_SESSION['custom_login'][$name] = $value;
    }
  }
  */
  if (!isset($_SESSION['custom_login'])) {
      $_SESSION['custom_login'] = array();
    }
    foreach ($prefs as $name => $value) {
      $_SESSION['custom_login'][$name] = $value;

    }
}