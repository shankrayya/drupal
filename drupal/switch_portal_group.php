<?php
define('DRUPAL_ROOT', getcwd());
include_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
if(count($_POST) < 1)
  exit('Access Denied');
echo json_encode(closeExistingAndCreateNewJwtForGroup($_POST['group_id']));