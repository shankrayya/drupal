<?php 

define('DRUPAL_ROOT', getcwd());

include_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
//create_jwt_token($return_url);
//exit();
$app_code = variable_get('jwt_secret_key');
if(isset($_SESSION['custom_login']['sisense_username'])){
	$username = $_SESSION['custom_login']['sisense_username'];
} else {
	if ( $parts = parse_url( $_SERVER['HTTP_REFERER'] ) ) {
		if($parts['host'] == 'localhost'){
			$username = variable_get('sisense_admin');
		} else if($parts['host'] == 'demo.mystn.com' ){
			$username = 'CTMGR@Agency.com';
		} else {	
				echo "not local";
				exit();
		}	
	} else {
		echo "Authentication failed";
		exit();
	}
}	

$rand = rand(0,99999);
$now = time();
$token = array(
	"iat" => $now,
	"sub" => $username,
	"jti" =>'xx'.$rand
);

$jwt_token= JWT::encode($token, $app_code);
$jwt_url =variable_get('jwt_url').$jwt_token;
if(isset($_GET["return_to"])) {
	$jwt_url .= "&return_to=" . urlencode($_GET["return_to"]);
}
header('location: '.$jwt_url);
exit();