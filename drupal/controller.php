<?php
define('DRUPAL_ROOT', getcwd());

include_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
$redirect_url = $_SERVER["HTTP_REFERER"];

$action='';
$action = (isset($_REQUEST['action']))  ? $_REQUEST['action']:'';

switch($action){
	
	case 'getCommunityChatEnabled':
		$response=getCommunityChatEnabled();
		//echo json_encode($response);
		break;
	
	case 'getUserCommunityLogo':
		$username = (isset($_REQUEST['username']))  ? $_REQUEST['username']:'';
		$response=get_user_community_logo($username);
		echo $response;
		break;
	/*	
	case 'getUserNotification':
		$response=get_user_notification();
		echo json_encode($response);
		break;
		
		
	case 'checkNewThreads':
		$response=check_new_threads();
		echo $response;
		break;
		
	case 'updateSeenNotification':
		$threadId = (isset($_REQUEST['threadId']))  ? $_REQUEST['threadId']:'';
		$response=update_seen_notification($threadId);
		echo json_encode($response);
		break;
	*/	
}


if(isset($_SESSION['account'])){
	resend_otp_email();
	
}else{
	$url_part = variable_get('drupal_dashboard_base_url');
	if(strpos($redirect_url,$url_part) !==false) {
		create_jwt_token($redirect_url);	
	}

}


exit();