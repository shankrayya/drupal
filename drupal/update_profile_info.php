<?php
define('DRUPAL_ROOT', getcwd());
include_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
if(count($_POST) < 1)
  exit('Access Denied');
module_load_include('inc', 'user_profile');
user_profile_update_profile_block_view();