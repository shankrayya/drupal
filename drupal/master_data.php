<!DOCTYPE html>
<html>
<head>
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="master_data/script1.js"></script>
  <link rel="stylesheet" type="text/css" href="master_data/style1.css">
  <link rel="stylesheet" type="text/css" href="master_data/animate.min.css">

</head>
<body>


<div class="menu">
<ul class="docs1 clearfix">
      <li class="tab00"><a href="master_data/master_data.php"><p style="font-family: sans-serif;">Travel Pie</p></a></li>

    </ul>
   </div>


   <div class="sidebar">
   
  <ul class="docs clearfix" style="color: white;">Masters
  <br><br><br><br>
      <ul class="tab1"><a href="master_data/air.php"><p1 style="color: black;margin-right: 30px;">Air</p1></a></ul>
      <ul class="tab2"><a href="master_data/hotel.php"><p2 style="color: black;margin-right: 30px;"">Hotel</p2></a></ul>
      <ul class="tab3"><a href="master_data/car.php"><p2 style="color: black;">Car</p2></a></ul>
      <ul class="tab4"><a href="master_data/generic.php"><p2 style="color: black;">Generic</p2></a></ul>
  </ul>
</div> 


<div class="box">
<div class="content">

<div class="img1">
  <img src="master_data/airline.jpg" alt="airline table" style="width:152px;height:152px;border:0;">
</div>

<div class="img2">
  <img src="master_data/airport.jpg" alt="airport table" style="width:152px;height:152px;border:0;">
</div>

<div class="img3">
  <img src="master_data/city.jpg" alt="city table" style="width:152px;height:152px;border:0;">
</div>

<div class="img4">
 <img src="master_data/country.jpg" alt="country table" style="width:152px;height:152px;border:0;">
</div>

<div class="img5">
 <img src="master_data/currency.jpg" alt="currency table" style="width:152px;height:152px;border:0;">
</div>

</div>
</div>

</body>
</html>