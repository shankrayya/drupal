<?php
define('DRUPAL_ROOT', getcwd());

include_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$action ='';
foreach($_REQUEST as $key =>$val){
	${$key}=$val;
}

switch($action){
	case 'get_users':
		$response = get_users();
		echo json_encode($response);
		break;
		
	case 'addUserTag':
		$response = add_user_tag($thread_id,$user_id,$content);
		echo json_encode($response);
		break;
		
	case 'removeUserTag':
		remove_user_tag($thread_id,$user_id);
		break;
		
	case 'getUsertags':
		$response=get_user_tags($thread_id);
		echo json_encode($response);
		break;
		
	case 'getUserImage':
		$response=get_user_image($user_id);
		echo json_encode($response);
		break;
}


exit();