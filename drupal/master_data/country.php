<!DOCTYPE html>
<html>
<head>
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="script11.js"></script>
  <link rel="stylesheet" type="text/css" href="style1.css">
  <link rel="stylesheet" type="text/css" href="animate.min.css">
  
  <style type="text/css">
    
    .box .content3a{
      display: none;
    }

    .docs li {
      float: left;
      padding: 8px 5px 8px 15px;
    }

    .sidebar{
      width: 148px;
    }

    button{
       position: fixed;
       left: 183px;
    }

  </style>

  <script>
      $(document).ready(function(){
  $('.content3 .tab3a a').click(function(e) {
  $(this).addClass('active');
  $('.box .content3').hide();
  $('.box .content3a').show();  
});
});


   $(document).ready(function(){
  $('.content3a .tab3aa a').click(function(e) {
  $(this).addClass('active');
  $('.box .content3').show();
  $('.box .content3a').hide();  
});
});
  </script>

</head>
<body>

<div class="menu">
<ul class="docs1 clearfix">
      <li class="tab00"><a href="newindex.php"><p style="font-family: sans-serif;">Travel Pie</p></a></li>
    </ul>
   </div>


   <div class="sidebar">
  <ul class="docs clearfix" style="color: white;">Masters
  <br><br><br><br>
    <ul class="tab1"><a href="air.php"><p1 style="color: black;margin-right: 30px;">Air</p1></a></ul>
    <ul class="tab2"><a href="hotel.php"><p2 style="color: black;margin-right: 30px;"">Hotel</p2></a></ul>
    <ul class="tab3"><a href="car.php"><p2 style="color: black;">Car</p2></a></ul>
    <ul class="tab4"><a href="generic.php"><p2 style="color: white;">Generic</p2></a></ul>
    <li class="tab3"><a href="country.php" style="color: white;">- Country</a></li>
    <li class="tab4"><a href="city.php">- City</a></li>
    <li class="tab5"><a href="currency.php">- Currency</a></li>
  </ul>
</div>


<div class="box">

<div class="file content3">
        <form action="search3.php" method="post">
<div id="srch">
  <input type="text" id="usrn" name="usrn11" required="required" style="background-color:#f2f2f2">
  <input type="submit" name="submit" value="Search" style="width:85">
</div>
<br>
<br>
<div class="nr">
<a href='add3.php'>
<input type='button' name='add' value='Add Record'>
</a>
</div>
<br>

<br>
<div class="tab3a">
<a href='javascript:void(0)'>
  <button>View All</button>
</a>
</div>
<br>

<?php

include("conn.php");

if ($conn === false) 
  die("<pre>".print_r(sqlsrv_errors(), true));

$sql = "SELECT * FROM dbo.country WHERE IS_ACTIVE = 1";
$query = sqlsrv_query($conn, $sql);
if ($query === false){
  exit("<pre>".print_r(sqlsrv_errors(), true));
}

echo "<h3>Active Country</h3><table id='mytable3' border=2 rules='cols'>
<tr>
<th>Code</th>
<th>Name</th>
<th>Currency</th>
<th></th>
<th></th>
</tr>";

$fc  = "#f2f2f2";
$sc = "#b7c9ef";
$i3=0;

while($row = sqlsrv_fetch_array($query))
{
  $color = ($i3%2==0) ? $sc : $fc;
echo "<tr bgcolor='$color'>";
echo "<td width=140>" . $row['COUNTRY_CODE'] . "</td>";
echo "<td width=140>" . $row['COUNTRY_NAME'] . "</td>";
echo "<td width=140>" . $row['CURRENCY'] . "</td>";
echo "<td width=60><a href='edit3.php?id=".$row['COUNTRY_KEY']."'>Edit</a></td>";
echo "<td width=60><a href='delete3.php?id=".$row['COUNTRY_KEY']."'>Delete</a></td>";

echo "</tr>";
$i3++;

}
echo "</table>";

sqlsrv_free_stmt($query);

?>
</form>

<p id=count3 style="text-align: right;"></p>
<script type="text/javascript">
  var x3 = document.getElementById("mytable3").rows.length;
  var rows = x3-1;
  document.getElementById("count3").innerHTML=rows + " Entries";
</script>
</div>




<div class="file content3a">
        <form action="search3.php" method="post">
<div id="srch">
  <input type="text" id="usrn" name="usrn11" required="required" style="background-color:#f2f2f2">
  <input type="submit" name="submit" value="Search" style="width:85">
</div>
<br>
<br>
<div class="nr">
<a href='add3.php'>
<input type='button' name='add' value='Add Record'>
</a>
</div>
<br>

<br>
<div class="tab3aa">
<a href='javascript:void(0)'>
  <button>View Active</button>
</a>
</div>
<br>

<?php

if ($conn === false) 
  die("<pre>".print_r(sqlsrv_errors(), true));

$sql = "SELECT * FROM dbo.country";
$query = sqlsrv_query($conn, $sql);
if ($query === false){
  exit("<pre>".print_r(sqlsrv_errors(), true));
}

echo "<h3>Country</h3><table id='mytable3a' border=2 rules='cols'>
<tr>
<th>Code</th>
<th>Name</th>
<th>Currency</th>
<th>Status</th>
<th></th>
<th></th>
</tr>";

$fc  = "#f2f2f2";
$sc = "#b7c9ef";
$i3a=0;

while($row = sqlsrv_fetch_array($query))
{
  $color = ($i3a%2==0) ? $sc : $fc;
echo "<tr bgcolor='$color'>";
echo "<td width=140>" . $row['COUNTRY_CODE'] . "</td>";
echo "<td width=140>" . $row['COUNTRY_NAME'] . "</td>";
echo "<td width=140>" . $row['CURRENCY'] . "</td>";
echo "<td width=80>" . $row['IS_ACTIVE'] . "</td>";
echo "<td width=60><a href='edit3.php?id=".$row['COUNTRY_KEY']."'>Edit</a></td>";
echo "<td width=60><a href='delete3.php?id=".$row['COUNTRY_KEY']."'>Delete</a></td>";

echo "</tr>";
$i3a++;

}
echo "</table>";

sqlsrv_free_stmt($query);

sqlsrv_close($conn);
?>
</form>

<p id=count3a style="text-align: right;"></p>
<script type="text/javascript">
  var x3a = document.getElementById("mytable3a").rows.length;
  var rows = x3a-1;
  document.getElementById("count3a").innerHTML=rows + " Entries";
</script>
</div>

</div>
</body>
</html>