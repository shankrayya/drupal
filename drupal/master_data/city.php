<!DOCTYPE html>
<html>
<head>
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="script11.js"></script>
  <link rel="stylesheet" type="text/css" href="style1.css">
  <link rel="stylesheet" type="text/css" href="animate.min.css">
  
  <style type="text/css">
    
    .box .content4a{
      display: none;
    }

    .docs li {
      float: left;
      padding: 8px 5px 8px 15px;
    }

    .sidebar{
      width: 148px;
    }

    button{
       position: fixed;
       left: 183px;
    }

  </style>

  <script>
      $(document).ready(function(){
  $('.content4 .tab4a a').click(function(e) {
  $(this).addClass('active');
  $('.box .content4').hide();
  $('.box .content4a').show();  
});
});


   $(document).ready(function(){
  $('.content4a .tab4aa a').click(function(e) {
  $(this).addClass('active');
  $('.box .content4').show();
  $('.box .content4a').hide();  
});
});
  </script>

</head>
<body>


<div class="menu">
<ul class="docs1 clearfix">
      <li class="tab00"><a href="../all"><p style="font-family: sans-serif;">Travel Pie Portal</p></a></li>
	  <li class="tab00"><a href="../master_data.php"><p style="font-family: sans-serif;">Master Data</p></a></li>
    </ul>
   </div>


   <div class="sidebar">
  <ul class="docs clearfix" style="color: white;">Masters
  <br><br><br><br>
    <ul class="tab1"><a href="air.php"><p1 style="color: black;margin-right: 30px;">Air</p1></a></ul>
    <ul class="tab2"><a href="hotel.php"><p2 style="color: black;margin-right: 30px;"">Hotel</p2></a></ul>
    <ul class="tab3"><a href="car.php"><p2 style="color: black;">Car</p2></a></ul>
    <ul class="tab4"><a href="generic.php"><p2 style="color: white;">Generic</p2></a></ul>
    <li class="tab3"><a href="country.php">- Country</a></li>
    <li class="tab4"><a href="city.php" style="color: white;">- City</a></li>
    <li class="tab5"><a href="currency.php">- Currency</a></li>
  </ul>
</div>


<div class="box">

<div class="file content4">
   <form action="search4.php" method="post">
<div id="srch">
  <input type="text" id="usrn" name="usrn11" required="required" style="background-color:#f2f2f2">
  <input type="submit" name="submit" value="Search" style="width:85">
</div>
<br>
<br>
<div class="nr">
<a href='add4.php'>
<input type='button' name='add' value='Add Record'>
</a>
</div>
<br>

<br>
<div class="tab4a">
<a href='javascript:void(0)'>
  <button>View All</button>
</a>
</div>
<br>

<?php

include("conn.php");

if ($conn === false) 
  die("<pre>".print_r(sqlsrv_errors(), true));

$sql = "SELECT * FROM dbo.city WHERE IS_ACTIVE = 1";
$query = sqlsrv_query($conn, $sql);
if ($query === false){
  exit("<pre>".print_r(sqlsrv_errors(), true));
}

echo "<h3>Active City</h3><table id='mytable4' border=2 rules='cols'>
<tr>
<th>Code</th>
<th>Name</th>
<th></th>
<th></th>
</tr>";

$fc  = "#f2f2f2";
$sc = "#b7c9ef";
$i4=0;

while($row = sqlsrv_fetch_array($query))
{
  $color = ($i4%2==0) ? $sc : $fc;
echo "<tr bgcolor='$color'>";
echo "<td width=140>" . $row['CITY_CODE'] . "</td>";
echo "<td width=140>" . $row['CITY_NAME'] . "</td>";
echo "<td width=60><a href='edit4.php?id=".$row['CITY_KEY']."'>Edit</a></td>";
echo "<td width=60><a href='delete4.php?id=".$row['CITY_KEY']."'>Delete</a></td>";

echo "</tr>";
$i4++;

}
echo "</table>";

sqlsrv_free_stmt($query);

?>
</form>

<p id=count4 style="text-align: right;"></p>
<script type="text/javascript">
  var x4 = document.getElementById("mytable4").rows.length;
  var rows = x4-1;
  document.getElementById("count4").innerHTML=rows + " Entries";
</script> 
</div>



<div class="file content4a">
   <form action="search4.php" method="post">
<div id="srch">
  <input type="text" id="usrn" name="usrn11" required="required" style="background-color:#f2f2f2">
  <input type="submit" name="submit" value="Search" style="width:85">
</div>
<br>
<br>
<div class="nr">
<a href='add4.php'>
<input type='button' name='add' value='Add Record'>
</a>
</div>
<br>

<br>
<div class="tab4aa">
<a href='javascript:void(0)'>
  <button>View Active</button>
</a>
</div>
<br>

<?php

if ($conn === false) 
  die("<pre>".print_r(sqlsrv_errors(), true));

$sql = "SELECT * FROM dbo.city";
$query = sqlsrv_query($conn, $sql);
if ($query === false){
  exit("<pre>".print_r(sqlsrv_errors(), true));
}

echo "<h3>City</h3><table id='mytable4a' border=2 rules='cols'>
<tr>
<th>Code</th>
<th>Name</th>
<th>Status</th>
<th></th>
<th></th>
</tr>";

$fc  = "#f2f2f2";
$sc = "#b7c9ef";
$i4a=0;

while($row = sqlsrv_fetch_array($query))
{
  $color = ($i4a%2==0) ? $sc : $fc;
echo "<tr bgcolor='$color'>";
echo "<td width=140>" . $row['CITY_CODE'] . "</td>";
echo "<td width=140>" . $row['CITY_NAME'] . "</td>";
echo "<td width=80>" . $row['IS_ACTIVE'] . "</td>";
echo "<td width=60><a href='edit4.php?id=".$row['CITY_KEY']."'>Edit</a></td>";
echo "<td width=60><a href='delete4.php?id=".$row['CITY_KEY']."'>Delete</a></td>";

echo "</tr>";
$i4a++;

}
echo "</table>";

sqlsrv_free_stmt($query);

sqlsrv_close($conn);
?>
</form>

<p id=count4a style="text-align: right;"></p>
<script type="text/javascript">
  var x4a = document.getElementById("mytable4a").rows.length;
  var rows = x4a-1;
  document.getElementById("count4a").innerHTML=rows + " Entries";
</script> 
</div>

</div>
</body>
</html>