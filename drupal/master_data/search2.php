<html>
<head>
	<style type="text/css">
		
	body{
		background-color: #ab9191;
	}

		form{
			margin-top: 150px;
		}

		table{
            margin:10px auto;
            border-collapse: collapse;
            width: 70%;
        }

		#count{
			text-align: right;
			font-size: 18px;
		}

		td{
            text-align: left;
            padding: 3px;
            font-size: 17px;
        }

        th {
            text-align: left;
            padding: 5px;
            background-color: #3f3342;
            color: #f2f2f2;
            font-size: 19px;

        }

		input{
			width: 85px;
		}
	</style>
</head>
<body>

<form>
<?php

include("conn.php");

if ($conn === false) 
  die("<pre>".print_r(sqlsrv_errors(), true));

//Create Variables
$usrn11 = $_POST['usrn11'];

if (is_numeric($usrn11)) {
	$sql = "SELECT * FROM dbo.airpot where AIRPORT_KEY='{$usrn11}'";
}

elseif (!is_numeric($usrn11)) {

	$sql = "SELECT * FROM dbo.airport where AIRPORT_NAME='{$usrn11}' OR AIRPORT_CODE='{$usrn11}' OR CITY_CODE='{$usrn11}' OR COUNTRY_CODE='{$usrn11}'";
}

$query = sqlsrv_query($conn, $sql);
if ($query === false){
  exit("<pre>".print_r(sqlsrv_errors(), true));
}

echo "<table id='mytable2' border=2 rules='cols'>
<tr>
<th>Code</th>
<th>Name</th>
<th>City Code</th>
<th>Country Code</th>
<th>Latitude</th>
<th>Longitude</th>
<th>Status</th>
<th></th>
<th></th>
</tr>";

$fc  = "#f2f2f2";
$sc = "#b7c9ef";
$i=0;

while($row = sqlsrv_fetch_array($query))
{
	$color = ($i%2==0) ? $sc : $fc;
echo "<tr bgcolor='$color' border= 1px solid black>";
echo "<td width=140>" . $row['AIRPORT_CODE'] . "</td>";
echo "<td width=100>" . $row['AIRPORT_NAME'] . "</td>";
echo "<td width=100>" . $row['CITY_CODE'] . "</td>";
echo "<td width=180>" . $row['COUNTRY_CODE'] . "</td>";
echo "<td width=140>" . $row['LATITUDE'] . "</td>";
echo "<td width=140>" . $row['LONGITUDE'] . "</td>";
echo "<td width=80>" . $row['IS_ACTIVE'] . "</td>";
echo "<td width=90><a href='edit2.php?id=".$row['AIRPORT_KEY']."'>Edit</a></td>";
echo "<td width=60><a href='delete2.php?id=".$row['AIRPORT_KEY']."'>Delete</a></td>";

echo "</tr>";
$i++;
}
echo "</table>";

echo "<a href='airport.php'>
		<input type='button' value='Back' style='margin-left:650px'>
		</a>";


sqlsrv_free_stmt($query);

sqlsrv_close($conn);
?>
</form>

<p id=count2></p>
<script type="text/javascript">
	var x = document.getElementById("mytable2").rows.length;
	var rows = x-1;
	document.getElementById("count2").innerHTML=rows + " Records";
</script>
</body>
</html>