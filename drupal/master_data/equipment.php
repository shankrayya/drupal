<!DOCTYPE html>
<html>
<head>
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="script11.js"></script>
  <link rel="stylesheet" type="text/css" href="styleside.css">
  <link rel="stylesheet" type="text/css" href="animate.min.css">
  
  <style type="text/css">
    
    .box .content6a{
      display: none;
    }

    .docs li {
      float: left;
      padding: 8px 5px 8px 15px;
    }
    .sidebar{
      width: 148px;
    }

    button{
       position: fixed;
       left: 183px;
    }

  </style>

  <script>
      $(document).ready(function(){
  $('.content6 .tab6a a').click(function(e) {
  $(this).addClass('active');
  $('.box .content6').hide();
  $('.box .content6a').show();  
});
});


   $(document).ready(function(){
  $('.content6a .tab6aa a').click(function(e) {
  $(this).addClass('active');
  $('.box .content6').show();
  $('.box .content6a').hide();  
});
});
  </script>

</head>
<body>


<div class="menu">
<ul class="docs1 clearfix">
      <li class="tab00"><a href="../all"><p style="font-family: sans-serif;">Travel Pie Portal</p></a></li>
	  <li class="tab00"><a href="../master_data.php"><p style="font-family: sans-serif;">Master Data</p></a></li>
      </ul>
   </div>


   <div class="sidebar">
  <ul class="docs clearfix" style="color: white;">Masters
  <br><br><br><br>
      <ul style="list-style-type: square;" class="tab1"><a href="air1.php"><p1 style="color: white;margin-right: 30px;">Air</p1></a></ul>
    <li class="tab1"><a href="airline.php">- Airline</a></li>
    <li class="tab2"><a href="airport.php">- Airport</a></li>
    <li class="tab6"><a href="equipment.php" style="color: white">- Equipment</a></li>
    <li class="tab7"><a href="status.php">- Status</a></li>
    <li class="tab8"><a href="ssr.php" style="margin-right: 20px;">- SSR</a></li>
    <li class="tab9"><a href="pcc.php">- PCC</a></li>
    <li class="tab10"><a href="custdk.php">- Cust DK</a></li>
    <li class="tab13"><a href="segment.php">- Segment</a></li>
    <ul class="tab2"><a href="hotel.php"><p2 style="color: black;margin-right: 30px;"">Hotel</p2></a></ul>
    <ul class="tab3"><a href="car.php"><p2 style="color: black;">Car</p2></a></ul>
    <ul class="tab4"><a href="generic.php"><p2 style="color: black;">Generic</p2></a></ul>
  </ul>
</div>


<div class="box">

<div class="file content6">
   <form action="search6.php" method="post">
<div id="srch">
  <input type="text" id="usrn" name="usrn11" required="required" style="background-color:#f2f2f2">
  <input type="submit" name="submit" value="Search" style="width:85">
</div>
<br>

<br>
<div class="nr">
<a href='add6.php'>
<input type='button' name='add' value='Add Record'>
</a>
</div>
<br>

<br>
<div class="tab6a">
<a href='javascript:void(0)'>
  <button>View All</button>
</a>
</div>
<br>

<?php

include("conn.php");

if ($conn === false) 
  die("<pre>".print_r(sqlsrv_errors(), true));

$sql = "SELECT * FROM dbo.equipment WHERE IS_ACTIVE = 1";
$query = sqlsrv_query($conn, $sql);
if ($query === false){
  exit("<pre>".print_r(sqlsrv_errors(), true));
}

echo "<h3>Active Equipment</h3><table id='mytable6' border=2 rules='cols'>
<tr>
<th>Code</th>
<th>Name</th>
<th></th>
<th></th>
</tr>";

$fc  = "#f2f2f2";
$sc = "#b7c9ef";
$i6=0;

while($row = sqlsrv_fetch_array($query))
{
  $color = ($i6%2==0) ? $sc : $fc;
echo "<tr bgcolor='$color'>";
echo "<td width=100>" . $row['CODE'] . "</td>";
echo "<td width=100>" . $row['NAME'] . "</td>";
echo "<td width=60><a href='edit6.php?id=".$row['EQUIPMENT_KEY']."'>Edit</a></td>";
echo "<td width=60><a href='delete6.php?id=".$row['EQUIPMENT_KEY']."'>Delete</a></td>";
echo "</tr>";
$i6++;

}
echo "</table>";

sqlsrv_free_stmt($query);

?>
</form>

<p id=count6 style="text-align: right;"></p>
<script type="text/javascript">
  var x6= document.getElementById("mytable6").rows.length;
  var rows = x6-1;
  document.getElementById("count6").innerHTML=rows + " Entries";
</script> 
</div>


<div class="file content6a">
   <form action="search6.php" method="post">
<div id="srch">
  <input type="text" id="usrn" name="usrn11" required="required" style="background-color:#f2f2f2">
  <input type="submit" name="submit" value="Search" style="width:85">
</div>
<br>
<br>
<div class="nr">
<a href='add6.php'>
<input type='button' name='add' value='Add Record'>
</a>
</div>
<br>

<br>
<div class="tab6aa">
<a href='javascript:void(0)'>
  <button>View Active</button>
</a>
</div>
<br>

<?php

if ($conn === false) 
  die("<pre>".print_r(sqlsrv_errors(), true));

$sql = "SELECT * FROM dbo.equipment";
$query = sqlsrv_query($conn, $sql);
if ($query === false){
  exit("<pre>".print_r(sqlsrv_errors(), true));
}

echo "<h3>Equipment</h3><table id='mytable6a' border=2 rules='cols'>
<tr>
<th>Code</th>
<th>Name</th>
<th>Status</th>
<th></th>
<th></th>
</tr>";

$fc  = "#f2f2f2";
$sc = "#b7c9ef";
$i6a=0;

while($row = sqlsrv_fetch_array($query))
{
  $color = ($i6a%2==0) ? $sc : $fc;
echo "<tr bgcolor='$color'>";
echo "<td width=100>" . $row['CODE'] . "</td>";
echo "<td width=100>" . $row['NAME'] . "</td>";
echo "<td width=80>" . $row['IS_ACTIVE'] . "</td>";
echo "<td width=60><a href='edit6.php?id=".$row['EQUIPMENT_KEY']."'>Edit</a></td>";
echo "<td width=60><a href='delete6.php?id=".$row['EQUIPMENT_KEY']."'>Delete</a></td>";
echo "</tr>";
$i6a++;

}
echo "</table>";

sqlsrv_free_stmt($query);

sqlsrv_close($conn);
?>
</form>

<p id=count6a style="text-align: right;"></p>
<script type="text/javascript">
  var x6a= document.getElementById("mytable6a").rows.length;
  var rows = x6a-1;
  document.getElementById("count6a").innerHTML=rows + " Entries";
</script> 
</div>

</div>
</body>
</html>