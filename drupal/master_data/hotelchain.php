<!DOCTYPE html>
<html>
<head>
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="script11.js"></script>
  <link rel="stylesheet" type="text/css" href="style1.css">
  <link rel="stylesheet" type="text/css" href="animate.min.css">
  
  <style type="text/css">
    
    .box .content11a{
      display: none;
    }

    .docs li {
      float: left;
      padding: 8px 5px 8px 15px;
    }

    .sidebar{
      width: 148px;
    }
    
    button{
       position: fixed;
       left: 183px;
    }

  </style>

  <script>
      $(document).ready(function(){
  $('.content11 .tab11a a').click(function(e) {
  $(this).addClass('active');
  $('.box .content11').hide();
  $('.box .content11a').show();  
});
});


   $(document).ready(function(){
  $('.content11a .tab11aa a').click(function(e) {
  $(this).addClass('active');
  $('.box .content11').show();
  $('.box .content11a').hide();  
});
});
  </script>

</head>
<body>


<div class="menu">
<ul class="docs1 clearfix">
     <li class="tab00"><a href="../all"><p style="font-family: sans-serif;">Travel Pie Portal</p></a></li>
	  <li class="tab00"><a href="../master_data.php"><p style="font-family: sans-serif;">Master Data</p></a></li>
    </ul>
   </div>


   <div class="sidebar">
  <ul class="docs clearfix" style="color: white;">Masters
  <br><br><br><br>
    <ul class="tab1"><a href="air.php"><p1 style="color: black;margin-right: 30px;">Air</p1></a></ul>
    <ul class="tab2"><a href="hotel.php"><p2 style="color: white;margin-right: 30px;"">Hotel</p2></a></ul>
    <li class="tab111"><a href="hotelchain.php" style="color: white;">- Chain</a></li>
    <ul class="tab3"><a href="car.php"><p2 style="color: black;">Car</p2></a></ul>
    <ul class="tab4"><a href="generic.php"><p2 style="color: black;">Generic</p2></a></ul>
  </ul>
</div>


<div class="box">

<div class="file content11">
   <form action="search11.php" method="post">
<div id="srch">
  <input type="text" id="usrn" name="usrn11" required="required" style="background-color:#f2f2f2">
  <input type="submit" name="submit" value="Search" style="width:85">
</div>
<br>
<br>
<div class="nr">
<a href='add11.php'>
<input type='button' name='add' value='Add Record'>
</a>
</div>
<br>

<br>
<div class="tab11a">
<a href='javascript:void(0)'>
  <button>View All</button>
</a>
</div>
<br>

<?php

include("conn.php");

if ($conn === false) 
  die("<pre>".print_r(sqlsrv_errors(), true));

$sql = "SELECT * FROM dbo.HOTEL_CHAIN WHERE IS_ACTIVE = 1";
$query = sqlsrv_query($conn, $sql);
if ($query === false){
  exit("<pre>".print_r(sqlsrv_errors(), true));
}

echo "<h3>Active Hotel Chain</h3><table id='mytable11' border=2 rules='cols'>
<tr>
<th>Chain Code</th>
<th>Chain Name</th>
<th></th>
<th></th>
</tr>";

$fc  = "#f2f2f2";
$sc = "#b7c9ef";
$i11=0;

while($row = sqlsrv_fetch_array($query))
{
  $color = ($i11%2==0) ? $sc : $fc;
echo "<tr bgcolor='$color'>";
echo "<td width=100>" . $row['CHAIN_CODE'] . "</td>";
echo "<td width=100>" . $row['CHAIN_NAME'] . "</td>";
echo "<td width=60><a href='edit11.php?id=".$row['HOTEL_CHAIN_KEY']."'>Edit</a></td>";
echo "<td width=60><a href='delete11.php?id=".$row['HOTEL_CHAIN_KEY']."'>Delete</a></td>";
echo "</tr>";
$i11++;

}
echo "</table>";

sqlsrv_free_stmt($query);

?>
</form>

<p id=count11 style="text-align: right;"></p>
<script type="text/javascript">
  var x11= document.getElementById("mytable11").rows.length;
  var rows = x11-1;
  document.getElementById("count11").innerHTML=rows + " Entries";
</script> 
</div>



<div class="file content11a">
   <form action="search11.php" method="post">
<div id="srch">
  <input type="text" id="usrn" name="usrn11" required="required" style="background-color:#f2f2f2">
  <input type="submit" name="submit" value="Search" style="width:85">
</div>
<br>
<br>
<div class="nr">
<a href='add11.php'>
<input type='button' name='add' value='Add Record'>
</a>
</div>
<br>

<br>
<div class="tab11aa">
<a href='javascript:void(0)'>
  <button>View Active</button>
</a>
</div>
<br>

<?php

if ($conn === false) 
  die("<pre>".print_r(sqlsrv_errors(), true));

$sql = "SELECT * FROM dbo.HOTEL_CHAIN";
$query = sqlsrv_query($conn, $sql);
if ($query === false){
  exit("<pre>".print_r(sqlsrv_errors(), true));
}

echo "<h3>Active Hotel Chain</h3><table id='mytable11a' border=2 rules='cols'>
<tr>
<th>Chain Code</th>
<th>Chain Name</th>
<th>Status</th>
<th></th>
<th></th>
</tr>";

$fc  = "#f2f2f2";
$sc = "#b7c9ef";
$i11a=0;

while($row = sqlsrv_fetch_array($query))
{
  $color = ($i11a%2==0) ? $sc : $fc;
echo "<tr bgcolor='$color'>";
echo "<td width=100>" . $row['CHAIN_CODE'] . "</td>";
echo "<td width=100>" . $row['CHAIN_NAME'] . "</td>";
echo "<td width=80>" . $row['IS_ACTIVE'] . "</td>";
echo "<td width=60><a href='edit11.php?id=".$row['HOTEL_CHAIN_KEY']."'>Edit</a></td>";
echo "<td width=60><a href='delete11.php?id=".$row['HOTEL_CHAIN_KEY']."'>Delete</a></td>";
echo "</tr>";
$i11a++;

}
echo "</table>";

sqlsrv_free_stmt($query);

sqlsrv_close($conn);
?>
</form>

<p id=count11a style="text-align: right;"></p>
<script type="text/javascript">
  var x11a= document.getElementById("mytable11a").rows.length;
  var rows = x11a-1;
  document.getElementById("count11a").innerHTML=rows + " Entries";
</script> 
</div>


</div>
</body>
</html5