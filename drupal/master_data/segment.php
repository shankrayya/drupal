<!DOCTYPE html>
<html>
<head>
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="script11.js"></script>
  <link rel="stylesheet" type="text/css" href="styleside.css">
  <link rel="stylesheet" type="text/css" href="animate.min.css">
  
  <style type="text/css">
    
    .box .content13a{
      display: none;
    }

    .docs li {
      float: left;
      padding: 8px 5px 8px 15px;
    }

    .sidebar{
      width: 148px;
    }

    button{
       position: fixed;
       left: 183px;
    }

  </style>

  <script>
      $(document).ready(function(){
  $('.content13 .tab13a a').click(function(e) {
  $(this).addClass('active');
  $('.box .content13').hide();
  $('.box .content13a').show();  
});
});


   $(document).ready(function(){
  $('.content13a .tab13aa a').click(function(e) {
  $(this).addClass('active');
  $('.box .content13').show();
  $('.box .content13a').hide();  
});
});
  </script>

</head>
<body>


<div class="menu">
<ul class="docs1 clearfix">
      <li class="tab00"><a href="newindex.php"><p style="font-family: sans-serif;">Travel Pie</p></a></li>
    </ul>
   </div>


   <div class="sidebar">
  <ul class="docs clearfix" style="color: white;">Masters
  <br><br><br><br>
    <ul style="list-style-type: square;" class="tab1"><a href="air1.php"><p1 style="color: white;margin-right: 30px;">Air</p1></a></ul>
    <li class="tab1"><a href="airline.php">- Airline</a></li>
    <li class="tab2"><a href="airport.php">- Airport</a></li>
    <li class="tab6"><a href="equipment.php">- Equipment</a></li>
    <li class="tab7"><a href="status.php">- Status</a></li>
    <li class="tab8"><a href="ssr.php" style="margin-right: 20px;">- SSR</a></li>
    <li class="tab9"><a href="pcc.php">- PCC</a></li>
    <li class="tab10"><a href="custdk.php">- Cust DK</a></li>
    <li class="tab13"><a href="segment.php" style="color: white">- Segment</a></li>
    <ul class="tab2"><a href="hotel.php"><p2 style="color: black;margin-right: 30px;"">Hotel</p2></a></ul>
    <ul class="tab3"><a href="car.php"><p2 style="color: black;">Car</p2></a></ul>
    <ul class="tab4"><a href="generic.php"><p2 style="color: black;">Generic</p2></a></ul>
  </ul>
</div>


<div class="box">

<div class="file content13">
   <form action="search13.php" method="post">
<div id="srch">
  <input type="text" id="usrn" name="usrn11" required="required" style="background-color:#f2f2f2">
  <input type="submit" name="submit" value="Search" style="width:85">
</div>
<br>
<br>
<div class="nr">
<a href='add13.php'>
<input type='button' name='add' value='Add Record'>
</a>
</div>
<br>

<br>
<div class="tab13a">
<a href='javascript:void(0)'>
  <button>View All</button>
</a>
</div>
<br>

<?php

include("conn.php");
if ($conn === false) 
  die("<pre>".print_r(sqlsrv_errors(), true));

$sql = "SELECT * FROM dbo.segment WHERE IS_ACTIVE = 1";
$query = sqlsrv_query($conn, $sql);
if ($query === false){
  exit("<pre>".print_r(sqlsrv_errors(), true));
}

echo "<h3>Active Segment</h3><table id='mytable13' border=2 rules='cols'>
<tr>
<th>Origin Airport</th>
<th>Destination Airport</th>
<th>Sector</th>
<th>Seg Origin</th>
<th>Seg Destination</th>
<th>Distance</th>
<th>Flight Type</th>
<th></th>
<th></th>
</tr>";

$fc  = "#f2f2f2";
$sc = "#b7c9ef";
$i13=0;

while($row = sqlsrv_fetch_array($query))
{
  $color = ($i13%2==0) ? $sc : $fc;
echo "<tr bgcolor='$color'>";
echo "<td width=140>" . $row['ORIGIN_AIRPORT'] . "</td>";
echo "<td width=140>" . $row['DESTINATION_AIRPORT'] . "</td>";
echo "<td width=140>" . $row['SECTOR'] . "</td>";
echo "<td width=140>" . $row['SEG_ORIGIN'] . "</td>";
echo "<td width=140>" . $row['SEG_DESTINATION'] . "</td>";
echo "<td width=140>" . $row['DISTANCE'] . "</td>";
echo "<td width=140>" . $row['FLIGHT_TYPE'] . "</td>";
echo "<td width=60><a href='edit13.php?id=".$row['SEGMENT_KEY']."'>Edit</a></td>";
echo "<td width=60><a href='delete13.php?id=".$row['SEGMENT_KEY']."'>Delete</a></td>";
echo "</tr>";
$i13++;

}
echo "</table>";

sqlsrv_free_stmt($query);

?>
</form>

<p id=count13 style="text-align: right;"></p>
<script type="text/javascript">
  var x13= document.getElementById("mytable13").rows.length;
  var rows = x13-1;
  document.getElementById("count13").innerHTML=rows + " Entries";
</script>
</div>






<div class="file content13a">
   <form action="search13.php" method="post">
<div id="srch">
  <input type="text" id="usrn" name="usrn11" required="required" style="background-color:#f2f2f2">
  <input type="submit" name="submit" value="Search" style="width:85">
</div>
<br>
<br>
<div class="nr">
<a href='add13.php'>
<input type='button' name='add' value='Add Record'>
</a>
</div>
<br>

<br>
<div class="tab13aa">
<a href='javascript:void(0)'>
  <button>View Active</button>
</a>
</div>
<br>

<?php

if ($conn === false) 
  die("<pre>".print_r(sqlsrv_errors(), true));

$sql = "SELECT * FROM dbo.segment";
$query = sqlsrv_query($conn, $sql);
if ($query === false){
  exit("<pre>".print_r(sqlsrv_errors(), true));
}

echo "<h3>Segment</h3><table id='mytable13a' border=2 rules='cols'>
<tr>
<th>Origin Airport</th>
<th>Destination Airport</th>
<th>Sector</th>
<th>Seg Origin</th>
<th>Seg Destination</th>
<th>Distance</th>
<th>Flight Type</th>
<th>Status</th>
<th></th>
<th></th>
</tr>";

$fc  = "#f2f2f2";
$sc = "#b7c9ef";
$i13a=0;

while($row = sqlsrv_fetch_array($query))
{
  $color = ($i13a%2==0) ? $sc : $fc;
echo "<tr bgcolor='$color'>";
echo "<td width=140>" . $row['ORIGIN_AIRPORT'] . "</td>";
echo "<td width=140>" . $row['DESTINATION_AIRPORT'] . "</td>";
echo "<td width=140>" . $row['SECTOR'] . "</td>";
echo "<td width=140>" . $row['SEG_ORIGIN'] . "</td>";
echo "<td width=140>" . $row['SEG_DESTINATION'] . "</td>";
echo "<td width=140>" . $row['DISTANCE'] . "</td>";
echo "<td width=140>" . $row['FLIGHT_TYPE'] . "</td>";
echo "<td width=80>" . $row['IS_ACTIVE'] . "</td>";
echo "<td width=60><a href='edit13.php?id=".$row['SEGMENT_KEY']."'>Edit</a></td>";
echo "<td width=60><a href='delete13.php?id=".$row['SEGMENT_KEY']."'>Delete</a></td>";
echo "</tr>";
$i13a++;

}
echo "</table>";

sqlsrv_free_stmt($query);

sqlsrv_close($conn);
?>
</form>

<p id=count13a style="text-align: right;"></p>
<script type="text/javascript">
  var x13a= document.getElementById("mytable13a").rows.length;
  var rows = x13a-1;
  document.getElementById("count13a").innerHTML=rows + " Entries";
</script>
</div>


</div>
</body>
</html9