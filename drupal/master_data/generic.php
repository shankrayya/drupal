<!DOCTYPE html>
<html>
<head>
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="script1.js"></script>
  <link rel="stylesheet" type="text/css" href="styleside.css">
  <link rel="stylesheet" type="text/css" href="animate.min.css">

  <style type="text/css">
    .docs li {
      float: left;
      padding: 8px 5px 8px 15px;
    }

    .sidebar{
      width: 148px;
    }
    
  </style>

</head>
<body>


<div class="menu">
<ul class="docs1 clearfix">
      <li class="tab00"><a href="../all"><p style="font-family: sans-serif;">Travel Pie Portal</p></a></li>
	  <li class="tab00"><a href="../master_data.php"><p style="font-family: sans-serif;">Master Data</p></a></li>
       </ul>
   </div>


   <div class="sidebar">
  <ul class="docs clearfix" style="color: white;">Masters
  <br><br><br><br>
    <ul class="tab1"><a href="air.php"><p1 style="color: black;margin-right: 30px;">Air</p1></a></ul>
    <ul class="tab2"><a href="hotel.php"><p2 style="color: black;margin-right: 30px;"">Hotel</p2></a></ul>
    <ul class="tab3"><a href="car.php"><p2 style="color: black;">Car</p2></a></ul>
    <ul class="tab4"><a href="generic.php"><p2 style="color: white;">Generic</p2></a></ul>
    <li class="tab3"><a href="country.php">- Country</a></li>
    <li class="tab4"><a href="city.php">- City</a></li>
    <li class="tab5"><a href="currency.php">- Currency</a></li>
  </ul>
</div> 


<div class="box">
<div class="content">

<div class="img1">
  <img src="airline.jpg" alt="airline table" style="width:152px;height:152px;border:0;">
</div>

<div class="img2">
  <img src="airport.jpg" alt="airport table" style="width:152px;height:152px;border:0;">
</div>

<div class="img3">
  <img src="city.jpg" alt="city table" style="width:152px;height:152px;border:0;">
</div>

<div class="img4">
 <img src="country.jpg" alt="country table" style="width:152px;height:152px;border:0;">
</div>

<div class="img5">
 <img src="currency.jpg" alt="currency table" style="width:152px;height:152px;border:0;">
</div>

</div>
</div>


</body>
</html>