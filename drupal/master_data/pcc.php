<!DOCTYPE html>
<html>
<head>
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="script11.js"></script>
  <link rel="stylesheet" type="text/css" href="styleside.css">
  <link rel="stylesheet" type="text/css" href="animate.min.css">
  
  <style type="text/css">
    
    .box .content9a{
      display: none;
    }
    
    .docs li {
      float: left;
      padding: 8px 5px 8px 15px;
    }

    .sidebar{
      width: 148px;
    }
    
    button{
       position: fixed;
       left: 183px;
    }

  </style>

  <script>
      $(document).ready(function(){
  $('.content9 .tab9a a').click(function(e) {
  $(this).addClass('active');
  $('.box .content9').hide();
  $('.box .content9a').show();  
});
});


   $(document).ready(function(){
  $('.content9a .tab9aa a').click(function(e) {
  $(this).addClass('active');
  $('.box .content9').show();
  $('.box .content9a').hide();  
});
});
  </script>

</head>
<body>


<div class="menu">
<ul class="docs1 clearfix">
      <li class="tab00"><a href="../all"><p style="font-family: sans-serif;">Travel Pie Portal</p></a></li>
	  <li class="tab00"><a href="../master_data.php"><p style="font-family: sans-serif;">Master Data</p></a></li>
    </ul>
   </div>


   <div class="sidebar">
  <ul class="docs clearfix" style="color: white;">Masters
  <br><br><br><br>
    <ul style="list-style-type: square;" class="tab1"><a href="air1.php"><p1 style="color: white;margin-right: 30px;">Air</p1></a></ul>
    <li class="tab1"><a href="airline.php">- Airline</a></li>
    <li class="tab2"><a href="airport.php">- Airport</a></li>
    <li class="tab6"><a href="equipment.php">- Equipment</a></li>
    <li class="tab7"><a href="status.php">- Status</a></li>
    <li class="tab8"><a href="ssr.php" style="margin-right: 20px;">- SSR</a></li>
    <li class="tab9"><a href="pcc.php" style="color: white">- PCC</a></li>
    <li class="tab10"><a href="custdk.php">- Cust DK</a></li>
    <li class="tab13"><a href="segment.php">- Segment</a></li>
    <ul class="tab2"><a href="hotel.php"><p2 style="color: black;margin-right: 30px;"">Hotel</p2></a></ul>
    <ul class="tab3"><a href="car.php"><p2 style="color: black;">Car</p2></a></ul>
    <ul class="tab4"><a href="generic.php"><p2 style="color: black;">Generic</p2></a></ul>
  </ul>
</div>


<div class="box">

<div class="file content9">
   <form action="search9.php" method="post">
<div id="srch">
  <input type="text" id="usrn" name="usrn11" required="required" style="background-color:#f2f2f2">
  <input type="submit" name="submit" value="Search" style="width:85">
</div>
<br>
<br>
<div class="nr">
<a href='add9.php'>
<input type='button' name='add' value='Add Record'>
</a>
</div>
<br>

<br>
<div class="tab9a">
<a href='javascript:void(0)'>
  <button>View All</button>
</a>
</div>
<br>

<?php

include("conn.php");

if ($conn === false) 
  die("<pre>".print_r(sqlsrv_errors(), true));

$sql = "SELECT * FROM dbo.PCC WHERE IS_ACTIVE = 1";
$query = sqlsrv_query($conn, $sql);
if ($query === false){
  exit("<pre>".print_r(sqlsrv_errors(), true));
}

echo "<h3>Active PCC</h3><table id='mytable9' border=2 rules='cols'>
<tr>
<th>Code</th>
<th>IATA No.</th>
<th>Description</th>
<th>Address</th>
<th>Phone1</th>
<th>Phone2</th>
<th>FAX</th>
<th>City Name</th>
<th>Coompany Title</th>
<th>Country</th>
<th>Region</th>
<th></th>
<th></th>
</tr>";

$fc  = "#f2f2f2";
$sc = "#b7c9ef";
$i9=0;

while($row = sqlsrv_fetch_array($query))
{
  $color = ($i9%2==0) ? $sc : $fc;
echo "<tr bgcolor='$color'>";
echo "<td width=100>" . $row['PCC_CODE'] . "</td>";
echo "<td width=100>" . $row['IATA_NO'] . "</td>";
echo "<td width=100>" . $row['DESCRIPTION'] . "</td>";
echo "<td width=100>" . $row['ADDRESS'] . "</td>";
echo "<td width=100>" . $row['PHONE1'] . "</td>";
echo "<td width=100>" . $row['PHONE2'] . "</td>";
echo "<td width=100>" . $row['FAX'] . "</td>";
echo "<td width=100>" . $row['CITY_NAME'] . "</td>";
echo "<td width=100>" . $row['COMPANY_TITLE'] . "</td>";
echo "<td width=100>" . $row['COUNTRY'] . "</td>";
echo "<td width=100>" . $row['REGION'] . "</td>";
echo "<td width=60><a href='edit9.php?id=".$row['PCC_KEY']."'>Edit</a></td>";
echo "<td width=60><a href='delete9.php?id=".$row['PCC_KEY']."'>Delete</a></td>";

echo "</tr>";
$i9++;

}
echo "</table>";

sqlsrv_free_stmt($query);

?>
</form>

<p id=count9 style="text-align: right;"></p>
<script type="text/javascript">
  var x9= document.getElementById("mytable9").rows.length;
  var rows = x9-1;
  document.getElementById("count9").innerHTML=rows + " Entries";
</script> 
</div>



<div class="file content9a">
   <form action="search9.php" method="post">
<div id="srch">
  <input type="text" id="usrn" name="usrn11" required="required" style="background-color:#f2f2f2">
  <input type="submit" name="submit" value="Search" style="width:85">
</div>
<br>
<br>
<div class="nr">
<a href='add9.php'>
<input type='button' name='add' value='Add Record'>
</a>
</div>
<br>

<br>
<div class="tab9aa">
<a href='javascript:void(0)'>
  <button>View Active</button>
</a>
</div>
<br>

<?php

if ($conn === false) 
  die("<pre>".print_r(sqlsrv_errors(), true));

$sql = "SELECT * FROM dbo.PCC";
$query = sqlsrv_query($conn, $sql);
if ($query === false){
  exit("<pre>".print_r(sqlsrv_errors(), true));
}

echo "<h3>PCC</h3><table id='mytable9a' border=2 rules='cols'>
<tr>
<th>Code</th>
<th>IATA No.</th>
<th>Description</th>
<th>Address</th>
<th>Phone1</th>
<th>Phone2</th>
<th>FAX</th>
<th>City Name</th>
<th>Coompany Title</th>
<th>Country</th>
<th>Region</th>
<th>Status</th>
<th></th>
<th></th>
</tr>";

$fc  = "#f2f2f2";
$sc = "#b7c9ef";
$i9a=0;

while($row = sqlsrv_fetch_array($query))
{
  $color = ($i9a%2==0) ? $sc : $fc;
echo "<tr bgcolor='$color'>";
echo "<td width=100>" . $row['PCC_CODE'] . "</td>";
echo "<td width=100>" . $row['IATA_NO'] . "</td>";
echo "<td width=100>" . $row['DESCRIPTION'] . "</td>";
echo "<td width=100>" . $row['ADDRESS'] . "</td>";
echo "<td width=100>" . $row['PHONE1'] . "</td>";
echo "<td width=100>" . $row['PHONE2'] . "</td>";
echo "<td width=100>" . $row['FAX'] . "</td>";
echo "<td width=100>" . $row['CITY_NAME'] . "</td>";
echo "<td width=100>" . $row['COMPANY_TITLE'] . "</td>";
echo "<td width=100>" . $row['COUNTRY'] . "</td>";
echo "<td width=100>" . $row['REGION'] . "</td>";
echo "<td width=60>" . $row['IS_ACTIVE'] . "</td>";
echo "<td width=60><a href='edit9.php?id=".$row['PCC_KEY']."'>Edit</a></td>";
echo "<td width=60><a href='delete9.php?id=".$row['PCC_KEY']."'>Delete</a></td>";

echo "</tr>";
$i9a++;

}
echo "</table>";

sqlsrv_free_stmt($query);

sqlsrv_close($conn);
?>
</form>

<p id=count9a style="text-align: right;"></p>
<script type="text/javascript">
  var x9a= document.getElementById("mytable9a").rows.length;
  var rows = x9a-1;
  document.getElementById("count9a").innerHTML=rows + " Entries";
</script> 
</div>


</div>
</body>
</html9