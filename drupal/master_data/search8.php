<html>
<head>
	<style type="text/css">
		
	body{
		background-color: #ab9191;
	}

		form{
			margin-top: 150px;
		}

		table{
            margin:10px auto;
            border-collapse: collapse;
            width: 70%;
        }

		#count{
			text-align: right;
			font-size: 18px;
		}

		td{
            text-align: left;
            padding: 3px;
            font-size: 17px;
        }

        th {
            text-align: left;
            padding: 5px;
            background-color: #3f3342;
            color: #f2f2f2;
            font-size: 19px;

        }

		input{
			width: 85px;
		}

	</style>
</head>
<body>

<form>
<?php

include("conn.php");

if ($conn === false) 
  die("<pre>".print_r(sqlsrv_errors(), true));

//Create Variables
$usrn11 = $_POST['usrn11'];

if (is_numeric($usrn11)) {
	$sql = "SELECT * FROM dbo.ssr where SSR_CODE='{$usrn11}' OR SSR_DESCRIPTION='{$usrn11}'";
}

elseif (!is_numeric($usrn11)) {

	$sql = "SELECT * FROM dbo.ssr where SSR_CODE='{$usrn11}' OR SSR_DESCRIPTION='{$usrn11}'";
}

$query = sqlsrv_query($conn, $sql);
if ($query === false){
  exit("<pre>".print_r(sqlsrv_errors(), true));
}

echo "<table id='mytable' border=2 rules='cols'>
<tr>
<th>Code</th>
<th>Description</th>
<th>Status</th>
<th></th>
<th></th>

$fc  = "#f2f2f2";
$sc = "#b7c9ef";
$i=0;

while($row = sqlsrv_fetch_array($query))
{
	$color = ($i%2==0) ? $sc : $fc;
echo "<tr bgcolor='$color'>";
echo "<td width=100>" . $row['SSR_CODE'] . "</td>";
echo "<td width=100>" . $row['SSR_DESCRIPTION'] . "</td>";
echo "<td width=80>" . $row['IS_ACTIVE'] . "</td>";
echo "<td width=60><a href='edit8.php?id=".$row['SSR_KEY']."'>Edit</a></td>";
echo "<td width=60><a href='delete8.php?id=".$row['SSR_KEY']."'>Delete</a></td>";
echo "</tr>";
$i++;
}
echo "</table>";

echo "<a href='ssr.php'>
		<input type='button' value='Back' style='margin-left:650px'>
		</a>";


sqlsrv_free_stmt($query);

sqlsrv_close($conn);
?>
</form>

<p id=count></p>
<script type="text/javascript">
	var x = document.getElementById("mytable").rows.length;
	var rows = x-1;
	document.getElementById("count").innerHTML=rows + " Entries";
</script>
</body>
</html>