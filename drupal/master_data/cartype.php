<!DOCTYPE html>
<html>
<head>
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="script11.js"></script>
  <link rel="stylesheet" type="text/css" href="style1.css">
  <link rel="stylesheet" type="text/css" href="animate.min.css">
  
  <style type="text/css">
    
    .box .content12a{
      display: none;
    }

    .docs li {
      float: left;
      padding: 8px 5px 8px 15px;
    }

    .sidebar{
      width: 148px;
    }

    button{
       position: fixed;
       left: 183px;
    }

  </style>

  <script>
      $(document).ready(function(){
  $('.content12 .tab12a a').click(function(e) {
  $(this).addClass('active');
  $('.box .content12').hide();
  $('.box .content12a').show();  
});
});


   $(document).ready(function(){
  $('.content12a .tab12aa a').click(function(e) {
  $(this).addClass('active');
  $('.box .content12').show();
  $('.box .content12a').hide();  
});
});
  </script>

</head>
<body>


<div class="menu">
<ul class="docs1 clearfix">
      <li class="tab00"><a href="newindex.php"><p style="font-family: sans-serif;">Travel Pie</p></a></li>
    </ul>
   </div>


   <div class="sidebar">
  <ul class="docs clearfix" style="color: white;">Masters
  <br><br><br><br>
    <ul style="list-style-type: square;" class="tab1"><a href="air.php"><p1 style="color: black;margin-right: 30px;">Air</p1></a></ul>
    <ul class="tab2"><a href="hotel.php"><p2 style="color: black;margin-right: 30px;"">Hotel</p2></a></ul>
    <ul class="tab3"><a href="car.php"><p2 style="color: white;">Car</p2></a></ul>
    <li class="tab12"><a href="cartype.php" style="color: white;">- Car Type</a></li>
    <ul class="tab4"><a href="generic.php"><p2 style="color: black;">Generic</p2></a></ul>
  </ul>
</div>


<div class="box">

<div class="file content12">
   <form action="search12.php" method="post">
<div id="srch">
  <input type="text" id="usrn" name="usrn11" required="required" style="background-color:#f2f2f2">
  <input type="submit" name="submit" value="Search" style="width:85">
</div>
<br>
<br>
<div class="nr">
<a href='add12.php'>
<input type='button' name='add' value='Add Record'>
</a>
</div>
<br>

<br>
<div class="tab12a">
<a href='javascript:void(0)'>
  <button>View All</button>
</a>
</div>
<br>

<?php

include("conn.php");

if ($conn === false) 
  die("<pre>".print_r(sqlsrv_errors(), true));

$sql = "SELECT * FROM dbo.CAR_TYPE WHERE IS_ACTIVE = 1";
$query = sqlsrv_query($conn, $sql);
if ($query === false){
  exit("<pre>".print_r(sqlsrv_errors(), true));
}

echo "<h3>Active Car Type</h3><table id='mytable12' border=2 rules='cols'>
<tr>
<th>Car Type Name</th>
<th></th>
<th></th>
</tr>";

$fc  = "#f2f2f2";
$sc = "#b7c9ef";
$i12=0;

while($row = sqlsrv_fetch_array($query))
{
  $color = ($i12%2==0) ? $sc : $fc;
echo "<tr bgcolor='$color'>";
echo "<td width=100>" . $row['CAR_TYPE_NAME'] . "</td>";
echo "<td width=60><a href='edit12.php?id=".$row['CAR_TYPE_KEY']."'>Edit</a></td>";
echo "<td width=60><a href='delete12.php?id=".$row['CAR_TYPE_KEY']."'>Delete</a></td>";
echo "</tr>";
$i12++;

}
echo "</table>";

sqlsrv_free_stmt($query);

?>
</form>

<p id=count12 style="text-align: right;"></p>
<script type="text/javascript">
  var x12= document.getElementById("mytable12").rows.length;
  var rows = x12-1;
  document.getElementById("count12").innerHTML=rows + " Entries";
</script> 
</div>



<div class="file content12a">
   <form action="search12.php" method="post">
<div id="srch">
  <input type="text" id="usrn" name="usrn11" required="required" style="background-color:#f2f2f2">
  <input type="submit" name="submit" value="Search" style="width:85">
</div>
<br>
<br>
<div class="nr">
<a href='add12.php'>
<input type='button' name='add' value='Add Record'>
</a>
</div>
<br>

<br>
<div class="tab12aa">
<a href='javascript:void(0)'>
  <button>View Active</button>
</a>
</div>
<br>

<?php

if ($conn === false) 
  die("<pre>".print_r(sqlsrv_errors(), true));

$sql = "SELECT * FROM dbo.CAR_TYPE";
$query = sqlsrv_query($conn, $sql);
if ($query === false){
  exit("<pre>".print_r(sqlsrv_errors(), true));
}

echo "<h3>Car Type</h3><table id='mytable12a' border=2 rules='cols'>
<tr>
<th>Car Type Name</th>
<th>Status</th>
<th></th>
<th></th>
</tr>";

$fc  = "#f2f2f2";
$sc = "#b7c9ef";
$i12a=0;

while($row = sqlsrv_fetch_array($query))
{
  $color = ($i12a%2==0) ? $sc : $fc;
echo "<tr bgcolor='$color'>";
echo "<td width=100>" . $row['CAR_TYPE_NAME'] . "</td>";
echo "<td width=80>" . $row['IS_ACTIVE'] . "</td>";
echo "<td width=60><a href='edit12.php?id=".$row['CAR_TYPE_KEY']."'>Edit</a></td>";
echo "<td width=60><a href='delete12.php?id=".$row['CAR_TYPE_KEY']."'>Delete</a></td>";
echo "</tr>";
$i12a++;

}
echo "</table>";

sqlsrv_free_stmt($query);

sqlsrv_close($conn);
?>
</form>

<p id=count12a style="text-align: right;"></p>
<script type="text/javascript">
  var x12a= document.getElementById("mytable12a").rows.length;
  var rows = x12a-1;
  document.getElementById("count12a").innerHTML=rows + " Entries";
</script> 
</div>


</div>
</body>
</html>