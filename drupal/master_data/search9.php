<html>
<head>
	<style type="text/css">
		
	body{
		background-color: #ab9191;
	}

		form{
			margin-top: 150px;
		}

		table{
            margin:10px auto;
            border-collapse: collapse;
            width: 70%;
        }

		#count{
			text-align: right;
			font-size: 18px;
		}

		td{
            text-align: left;
            padding: 3px;
            font-size: 17px;
        }

        th {
            text-align: left;
            padding: 5px;
            background-color: #3f3342;
            color: #f2f2f2;
            font-size: 19px;

        }

		input{
			width: 85px;
		}

	</style>
</head>
<body>

<form>
<?php

include("conn.php");

if ($conn === false) 
  die("<pre>".print_r(sqlsrv_errors(), true));

//Create Variables
$usrn11 = $_POST['usrn11'];

if (is_numeric($usrn11)) {
	$sql = "SELECT * FROM dbo.pcc where PCC_CODE='{$usrn11}' OR IATA_NO='{$usrn11}' OR DESCRIPTION='{$usrn11}' OR ADDRESS='{$usrn11}' OR PHONE1='{$usrn11}' OR PHONE2='{$usrn11}' OR FAX='{$usrn11}' OR CITY_NAME='{$usrn11}' OR COMPANY_TITLE='{$usrn11}' OR COUNTRY='{$usrn11}' OR REGION='{$usrn11}'";
}

elseif (!is_numeric($usrn11)) {

	$sql = "SELECT * FROM dbo.pcc where PCC_CODE='{$usrn11}' OR IATA_NO='{$usrn11}' OR DESCRIPTION='{$usrn11}' OR ADDRESS='{$usrn11}' OR PHONE1='{$usrn11}' OR PHONE2='{$usrn11}' OR FAX='{$usrn11}' OR CITY_NAME='{$usrn11}' OR COMPANY_TITLE='{$usrn11}' OR COUNTRY='{$usrn11}' OR REGION='{$usrn11}'";
}

$query = sqlsrv_query($conn, $sql);
if ($query === false){
  exit("<pre>".print_r(sqlsrv_errors(), true));
}

echo "<table id='mytable' border=2 rules='cols'>
<tr>
<th>Code</th>
<th>IATA No.</th>
<th>Description</th>
<th>Address</th>
<th>Phone1</th>
<th>Phone2</th>
<th>FAX</th>
<th>City Name</th>
<th>Coompany Title</th>
<th>Country</th>
<th>Region</th>
<th>Status</th>
<th></th>
<th></th>
</tr>";

$fc  = "#f2f2f2";
$sc = "#b7c9ef";
$i=0;

while($row = sqlsrv_fetch_array($query))
{
	$color = ($i%2==0) ? $sc : $fc;
echo "<tr bgcolor='$color'>";
echo "<td width=100>" . $row['PCC_CODE'] . "</td>";
echo "<td width=100>" . $row['IATA_NO'] . "</td>";
echo "<td width=100>" . $row['DESCRIPTION'] . "</td>";
echo "<td width=100>" . $row['ADDRESS'] . "</td>";
echo "<td width=100>" . $row['PHONE1'] . "</td>";
echo "<td width=100>" . $row['PHONE2'] . "</td>";
echo "<td width=100>" . $row['FAX'] . "</td>";
echo "<td width=100>" . $row['CITY_NAME'] . "</td>";
echo "<td width=100>" . $row['COMPANY_TITLE'] . "</td>";
echo "<td width=100>" . $row['COUNTRY'] . "</td>";
echo "<td width=100>" . $row['REGION'] . "</td>";
echo "<td width=60>" . $row['IS_ACTIVE'] . "</td>";
echo "<td width=60><a href='edit9.php?id=".$row['PCC_KEY']."'>Edit</a></td>";
echo "<td width=60><a href='delete9.php?id=".$row['PCC_KEY']."'>Delete</a></td>";

echo "</tr>";
$i++;
}
echo "</table>";

echo "<a href='pcc.php'>
		<input type='button' value='Back' style='margin-left:650px'>
		</a>";


sqlsrv_free_stmt($query);

sqlsrv_close($conn);
?>
</form>

<p id=count></p>
<script type="text/javascript">
	var x = document.getElementById("mytable").rows.length;
	var rows = x-1;
	document.getElementById("count").innerHTML=rows + " Entries";
</script>
</body>
</html>