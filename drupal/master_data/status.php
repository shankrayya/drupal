<!DOCTYPE html>
<html>
<head>
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="script11.js"></script>
  <link rel="stylesheet" type="text/css" href="styleside.css">
  <link rel="stylesheet" type="text/css" href="animate.min.css">
  
  <style type="text/css">
    
    .box .content7a{
      display: none;
    }

    .docs li {
      float: left;
      padding: 8px 5px 8px 15px;
    }

    .sidebar{
      width: 148px;
    }

    button{
       position: fixed;
       left: 183px;
    }

  </style>

  <script>
      $(document).ready(function(){
  $('.content7 .tab7a a').click(function(e) {
  $(this).addClass('active');
  $('.box .content7').hide(); 
  $('.box .content7a').show();  
});
});


   $(document).ready(function(){
  $('.content7a .tab7aa a').click(function(e) {
  $(this).addClass('active');
  $('.box .content7').show();
  $('.box .content7a').hide();  
});
});
  </script>

</head>
<body>


<div class="menu">
<ul class="docs1 clearfix">
      <li class="tab00"><a href="newindex.php"><p style="font-family: sans-serif;">Travel Pie</p></a></li>
    </ul>
   </div>


   <div class="sidebar">
  <ul class="docs clearfix" style="color: white;">Masters
  <br><br><br><br>
    <ul style="list-style-type: square;" class="tab1"><a href="air1.php"><p1 style="color: white;margin-right: 30px;">Air</p1></a></ul>
    <li class="tab1"><a href="airline.php">- Airline</a></li>
    <li class="tab2"><a href="airport.php">- Airport</a></li>
    <li class="tab6"><a href="equipment.php">- Equipment</a></li>
    <li class="tab7"><a href="status.php" style="color: white">- Status</a></li>
    <li class="tab8"><a href="ssr.php" style="margin-right: 20px;">- SSR</a></li>
    <li class="tab9"><a href="pcc.php">- PCC</a></li>
    <li class="tab10"><a href="custdk.php">- Cust DK</a></li>
    <li class="tab13"><a href="segment.php">- Segment</a></li>
    <ul class="tab2"><a href="hotel.php"><p2 style="color: black;margin-right: 30px;"">Hotel</p2></a></ul>
    <ul class="tab3"><a href="car.php"><p2 style="color: black;">Car</p2></a></ul>
    <ul class="tab4"><a href="generic.php"><p2 style="color: black;">Generic</p2></a></ul>
  </ul>
</div>


<div class="box">

<div class="file content7">
   <form action="search7.php" method="post">
<div id="srch">
  <input type="text" id="usrn" name="usrn11" required="required" style="background-color:#f2f2f2">
  <input type="submit" name="submit" value="Search" style="width:85">
</div>
<br>
<br>
<div class="nr">
<a href='add7.php'>
<input type='button' name='add' value='Add Record'>
</a>
</div>
<br>

<br>
<div class="tab7a">
<a href='javascript:void(0)'>
  <button>View All</button>
</a>
</div>
<br>

<?php

include("conn.php");

if ($conn === false) 
  die("<pre>".print_r(sqlsrv_errors(), true));

$sql = "SELECT * FROM dbo.status WHERE IS_ACTIVE = 1";
$query = sqlsrv_query($conn, $sql);
if ($query === false){
  exit("<pre>".print_r(sqlsrv_errors(), true));
}

echo "<h3>Active Status</h3><table id='mytable7' border=2 rules='cols'>
<tr>
<th>Code</th>
<th>Type</th>
<th>Description</th>
<th></th>
<th></th>
</tr>";

$fc  = "#f2f2f2";
$sc = "#b7c9ef";
$i7=0;

while($row = sqlsrv_fetch_array($query))
{
  $color = ($i7%2==0) ? $sc : $fc;
echo "<tr bgcolor='$color'>";
echo "<td width=100>" . $row['STATUS_CODE'] . "</td>";
echo "<td width=100>" . $row['STATUS_TYPE'] . "</td>";
echo "<td width=100>" . $row['DESCRIPTION'] . "</td>";
echo "<td width=60><a href='edit7.php?id=".$row['STATUS_CODE']."'>Edit</a></td>";
echo "<td width=60><a href='delete7.php?id=".$row['STATUS_CODE']."'>Delete</a></td>";

echo "</tr>";
$i7++;

}
echo "</table>";

sqlsrv_free_stmt($query);

?>
</form>

<p id=count7 style="text-align: right;"></p>
<script type="text/javascript">
  var x7= document.getElementById("mytable7").rows.length;
  var rows = x7-1;
  document.getElementById("count7").innerHTML=rows + " Entries";
</script> 
</div>




<div class="file content7a">
   <form action="search7.php" method="post">
<div id="srch">
  <input type="text" id="usrn" name="usrn11" required="required" style="background-color:#f2f2f2">
  <input type="submit" name="submit" value="Search" style="width:85">
</div>
<br>
<br>
<div class="nr">
<a href='add7.php'>
<input type='button' name='add' value='Add Record'>
</a>
</div>
<br>

<br>
<div class="tab7aa">
<a href='javascript:void(0)'>
  <button>View Active</button>
</a>
</div>
<br>

<?php

if ($conn === false) 
  die("<pre>".print_r(sqlsrv_errors(), true));

$sql = "SELECT * FROM dbo.status";
$query = sqlsrv_query($conn, $sql);
if ($query === false){
  exit("<pre>".print_r(sqlsrv_errors(), true));
}

echo "<h3>Active Status</h3><table id='mytable7a' border=2 rules='cols'>
<tr>
<th>Code</th>
<th>Type</th>
<th>Description</th>
<th>Status</th>
<th></th>
<th></th>
</tr>";

$fc  = "#f2f2f2";
$sc = "#b7c9ef";
$i7a=0;

while($row = sqlsrv_fetch_array($query))
{
  $color = ($i7a%2==0) ? $sc : $fc;
echo "<tr bgcolor='$color'>";
echo "<td width=100>" . $row['STATUS_CODE'] . "</td>";
echo "<td width=100>" . $row['STATUS_TYPE'] . "</td>";
echo "<td width=100>" . $row['DESCRIPTION'] . "</td>";
echo "<td width=80>" . $row['IS_ACTIVE'] . "</td>";
echo "<td width=60><a href='edit7.php?id=".$row['STATUS_CODE']."'>Edit</a></td>";
echo "<td width=60><a href='delete7.php?id=".$row['STATUS_CODE']."'>Delete</a></td>";

echo "</tr>";
$i7a++;

}
echo "</table>";

sqlsrv_free_stmt($query);

sqlsrv_close($conn);
?>
</form>

<p id=count7a style="text-align: right;"></p>
<script type="text/javascript">
  var x7a= document.getElementById("mytable7a").rows.length;
  var rows = x7a-1;
  document.getElementById("count7a").innerHTML=rows + " Entries";
</script> 
</div>

</div>


</body>
</html>